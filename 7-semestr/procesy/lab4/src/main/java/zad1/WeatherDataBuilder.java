package zad1;

import zad1.auxillary.PressureData;
import zad1.auxillary.WeatherData;
import zad1.auxillary.WindData;

public class WeatherDataBuilder {
    private WindData windData = null;
    private PressureData pressureData = null;
    private TemperatureLoader temperatureLoader = null;

    public WeatherDataBuilder setWindData(WindData windData) {
        this.windData = windData;
        return this;
    }

    public WeatherDataBuilder setPressureData(PressureData pressureData) {
        this.pressureData = pressureData;
        return this;
    }

    public WeatherDataBuilder setTemperatureLoader(TemperatureLoader temperatureLoader) {
        this.temperatureLoader = temperatureLoader;
        return this;
    }

    public WeatherData build() {
        return new WeatherData(windData, pressureData, temperatureLoader);
    }
}
