package zad1.auxillary;

import lombok.AllArgsConstructor;
import zad1.TemperatureLoader;

@AllArgsConstructor
public class WeatherData {
    private WindData windData;
    private PressureData pressureData;
    private TemperatureLoader temperatureLoader;
}
