package zad1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import zad1.auxillary.CrudRepository;
import zad1.auxillary.Facade;
import zad1.auxillary.RestController;
import zad1.auxillary.Service;

import java.net.URL;
import java.util.*;

public class Main {
    // 1
    public Date userRegistrationDate;

    // 2
    public Payment[] payments;
    public static final double PI = 3.14;
    public ArrayList<Member> members;

    // 3
    public URL url;
    public List<User> users;
    public double[] numbers;

    public enum AlarmType {
        LOW, MEDIUM, HIGH, CRITICAL
    }

    public enum UserPermission {
        USER, ADMIN, SUPER_ADMIN, SUPPORT, ANONYMOUS, GUEST
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Alarm {
        private int hours;
        private int minutes;
        private int seconds;
        private String message;
    }

    public static void main(String[] args) {
        // 9
        var rng = new Random();

        double i = rng.nextDouble(2, 4);
        int radius = rng.nextInt(-10, 10);

        if (i == PI && radius > 0) {
            double area = 2 * i * radius;
        }

        final int SECONDS_IN_HOUR = 3600;
        var alarmType = AlarmType.CRITICAL;
        var userPermissions = EnumSet.of(UserPermission.ADMIN, UserPermission.SUPER_ADMIN);

        int hours = rng.nextInt(24);
        int minutes = rng.nextInt(60);
        int seconds = rng.nextInt(60);
        String message = "Hello World!";

        if (userPermissions.containsAll(EnumSet.of(UserPermission.ADMIN, UserPermission.SUPER_ADMIN))) {
            System.out.println("User has appropriate permissions");
            AlarmType type = alarm(hours, minutes, seconds, message);
            switch (type) {
                case CRITICAL:
                    System.out.println("Critical alarm");
                    break;
                case HIGH:
                    System.out.println("High alarm");
                    break;
                case MEDIUM:
                    System.out.println("Medium alarm");
                    break;
                case LOW:
                    System.out.println("Low alarm");
                    break;
            }
        }
    }

    public static AlarmType alarm(int hours, int minutes, int seconds, String message) {
        var alarm = new Alarm(hours, minutes, seconds, message);
        var rng = new Random();

        return AlarmType.values()[rng.nextInt(AlarmType.values().length)];
    }

    // 8
    public static class Mammal {
    }

    public static class SecretPersistence {
    }

    public String getNewLoggedInUserName(Date date) {
        return "";
    }

    // 6
    public static class ShopController extends RestController {
    }

    public interface DriverRepository extends CrudRepository {
    }

    public static class FoodService extends Service {
    }

    public static class PaymentFacade extends Facade {
    }

    // ---
    public static class User {
    }

    public static class Member {
    }

    public static class Item {
    }

    public static class Payment {
    }

    // 1
    public List<Item> getUserBoughtItems() {
        return List.of();
    }
}
