package zad2.auxillary;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Player {
    private String country;
    private int winCount;
}
