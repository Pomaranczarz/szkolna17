package zad2;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import zad2.auxillary.*;

import java.util.Date;
import java.util.List;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SummerTripPlan {
    TripPlan tripPlan;
    List<Member> members;
    Accomodation[] sleepPlaces;
    List<Ticket> tickets;
    Date startTripDate;
    Date[] tripMiddleDates;
    Date endTripDate;
    List<Item> itemsToTake;
    Contact[] contacts;
    Weather weatherInfo;
    List<MedicalInfo> medicalInfos;
}
