package zad2;

import zad2.auxillary.Document;
import zad2.auxillary.DocumentRepository;
import zad2.auxillary.User;

import java.util.Date;

public class DocumentService {
    private final DocumentRepository documentRepository = new DocumentRepository();

    public User saveDocument(Document document) {
        document.setCreationDate(new Date());
        document.setCreator(sessionGetUserId());

        return documentRepository.save(document);
    }

    private String sessionGetUserId() {
        return "";
    }
}
