package zad2;

import zad2.auxillary.*;

@RestController
public class DocumentController {
    private final DocumentService documentService = new DocumentService();

    @PostMapping("/api/document")
    public User saveDocument(@RequestBody Document doc) {
        return documentService.saveDocument(doc);
    }
}
