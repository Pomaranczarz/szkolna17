package zad2;

import zad2.auxillary.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Main {
    private final ProductRepository productRepository = new ProductRepository();
    private final PriceRepository priceRepository = new PriceRepository();
    private final ShopRepository shopRepository = new ShopRepository();
    private final UserRepository userRepository = new UserRepository();
    private final GameRepository gameRepository = new GameRepository();

    public static void main(String[] args) {
        int idCountry = 0, idCompany = 0, idDepartment = 0, idEmployee = 0;
        var country = getCountry(idCountry).orElseThrow();
        var company = country.getCompany(idCompany).orElseThrow();
        var department = company.getDepartment(idDepartment).orElseThrow();
        var employee = department.getEmployee(idEmployee).orElseThrow();
        var privileges = employee.getPermissions();
    }

    private static Optional<Country> getCountry(int idCountry) {
        return Optional.empty();
    }

    public void saveProduct(String productName, double price) {
        productRepository.save(productName);
        priceRepository.save(price);
    }

    public String getCurrentUser() {
        return "";
    }

    public void savePurchase(String name, double price) {
        shopRepository.newPurchase(name, price);
        userRepository.updateUserPurchase(getCurrentUser(), name, price);
    }

    public List<Product> updateProduct(long productId, String productName) {
        Product product = productRepository.find(productId);
        product.setName(productName);
        productRepository.save(product);

        var products = productRepository.findAll();
        products.forEach(System.out::println);

        return products;
    }

    public void modifySummerTripPlan(SummerTripPlan summerTripPlan) {
    }

    public int saveGameResult(Player player, Result result) {
        if (Objects.equals(player.getCountry(), "Poland") && result.getScore() > 1000)
            player.setWinCount(player.getWinCount() + 1);

        return gameRepository.save(player, result);
    }

    public boolean checkUser(long userId, Token token) {
        User user = userRepository.findById(userId);

        return userRepository.verifyUser(user, token);
    }

    public User updateUser(long userId, Token token, String userName, String userTelNumber) {
        if (checkUser(userId, token)) {
            User user = userRepository.findById(userId);
            user.setPhoneNumber(userTelNumber);
            return userRepository.save(user);
        }

        return null;
    }
}
