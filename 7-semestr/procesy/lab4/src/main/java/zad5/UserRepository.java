package zad5;

import java.util.List;
import java.util.Optional;

/**
 * A repository interface for managing instances of the {@link User} entity.
 */
public interface UserRepository {
    /**
     * Finds a user by its name.
     *
     * @param name the name of the user
     * @return an optional containing the user, or an empty optional if no user is found
     */
    Optional<User> findByName(String name);

    /**
     * Finds all users in the database.
     *
     * @return a list of all users
     */
    List<User> findAll();

    /**
     * Finds a user by its id.
     *
     * @param id the id of the user
     * @return an optional containing the user, or an empty optional if no user is found
     */
    Optional<User> findById(long id);

    /**
     * Finds a list of users by their ids.
     *
     * @param ids the ids of the users
     * @return a list of users
     */
    List<User> findAllById(Iterable<Long> ids);

    /**
     * Saves a user to the database.
     *
     * @param user the user to save
     * @return the saved user
     */
    User save(User user);

    /**
     * Deletes a user from the database by its id.
     *
     * @param id the id of the user to delete
     */
    void deleteById(long id);

    /**
     * Deletes a list of users from the database by their ids.
     *
     * @param ids the ids of the users to delete
     */
    void deleteAllByIdInBatch(Iterable<Long> ids);

    /**
     * Deletes a list of users from the database.
     *
     * @param users the users to delete
     */
    void deleteAll(Iterable<? extends User> users);

    /**
     * Deletes a user from the database.
     *
     * @param user the user to delete
     */
    void delete(User user);
}