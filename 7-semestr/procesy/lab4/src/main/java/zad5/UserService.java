package zad5;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service class for managing instances of the {@link User} entity.
 */
@AllArgsConstructor
public class UserService implements CrudService<User> {
    private final UserRepository userRepository;

    /**
     * Finds all users in the database.
     *
     * @return a list of all users
     */
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Finds a user by its id.
     *
     * @param id the id of the user
     * @return an optional containing the user, or an empty optional if no user is found
     */
    @Override
    public Optional<User> findById(long id) {
        return userRepository.findById(id);
    }

    /**
     * Finds a list of users by their ids.
     *
     * @param ids the ids of the users
     * @return a list of users
     */
    @Override
    public List<User> findAllById(Iterable<Long> ids) {
        return userRepository.findAllById(ids);
    }

    /**
     * Finds a user by its name.
     *
     * @param name the name of the user
     * @return an optional containing the user, or an empty optional if no user is found
     */
    public Optional<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    /**
     * Saves a user to the database.
     *
     * @param user the user to save
     * @return an optional containing the saved user, or an empty optional if the user could not be saved
     */
    @Override
    public Optional<User> save(User user) {
        try {
            return Optional.of(userRepository.save(user));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    /**
     * Deletes a user from the database.
     *
     * @param user the user to delete
     */
    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    /**
     * Saves a list of users to the database.
     *
     * @param users the users to save
     * @return a list of saved users
     */
    @Override
    public List<User> saveAll(Iterable<User> users) {
        var savedUsers = new ArrayList<User>();
        users.forEach(user -> savedUsers.add(userRepository.save(user)));

        return savedUsers;
    }

    /**
     * Deletes a user from the database by its id.
     *
     * @param id the id of the user to delete
     */
    @Override
    public void deleteById(long id) {
        userRepository.deleteById(id);
    }

    /**
     * Deletes a list of users from the database by their ids.
     *
     * @param ids the ids of the users to delete
     */
    @Override
    public void deleteAllById(Iterable<Long> ids) {
        userRepository.deleteAllByIdInBatch(ids);
    }

    /**
     * Deletes a list of users from the database.
     *
     * @param users the users to delete
     */
    @Override
    public void deleteAll(Iterable<? extends User> users) {
        userRepository.deleteAll(users);
    }

    /**
     * Updates a user in the database, or creates a new one if the user does not exist.
     *
     * @param id   the id of the user to update
     * @param user the user with the updated values
     * @return an optional containing the updated user, or an empty optional if the user could not be updated
     */
    @Override
    public Optional<User> upsert(long id, User user) {
        var userToUpdateOpt = userRepository.findById(id);
        if (userToUpdateOpt.isEmpty())
            return Optional.of(userRepository.save(user));

        var userToUpdate = userToUpdateOpt.get();
        userToUpdate.setName(user.getName());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setRole(user.getRole());
        userToUpdate.setPassword(user.getPassword());

        return Optional.of(userRepository.save(userToUpdate));
    }

    /**
     * Updates a user in the database, without changing its id.
     *
     * @param id   the id of the user to update
     * @param user the user with the updated values
     * @return an optional containing the updated user, or an empty optional if the user could not be updated
     */
    @Override
    public Optional<User> patch(long id, User user) {
        var userToPatchOpt = userRepository.findById(id);
        if (userToPatchOpt.isEmpty())
            return Optional.empty();

        var userToPatch = userToPatchOpt.get();
        if (user.getRole() != null) userToPatch.setRole(user.getRole());
        if (user.getEmail() != null) userToPatch.setEmail(user.getEmail());
        if (user.getPassword() != null) userToPatch.setPassword(user.getPassword());
        if (user.getName() != null) userToPatch.setName(user.getName());

        return Optional.of(userRepository.save(userToPatch));
    }
}