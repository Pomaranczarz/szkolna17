/**
 * Maps {@link User} to {@link UserResponse}.
 */
package zad5;

import lombok.AllArgsConstructor;

/**
 * Maps {@link User} to {@link UserResponse}.
 */
@AllArgsConstructor
public class UserResponseMapper implements ResponseMapper<User, UserResponse> {
    /**
     * Maps a single {@link User} to a single {@link UserResponse}.
     *
     * @param user the user to map
     * @return the mapped user response
     */
    @Override
    public UserResponse mapFromSingleObject(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .role(user.getRole())
                .build();
    }
}