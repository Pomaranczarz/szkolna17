package zad5;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class User {
    public enum Role {
        ADMIN,
        USER
    }

    private long id;

    private String name;

    private String email;

    private String password;

    private Role role;
}