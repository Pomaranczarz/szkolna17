package zad5;

import java.util.List;

/**
 * Interface for mapping objects of type T to objects of type U.
 *
 * @param <T> the source object type
 * @param <U> the target object type
 */
public interface ResponseMapper<T, U> {
    /**
     * Maps a single object of type T to an object of type U.
     *
     * @param t the object to map
     * @return the mapped object
     */
    U mapFromSingleObject(T t);

    /**
     * Maps a list of objects of type T to a list of objects of type U.
     *
     * @param ts the list of objects to map
     * @return the list of mapped objects
     */
    default List<U> mapFromList(List<T> ts) {
        return ts.stream()
                .map(this::mapFromSingleObject)
                .toList();
    }
}