package zad5;

/**
 * A service interface for converting DTOs to entities.
 *
 * @param <T> the type of the entity
 */
public interface DtoService<T> {

    /**
     * Converts a DTO to an entity.
     *
     * @param <U> the type of the DTO
     * @param dto the DTO to convert
     * @return the converted entity
     */
    <U> T dtoToEntity(U dto);
}