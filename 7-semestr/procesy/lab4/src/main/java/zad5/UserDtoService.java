package zad5;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserDtoService implements DtoService<User> {
    public static class UserPatchRequest {
    }

    public static class UserRegisterRequest {
    }

    public static class LoginRequest {
    }

    public static class UserPostRequest {
    }

    /**
     * Creates a new User entity from a DTO object.
     *
     * @param dto the DTO object
     * @return the created User entity
     */
    @Override
    public <U> User dtoToEntity(U dto) {
        return null;
    }

    /**
     * Maps a UserPatchRequest DTO to a User entity.
     *
     * @param userDto the DTO object
     * @return the mapped User entity
     */
    private User patchUserDtoToUser(UserPatchRequest userDto) {
        return null;
    }

    /**
     * Maps a UserPostRequest DTO to a User entity.
     *
     * @param userDto the DTO object
     * @return the mapped User entity
     */
    private User userDetailsResponseToUser(UserPostRequest userDto) {
        return null;
    }

    /**
     * Maps a UserRegisterRequest DTO to a User entity.
     *
     * @param userDto the DTO object
     * @return the mapped User entity
     */
    private User registerUserDtoToUser(UserRegisterRequest userDto) {
        return null;
    }

    /**
     * Maps a LoginRequest DTO to a User entity.
     *
     * @param userDto the DTO object
     * @return the mapped User entity
     */
    private User loginUserDtoToUser(LoginRequest userDto) {
        return null;
    }
}
