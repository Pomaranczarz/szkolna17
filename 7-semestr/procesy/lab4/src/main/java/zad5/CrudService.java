package zad5;

import java.util.List;
import java.util.Optional;

/**
 * Interface for CRUD operations on a generic type.
 *
 * @param <T> the type of the entity
 */
public interface CrudService<T> {

    /**
     * Finds all entities.
     *
     * @return a list of all entities
     */
    List<T> findAll();

    /**
     * Finds an entity by its id.
     *
     * @param id the id of the entity
     * @return an optional containing the entity, or an empty optional if no entity is found
     */
    Optional<T> findById(long id);

    /**
     * Finds entities by their ids.
     *
     * @param ids the ids of the entities
     * @return a list of entities
     */
    List<T> findAllById(Iterable<Long> ids);

    /**
     * Saves an entity.
     *
     * @param t the entity to save
     * @return an optional containing the saved entity, or an empty optional if the entity could not be saved
     */
    Optional<T> save(T t);

    /**
     * Saves a list of entities.
     *
     * @param t the entities to save
     * @return a list of saved entities
     */
    List<T> saveAll(Iterable<T> t);

    /**
     * Deletes an entity.
     *
     * @param t the entity to delete
     */
    void delete(T t);

    /**
     * Deletes an entity by its id.
     *
     * @param id the id of the entity to delete
     */
    void deleteById(long id);

    /**
     * Deletes a list of entities.
     *
     * @param t the entities to delete
     */
    void deleteAll(Iterable<? extends T> t);

    /**
     * Deletes entities by their ids.
     *
     * @param ids the ids of the entities to delete
     */
    void deleteAllById(Iterable<Long> ids);

    /**
     * Updates an entity, or creates a new one if it does not exist.
     *
     * @param id the id of the entity to update
     * @param t  the entity with the updated values
     * @return an optional containing the updated entity, or an empty optional if the entity could not be updated
     */
    Optional<T> upsert(long id, T t);

    /**
     * Updates an entity, without changing its id.
     *
     * @param id the id of the entity to update
     * @param t  the entity with the updated values
     * @return an optional containing the updated entity, or an empty optional if the entity could not be updated
     */
    Optional<T> patch(long id, T t);
}