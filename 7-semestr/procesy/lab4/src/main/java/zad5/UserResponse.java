package zad5;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class UserResponse {
    Long id;

    String name;

    String email;

    User.Role role;
}