package zad4.auxillary;

public class ValidationException extends RuntimeException {
    public ValidationException(String paymentCardExpired) {
        super(paymentCardExpired);
    }
}
