package zad4;

import zad2.auxillary.Ticket;
import zad4.auxillary.*;

import java.util.Optional;

public class Main {
    Invoice invoice;

    public Object createTicket(Ticket ticket) {
        if (loadTicket(ticket.getId()) instanceof Ticket) {
            throw new TicketAlreadyExistsException();
        } else {
            ticket = saveTicket(ticket);
            if (ticket == null)
                throw new TicketPersistanceException();
        }

        return ticket;
    }

    public void processMovie(Movie movie) {
        MovieProcessor movieProcessor = new MovieProcessor();

        try {
            movieProcessor.process(movie);
        } catch (ProcessException e) {
            System.out.println(e.getMessage());
        } finally {
            movieProcessor.close();
        }
    }

    public void validatePaymentCard(String cardNumber) {
        CardRepository cardRepository = new CardRepository();
        if (!cardRepository.verifyExpirationDate(cardNumber)) {
            throw new ValidationException("Payment card [" + cardNumber + "] expired");
        }
    }

    public Optional<Invoice> createInvoice(Invoice invoice) {
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        if (invoice == null || invoiceRepository.findById(invoice.getId()).isEmpty())
            return Optional.empty();

        return invoiceRepository.save(invoice);
    }

    public Optional<Invoice> editInvoice(int invoiceId, Invoice invoice) {
        if (invoice == null)
            return Optional.empty();

        InvoiceRepository invoiceRepository = new InvoiceRepository();

        invoiceRepository.findById(invoiceId).ifPresentOrElse(
                inv -> {
                    this.invoice = inv;
                    invoiceRepository.save(inv);
                },
                () -> {
                    invoiceRepository.register(invoiceId, null);
                }
        );

        return Optional.of(invoice);
    }

    private Ticket saveTicket(Ticket ticket) {
        return null;
    }

    private Object loadTicket(Object id) {
        return null;
    }
}
