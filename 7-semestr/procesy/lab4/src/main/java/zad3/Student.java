package zad3;

import zad3.auxillary.Lecture;
import zad3.auxillary.Mark;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class Student {
    /*
    Copyright (C) 2021,2022
    by Lublin University of Technology.
    All rights reserved.
    Released under the terms
    of the GNU General Public License version 2 or later.
    */
    private String studentName;
    private int studentIndex;
    private Logger logger;

    Student(String studentName, int studentIndex) {
        this.studentName = studentName;
        this.studentIndex = studentIndex;
        logger.info("Student: {} created.");
    }

    /**
     * <p> this method publishes results based on date
     * </p>
     *
     * @param date date from when results should be published
     * @return List of Result entities
     * @since 16.0
     */
    public List<Mark> publishStudentMarks(Date date, int studentIndex) {
        return List.of();
    }

    public Student setStudentPromotion(Student student, Lecture lecture) {
        return null;
    }

    // This method will be used for User Story: see Jira-1254
    // ticket
    // public void checkPayments(){
    //}

    public void graduateStudent() {
    }

    // TODO: this method should be extended - Feature 10.1
    public void checkStudentAbsence() {
    }
}