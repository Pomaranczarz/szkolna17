import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import zad1.Thermometer;

import static org.junit.jupiter.api.Assertions.*;

public class ThermometerTest {
    Thermometer thermometer;

    @BeforeEach
    void init() {
        thermometer = Thermometer.createInstance(-10, 20);
    }

    @Test
    void exceptionTesting() {
        assertThrows(IllegalArgumentException.class, () -> thermometer.setCurrentTemp(30), "Exception should be thrown");
        thermometer.setCurrentTemp(10);
        assertThrows(IllegalArgumentException.class, () -> thermometer.addDegrees(20), "Exception should be thrown");
        assertThrows(IllegalArgumentException.class, () -> thermometer.subtractDegrees(21), "Exception should be thrown");
    }

    @Test
    void testSetCurrentTemp() {
        thermometer.setCurrentTemp(20);
        assertEquals(20, thermometer.getCurrentTemp());
    }

    @Test
    void setAddDegrees() {
        thermometer.setCurrentTemp(0);
        thermometer.addDegrees(5);

        assertEquals(5, thermometer.getCurrentTemp());
        assertNotEquals(0, thermometer.getCurrentTemp());
        assertFalse(thermometer.isBelowZero());
    }

    @Test
    void setSubtractDegrees() {
        thermometer.setCurrentTemp(0);
        thermometer.subtractDegrees(5);

        assertEquals(-5, thermometer.getCurrentTemp());
        assertTrue(thermometer.isBelowZero());
    }
}
