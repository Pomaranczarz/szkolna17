import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import zad1.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    @Test
    @DisplayName("Multiplication of calculator")
    void testMultiply() {
        assertEquals(10, calculator.multiply(2, 5), "Multiplication should work");
    }

    @RepeatedTest(10)
    @DisplayName("Ensure correct multiplication by zero")
    void testMultiplicationByZero() {
        assertEquals(0, calculator.multiply(0, 10), "Multiplication by zero should be zero");

        assertEquals(0, calculator.multiply(1, 0), "Multiplication by zero should be zero");
    }

    @RepeatedTest(10)
    @DisplayName("Ensure correct multiplication by one")
    void testMultiplicationByOne() {
        assertEquals(5, calculator.multiply(5, 1), "Multiplication five by one should be five");

        assertEquals(5, calculator.multiply(1, 5), "Multiplication one by five should be five");
    }

    @Test
    @DisplayName("Ensure correct subtraction")
    void testSubtraction() {
        assertEquals(5, calculator.subtract(10, 5), "Subtraction should work");
    }

    @Test
    @DisplayName("Ensure correct addition")
    void testAddition() {
        assertEquals(15, calculator.add(10, 5), "Addition should work");
    }

    @Test
    @DisplayName("Ensure correct division")
    void testDivision() {
        assertEquals(2, calculator.divide(10, 5), "Division should work");
    }

    @Test
    @DisplayName("Ensure correct division by zero")
    void testDivisionByZero() {
        assertThrows(ArithmeticException.class, () -> calculator.divide(10, 0), "Division by zero should throw exception");
    }
}
