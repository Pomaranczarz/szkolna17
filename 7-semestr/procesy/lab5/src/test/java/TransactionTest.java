import org.junit.jupiter.api.Test;
import zad1.Transaction;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionTest {
    @Test
    void groupedAssertions() {
        Transaction transaction = Transaction.builder()
                .paid(120)
                .earned(160)
                .name("Paweł")
                .lastname("Kowalski")
                .build();

        assertAll(
                () -> assertEquals(120, transaction.getPaid()),
                () -> assertEquals(160, transaction.getEarned()),
                () -> assertEquals("Paweł", transaction.getName()),
                () -> assertEquals("Kowalski", transaction.getLastname())
        );
    }

    @Test
    void isNameOrLastnamePawel() {
        Transaction transaction = Transaction.builder()
                .paid(120)
                .earned(160)
                .name("Paweł")
                .lastname("Kowalski")
                .build();

        assertAll(
                () -> {
                    transaction.setName("Paweł");
                    assertEquals("Paweł", transaction.getName());
                },
                () -> {
                    transaction.setLastname("Paweł");
                    assertEquals("Paweł", transaction.getLastname());
                }
        );
    }

    @Test
    void testTimeout() {
        Transaction transaction = new Transaction();

        int result = assertTimeout(Duration.ofSeconds(2), () -> transaction.makeRequest());

        assertEquals(404, result);
    }
}
