import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import zad1.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParametrizedCalculatorTest {
    private final Calculator calculator = new Calculator();

    @ParameterizedTest
    @CsvFileSource(resources = "/data.csv")
    void parametrizedTestAddition(int a, int b, int result) {
        assertEquals(result, calculator.add(a, b), "Addition should work");
    }
}
