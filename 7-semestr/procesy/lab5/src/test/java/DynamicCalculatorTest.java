import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import zad1.Calculator;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DynamicCalculatorTest {
    @AllArgsConstructor
    @Getter
    @Setter
    @Builder
    private static class CalculatorTestCase<T> {
        int a;
        int b;
        T expectedResult;
    }

    @TestFactory
    Stream<DynamicTest> testDifferentDivisions() {
        Calculator calculator = new Calculator();

        List<CalculatorTestCase<?>> cases = List.of(
                new CalculatorTestCase<>(10, 2, 5),
                new CalculatorTestCase<>(8, 2, 4),
                new CalculatorTestCase<>(0, 2137, 0),
                new CalculatorTestCase<>(512, 2, 256),
                new CalculatorTestCase<>(10, 0, new ArithmeticException())
        );

        return cases.stream().map(testCase -> {
            int a = testCase.getA();
            int b = testCase.getB();
            Object result = testCase.getExpectedResult();

            return DynamicTest.dynamicTest(a + " / " + b + " = " + result.toString(),
                    () -> {
                        if (result instanceof ArithmeticException)
                            assertThrows(ArithmeticException.class, () -> calculator.divide(a, b));
                        else
                            assertEquals((int) result, calculator.divide(a, b));
                    }
            );
        });
    }
}
