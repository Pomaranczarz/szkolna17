package zad1;

import lombok.*;

import java.time.Duration;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Transaction {
    private int paid;
    private int earned;
    private String name;
    private String lastname;

    public int makeRequest() {
        try {
            Thread.sleep(Duration.ofSeconds(2));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return 404;
    }
}
