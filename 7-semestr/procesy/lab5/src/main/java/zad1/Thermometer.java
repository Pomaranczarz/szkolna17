package zad1;

public class Thermometer {
    int minTemp;
    int maxTemp;
    int currentTemp;

    public Thermometer() {
        this.minTemp = -20;
        this.maxTemp = 50;
        this.currentTemp = 0;
    }

    Thermometer(int minTemp, int maxTemp) {
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
    }

    public static Thermometer createInstance(int minTemp, int maxTemp) {
        return new Thermometer(minTemp, maxTemp);
    }

    public int getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(int currentTemp) {
        if (currentTemp > maxTemp)
            throw new IllegalArgumentException(String.format("Cannot set current temp [%s] higher than max temp [%s]", currentTemp, maxTemp));
        if (currentTemp < minTemp)
            throw new IllegalArgumentException(String.format("Cannot set current temp [%s] lower than min temp [%s]", currentTemp, minTemp));
        this.currentTemp = currentTemp;
    }

    public void addDegrees(int degrees) {
        setCurrentTemp(currentTemp + degrees);
    }

    public void subtractDegrees(int degrees) {
        setCurrentTemp(currentTemp - degrees);
    }

    public boolean isBelowZero() {
        return currentTemp < 0;
    }
}
