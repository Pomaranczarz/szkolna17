package FunctionalTesting.repository;

import FunctionalTesting.domain.ClientDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientDetailsRepository extends JpaRepository<ClientDetailsEntity, Long> {
}
