package FunctionalTesting.domain;

import lombok.Value;

@Value
public class ClientContact {
    String email;
    String phone;
}
