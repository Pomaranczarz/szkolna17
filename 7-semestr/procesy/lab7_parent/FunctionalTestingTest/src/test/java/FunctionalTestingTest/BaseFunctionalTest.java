package FunctionalTestingTest;

import FunctionalTesting.FunctionalTestingApplication;
import FunctionalTestingTest.steps.ClientDetailsServiceSteps;
import FunctionalTestingTest.steps.ContactServiceSteps;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FunctionalTestingApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public abstract class BaseFunctionalTest {
    @Rule
    public WireMockRule contactsServiceMock = new WireMockRule(options().port(8777));

    @Autowired
    protected ClientDetailsServiceSteps clientDetailsServiceSteps;

    private static final Logger LOGGER = LoggerFactory.getLogger("Docker-Container");

    @Autowired
    protected ContactServiceSteps contactServiceSteps;

    @TestConfiguration
    @ComponentScan("FunctionalTestingTest.steps")
    public static class StepsConfiguration {

    }

    public static String readFile(String path) {
        try {
            ClassLoader classLoader = BaseFunctionalTest.class.getClassLoader();
            return FileUtils.readFileToString(
                    new File(classLoader.getResource(path).getFile()),
                    Charset.defaultCharset());
        } catch (IOException e) {
            throw new RuntimeException("File cannot be read [" + path + "]", e);
        }
    }
}
