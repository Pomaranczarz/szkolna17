package FunctionalTestingTest.steps;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ClientDetailsServiceSteps implements ApplicationListener<WebServerInitializedEvent> {
    private int servicePort;

    public String getClient(long clientId) {
        return RestAssured.given().port(servicePort)
                .when().get("client/" + clientId)
                .then().statusCode(200)
                .contentType(ContentType.JSON)
                .extract().asString();
    }

    public void saveClientDetails(long clientId, String body) {
        RestAssured.given().port(servicePort)
                .body(body).contentType(ContentType.JSON)
                .when().post("client/" + clientId + "/details")
                .then().statusCode(200);
    }

    public String getClientDetails(long clientId) {
        return RestAssured.given().port(servicePort)
                .when().get("client/" + clientId + "/details")
                .then().statusCode(200)
                .contentType(ContentType.JSON)
                .extract().asString();
    }

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        servicePort = event.getWebServer().getPort();
    }
}
