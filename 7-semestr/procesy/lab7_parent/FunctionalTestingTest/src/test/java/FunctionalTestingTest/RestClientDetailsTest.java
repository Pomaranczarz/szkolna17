package FunctionalTestingTest;

import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class RestClientDetailsTest extends BaseFunctionalTest {
    private static final long CLIENT_ID = 123456L;
    private final String clientContactsResponse = readFile("json/client_contacts.json");
    private final String clientDetails = readFile("json/client_details.json");
    private final String expectedClientResponse = readFile("json/client.json");

    @Test
    public void shouldSaveAndRetrieveClientDetails() throws Exception {
        clientDetailsServiceSteps.saveClientDetails(CLIENT_ID, clientDetails);

        String clientDetails = clientDetailsServiceSteps.getClientDetails(CLIENT_ID);

        JSONAssert.assertEquals(clientDetails, clientDetails, false);
    }

    @Test
    public void shouldSaveClientDetailsAndRetrieveUser() throws Exception {
        clientDetailsServiceSteps.saveClientDetails(CLIENT_ID, clientDetails);

        contactServiceSteps.expectGetClientContacts(CLIENT_ID, clientContactsResponse);

        String actualClientResponse = clientDetailsServiceSteps.getClient(CLIENT_ID);

        JSONAssert.assertEquals(expectedClientResponse, actualClientResponse, false);
    }
}
