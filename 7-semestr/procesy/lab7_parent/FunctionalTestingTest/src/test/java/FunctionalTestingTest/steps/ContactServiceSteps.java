package FunctionalTestingTest.steps;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.springframework.stereotype.Component;

@Component
public class ContactServiceSteps {
    public void expectGetClientContacts(long clientId, String body) {
        WireMock.stubFor(WireMock.get(WireMock.urlPathMatching("/contacts"))
                .withQueryParam("clientId", WireMock.equalTo(String.valueOf(clientId)))
                .willReturn(WireMock.okJson(body)));
    }
}
