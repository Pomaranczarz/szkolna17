package zad3;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserService {
    private final Hasher userHashAlgorithm;
    private final Hasher superUserHashAlgorithm;

    public void logIn(String username, String password) {

    }

    public void logOut(String username) {

    }

    public void logOutWIthPwd(String username, String password) {

    }

}
