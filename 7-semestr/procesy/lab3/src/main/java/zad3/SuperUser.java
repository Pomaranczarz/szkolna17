package zad3;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuperUser extends User {
    private double age;
    private double[] permissions;

    public SuperUser(String username, String login, String pwdHash, UserService userService, double age, double[] permissions) {
        super(username, login, pwdHash, userService);
        this.age = age;
        this.permissions = permissions;
    }

    @Override
    public void logIn(String pwd) {
        var hashedPwd = getUserService().getSuperUserHashAlgorithm().hash(pwd);

        if (getPwdHash().equals(hashedPwd))
            getUserService().logIn(getUsername(), getPwdHash().toUpperCase());
    }

    @Override
    public void logOut() {
        getUserService().logOutWIthPwd(getUsername(), getPwdHash().toUpperCase());
    }
}
