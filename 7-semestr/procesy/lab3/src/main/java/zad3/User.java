package zad3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class User {
    private String username;
    private String login;
    private String pwdHash;
    private UserService userService;

    public void logIn(String pwd) {
        var hashedPwd = userService.getUserHashAlgorithm().hash(pwd);

        if (pwdHash.equals(hashedPwd))
            userService.logIn(username, pwdHash);
    }

    public void logOut() {
        userService.logOut(username);
    }
}
