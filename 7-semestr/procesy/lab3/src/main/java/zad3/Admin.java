package zad3;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Admin extends User {
    private double age;
    private double[] permissions;
    private boolean loggedIn = false;

    public Admin(String username, String login, String pwdHash, UserService userService, double age, double[] permissions, boolean loggedIn) {
        super(username, login, pwdHash, userService);
        this.age = age;
        this.permissions = permissions;
        this.loggedIn = loggedIn;
    }

    @Override
    public void logIn(String pwd) {
        var hashedPwd = getUserService().getUserHashAlgorithm().hash(pwd);

        if (getPwdHash().equals(hashedPwd)) {
            getUserService().logIn(getUsername(), getPwdHash().toUpperCase());
            loggedIn = true;
        }
    }

    @Override
    public void logOut() {
        getUserService().logOutWIthPwd(getUsername(), getPwdHash().toUpperCase());
        loggedIn = false;
    }
}
