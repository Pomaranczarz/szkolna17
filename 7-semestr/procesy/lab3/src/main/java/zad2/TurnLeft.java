package zad2;

public class TurnLeft extends FlightOperations {
    @Override
    public void perform() {
        setDirection(getDirection() - getAngle());
    }
}
