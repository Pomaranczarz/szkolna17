package zad2;

public class TurnRight extends FlightOperations {
    @Override
    public void perform() {
        setDirection(getDirection() + getAngle());
    }
}
