package zad2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Teacher implements Human {
    private int employeeNumber;

    @Override
    public int getHumanId() {
        return getEmployeeNumber();
    }
}
