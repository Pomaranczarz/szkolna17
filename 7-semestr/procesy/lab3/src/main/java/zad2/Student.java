package zad2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Student implements Human {
    private int index;

    @Override
    public int getHumanId() {
        return getIndex();
    }
}
