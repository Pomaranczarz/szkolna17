package zad2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class FlightOperations {
    private double angle;
    private double direction;

    public abstract void perform();
}
