package zad4;

import java.io.File;

public interface VideoProcessor extends MediaProcessor {
    Mpeg loadVideoFile(File f);

    void saveVideoFile(File f);

    Avi convertToAvi(Mpeg mpeg);
}

