package zad4;

import java.io.File;

public interface MediaProcessor {
    void addAuthor(MediaFile mf);

    void playFile(File f);
}
