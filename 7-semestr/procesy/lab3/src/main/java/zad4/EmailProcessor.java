package zad4;

import java.io.File;

public interface EmailProcessor {
    void attachFileToEmail(File f);

    void sendEmail(Message m);

    Message[] readEmails(String server, String login, String pwd);
}
