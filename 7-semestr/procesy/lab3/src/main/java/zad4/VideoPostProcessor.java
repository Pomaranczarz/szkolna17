package zad4;

public interface VideoPostProcessor extends MediaPostProcessor {
    Mpeg filterVideo(Mpeg mpeg);
}
