package zad4;

import java.io.File;

public interface Mp3Processor extends MediaProcessor {
    Mp3 loadMusicFile(File f);

    Mp3 convertToMp3(Wave wave);

    Mp3 extractFromAvi(Avi avi);

    Mp3 extractFromMpeg(Mpeg mpeg);
}
