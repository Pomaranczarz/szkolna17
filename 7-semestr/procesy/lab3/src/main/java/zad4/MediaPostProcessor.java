package zad4;

import java.io.File;

public interface MediaPostProcessor extends MediaProcessor {
    void cutFile(File f, double start, double end);
}

