package zad4;

import java.io.File;

public interface AudioProcessor extends MediaProcessor {
    void saveMusicFile(File f);
}
