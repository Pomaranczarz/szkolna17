package zad4;

import java.io.File;

public interface ImageProcessor {
    Jpeg loadPicture(File f);

    void savePicture(File f);

    Jpeg addAuthorToPicture(Jpeg jpeg);
}
