package zad4;

public interface Mp3PostProcessor extends MediaPostProcessor {
    Mp3 filterMusic(Mp3 mp3);
}

