package zad1;

public class Item {
    private Status status;
    private String name;
    private int price;

    public enum Status {
        NEW,
        SOLD
    }

    protected Item(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
