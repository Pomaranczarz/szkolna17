package zad1;

import java.util.ArrayList;
import java.util.List;

public class ItemStorage {
    private final List<Item> items = new ArrayList<>();

    public void addItem(Item item) {
        items.add(item);
    }

    public void sellItem(int storageId) {
        Item item = items.get(storageId);
        item.setStatus(Item.Status.SOLD);
    }
}
