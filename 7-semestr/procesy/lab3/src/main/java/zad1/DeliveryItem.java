package zad1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class DeliveryItem {
    private String itemName;
    private int quantity;
}
