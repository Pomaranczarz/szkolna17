package zad1;

public class ItemFactory {
    public static Item produce(String name, int price) {
        return new Item(name, price);
    }
}
