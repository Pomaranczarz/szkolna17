package zad1;

import java.util.Random;

public class BillCalculationService {
    public static int calculateBill(int quantity) {
        var rand = new Random();
        return rand.nextInt(100) * quantity;
    }
}
