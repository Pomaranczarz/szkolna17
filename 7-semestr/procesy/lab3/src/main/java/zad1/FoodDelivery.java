package zad1;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FoodDelivery {
    private String orderId;
    private Customer customer;
    private List<DeliveryItem> deliveryItems;
    private int totalBill;

    public void prepareOrder() {
    }

    public void delivery() {
    }

    public static void main(String[] args) {
        Customer customer = new Customer("Ann Smiths", "New York, 1st Avenue 1234");
        DeliveryItem deliveryItem = new DeliveryItem("Ragu", 1);

        var foodDelivery = OrderService.makeOrder(customer, List.of(deliveryItem));

        foodDelivery.prepareOrder();
        foodDelivery.delivery();
    }
}
