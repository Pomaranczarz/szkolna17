package zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientManager {
    private final List<Client> clients = new ArrayList<>();

    public static class ClientData {
        public String name;
        public String phone;
    }

    public Optional<Client> changeClientData(int clientId, ClientData clientData) {
        try {
            Client client = clients.get(clientId);
            client.setName(clientData.name);
            client.setPhone(clientData.phone);

            return Optional.of(client);
        } catch (IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }
}
