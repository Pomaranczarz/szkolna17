package zad1;

public class InternetStore {
    private final ItemStorage itemStorage = new ItemStorage();
    private final ClientManager clientManager = new ClientManager();

    public void addItemToStorage(Item item) {
        itemStorage.addItem(item);
    }

    public void sellItemFromStorage(int storageIndex) {
        itemStorage.sellItem(storageIndex);
    }

    public Item produceNewItem(String name, int price) {
        return ItemFactory.produce(name, price);
    }

    public void changeClientData(int clientId, ClientManager.ClientData clientData) {
        clientManager.changeClientData(clientId, clientData);
    }
}
