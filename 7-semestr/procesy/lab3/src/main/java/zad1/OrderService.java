package zad1;

import java.util.List;

public class OrderService {
    private static Integer ordersPlaced = 0;

    public static FoodDelivery makeOrder(Customer customer, List<DeliveryItem> deliveryItems) {
        var totalQuantity = deliveryItems.stream().map(DeliveryItem::getQuantity).reduce(0, Integer::sum);

        var fd = FoodDelivery.builder()
                .customer(customer)
                .deliveryItems(deliveryItems)
                .orderId(ordersPlaced.toString())
                .totalBill(
                        BillCalculationService.calculateBill(totalQuantity)
                )
                .build();

        ordersPlaced++;

        return fd;
    }
}
