package zad5;

public class BlackSoldier extends Player implements Opponent {
    public BlackSoldier(String name) {
        super(name);
    }

    @Override
    void up() {

    }

    @Override
    void down() {

    }

    @Override
    void forward() {

    }

    @Override
    void backward() {

    }

    @Override
    void stop() {

    }
}
