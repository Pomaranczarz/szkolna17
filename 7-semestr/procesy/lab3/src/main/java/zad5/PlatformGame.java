package zad5;

import java.util.ArrayList;
import java.util.Random;

public class PlatformGame {
    Level currentLevel;
    LevelManager levelManager = new LevelManager();
    GameMap gameMap;
    Player player;
    ArrayList<Platform> platforms = new ArrayList<>();
    ArrayList<Opponent> opponents = new ArrayList<>();
    ArrayList<Treasure> treasures = new ArrayList<>();
    double score;
    String playerName;
    Image avatar;

    public PlatformGame(String playerName, int platformCount, int opponentCount, int treasureCount) {
        currentLevel = new FirstLevel("Level for beginners");
        gameMap = new EuropeMap("Green background");
        player = new GreenSoldier("US Army");

        addPlatforms(platformCount);
        addOpponents(opponentCount);
        addTreasures(treasureCount);

        score = 0;
        this.playerName = playerName;
        avatar = new Avatar(this.playerName);
    }

    private void addPlatforms(int count) {
        for (int i = 0; i < count; i++)
            platforms.add(new GroundPlatform(Colour.BROWN));
    }

    private void addOpponents(int count) {
        for (int i = 0; i < count; i++)
            opponents.add(
                    i % 2 == 0 ?
                            new Alien("Alien " + i) :
                            new BlackSoldier("BSoldier " + i)
            );
    }

    private void addTreasures(int count) {
        var rng = new Random();
        for (int i = 0; i < count; i++)
            treasures.add(new Treasure(rng.nextInt(300)));
    }

    public void changeLevel() {
        currentLevel = levelManager.nextLevel();
    }

    public void movePlayer(int direction) {
        switch (direction) {
            case 1:
                player.up();
                break;
            case 2:
                player.down();
                break;
            case 3:
                player.forward();
                break;
            case 4:
                player.backward();
                break;
            default:
                player.stop();
        }
    }
}
