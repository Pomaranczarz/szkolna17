package zad5;

public class GreenSoldier extends Player implements Opponent {
    public GreenSoldier(String name) {
        super(name);
    }

    @Override
    void up() {

    }

    @Override
    void down() {

    }

    @Override
    void forward() {

    }

    @Override
    void backward() {

    }

    @Override
    void stop() {

    }
}
