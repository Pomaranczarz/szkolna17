package zad5;

public abstract class Player {
    public Player(String name) {
        
    }

    abstract void up();

    abstract void down();

    abstract void forward();

    abstract void backward();

    abstract void stop();
}
