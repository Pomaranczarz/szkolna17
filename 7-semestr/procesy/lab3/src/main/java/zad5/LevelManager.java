package zad5;

import java.util.ArrayList;
import java.util.List;

public class LevelManager {
    private final List<Level> levels = new ArrayList<>();
    private int levelIndex = 0;

    public void addLevel(Level level) {
        levels.add(level);
    }

    public Level nextLevel() {
        levelIndex = (levelIndex + 1) % levels.size();
        return levels.get(levelIndex);
    }
}
