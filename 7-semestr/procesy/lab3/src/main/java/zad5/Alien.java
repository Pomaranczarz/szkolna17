package zad5;

public class Alien extends Player implements Opponent {
    public Alien(String name) {
        super(name);
    }

    @Override
    void up() {

    }

    @Override
    void down() {

    }

    @Override
    void forward() {

    }

    @Override
    void backward() {

    }

    @Override
    void stop() {

    }
}
