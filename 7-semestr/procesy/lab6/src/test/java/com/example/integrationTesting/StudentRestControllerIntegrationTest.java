package com.example.integrationTesting;

import com.example.integrationTesting.domain.StudentEntity;
import com.example.integrationTesting.repository.StudentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = IntegrationTestingApplication.class
)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class StudentRestControllerIntegrationTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private StudentRepository repository;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    @BeforeEach
    public void clearDatabase() {
        repository.deleteAll();
    }

    @Test
    public void whenValidInput_thenCreateStudent() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        StudentEntity john = new StudentEntity(null, "John Smiths", formatter.parse("2001-05-05"));

        mvc.perform(post("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(john)));

        List<StudentEntity> found = repository.findAll();
        assertThat(found).extracting(StudentEntity::getName).containsOnly("John Smiths");
    }

    @Test
    public void givenStudents_whenGetStudents_thenStatus200() throws Exception {
        createTestStudent("Paul Simon", formatter.parse("2000-03-03"));
        createTestStudent("Axel Manhattan", formatter.parse("2002-02-10"));

        mvc.perform(get("/api/students")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(2))))
                .andExpect(jsonPath("$[0].name", is("Paul Simon")))
                .andExpect(jsonPath("$[1].name", is("Axel Manhattan")));
    }

    private void createTestStudent(String name, Date birthday) {
        StudentEntity student = new StudentEntity(null, name, birthday);
        repository.saveAndFlush(student);
    }

    @Test
    public void whenInvalidId_then404ShouldBeReturned() throws Exception {
        mvc.perform(get("/api/students/{id}", 5)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void whenValidName_thenStudentShouldBeFound() throws Exception {
        createTestStudent("John Lennon", formatter.parse("2000-03-03"));

        mvc.perform(get("/api/students/name/{name}", "John Lennon")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("John Lennon")));
    }

    @Test
    public void whenValidName_thenExistsShouldReturnTrue() throws Exception {
        createTestStudent("George Harrison", formatter.parse("2000-03-03"));

        mvc.perform(post("/api/exists")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("George Harrison"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().string("true"));
    }

    @Test
    public void whenInvalidName_thenExistsShouldReturnFalse() throws Exception {
        mvc.perform(post("/api/exists")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("Ringo Starr"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().string("false"));
    }
}
