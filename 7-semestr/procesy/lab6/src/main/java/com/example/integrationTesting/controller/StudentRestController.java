package com.example.integrationTesting.controller;

import com.example.integrationTesting.domain.StudentEntity;
import com.example.integrationTesting.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentRestController {
    @Autowired
    private StudentService studentService;

    @PostMapping("/students")
    public ResponseEntity<StudentEntity> createStudent(@RequestBody StudentEntity student) {
        var saved = studentService.save(student);

        return ResponseEntity.status(HttpStatus.CREATED).body(saved);
    }

    @GetMapping("/students")
    public List<StudentEntity> getAllStudents() {
        return studentService.getAllStudents();
    }

    @GetMapping("/students/{id}")
    public ResponseEntity<StudentEntity> getStudentById(@PathVariable Long id) {
        var student = studentService.getStudentById(id);

        if (student == null)
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok().body(student);
    }

    @GetMapping("/students/name/{name}")
    public ResponseEntity<StudentEntity> getStudentByName(@PathVariable String name) {
        var student = studentService.getStudentByName(name);

        return ResponseEntity.ok().body(student);
    }

    @PostMapping("/exists")
    public ResponseEntity<Boolean> exists(@RequestBody String name) {
        return ResponseEntity.ok().body(studentService.exists(name));
    }
}
