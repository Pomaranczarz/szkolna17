package com.example.integrationTesting.service;

import com.example.integrationTesting.domain.StudentEntity;

import java.util.List;

public interface StudentService {
    StudentEntity getStudentById(Long id);

    StudentEntity getStudentByName(String name);

    List<StudentEntity> getAllStudents();

    boolean exists(String name);

    StudentEntity save(StudentEntity student);
}
