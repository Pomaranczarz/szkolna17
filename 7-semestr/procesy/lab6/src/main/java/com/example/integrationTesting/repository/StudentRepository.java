package com.example.integrationTesting.repository;

import com.example.integrationTesting.domain.StudentEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface StudentRepository extends JpaRepository<StudentEntity, Long> {
    StudentEntity findByName(String name);

    List<StudentEntity> findAll();

    StudentEntity findByBirthdayAfter(Date date);

    StudentEntity findByBirthdayBeforeAndName(Date date, String name);
}
