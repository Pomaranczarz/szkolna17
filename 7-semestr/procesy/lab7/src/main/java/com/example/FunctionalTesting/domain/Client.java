package com.example.FunctionalTesting.domain;

import lombok.Value;

@Value(staticConstructor = "of")
public class Client {
    ClientDetails clientDetails;
    ClientContact clientContact;
}
