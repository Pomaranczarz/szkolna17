package com.example.FunctionalTesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FunctionalTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(FunctionalTestingApplication.class, args);
	}

}
