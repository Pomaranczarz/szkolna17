import org.hibernate.Session;

public class Main {
    public static void main(final String[] args) {
        var sessionFactory = new HibernateFactory().getSessionFactory();
        Session session = sessionFactory.openSession();

        final var students = session.createNamedQuery("Student.findAll", Student.class).list();
        students.forEach(System.out::println);

        session.close();
    }
}
