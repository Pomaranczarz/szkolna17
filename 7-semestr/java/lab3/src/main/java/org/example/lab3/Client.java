package org.example.lab3;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Supplier;

public class Client {
    JTextArea receivedMsg;
    JTextField msg;
    BufferedReader reader;
    PrintWriter writer;
    Socket socket;

    private static final String VIGENERE_KEYWORD = "KICIA";

    public static void main(String[] args) {
        Client client = new Client();
        client.showConnectionDialog();
    }

    private static String encryptVigenere(String msg) {
        return doVigenere(msg, true);
    }

    private static String decryptVigenere(String msg) {
        return doVigenere(msg, false);
    }

    private static String doVigenere(String msg, boolean encrypt) {
        StringBuilder sb = new StringBuilder();
        int keywordIndex = 0;

        for (int i = 0; i < msg.length(); ++i) {
            char c = msg.charAt(i);

            if (Character.isAlphabetic(c)) {
                char base = Character.isUpperCase(c) ? 'A' : 'a';
                int offset = VIGENERE_KEYWORD.charAt(keywordIndex % VIGENERE_KEYWORD.length()) - 'A';
                char encrypted = (char) (((c - base + (encrypt ? offset : (-offset + 26))) % 26) + base);
                sb.append(encrypted);

                keywordIndex++;
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    private void showConnectionDialog() {
        JFrame frame = new JFrame("Connection");
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        JLabel messageLabel = new JLabel("Do you want to connect to the chat server?");
        messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(messageLabel, BorderLayout.NORTH);

        JButton okButton = new JButton("OK");
        JButton cancelButton = new JButton("Cancel");

        okButton.addActionListener(e -> {
            frame.dispose();
            connect();
        });

        cancelButton.addActionListener(e -> {
            System.exit(0);
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        panel.add(buttonPanel, BorderLayout.SOUTH);

        frame.getContentPane().add(panel);
        frame.setSize(new Dimension(300, 75));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }


    private void connect() {
        configure();

        JFrame frame = new JFrame("Prosty klient czatu");
        JPanel panel = new JPanel();

        receivedMsg = new JTextArea(15, 50);
        receivedMsg.setLineWrap(true);
        receivedMsg.setWrapStyleWord(true);
        receivedMsg.setEditable(true);

        JScrollPane scroll = new JScrollPane(receivedMsg);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        msg = new JTextField(20);

        JButton sendButton = getSendButton();

        panel.add(scroll);
        panel.add(msg);
        panel.add(sendButton);

        Thread receiverThread = new Thread(() -> {
            String msg;
            try {
                while ((msg = reader.readLine()) != null) {
                    System.out.println("Received: " + msg);
                    receivedMsg.append(decryptVigenere(msg) + "\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        receiverThread.start();

        frame.getContentPane().add(BorderLayout.CENTER, panel);
        frame.setSize(new Dimension(600, 400));
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }

    private JButton getSendButton() {
        Supplier<String> getMessageString = () -> {
            String message = socket.getLocalPort() +
                    " at " +
                    LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) +
                    ": " + msg.getText();

            return encryptVigenere(message);
        };

        JButton sendButton = new JButton("Wyślij");
        sendButton.addActionListener(e -> {
            try {
                writer.println(getMessageString.get());
                writer.flush();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            msg.setText("");
            msg.requestFocus();
        });
        return sendButton;
    }

    private void configure() {
        try {
            socket = new Socket("127.0.0.1", 2020);
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream(), true);

            System.out.println("Created connection to server: " + socket.toString());
        } catch (Exception e) {
            System.out.println("Error configuring network");
            e.printStackTrace();
        }
    }
}
