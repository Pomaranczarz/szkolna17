package org.example.lab3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    ArrayList<PrintWriter> outStreams;

    public class ClientService implements Runnable {
        BufferedReader reader;
        Socket socket;

        public ClientService(Socket socket) {
            try {
                this.socket = socket;
                InputStreamReader reader = new InputStreamReader(socket.getInputStream());
                this.reader = new BufferedReader(reader);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            String msg;

            try {
                while ((msg = reader.readLine()) != null) {
                    System.out.println("Received: " + msg);
                    sendToAll(msg);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendToAll(String msg) {
        var it = outStreams.iterator();

        while (it.hasNext()) {
            try {
                PrintWriter writer = it.next();
                writer.println(msg);
                writer.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void launch() {
        outStreams = new ArrayList<>();
        try (ServerSocket serverSocket = new ServerSocket(2020)) {
            while (true) {
                Socket clientSocket = serverSocket.accept();

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
                outStreams.add(writer);

                Thread clientThread = new Thread(new ClientService(clientSocket));
                clientThread.start();

                System.out.println("Connected to client: " + clientSocket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Server().launch();
    }
}
