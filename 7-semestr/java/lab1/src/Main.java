import java.io.File;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    private static final List<String> subjectsNames = Arrays.asList(
            "Integracja Systemów",
            "Interakcja człowiek-komputer",
            "Programowanie aplikacji mobilnych na platformę Android",
            "Programowanie aplikacji mobilnych na platformę iOS",
            "Programowanie aplikacji w chmurze obliczeniowej",
            "Projekt zespołowy - projektowanie",
            "Szkielety programistyczne w aplikacjach internetowych",
            "Zarządzanie bazami SQL i NoSQL",
            "Projekt zespołowy - implementacja",
            "Seminarium dyplomowe",
            "Zaawansowane programowanie w Javie",
            "Architektura i programowanie w .NET",
            "Procesy wytwarzania oprogramowania",
            "Komponentowe podejście do wytwarzania aplikacji"
    );

    public static void main(String[] args) {
        // Usuwanie plików utworzonych przez poprzednie uruchomienie programu
        if (new File("subjects_by_mark.txt").delete() &&
                new File("subjects_by_mark_frequency.txt").delete()) {
            System.out.println("Usunięto pliki z poprzedniego uruchomienia programu.");
        } else {
            System.out.println("Wystąpił błąd podczas usuwania plików z poprzedniego uruchomienia programu.");
        }

        // Zadanie 1.
        System.out.println("Łancuchy zawierające 'zaaw' lub 'Zaaw':");
        subjectsNames.stream().filter(s -> s.matches(".*[zZ]aaw.*")).forEach(System.out::println);

        var marks = subjectsNames.stream().map(s -> getRandomInt(2, 5)).toArray(Integer[]::new);
        HashMap<Integer, Integer> marksHistogram = new HashMap<>();

        Arrays.stream(marks).forEach(m -> marksHistogram.merge(m, 1, Integer::sum));

        System.out.println("Histogram ocen: ");
        for (var mark : marksHistogram.keySet())
            System.out.println(mark + ": " + marksHistogram.get(mark));
        
        // Zadanie 2.
        var subjects = zipToSubject(subjectsNames.stream(), Arrays.stream(marks));

        Comparator<Subject> byMarkComparator = Comparator.comparingInt(lhs -> lhs.mark);
        Comparator<Subject> byMarkFrequencyComparator = Comparator.comparingInt(lhs -> marksHistogram.get(lhs.mark));

        subjects.stream().sorted(byMarkComparator).forEach(subject -> printSubjectToFile("subjects_by_mark.txt", subject));

        subjects.stream().sorted(byMarkFrequencyComparator).forEach(subject -> printSubjectToFile("subjects_by_mark_frequency.txt", subject));

        // Zadanie 3.
        var subjectsFromFile = readSubjectsFromFile("subjects_by_mark.txt");

        var avg = subjectsFromFile.stream().mapToInt(s -> s.mark).average().getAsDouble();
        System.out.println("Średnia ocen: " + avg);

        Subject lowestMark = subjectsFromFile.stream().min(byMarkComparator).get();
        Subject highestMark = subjectsFromFile.stream().max(byMarkComparator).get();
        System.out.println("Najniższa ocena: " + lowestMark);
        System.out.println("Najwyzsza ocena: " + highestMark);
    }

    private static ArrayList<Subject> readSubjectsFromFile(String filename) {
        var subjects = new ArrayList<Subject>();
        try (var reader = new java.io.BufferedReader(new java.io.FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                var lineSplit = line.split(": ");
                subjects.add(new Subject(lineSplit[0], Integer.parseInt(lineSplit[1])));
            }

            return subjects;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static void printSubjectToFile(String filename, Subject subject) {
        try (var writer = new java.io.FileWriter(filename, true)) {
            writer.write(subject.toString() + "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static ArrayList<Subject> zipToSubject(Stream<String> subjectNames, Stream<Integer> marks) {
        var namesIter = subjectNames.iterator();
        var marksIter = marks.iterator();

        ArrayList<Subject> subjects = new ArrayList<>();

        while (namesIter.hasNext() && marksIter.hasNext())
            subjects.add(new Subject(namesIter.next(), marksIter.next()));

        return subjects;
    }

    public static class Subject {
        public String subject;
        public int mark;

        public Subject(String subject, int mark) {
            this.subject = subject;
            this.mark = mark;
        }

        @Override
        public String toString() {
            return subject + ": " + mark;
        }
    }

    private static int getRandomInt(int min, int max) {
        return (int) ((Math.random() * (max - min + 1)) + min);
    }
}