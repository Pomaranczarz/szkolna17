import { useParams } from "react-router-dom";
import { Responses } from "../../common/Responses";
import { useEffect, useState } from "react";
import { GameController } from "../../common/GameController";
import UserGamesController from "../../common/UserGamesController";
import GameGenresController from "../../common/GameGenresController";
import GameGenres from "../partial/GameGenres";

export const Game = () => {
    const [game, setGame] = useState<Responses.Game | null>(null);
    const [genres, setGenres] = useState<Responses.Genre[]>([]);
    const [currentUserAddedGame, setCurrentUserAddedGame] = useState<boolean>(false);

    const params = useParams();
    const gameController = new GameController();
    const userGamesController = new UserGamesController();

    useEffect(() => {
        const fetchGame = async () => {
            setGame(await gameController.getById(parseInt(params.id as string)));
        };

        const fetchIfUserAddedGame = async () => {
            const userGames: Responses.Game[] = await userGamesController.getCurrentUserGames();
            setCurrentUserAddedGame(userGames.some((game) => game.id === parseInt(params.id as string)));
        };

        const fetchGenres = async () => {
            const gameGenresController = new GameGenresController();
            const genres = await gameGenresController.getOwnedEntities(parseInt(params.id as string));
            setGenres(genres);
        };

        fetchIfUserAddedGame();
        fetchGame();
        fetchGenres();
    }, [params.id]);

    const addGameToCurrentUser = async () => {
        await userGamesController.addGameToCurrentUser(parseInt(params.id as string));
        setCurrentUserAddedGame(true);
    };

    const removeGameFromCurrentUser = async () => {
        await userGamesController.removeGameFromCurrentUser(parseInt(params.id as string));
        setCurrentUserAddedGame(false);
    };

    return (
        <div className="body-container">
            {game ? (
                <div className="game-container">
                    <div>
                        <div>
                            <h1>{game.title}</h1>
                            {currentUserAddedGame ? (
                                <button className="btn btn-danger" onClick={removeGameFromCurrentUser}>Remove from collection</button>
                            ) : (
                                <button className="btn btn-primary" onClick={addGameToCurrentUser}>Add to collection</button>
                            )}
                        </div>
                        <GameGenres genres={genres} />
                        <h2>Description: </h2> <p>{game.description}</p>
                        <h2>Release year: </h2> <p>{game.releaseYear}</p>
                        <h2>Rating: </h2> <p>{game.rating}</p>
                    </div>
                </div>
            ) : (
                <div>
                    <h1>Loading...</h1>
                </div>
            )
            }
        </div>
    );
};
