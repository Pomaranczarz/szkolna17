import { useEffect, useMemo, useState } from "react";
import { Responses } from "../../common/Responses";
import GameRow from "../partial/GameRow";
import UserGamesController from "../../common/UserGamesController";
import GamesHeader from "../partial/GamesHeader";
import EntitiesTable from "../partial/EntitiesTable";

export const UserGames = () => {
    const [games, setGames] = useState<Responses.Game[]>([]);
    const userGamesController = useMemo(() => new UserGamesController(), []);

    useEffect(() => {
        const fetchUserGames = async () => {
            setGames(await userGamesController.getCurrentUserGames());
        };

        fetchUserGames();
    }, [userGamesController]);

    const handleAddGame = async (id: number) => {
        await userGamesController.addGameToCurrentUser(id);
    };

    const handleRemoveGame = async (id: number) => {
        await userGamesController.removeGameFromCurrentUser(id);
        setGames(games.filter(game => game.id !== id));
    };

    return (
        <>
            {games.length > 0 ? (
                <div className="user-games-container">
                    <EntitiesTable
                        HeaderComponent={GamesHeader}
                        BodyComponent={GameRow}
                        data={games}
                        onAdd={handleAddGame}
                        onDelete={handleRemoveGame}
                    />
                </div>
            ) : (
                <div>
                    <p>No games found</p>
                </div>
            )}
        </>
    );
};
