import { UserGames } from "./UserGames";
import { useUserContext } from "./UserContext";

export const Profile = () => {
    const { user } = useUserContext();

    return (
        <div className="body-container">
            {user ? (
                <div className="profile-container">
                    <h1>{user.name}</h1>
                    <p>{user.email}</p>
                    <p>Your games:</p>
                    <UserGames />
                </div>
            ) : (
                <p>Loading...</p>
            )}
        </div>
    );
};

export default Profile;
