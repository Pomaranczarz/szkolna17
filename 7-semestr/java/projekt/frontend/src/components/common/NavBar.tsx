import { useNavigate } from "react-router-dom";
import { Role } from "../../common/Globals";
import { useUserContext } from "./UserContext";

export const NavBar = () => {
    const { user, setUser } = useUserContext();
    const navigate = useNavigate();

    const handleHomeClick = () => {
        navigate("/home");
    };

    const handleBrowseGamesClick = () => {
        navigate("/games");
    };

    const handleBrowseGenresClick = () => {
        navigate("/genres");
    };

    const handleProfileClick = () => {
        navigate("/profile");
    };

    const handleLogoutClick = () => {
        sessionStorage.removeItem("token");
        setUser(null);

        navigate("/login");
    };

    const handleAdminClick = () => {
        navigate("/admin");
    };

    const handleLoginClick = () => {
        navigate("/login");
    };

    const handleRegisterClick = () => {
        navigate("/register");
    };

    return (
        <nav className="navbar">
            <div className="logo">
                <span>Games Browser</span>
            </div>
            {user ? (
                <div className="container">
                    <button className="btn btn-primary" onClick={handleBrowseGenresClick}>Browse genres</button>
                    <button className="btn btn-primary" onClick={handleHomeClick}>Browse games</button>
                    <button className="btn btn-primary" onClick={handleProfileClick}>Profile</button>
                    {user.role === Role.ADMIN && <button className="btn btn-primary" onClick={handleAdminClick}>Admin</button>}
                    <button className="btn btn-primary" onClick={handleLogoutClick}>Logout</button>
                </div>
            ) : (
                <div className="container">
                    <button className="btn btn-primary" onClick={handleLoginClick}>Login</button>
                    <button className="btn btn-primary" onClick={handleRegisterClick}>Register</button>
                </div>
            )}
        </nav>
    );
};
