import React, { useState, useEffect } from 'react';
import { Responses } from '../../common/Responses';
import { GameController } from '../../common/GameController';
import GameCard from '../partial/GameCard';

export const Home = () => {
    const [games, setGames] = useState<Responses.Game[]>([]);
    const [latestGames, setLatestGames] = useState<Responses.Game[]>([]);
    const [bestGames, setBestGames] = useState<Responses.Game[]>([]);

    useEffect(() => {
        const fetchGames = async () => {
            const gameController = new GameController();
            const allGames = await gameController.getAll();

            setGames(allGames);
            // latest games are games with 10 highest ids
            setLatestGames(allGames.sort((a, b) => b.id - a.id).slice(0, 10));
            // best games are games with 10 highest ratings
            setBestGames(allGames.sort((a, b) => b.rating - a.rating).slice(0, 10));
        };
        fetchGames();
    }, []);

    return (
        <div className="body-container">
            <div className="home-container">
                <div className="column column-left">
                    <h2>Latest Games</h2>
                    {latestGames.map((game) => (
                        <GameCard key={game.id} {...game} />
                    ))}
                </div>
                <div className="column column-middle">
                    <h2>All Games</h2>
                    {games.map((game) => (
                        <GameCard key={game.id} {...game} />
                    ))}
                </div>
                <div className="column column-right">
                    <h2>Best Games</h2>
                    {bestGames.map((game) => (
                        <GameCard key={game.id} {...game} />
                    ))}
                </div>
            </div>
        </div>
    );
};
