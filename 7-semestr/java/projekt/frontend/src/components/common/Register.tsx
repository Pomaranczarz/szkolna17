import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Requests } from "../../common/Requests";
import AuthController from "../../common/AuthController";
import { validateEmail, validatePassword, validateUsername } from "../../common/UserUtils";

export const Register = () => {
    const [username, setUsername] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [errors, setErrors] = useState<Map<string, string>>(new Map<string, string>());

    const navigate = useNavigate();

    const authController = new AuthController();

    const handleFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (!validateEmail(email)) setErrors(errors.set("email", "Invalid email"));
        else errors.delete("email");

        if (!validateUsername(username))
            setErrors(
                errors.set(
                    "username",
                    "Invalid username (at least 3 characters and at most 64 characters)",
                ),
            );
        else
            errors.delete("username");

        if (!validatePassword(password))
            setErrors(
                errors.set(
                    "password",
                    "Invalid password (at least 8 characters, including at least 1 uppercase letter, 1 lowercase letter, 1 number, and 1 special character)",
                ),
            );
        else
            errors.delete("password");

        if (errors.size === 0) {
            const registerData: Requests.Register = {
                name: username,
                email: email,
                password: password,
            };

            authController
                .register(registerData)
                .then((response) => {
                    if (!response) return;
                    authController.login({ name: username, password: password })
                        .then((response) => {
                            console.log(response);
                            navigate("/");
                        })
                        .catch((error) => {
                            console.error(error);
                        })
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    };

    return (
        <div className="body-container">
            <form onSubmit={handleFormSubmit}>
                <label htmlFor="username">Username</label>
                <input
                    type="text"
                    id="username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                />
                {errors.has("username") && <span className="error">{errors.get("username")}</span>}
                <br></br>
                <label htmlFor="email">Email</label>
                <input
                    type="email"
                    id="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                {errors.has("email") && <span className="error">{errors.get("username")}</span>} <br></br>
                <label htmlFor="password">Password</label>
                <input
                    type="password"
                    id="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                {errors.has("password") && <span className="error">{errors.get("password")}</span>} <br></br>
                <button className="btn btn-primary" type="submit">Register</button>
            </form>
        </div>
    );
};

export default Register;
