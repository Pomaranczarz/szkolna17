import { useState } from "react";
import { Requests } from "../../common/Requests";
import { useNavigate } from "react-router-dom";
import AuthController from "../../common/AuthController";
import { Responses } from "../../common/Responses";
import { useUserContext } from "./UserContext";

export const Login = () => {
    const [name, setName] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [error, setError] = useState<string | null>(null);
    const navigate = useNavigate();

    const { setUser } = useUserContext();
    const authController = new AuthController();


    const handleFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const loginData: Requests.Login = {
            name: name,
            password: password,
        };

        authController
            .login(loginData)
            .then(async (response) => {
                if (!response) return;
                sessionStorage.setItem("token", response.token);
                sessionStorage.setItem("expiresIn", response.expiresIn.toString());

                const currentUser: Responses.User | null = await authController.getCurrentUser();
                if (!currentUser) return;

                setUser(currentUser);

                navigate("/home");
            })
            .catch((error) => {
                setError(error.response.data.message);
            });
    };

    return (
        <div className="body-container">
            <h1>Login</h1>
            <form onSubmit={handleFormSubmit}>
                <div>
                    <label htmlFor="name">Name</label>
                    <input
                        type="text"
                        id="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <div>
                    {error ? <p>{error}</p> : null}
                    <button className="btn btn-primary" type="submit">Login</button>
                </div>
            </form>
            <p>
                Don't have an account? <a href="/register">Register here</a>.
            </p>
        </div>
    );
};

export default Login;
