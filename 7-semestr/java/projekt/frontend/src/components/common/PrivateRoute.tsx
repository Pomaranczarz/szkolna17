import { Navigate, Outlet } from "react-router-dom";
import { Role } from "../../common/Globals";
import { useUserContext } from "./UserContext";

interface PrivateRouteProps {
    requiredRole?: Role;
}

const PrivateRoute = ({ requiredRole }: PrivateRouteProps) => {
    const { user, loading } = useUserContext();

    if (loading) {
        return <div>Loading...</div>;
    }

    if (!user) {
        return <Navigate to="/login" />
    }

    if (requiredRole === Role.ADMIN && user.role !== Role.ADMIN) {
        return <Navigate to="/notfound" />;
    }


    return <Outlet />
}

export default PrivateRoute;