import React, { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { Responses } from "../../common/Responses";
import AuthController from "../../common/AuthController";

interface UserContextType {
    user: Responses.User | null;
    setUser: React.Dispatch<React.SetStateAction<Responses.User | null>>;
    loading: boolean;
}

const UserContext = createContext<UserContextType | undefined>(undefined);

export const UserProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
    const [user, setUser] = useState<Responses.User | null>(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchUserData = async () => {
            const authController = new AuthController();

            try {
                const user: Responses.User | null = await authController.getCurrentUser();
                setUser(user);
            } catch {
                setUser(null);
            } finally {
                setLoading(false);
            }
        };

        fetchUserData();
    }, []);

    return (
        <UserContext.Provider value={{ user, setUser, loading }}>
            {children}
        </UserContext.Provider>
    );
};

export const useUserContext = (): UserContextType => {
    const context = useContext(UserContext);
    if (context === undefined) {
        throw new Error("useUserContext must be used within a UserProvider");
    }
    return context;
};
