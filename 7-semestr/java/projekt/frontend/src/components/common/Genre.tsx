import { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";
import GenreController from "../../common/GenreController";
import { GenreGamesController } from "../../common/GenreGamesController";
import { Responses } from "../../common/Responses";
import UserGamesController from "../../common/UserGamesController";
import EntitiesTable from "../partial/EntitiesTable";
import GameRow from "../partial/GameRow";
import GamesHeader from "../partial/GamesHeader";

export const Genre = () => {
    const params = useParams();
    const [genre, setGenre] = useState<Responses.Genre | null>(null);
    const [games, setGames] = useState<Responses.Game[]>([]);
    const userGamesController = useMemo(() => new UserGamesController(), []);

    useState(() => {
        const fetchGenre = async () => {
            const genreController = new GenreController();
            setGenre(await genreController.getById(parseInt(params.id as string)));
        };
        fetchGenre();
    });

    useEffect(() => {
        const fetchGames = async () => {
            const genreGamesController = new GenreGamesController();
            setGames(await genreGamesController.getOwnedEntities(genre?.id as number));
        };
        if (genre !== null)
            fetchGames();

    }, [genre])

    const handleAddGame = async (id: number) => {
        await userGamesController.addGameToCurrentUser(id);
    };

    const handleRemoveGame = async (id: number) => {
        await userGamesController.removeGameFromCurrentUser(id);
    };

    return (
        <div className="body-container">
            {genre !== null ? (
                <div>
                    <h2>{genre.name} games</h2>
                    <EntitiesTable
                        HeaderComponent={GamesHeader}
                        BodyComponent={GameRow}
                        data={games}
                        onAdd={handleAddGame}
                        onDelete={handleRemoveGame}
                    />
                </div>
            ) : (
                <div>Loading...</div>
            )}
        </div>
    );
};

export default Genre;
