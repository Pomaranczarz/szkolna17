import { useEffect, useState } from "react";
import GenreController from "../../common/GenreController";
import { Responses } from "../../common/Responses";
import GenreCard from "../partial/GenreCard";

export const BrowseGenres = () => {
    const [genres, setGenres] = useState<Responses.Genre[]>([]);

    useEffect(() => {
        const fetchGenres = async () => {
            const genresController = new GenreController();
            setGenres(await genresController.getAll());
        };

        fetchGenres();
    }, []);

    return (
        <div className="body-container">
            <h2>Genres</h2>
            <div className="genres-container">
                {genres.map((genre) => (
                    <GenreCard key={genre.id} genre={genre} />
                ))}
            </div>
        </div>
    );
};

export default BrowseGenres;
