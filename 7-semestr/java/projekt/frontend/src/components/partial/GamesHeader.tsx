export const GamesHeader = () => {
    return (
        <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Release Year</th>
                <th>Rating</th>
                <th>Quick Actions</th>
            </tr>
        </thead>
    );
};

export default GamesHeader;
