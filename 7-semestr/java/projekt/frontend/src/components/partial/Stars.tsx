export const Stars = ({ count }: { count: number }) => {
    const fullStars: number = Math.floor(count / 2);
    const halfStar: boolean = count % 2 === 1;

    const makeStarsString = () => {
        const stars = [];
        for (let i = 0; i < fullStars; i++) {
            stars.push('★');
        }
        if (halfStar) {
            stars.push('⯪');
        }
        for (let i = 0; i < 5 - fullStars - (halfStar ? 1 : 0); i++) {
            stars.push('☆');
        }
        return stars.join('');
    };

    return <span>{makeStarsString()}</span>;
};

export default Stars;
