import { Responses } from "../../common/Responses";

export const GenreRow = (genre: Responses.Genre) => {
    return (
        <div className="genre-row">
            {genre.name}
        </div>
    );
};

export default GenreRow;
