import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GenreGamesController } from "../../common/GenreGamesController";
import { Responses } from "../../common/Responses";
import Stars from "./Stars";

interface GenreCardProps {
    genre: Responses.Genre;
};

export const GenreCard = ({ genre }: GenreCardProps) => {
    const [genreGamesCount, setGenreGamesCount] = useState<number>(0);
    const [averageGamesRating, setAverageGamesRating] = useState<number>(0.0);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchGamesCount = async () => {
            const getAverageGamesRating = (games: Responses.Game[]): number => {
                const avg = games.reduce((sum, game) => sum + game.rating, 0) / games.length;
                return isNaN(avg) ? 0 : avg;
            };

            const genreGamesController = new GenreGamesController();
            const games = await genreGamesController.getOwnedEntities(genre.id);
            setGenreGamesCount(games.length);
            setAverageGamesRating(getAverageGamesRating(games));
        };

        fetchGamesCount();
    });

    return (
        <div className="genre-card" style={{ cursor: "pointer" }} onClick={() => { navigate(`/genres/${genre.id}`) }}>
            <h2 className="genre-name">{genre.name}</h2>
            <div className="genre-info">
                <span>Games in genre: {genreGamesCount}</span>
                <span>Average rating: <Stars count={averageGamesRating} /></span>
            </div>
        </div>
    );
};

export default GenreCard;
