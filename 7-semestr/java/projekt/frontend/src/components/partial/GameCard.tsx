import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Responses } from '../../common/Responses';
import { Stars } from './Stars';

export const GameCard = (game: Responses.Game) => {
    const navigate = useNavigate();

    return (
        <div className="game-card" style={{ cursor: "pointer" }} onClick={() => { navigate(`/games/${game.id}`) }}>
            <h2 className="game-title">{game.title}</h2>
            <div className="game-info">
                <span className="game-release-year">Release year: {game.releaseYear}</span>
                <span>Rating: <Stars count={game.rating} /></span>
            </div>
        </div>
    );
};

export default GameCard;
