interface EntitiesTableProps<T extends { id: number }> {
    HeaderComponent: React.ComponentType;
    BodyComponent: React.ComponentType<{ entity: T, onDelete: (id: number) => void, onAdd: (id: number) => void }>;
    data: T[];
    onDelete: (id: number) => void;
    onAdd: (id: number) => void;
};

export const EntitiesTable = <T extends { id: number },>({ HeaderComponent, BodyComponent, data, onDelete, onAdd }: EntitiesTableProps<T>) => {
    return (
        <table>
            <HeaderComponent />
            <tbody>
                {data.map((entity) => (
                    <BodyComponent key={entity.id} entity={entity} onDelete={onDelete} onAdd={onAdd} />
                ))}
            </tbody>
        </table>
    );
};

export default EntitiesTable;
