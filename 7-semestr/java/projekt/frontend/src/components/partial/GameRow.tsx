import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Responses } from "../../common/Responses";
import UserGamesController from "../../common/UserGamesController";
import { Stars } from "./Stars";

interface GameRowProps {
    entity: Responses.Game;
    onDelete: (id: number) => void;
    onAdd: (id: number) => void;
};

export const GameRow = ({ entity, onAdd, onDelete }: GameRowProps) => {
    const navigate = useNavigate();
    const description = entity.description;
    const [isAdded, setIsAdded] = useState<boolean>(false);
    useEffect(() => {
        const fetchIfUserAddedTheGame = async () => {
            const userGamesController = new UserGamesController();
            const games = await userGamesController.getCurrentUserGames();

            setIsAdded(games.find(game => game.id === entity.id) !== undefined);
        };

        fetchIfUserAddedTheGame();
        console.log(`User has ${isAdded ? '' : 'not'} added game`);
    }, [entity.id, isAdded]);

    const handleAdd = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation();
        onAdd(entity.id)
        setIsAdded(!isAdded);
    };

    const handleDelete = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation();
        onDelete(entity.id);
        setIsAdded(!isAdded);
    };

    return (
        <tr onClick={() => navigate(`/games/${entity.id}`)} style={{ cursor: "pointer" }}>
            <td>{entity.title}</td>
            <td>{`${description.length > 97 ? description.substring(0, 97) + "..." : description}`}</td>
            <td>{entity.releaseYear}</td>
            <td>
                <Stars count={entity.rating} />
            </td>
            <td>
                {isAdded ?
                    <button onClick={handleDelete} className="btn btn-danger">Delete</button> :
                    <button onClick={handleAdd} className="btn btn-primary">Add</button>
                }
            </td>
        </tr>
    );
};

export default GameRow;
