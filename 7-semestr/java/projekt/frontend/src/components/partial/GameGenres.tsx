import { Responses } from "../../common/Responses";
import GenreRow from "../partial/GenreRow";

interface GameGenresProps {
    genres: Responses.Genre[];
}

export const GameGenres = ({ genres }: GameGenresProps) => {
    return (
        <div className="game-genres-container">
            <h2>Genres: </h2>
            {genres.map((genre) => (
                <GenreRow key={genre.id} {...genre} />
            ))}
        </div>
    );
};

export default GameGenres;
