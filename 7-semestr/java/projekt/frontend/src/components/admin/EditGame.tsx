import { useEffect, useMemo, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { GameController } from "../../common/GameController";
import GameGenresController from "../../common/GameGenresController";
import { validateDescription, validateReleaseYear, validateTitle } from "../../common/GameUtils";
import GenreController from "../../common/GenreController";
import { Responses } from "../../common/Responses";

export const EditGame = () => {
    const navigate = useNavigate();
    const params = useParams();
    const gameController = useMemo(() => new GameController(), []);
    const gameGenresController = useMemo(() => new GameGenresController(), []);
    const [editedGame, setEditedGame] = useState<Responses.Game | null>(null);
    const [errors, setErrors] = useState<Map<string, string>>(new Map<string, string>());
    const [selectedGenres, setSelectedGenres] = useState<number[]>([]);
    const [genres, setGenres] = useState<Responses.Genre[]>([]);

    useEffect(() => {
        const fetchEditedGame = async () => {
            setEditedGame(await gameController.getById(parseInt(params?.id as string)));
        };
        const fetchGenres = async () => {
            const genresController = new GenreController();
            setGenres(await genresController.getAll());
        };

        fetchGenres();
        fetchEditedGame();
    }, []);

    useEffect(() => {
        const fetchGameGenres = async () => {
            if (editedGame === null)
                return;

            const genres = await gameGenresController.getOwnedEntities(editedGame.id);
            setSelectedGenres(genres.map(genre => genre.id));
        };

        fetchGameGenres();
    }, [editedGame])

    const handleFormSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const game = editedGame!;

        if (!validateTitle(game.title)) setErrors(errors.set("title", "Invalid title"));
        else errors.delete("title");

        if (!validateDescription(game.description)) setErrors(errors.set("description", "Invalid description"));
        else errors.delete("description");

        if (!validateReleaseYear(game.releaseYear)) setErrors(errors.set("releaseYear", "Invalid release year"));
        else errors.delete("releaseYear");

        if (errors.size === 0) {
            const gameController = new GameController();
            const updatedGame = await gameController.updateById(game.id,
                {
                    title: game.title,
                    description: game.description,
                    releaseYear: game.releaseYear,
                    rating: game.rating
                });

            if (updatedGame === null) {
                errors.set("request", "Failed to edit game, try again later");
                return;
            }

            const currentGenres = await gameGenresController.getOwnedEntities(updatedGame.id);
            const addedGenres = selectedGenres.filter(genreId => !currentGenres.map(genre => genre.id).includes(genreId));
            const removedGenres = currentGenres.filter(genre => !selectedGenres.includes(genre.id)).map(genre => genre.id);

            for (let genreId of addedGenres) {
                gameGenresController.addOwnedEntity(updatedGame.id, genreId)
                    .catch((error) => {
                        errors.set("request", error);
                    });
            }

            for (let genreId of removedGenres) {
                gameGenresController.removeOwnedEntity(updatedGame.id, genreId)
                    .catch((error) => {
                        errors.set("request", error)
                    });
            }

            if (errors.size === 0)
                navigate("/admin")
        }
    };

    return (
        <div className="body-container">
            <h1>Edit game</h1>
            {editedGame !== null ? (
                <form onSubmit={handleFormSubmit}>
                    <label className="game-title-field">
                        Title:
                        <input required type="text" value={editedGame.title} onChange={(e) => editedGame.title = e.target.value} />
                        {errors.get("title") && <span className="text-danger">{errors.get("title")}</span>}
                    </label>
                    <div>
                        Genres:
                        <div className="checkbox-group">
                            {genres.map((genre) => (
                                <label key={genre.id}>
                                    {genre.name}
                                    <input
                                        type="checkbox"
                                        value={genre.id}
                                        checked={selectedGenres.includes(genre.id)}
                                        onChange={(e) => {
                                            if (e.target.checked) {
                                                setSelectedGenres([...selectedGenres, genre.id]);
                                            } else {
                                                setSelectedGenres(selectedGenres.filter(id => id !== genre.id));
                                            }
                                        }}
                                    />
                                </label>
                            ))}
                        </div>
                    </div>
                    <label className="game-description-field">
                        Description:
                        <textarea required value={editedGame.description} onChange={(e) => editedGame.description = e.target.value} />
                        {errors.get("description") && <span className="text-danger">{errors.get("description")}</span>}
                    </label>
                    <label>
                        Release year:
                        <input required type="number" value={editedGame.releaseYear} onChange={(e) => editedGame.releaseYear = Number(e.target.value)} />
                        {errors.get("releaseYear") && <span className="text-danger">{errors.get("releaseYear")}</span>}
                    </label>
                    <label>
                        Rating:
                        <input required type="number" min={0} max={10} step={1} value={editedGame.rating} onChange={(e) => editedGame.rating = Number(e.target.value)} />
                        {errors.get("rating") && <span className="text-danger">{errors.get("rating")}</span>}
                    </label>
                    <button className='btn btn-primary' type="submit">Create</button>
                    {errors.get("request") && <span className="text-danger">{errors.get("request")}</span>}
                </form>
            ) : (
                <div>
                    Loading...
                </div>
            )
            }
        </div >
    );
};
