import { useEffect, useState } from "react";
import { Responses } from "../../common/Responses";
import { Role } from "../../common/Globals";
import { useNavigate, useParams } from "react-router-dom";
import { UserController } from "../../common/UserController";
import { useUserContext } from "../common/UserContext";
import { validateEmail, validatePassword, validateUsername } from "../../common/UserUtils";
import { Requests } from "../../common/Requests";

// TODO: add form validation

export const EditUser = () => {
    const params = useParams();
    const navigate = useNavigate();

    const [editedUser, setEditedUser] = useState<Responses.User | null>(null);
    const [errors, setErrors] = useState<Map<string, string>>(new Map<string, string>());
    const currentUserId = useUserContext().user?.id;
    const userController = new UserController();

    useEffect(() => {
        const fetchEditedUser = async () => {
            setEditedUser(await userController.getById(parseInt(params.id as string)));
        };

        fetchEditedUser();
    }, []);

    const validateFields = () => {
        const user = editedUser!;

        if (!validateEmail(user.email)) setErrors(errors.set("email", "Invalid email"));
        else errors.delete("email");

        if (!validateUsername(user.name))
            setErrors(
                errors.set(
                    "username",
                    "Invalid username (at least 3 characters and at most 64 characters)",
                ),
            );
        else
            errors.delete("username");
    };

    const handleFormSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        validateFields();
        const user = editedUser!;

        if (errors.size === 0) {
            const patchData = {
                name: user.name,
                email: user.email,
                role: user.role,
            } as Requests.UserPatch;

            userController.updateById(user.id, patchData)
                .then((response) => {
                    console.log(response);
                    navigate("/admin");
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    };

    return (
        <div className="body-container">
            {editedUser !== null ? (
                <form onSubmit={handleFormSubmit}>
                    <label htmlFor="username">Username</label>
                    <input type="text" id="username" value={editedUser.name} onChange={(e) => setEditedUser({ ...editedUser, name: e.target.value })} />
                    {errors.has("username") && <span className="text-danger">{errors.get("username")}</span>}

                    <label htmlFor="email">Email</label>
                    <input type="email" id="email" value={editedUser.email} onChange={(e) => setEditedUser({ ...editedUser, email: e.target.value })} />
                    {errors.has("email") && <span className="text-danger">{errors.get("email")}</span>}

                    <fieldset disabled={currentUserId === editedUser.id}>
                        <legend>Role</legend>
                        <label>
                            <input type="radio" name="role" value="ADMIN" checked={editedUser.role === Role.ADMIN} onChange={(e) => setEditedUser({ ...editedUser, role: e.target.value as Role })} />
                            Admin
                        </label>
                        <label>
                            <input type="radio" name="role" value="USER" checked={editedUser.role === Role.USER} onChange={(e) => setEditedUser({ ...editedUser, role: e.target.value as Role })} />
                            User
                        </label>
                    </fieldset>

                    <button type="submit" className="btn btn-primary">Save</button>
                </form>
            ) : (
                <div>
                    Loading...
                </div>
            )}
        </div>
    );
};

export default EditUser;
