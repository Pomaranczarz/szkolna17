import React, { useState, useEffect } from 'react';
import { Responses } from '../../common/Responses';
import { AdminUserRow } from './partial/AdminUserRow';
import { GameController } from '../../common/GameController';
import { UserController } from '../../common/UserController';
import { GenreController } from '../../common/GenreController';
import AdminGameRow from './partial/AdminGameRow';
import { useNavigate } from 'react-router-dom';
import AdminEntitiesTable from './partial/AdminEntitiesTable';
import AdminGameHeader from './partial/AdminGameHeader';
import AdminUsersHeader from './partial/AdminUsersHeader';
import AdminGenreHeader from './partial/AdminGenreHeader';
import AdminGenreRow from './partial/AdminGenreRow';

export const Admin = () => {
    const [users, setUsers] = useState<Responses.User[]>([]);
    const [games, setGames] = useState<Responses.Game[]>([]);
    const [genres, setGenres] = useState<Responses.Genre[]>([]);

    const navigate = useNavigate();
    const handleCreateNewGame = () => {
        navigate("/games/create");
    };
    const handleCreateNewUser = () => {
        navigate("/users/create");
    };
    const handleCreateNewGenre = () => {
        navigate("/genres/create");
    };

    const handleDeleteGame = (id: number) => {
        const gamesController = new GameController();
        gamesController.deleteById(id)
            .then((response) => {
                setGames(games.filter(game => game.id !== id));
            })
            .catch((error) => {
                console.error(error);
            });
    };

    const handleDeleteUser = (id: number) => {
        const usersController = new UserController();
        usersController.deleteById(id)
            .then((response) => {
                setUsers(users.filter(user => user.id !== id));
            })
            .catch((error) => {
                console.error(error);
            });
    };

    const handleDeleteGenre = (id: number) => {
        const genresController = new GenreController();
        genresController.deleteById(id)
            .then((response) => {
                setGenres(genres.filter(genre => genre.id !== id));
            })
            .catch((error) => {
                console.error(error);
            });
    };

    useEffect(() => {
        const fetchUsers = async () => {
            const userController = new UserController();
            setUsers(await userController.getAll());
        };

        const fetchGames = async () => {
            const gameController = new GameController();
            setGames(await gameController.getAll());
        };

        const fetchGenres = async () => {
            const genreController = new GenreController();
            setGenres(await genreController.getAll());
        };

        const fetchData = async () => {
            try {
                await fetchUsers();
                await fetchGames();
                await fetchGenres();
            } catch (error) {
                console.error("Error fetching data:", error);
            }
        };

        fetchData();
    }, []);

    return (
        <div className="admin-container">
            <div className='column' id='usersColumn'>
                <h2>Users</h2>
                <button className='btn btn-primary' onClick={handleCreateNewUser}>Create new user</button>
                <AdminEntitiesTable
                    HeaderComponent={AdminUsersHeader}
                    BodyComponent={AdminUserRow}
                    data={users}
                    onDelete={handleDeleteUser}
                    onEdit={(id: number) => navigate(`/users/edit/${id}`)}
                />
            </div>

            <div className="column" id="gamesColumn">
                <h2>Games</h2>
                <button className='btn btn-primary' onClick={handleCreateNewGame}>Create new game</button>
                <AdminEntitiesTable
                    HeaderComponent={AdminGameHeader}
                    BodyComponent={AdminGameRow}
                    data={games}
                    onDelete={handleDeleteGame}
                    onEdit={(id: number) => navigate(`/games/edit/${id}`)}
                />
            </div>

            <div className='column' id='genresColumn'>
                <h2>Genres</h2>
                <button className='btn btn-primary' onClick={handleCreateNewGenre}>Create new genre</button>
                <AdminEntitiesTable
                    HeaderComponent={AdminGenreHeader}
                    BodyComponent={AdminGenreRow}
                    data={genres}
                    onDelete={handleDeleteGenre}
                    onEdit={(id: number) => navigate(`/genres/edit/${id}`)}
                />
            </div>
        </div>
    );
};

export default Admin;
