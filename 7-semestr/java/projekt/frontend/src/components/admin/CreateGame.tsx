import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GameController } from "../../common/GameController";
import GameGenresController from "../../common/GameGenresController";
import { validateDescription, validateReleaseYear, validateTitle } from "../../common/GameUtils";
import GenreController from "../../common/GenreController";
import { Requests } from "../../common/Requests";
import { Responses } from "../../common/Responses";

export const CreateGame = () => {
    const [errors, setErrors] = useState<Map<string, string>>(new Map<string, string>());
    const [title, setTitle] = useState<string>("");
    const [description, setDescription] = useState<string>("");
    const [releaseYear, setReleaseYear] = useState<number>(0);
    const [rating, setRating] = useState<number>(0);
    const [selectedGenres, setSelectedGenres] = useState<number[]>([]);
    const [genres, setGenres] = useState<Responses.Genre[]>([]);

    const navigate = useNavigate();

    useEffect(() => {
        const fetchGenres = async () => {
            const genresController = new GenreController();
            setGenres(await genresController.getAll());
        };

        fetchGenres();
    }, []);

    const handleFormSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!validateTitle(title)) setErrors(errors.set("title", "Invalid title"));
        else errors.delete("title");

        if (!validateDescription(description)) setErrors(errors.set("description", "Invalid description"));
        else errors.delete("description");

        if (!validateReleaseYear(releaseYear)) setErrors(errors.set("releaseYear", "Invalid release year"));
        else errors.delete("releaseYear");

        if (errors.size === 0) {
            const gameController = new GameController();
            const game = await gameController.create({ title: title, description: description, releaseYear: releaseYear, rating: rating } as Requests.GamePost);

            if (selectedGenres.length > 0 && game !== null) {
                const gameGenresController = new GameGenresController();
                for (const genreId of selectedGenres) {
                    const modifiedGame = await gameGenresController.addOwnedEntity(game.id, genreId);
                    if (modifiedGame === null) {
                        errors.set("request", "Failed to add genre to game, try again later");
                        break;
                    }
                }
                if (!errors.get("request"))
                    navigate("/admin");
            }
            else {
                errors.set("request", "Failed to create game, try again later");
            }
        }
    };

    return (
        <div className="body-container">
            <h1>Create game</h1>
            <form onSubmit={handleFormSubmit}>
                <label className="game-title-field">
                    Title:
                    <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
                    {errors.get("title") && <span className="text-danger">{errors.get("title")}</span>}
                </label>
                <label>
                    Genres:
                    <div className="checkbox-group">
                        {genres.map((genre) => (
                            <label key={genre.id}>
                                <input
                                    type="checkbox"
                                    value={genre.id}
                                    checked={selectedGenres.includes(genre.id)}
                                    onChange={(e) => {
                                        if (e.target.checked) {
                                            setSelectedGenres([...selectedGenres, genre.id]);
                                        } else {
                                            setSelectedGenres(selectedGenres.filter(id => id !== genre.id));
                                        }
                                    }}
                                />
                                {genre.name}
                            </label>
                        ))}
                    </div>
                </label>
                <label className="game-description-field">
                    Description:
                    <textarea value={description} onChange={(e) => setDescription(e.target.value)} />
                    {errors.get("description") && <span className="text-danger">{errors.get("description")}</span>}
                </label>
                <label>
                    Release year:
                    <input type="number" value={releaseYear} onChange={(e) => setReleaseYear(Number(e.target.value))} />
                    {errors.get("releaseYear") && <span className="text-danger">{errors.get("releaseYear")}</span>}
                </label>
                <label>
                    Rating:
                    <input type="number" min={0} max={10} step={1} value={rating} onChange={(e) => setRating(Number(e.target.value))} />
                    {errors.get("rating") && <span className="text-danger">{errors.get("rating")}</span>}
                </label>
                <button className='btn btn-primary' type="submit">Create</button>
                {errors.get("request") && <span className="text-danger">{errors.get("request")}</span>}
            </form>
        </div>
    );
};

export default CreateGame;
