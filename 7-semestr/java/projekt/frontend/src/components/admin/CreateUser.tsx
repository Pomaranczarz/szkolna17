import { useEffect, useState } from "react";
import { UserController } from "../../common/UserController";
import { Role } from "../../common/Globals";
import { Requests } from "../../common/Requests";
import { useNavigate } from "react-router-dom";

export const CreateUser = () => {
    const [errors, setErrors] = useState<Map<string, string>>(new Map<string, string>());
    const [name, setName] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");

    const navigate = useNavigate();

    const validateName = (str: string): boolean => {
        return str.length >= 3 && str.length <= 64;
    };

    const validateEmail = (str: string): boolean => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(str);
    };

    const validatePassword = (str: string): boolean => {
        return password.length >= 8;
    };

    useEffect(() => {
        const checkFields = () => {
            if (!validateName(name)) setErrors(errors.set("name", "Invalid name"));
            else errors.delete("name");

            if (!validateEmail(email)) setErrors(errors.set("email", "Invalid email"));
            else errors.delete("email");

            if (!validatePassword(password)) setErrors(errors.set("password", "Invalid password"));
            else errors.delete("password");
        };

        checkFields();
    });

    const handleFromSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (errors.size === 0) {
            const userController = new UserController();
            userController.create({ name: name, email: email, password: password, role: Role.USER } as Requests.UserPost)
                .then((response) => {
                    console.log(response);
                    navigate("/admin");
                })
                .catch((error) => {
                    console.error(error);
                    errors.set("request", "Failed to create user, try again later");
                })
        }
    };

    return (
        <div className="body-container">
            <h1>Create User</h1>
            <form onSubmit={handleFromSubmit}>
                <label>
                    Name:
                    <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
                </label>
                {errors.get("name") !== undefined && <span className="text-danger">{errors.get("name")}</span>}
                <label>
                    Email:
                    <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                </label>
                {errors.get("email") !== undefined && <span className="text-danger">{errors.get("email")}</span>}
                <label>
                    Password:
                    <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                </label>
                {errors.get("password") !== undefined && <span className="text-danger">{errors.get("password")}</span>}
                <button className="btn btn-primary" type="submit">Create</button>
            </form>
            {errors.get("request") !== undefined && <span className="text-danger">{errors.get("request")}</span>}
        </div>
    );
};

export default CreateUser;
