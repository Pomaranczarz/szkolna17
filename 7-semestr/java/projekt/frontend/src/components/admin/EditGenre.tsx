import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { GenreController } from "../../common/GenreController";
import { Responses } from "../../common/Responses";

export const EditGenre = () => {
    const navigate = useNavigate();

    const params = useParams();
    const [genre, setGenre] = useState<Responses.Genre | null>(null);
    const [errors] = useState<Map<string, string>>(new Map<string, string>());
    const genreController = new GenreController();

    useEffect(() => {
        const fetchGenre = async () => {
            setGenre(await genreController.getById(parseInt(params.id as string)));
        }

        fetchGenre();
    }, [params.id]);

    const handleFromSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!genre)
            return;

        if (genre.name.length < 1 || genre.name.length > 128) {
            errors.set("name", "Invalid name (at least 1 character and at most 128 characters)");
        }

        genreController.updateById(genre?.id as number, genre as Responses.Genre).then(() => {
            navigate("/admin");
        })
            .catch((error) => {
                console.error(error);
                errors.set("request", "Failed to update genre");
            })
    }

    return (
        <div className="body-container">
            {genre !== null ? (
                <form onSubmit={handleFromSubmit}>
                    <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input type="text" className="form-control" id="name" name="name" defaultValue={genre.name} onChange={(e) => genre.name = e.target.value} />
                        {errors.has("name") && <span className="text-danger">{errors.get("name")}</span>}
                    </div>
                    <button type="submit" className="btn btn-primary">Save</button>
                    {errors.has("request") && <span className="text-danger">{errors.get("request")}</span>}
                </form>
            ) : (
                <div>
                    Loading...
                </div>
            )}
        </div>
    );
};
