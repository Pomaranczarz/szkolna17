import { useNavigate } from "react-router-dom";
import { Responses } from "../../../common/Responses";

interface AdminGenreRowProps {
    entity: Responses.Genre,
    onDelete: (id: number) => void,
    onEdit: (id: number) => void;
};

export const AdminGenreRow: React.FC<AdminGenreRowProps> = (
    { entity, onDelete, onEdit }
) => {
    const navigate = useNavigate();

    const handleEdit = async (e: React.MouseEvent) => {
        e.stopPropagation();
        onEdit(entity.id);
    }

    const handleDelete = async (e: React.MouseEvent) => {
        e.stopPropagation();
        onDelete(entity.id);
    }

    return (
        <tr onClick={() => navigate(`/genres/${entity.id}`)}>
            <td>{entity.id}</td>
            <td>{entity.name}</td>
            <td>
                <button onClick={handleDelete} className="btn btn-danger">Delete</button>
                <button onClick={handleEdit} className="btn btn-primary">Edit</button>
            </td>
        </tr>
    );
};

export default AdminGenreRow;
