export const AdminUsersHeader = () => {
    return (
        <thead>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Email</th>
                <th>Role</th>
                <th>Quick Actions</th>
            </tr>
        </thead>
    );
};

export default AdminUsersHeader;
