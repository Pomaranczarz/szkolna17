import { useNavigate } from "react-router-dom";
import { Responses } from "../../../common/Responses";
import { Stars } from "../../partial/Stars";

interface AdminGameRowProps {
    entity: Responses.Game;
    onDelete: (id: number) => void;
    onEdit: (id: number) => void;
};

export const AdminGameRow = ({ entity, onDelete, onEdit }: AdminGameRowProps) => {
    const navigate = useNavigate();
    const description = entity.description;

    const handleDelete = async (e: React.MouseEvent) => {
        e.stopPropagation();
        onDelete(entity.id);
    }

    const handleEdit = async (e: React.MouseEvent) => {
        e.stopPropagation();
        onEdit(entity.id);
    };

    return (
        <tr onClick={() => navigate(`/games/${entity.id}`)} style={{ cursor: "pointer" }}>
            <td>{entity.id}</td>
            <td>{entity.title}</td>
            <td>{`${description.length > 70 ? description.substring(0, 65) + "..." : description}`}</td>
            <td>{entity.releaseYear}</td>
            <td>
                <Stars count={entity.rating} />
            </td>
            <td>
                <button onClick={handleDelete} className="btn btn-danger">Delete</button>
                <button onClick={handleEdit} className="btn btn-primary">Edit</button>
            </td>
        </tr>
    );
};

export default AdminGameRow;
