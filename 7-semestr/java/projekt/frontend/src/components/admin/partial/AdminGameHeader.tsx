export const AdminGameHeader = () => {
    return (
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th>Release Year</th>
                <th>Rating</th>
                <th>Quick Actions</th>
            </tr>
        </thead>
    );
};

export default AdminGameHeader;
