export const AdminGenreHeader = () => {
    return (
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Quick Actions</th>
            </tr>
        </thead>
    );
};

export default AdminGenreHeader;
