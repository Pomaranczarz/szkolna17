interface AdminEntitiesTableProps<T extends { id: number }> {
    HeaderComponent: React.ComponentType;
    BodyComponent: React.ComponentType<{ entity: T, onDelete: (id: number) => void, onEdit: (id: number) => void }>;
    data: T[];
    onDelete: (id: number) => void;
    onEdit: (id: number) => void;
};

export const AdminEntitiesTable = <T extends { id: number },>({ HeaderComponent, BodyComponent, data, onDelete, onEdit }: AdminEntitiesTableProps<T>) => {
    return (
        <table>
            <HeaderComponent />
            <tbody>
                {data.map((entity) => (
                    <BodyComponent key={entity.id} entity={entity} onDelete={onDelete} onEdit={onEdit} />
                ))}
            </tbody>
        </table>
    );
};

export default AdminEntitiesTable;
