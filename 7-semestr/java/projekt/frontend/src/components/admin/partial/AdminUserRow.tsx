import { useNavigate } from "react-router-dom";
import { Responses } from "../../../common/Responses";

interface AdminUserRowProps {
    entity: Responses.User;
    onDelete: (id: number) => void;
    onEdit: (id: number) => void;
};

export const AdminUserRow = ({ entity, onDelete, onEdit }: AdminUserRowProps) => {
    const navigate = useNavigate();

    const handleEdit = async (e: React.MouseEvent) => {
        e.stopPropagation();
        onEdit(entity.id);
    };

    const handleDelete = async (e: React.MouseEvent) => {
        e.stopPropagation();
        onDelete(entity.id);
    };

    return (
        <tr onClick={() => navigate(`/users/${entity.id}`)} style={{ cursor: "pointer" }}>
            <td>{entity.id}</td>
            <td>{entity.name}</td>
            <td>{entity.email}</td>
            <td>{entity.role}</td>
            <td>
                <button onClick={handleDelete} className="btn btn-danger">Delete</button>
                <button onClick={handleEdit} className="btn btn-primary">Edit</button>
            </td>
        </tr>
    );
};

export default AdminUserRow;
