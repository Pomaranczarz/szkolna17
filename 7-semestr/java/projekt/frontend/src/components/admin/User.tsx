import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import { Responses } from "../../common/Responses";
import GameRow from "../partial/GameRow";
import { UserController } from "../../common/UserController";
import UserGamesController from "../../common/UserGamesController";
import EntitiesTable from "../partial/EntitiesTable";
import GamesHeader from "../partial/GamesHeader";

export const User = () => {
    const [user, setUser] = useState<Responses.User | null>(null);
    const [userGames, setUserGames] = useState<Responses.Game[]>([]);

    const params = useParams();

    useEffect(() => {
        const fetchUser = async () => {
            const userController = new UserController();
            setUser(await userController.getById(parseInt(params.id as string)));
        };

        fetchUser();
    }, [params.id]);

    useEffect(() => {
        const fetchUserGames = async () => {
            const userGamesController = new UserGamesController();
            setUserGames(await userGamesController.getUserGames(parseInt(params.id as string)));
        };

        fetchUserGames();
    }, [params.id]);

    const handleAddGame = (id: number) => {

    };

    const handleRemoveGame = (id: number) => {

    };

    return (
        <div className="body-container">
            {user ? (
                <div>
                    <h1>{user.name}</h1>
                    <p>{user.email}</p>
                    <p>{user.role}</p>
                    {userGames.length > 0 ? (
                        <>
                            <p>{user.name}'s games: </p>
                            <EntitiesTable
                                HeaderComponent={GamesHeader}
                                BodyComponent={GameRow}
                                data={userGames}
                                onAdd={handleAddGame}
                                onDelete={handleRemoveGame}
                            />
                        </>
                    ) : (
                        <p>User has no games.</p>
                    )}
                </div>
            ) : (
                <div>
                    Loading...
                </div>
            )}
        </div>
    );
}
