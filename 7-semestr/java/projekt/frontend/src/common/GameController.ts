import axios from "axios";
import { SERVER_BASE_URL } from "./Globals";
import { Responses } from "./Responses";
import { Requests } from "./Requests";
import { CrudController } from "./CrudController";
import BaseController from "./BaseController";

export class GameController extends BaseController implements CrudController<Responses.Game> {
    getAll = async (): Promise<Responses.Game[]> => {
        try {
            const response = await axios
                .get(`${SERVER_BASE_URL}/games`, BaseController.getAuthHeadersFromToken());
            console.log(response);
            return response.data as Responses.Game[];
        } catch (error) {
            console.error(error);
            return [];
        }
    }

    getById = async (id: number): Promise<Responses.Game | null> => {
        return axios
            .get(`${SERVER_BASE_URL}/games/${id}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Game;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    create = async (game: Requests.GamePost): Promise<Responses.Game | null> => {
        return axios
            .post(`${SERVER_BASE_URL}/games`, game, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Game;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    updateById = async (id: number, game: Requests.GamePatch): Promise<Responses.Game | null> => {
        return axios
            .put(`${SERVER_BASE_URL}/games/${id}`, game, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Game;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    deleteById = async (id: number): Promise<boolean> => {
        return axios
            .delete(`${SERVER_BASE_URL}/games/${id}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return true;
            })
            .catch((error) => {
                console.error(error);
                return false;
            });
    }
}