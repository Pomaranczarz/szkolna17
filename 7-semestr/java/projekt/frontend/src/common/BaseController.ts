export class BaseController {
    static getAuthHeadersFromToken = () => {
        return {
            headers: {
                Authorization: `Bearer ${sessionStorage.getItem("token")}`,
            },
        };
    };
};

export default BaseController;