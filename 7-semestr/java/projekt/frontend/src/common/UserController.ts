import axios from "axios";
import { SERVER_BASE_URL } from "./Globals";
import { Responses } from "./Responses";
import { Requests } from "./Requests";
import CrudController from "./CrudController";
import BaseController from "./BaseController";

export class UserController extends BaseController implements CrudController<Responses.User> {
    getAll = async (): Promise<Responses.User[]> => {
        return axios
            .get(`${SERVER_BASE_URL}/users`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.User[];
            })
            .catch((error) => {
                console.error(error);
                return [];
            });
    }

    getById = async (id: number): Promise<Responses.User | null> => {
        return axios
            .get(`${SERVER_BASE_URL}/users/${id}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.User;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    deleteById = async (id: number): Promise<boolean> => {
        return axios
            .delete(`${SERVER_BASE_URL}/users/${id}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return true;
            })
            .catch((error) => {
                console.error(error);
                return false;
            });
    }

    updateById = async (id: number, user: Requests.UserPatch): Promise<Responses.User | null> => {
        return axios
            .patch(`${SERVER_BASE_URL}/users/${id}`, user, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.User;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    create = async (user: Requests.UserPost): Promise<Responses.User | null> => {
        return axios
            .post(`${SERVER_BASE_URL}/users`, user, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.User;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }
}
