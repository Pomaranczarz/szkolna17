import axios from "axios";
import { Requests } from "./Requests";
import { SERVER_BASE_URL } from "./Globals";
import BaseController from "./BaseController";
import { Responses } from "./Responses";

type User = Responses.User;

export class AuthController extends BaseController {
    login = async (data: Requests.Login): Promise<Responses.Login | null> => {
        return axios
            .post(`${SERVER_BASE_URL}/auth/login`, data)
            .then((response) => {
                console.log(response);
                return response.data as Responses.Login;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    register = async (data: Requests.Register): Promise<User | null> => {
        return axios
            .post(`${SERVER_BASE_URL}/auth/register`, data)
            .then((response) => {
                console.log(response);
                return response.data as User;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    getCurrentUser = async (): Promise<User | null> => {
        return axios
            .get(`${SERVER_BASE_URL}/users/me`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as User;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }
};

export default AuthController;
