export interface CrudController<T> {
    getAll: () => Promise<T[]>;
    getById: (id: number) => Promise<T | null>;
    create: (data: any) => Promise<T | null>;
    updateById: (id: number, data: T) => Promise<T | null>;
    deleteById: (id: number) => Promise<boolean>;
}

export default CrudController;