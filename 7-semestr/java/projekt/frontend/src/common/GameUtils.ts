import { Responses } from "./Responses";

export const sortByTitleAsc = (games: Responses.Game[]): Responses.Game[] => {
    return games.sort((a, b) => (a.title ?? '').localeCompare(b.title ?? ''));
}

export const sortByTitleDesc = (games: Responses.Game[]): Responses.Game[] => {
    return games.sort((a, b) => (b.title ?? '').localeCompare(a.title ?? ''));
}

export const sortByRatingAsc = (games: Responses.Game[]): Responses.Game[] => {
    return games.sort((a, b) => (a.rating ?? 0) - (b.rating ?? 0));
}

export const sortByRatingDesc = (games: Responses.Game[]): Responses.Game[] => {
    return games.sort((a, b) => (b.rating ?? 0) - (a.rating ?? 0));
}

export const sortByReleaseYearAsc = (games: Responses.Game[]): Responses.Game[] => {
    return games.sort((a, b) => (a.releaseYear ?? 0) - (b.releaseYear ?? 0));
}

export const sortByReleaseYearDesc = (games: Responses.Game[]): Responses.Game[] => {
    return games.sort((a, b) => (b.releaseYear ?? 0) - (a.releaseYear ?? 0));
}
export const sortByDescriptionAsc = (games: Responses.Game[]): Responses.Game[] => {
    return games.sort((a, b) => (a.description ?? '').localeCompare(b.description ?? ''));
}

export const sortByDescriptionDesc = (games: Responses.Game[]): Responses.Game[] => {
    return games.sort((a, b) => (b.description ?? '').localeCompare(a.description ?? ''));
}

export const validateTitle = (title: string): boolean => {
    return title.length >= 1 && title.length <= 128;
};

export const validateDescription = (description: string): boolean => {
    return description.length >= 1 && description.length <= 512;
}

export const validateReleaseYear = (year: number): boolean => {
    return year >= 1940 && year <= 2100;
};
