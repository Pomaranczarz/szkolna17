export enum Role {
    ADMIN = "ADMIN",
    USER = "USER",
}

export const SERVER_BASE_URL = "http://localhost:5000/api";
