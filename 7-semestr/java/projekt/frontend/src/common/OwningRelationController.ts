export interface OwningRelationController<T, U> {
    getOwnedEntities: (ownerId: number) => Promise<U[]>;
    addOwnedEntity: (ownerId: number, entityId: number) => Promise<T | null>;
    removeOwnedEntity: (ownerId: number, entityId: number) => Promise<T | null>;
};

export default OwningRelationController;
