import { Role } from "./Globals";

export namespace Requests {
    export interface GamePatch {
        title?: string;
        description?: string;
        releaseYear?: number;
        rating?: number;
    }

    export interface GamePost {
        title: string;
        description: string;
        releaseYear: number;
        rating: number;
    }

    export interface GenrePatch {
        name?: string;
        gamesIds?: number[];
    }

    export interface GenrePost {
        name: string;
        gamesIds: number[];
    }

    export interface Login {
        name: string;
        password: string;
    }

    export interface Register {
        name: string;
        email: string;
        password: string;
    }

    export interface UserPatch {
        name?: string;
        email?: string;
        password?: string;
        role?: Role;
    }

    export interface UserPost {
        name: string;
        email: string;
        password: string;
        role: Role;
    }
}

