import axios from "axios";
import BaseController from "./BaseController";
import { SERVER_BASE_URL } from "./Globals";
import OwningRelationController from "./OwningRelationController";
import { Responses } from "./Responses";

type Game = Responses.Game;
type Genre = Responses.Genre;

export class GenreGamesController extends BaseController implements OwningRelationController<Genre, Game> {
    getOwnedEntities = async (genreId: number): Promise<Responses.Game[]> => {
        return axios
            .get(`${SERVER_BASE_URL}/genres/${genreId}/games`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Game[];
            })
            .catch((error) => {
                console.error(error);
                return [];
            });
    };

    addOwnedEntity = async (genreId: number, gameId: number): Promise<Genre | null> => {
        return axios
            .post(`${SERVER_BASE_URL}/genres/${genreId}/games/${gameId}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Genre;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    };

    removeOwnedEntity = async (genreId: number, gameId: number): Promise<Genre | null> => {
        return axios
            .delete(`${SERVER_BASE_URL}/genres/${genreId}/games/${gameId}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Genre;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    };
}
