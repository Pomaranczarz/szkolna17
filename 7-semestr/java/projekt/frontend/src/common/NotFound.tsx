export const NotFound = () => {
    return (
        <div>
            <h1>404</h1>
            <h2>Not Found</h2>
            <p>The requested page could not be found</p>
        </div>
    );
};

export default NotFound;
