import { Role } from "./Globals";

export namespace Responses {
    export interface Login {
        token: string;
        expiresIn: number;
    };

    export interface User {
        id: number;
        name: string;
        email: string;
        password: string;
        role: Role;
        gamesIds: number[];
    };

    export interface Genre {
        id: number;
        name: string;
    };

    export interface AdminGenre extends Genre {
        gamesIds: number[];
    };

    export interface Game {
        id: number;
        title: string;
        description: string;
        releaseYear: number;
        rating: number;
    };

    export interface AdminGame extends Game {
        genresIds: number[];
        usersIds: number[];
    };
}
