export const validateEmail = (str: string): boolean => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(str);
};

export const validateUsername = (str: string): boolean => {
    return str.length >= 3 && str.length <= 64;
};

export const validatePassword = (str: string): boolean => {
    return str.length >= 8;
};
