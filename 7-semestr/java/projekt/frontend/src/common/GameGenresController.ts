import axios from "axios";
import BaseController from "./BaseController";
import { SERVER_BASE_URL } from "./Globals";
import OwningRelationController from "./OwningRelationController";
import { Responses } from "./Responses";

type Genre = Responses.Genre;
type Game = Responses.Game;

export class GameGenresController extends BaseController implements OwningRelationController<Game, Genre> {
    getOwnedEntities = async (gameId: number): Promise<Genre[]> => {
        return axios
            .get(`${SERVER_BASE_URL}/games/${gameId}/genres`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Genre[];
            })
            .catch((error) => {
                console.error(error);
                return [];
            });
    }

    addOwnedEntity = async (gameId: number, genreId: number): Promise<Game | null> => {
        return axios
            .post(`${SERVER_BASE_URL}/games/${gameId}/genres/${genreId}`, {}, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Game;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    removeOwnedEntity = async (gameId: number, genreId: number): Promise<Game | null> => {
        return axios
            .delete(`${SERVER_BASE_URL}/games/${gameId}/genres/${genreId}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Game;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }
}

export default GameGenresController;
