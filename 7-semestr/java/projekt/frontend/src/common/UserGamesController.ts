import axios from "axios";
import BaseController from "./BaseController";
import { Responses } from "./Responses";
import { SERVER_BASE_URL } from "./Globals";
import { UserController } from "./UserController";
import AuthController from "./AuthController";

export class UserGamesController extends BaseController {
    userController = new UserController();
    authController = new AuthController();

    getUserGames = async (userId: number): Promise<Responses.AdminGame[]> => {
        return axios
            .get(`${SERVER_BASE_URL}/users/${userId}/games`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.AdminGame[];
            })
            .catch((error) => {
                console.error(error);
                return [];
            });
    }

    getCurrentUserGames = async (): Promise<Responses.Game[]> => {
        const currentUser: Responses.User | null = await this.authController.getCurrentUser();
        if (!currentUser) return [];

        return axios
            .get(`${SERVER_BASE_URL}/users/me/games`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Game[];
            })
            .catch((error) => {
                console.error(error);
                return [];
            });
    }

    addGameToUser = async (userId: number, gameId: number): Promise<Responses.AdminGame | null> => {
        return axios
            .post(`${SERVER_BASE_URL}/users/${userId}/games/${gameId}`, {}, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.AdminGame;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    addGameToCurrentUser = async (gameId: number): Promise<Responses.Game | null> => {
        const currentUser: Responses.User | null = await this.authController.getCurrentUser();
        if (!currentUser) return null;

        return axios
            .post(`${SERVER_BASE_URL}/users/me/games/${gameId}`, {}, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Game;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    removeGameFromUser = async (userId: number, gameId: number): Promise<boolean> => {
        return axios
            .delete(`${SERVER_BASE_URL}/users/${userId}/games/${gameId}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return true;
            })
            .catch((error) => {
                console.error(error);
                return false;
            });
    }

    removeGameFromCurrentUser = async (gameId: number): Promise<boolean> => {
        const currentUser: Responses.User | null = await this.authController.getCurrentUser();
        if (!currentUser) return false;

        return axios
            .delete(`${SERVER_BASE_URL}/users/me/games/${gameId}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return true;
            })
            .catch((error) => {
                console.error(error);
                return false;
            });
    }
}

export default UserGamesController;