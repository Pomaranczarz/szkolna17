import axios from "axios";
import { SERVER_BASE_URL } from "./Globals";
import { Responses } from "./Responses";
import { CrudController } from "./CrudController";
import BaseController from "./BaseController";
import { Requests } from "./Requests";

export class GenreController extends BaseController implements CrudController<Responses.Genre> {
    getAll = async (): Promise<Responses.Genre[]> => {
        return axios
            .get(`${SERVER_BASE_URL}/genres`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Genre[];
            })
            .catch((error) => {
                console.error(error);
                return [];
            });
    }

    getById = async (id: number): Promise<Responses.Genre | null> => {
        return axios
            .get(`${SERVER_BASE_URL}/genres/${id}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Genre;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    create = async (genre: Requests.GenrePost): Promise<Responses.Genre | null> => {
        return axios
            .post(`${SERVER_BASE_URL}/genres`, genre, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Genre;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    updateById = async (id: number, entity: Responses.Genre): Promise<Responses.Genre | null> => {
        return axios
            .put(`${SERVER_BASE_URL}/genres/${id}`, entity, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return response.data as Responses.Genre;
            })
            .catch((error) => {
                console.error(error);
                return null;
            });
    }

    deleteById = async (id: number): Promise<boolean> => {
        return axios
            .delete(`${SERVER_BASE_URL}/genres/${id}`, BaseController.getAuthHeadersFromToken())
            .then((response) => {
                console.log(response);
                return true;
            })
            .catch((error) => {
                console.error(error);
                return false;
            });
    }
}

export default GenreController;
