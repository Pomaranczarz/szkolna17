import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { NavBar } from "./components/common/NavBar";
import NotFound from "./common/NotFound";
import Login from "./components/common/Login";
import Register from "./components/common/Register";
import Admin from "./components/admin/Admin";
import Profile from "./components/common/Profile";
import { User } from "./components/admin/User";
import EditUser from "./components/admin/EditUser";
import { EditGenre } from "./components/admin/EditGenre";
import { UserProvider } from "./components/common/UserContext";
import PrivateRoute from "./components/common/PrivateRoute";
import { Role } from "./common/Globals";
import { EditGame } from "./components/admin/EditGame";
import { Home } from "./components/common/Home";
import CreateGame from "./components/admin/CreateGame";
import CreateUser from "./components/admin/CreateUser";
import { CreateGenre } from "./components/admin/CreateGenre";
import { Game } from "./components/common/Game";
import Genre from "./components/common/Genre";
import BrowseGenres from "./components/common/BrowseGenres";

function App() {
    return (
        <>
            <BrowserRouter>
                <UserProvider>
                    <NavBar />
                    <Routes>
                        <Route path="/login" element={<Login />} />
                        <Route path="/register" element={<Register />} />
                        <Route path="*" element={<NotFound />} />
                        <Route path="notfound" element={<NotFound />} />

                        <Route element={<PrivateRoute />}>
                            <Route path="/home" element={<Home />} />
                            <Route path="/profile" element={<Profile />} />
                            <Route path="/" element={<Home />} />
                            <Route path="/games/:id" element={<Game />} />
                            <Route path="/genres" element={<BrowseGenres />} />
                            <Route path="/genres/:id" element={<Genre />} />
                        </Route>

                        <Route element={<PrivateRoute requiredRole={Role.ADMIN} />} >
                            <Route path="/games/edit/:id" element={<EditGame />} />
                            <Route path="/games/create" element={<CreateGame />} />
                            <Route path="/users/:id" element={<User />} />
                            <Route path="/users/edit/:id" element={<EditUser />} />
                            <Route path="/users/create" element={<CreateUser />} />
                            <Route path="/admin" element={<Admin />} />
                            <Route path="/genres/edit/:id" element={<EditGenre />} />
                            <Route path="/genres/create" element={<CreateGenre />} />
                        </Route>
                    </Routes>
                </UserProvider>
            </BrowserRouter>
        </>
    );
}

export default App;
