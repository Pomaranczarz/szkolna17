package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.exception.EntityAlreadyPresentException;
import com.pomaranczarz.GamesBrowser.exception.EntityNotFoundException;
import com.pomaranczarz.GamesBrowser.exception.EntityPersistenceException;
import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GameGenresService implements OwningRelationService<Game, Genre> {
    private final CrudService<Genre> genreService;
    private final CrudService<Game> gameService;

    /**
     * Returns all genres related to the given game.
     *
     * @param gameId id of the game to get genres from
     * @return all genres of the game
     */
    @Override
    public List<Genre> getOwnedEntities(long gameId) {
        var gameOpt = gameService.findById(gameId);
        if (gameOpt.isEmpty())
            throw new EntityNotFoundException("Requested game [ID: " + gameId + "] not found");

        var game = gameOpt.get();
        var genres = game.getGenres();

        return genres.stream().toList();
    }

    /**
     * Adds a genre to a game. Only accessible to users with the ADMIN role.
     *
     * @param gameId  id of the game to add genre to
     * @param genreId id of the genre to add
     * @return updated game with added genre
     */
    @Override
    public Game addOwnedEntity(long gameId, long genreId) {
        var gameOpt = gameService.findById(gameId);
        if (gameOpt.isEmpty())
            throw new EntityNotFoundException("Requested game [ID: " + gameId + "] not found");
        var genreOpt = genreService.findById(genreId);
        if (genreOpt.isEmpty())
            throw new EntityNotFoundException("Requested genre [ID: " + genreId + "] not found");

        var game = gameOpt.get();
        var genre = genreOpt.get();

        if (game.getGenres().contains(genre))
            throw new EntityAlreadyPresentException(String.format("Genre[ID: %d] already present on the game [ID: %d]", genreId, gameId));

        game.getGenres().add(genre);

        if (gameService.save(game).isEmpty())
            throw new EntityPersistenceException("Error saving game [ID: " + gameId + "]");

        return game;
    }

    /**
     * Removes a genre from a game. Only accessible to users with the ADMIN role.
     *
     * @param gameId  id of the game to remove genre from
     * @param genreId id of the genre to remove
     * @return updated game with removed genre
     */
    @Override
    public Game removeOwnedEntity(long gameId, long genreId) {
        var gameOpt = gameService.findById(gameId);
        if (gameOpt.isEmpty())
            throw new EntityNotFoundException(String.format("Requested game [ID: %d] not found", gameId));
        var genreOpt = genreService.findById(genreId);
        if (genreOpt.isEmpty())
            throw new EntityNotFoundException(String.format("Requested genre [ID: %d] not found", genreId));

        var game = gameOpt.get();
        var genre = genreOpt.get();

        if (!game.getGenres().contains(genre))
            throw new EntityNotFoundException(String.format("Genre [ID: %d] not present on the game [ID: %d]", genreId, gameId));

        game.getGenres().remove(genre);

        if (gameService.save(game).isEmpty())
            throw new EntityPersistenceException("Error saving game [ID: " + gameId + "]");

        return game;
    }
}
