package com.pomaranczarz.GamesBrowser.response.genre;

import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import org.springframework.stereotype.Component;

@Component
public class GenreResponseMapper implements ResponseMapper<Genre, GenreResponse> {
    @Override
    public GenreResponse mapFromSingleObject(Genre genre) {
        return GenreResponse.builder()
                .id(genre.getId())
                .name(genre.getName())
                .build();
    }
}
