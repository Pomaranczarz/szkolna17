package com.pomaranczarz.GamesBrowser.response.genre;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString
public class GenreResponse {
    @NotNull
    private Long id;
    @NotBlank
    private String name;
}

