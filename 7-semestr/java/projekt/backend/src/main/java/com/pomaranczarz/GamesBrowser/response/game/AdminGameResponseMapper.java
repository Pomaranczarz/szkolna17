package com.pomaranczarz.GamesBrowser.response.game;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class AdminGameResponseMapper implements ResponseMapper<Game, AdminGameResponse> {
    @Override
    public AdminGameResponse mapFromSingleObject(Game game) {
        return AdminGameResponse.builder()
                .id(game.getId())
                .title(game.getTitle())
                .rating(game.getRating())
                .releaseYear(game.getReleaseYear())
                .description(game.getDescription())
                .genresIds(game.getGenres() == null ?
                        Set.of() :
                        game.getGenres().stream().map(Genre::getId).collect(Collectors.toSet()))
                .usersIds(game.getUsers() == null ?
                        Set.of() :
                        game.getUsers().stream().map(User::getId).collect(Collectors.toSet()))
                .build();
    }
}
