package com.pomaranczarz.GamesBrowser.exception;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map<String, Object>> handleSecurityException(Exception exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(problemDetail("Internal Server Error", exception.getMessage()));
    }

    @ExceptionHandler(InvalidRegistrationDataException.class)
    public ResponseEntity<Map<String, Object>> handleBadRegistrationDataException(InvalidRegistrationDataException exception) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(problemDetail("Invalid registration data", "Provided registration data was invalid"));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Map<String, Object>> handleEntityNotFoundException(EntityNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(problemDetail("Requested Entity Not Found", exception.getMessage()));
    }

    @ExceptionHandler(EntityPersistenceException.class)
    public ResponseEntity<Map<String, Object>> handleEntityPersistenceException(EntityPersistenceException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(problemDetail("Internal Server Error", exception.getMessage()));
    }

    @ExceptionHandler(EntityAlreadyPresentException.class)
    public ResponseEntity<Map<String, Object>> handleEntityAlreadyPresent(EntityAlreadyPresentException exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(problemDetail("Entity Already Present", exception.getMessage()));
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<Map<String, Object>> handleBadCredentialsException(BadCredentialsException exception) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(problemDetail("Bad Credentials", "The username or password is incorrect"));
    }

    @ExceptionHandler(AccountStatusException.class)
    public ResponseEntity<Map<String, Object>> handleAccountStatusException(AccountStatusException exception) {
        return ResponseEntity.status(HttpStatus.LOCKED)
                .body(problemDetail("Account Locked", "The account is locked"));
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Map<String, Object>> handleAccessDeniedException(AccessDeniedException exception) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(problemDetail("Access Denied", "You are not authorized to access this resource"));
    }

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity<Map<String, Object>> handleSignatureException(SignatureException exception) {
        return ResponseEntity.badRequest()
                .body(problemDetail("Invalid JWT Signature", "The JWT signature is invalid"));
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<Map<String, Object>> handleExpiredJwtException(ExpiredJwtException exception) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(problemDetail("Expired JWT Token", "The JWT token has expired"));
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Map<String, Object>> handleUsernameNotFoundException(UsernameNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(problemDetail("User Not Found", "The user does not exist"));
    }

    private Map<String, Object> problemDetail(String title, String detail) {
        Map<String, Object> problem = new HashMap<>();
        problem.put("detail", detail);
        problem.put("title", title);
        return problem;
    }
}