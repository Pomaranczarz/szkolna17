package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
    private UserCrudService userService;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return userService.findByName(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }
}
