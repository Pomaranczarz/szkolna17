package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.dto.GenrePatchRequest;
import com.pomaranczarz.GamesBrowser.dto.GenrePostRequest;
import com.pomaranczarz.GamesBrowser.model.Genre;
import org.springframework.stereotype.Service;

@Service
public class GenreDtoService implements DtoService<Genre> {
    private CrudService<Genre> gameService;

    @Override
    public <U> Genre dtoToEntity(U dto) {
        return switch (dto) {
            case GenrePostRequest genrePostRequest -> postGenreDtoToGenre(genrePostRequest);
            case GenrePatchRequest patchGenreData -> patchGenreDtoToGenre(patchGenreData);
            default -> null;
        };
    }

    private Genre postGenreDtoToGenre(GenrePostRequest genrePostRequest) {
        return Genre.builder()
                .name(genrePostRequest.getName())
                .build();
    }

    private Genre patchGenreDtoToGenre(GenrePatchRequest genrePatchRequest) {
        return Genre.builder()
                .name(genrePatchRequest.getName())
                .build();
    }
}
