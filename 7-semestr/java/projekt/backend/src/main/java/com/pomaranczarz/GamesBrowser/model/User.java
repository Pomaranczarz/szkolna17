package com.pomaranczarz.GamesBrowser.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User implements UserDetails {
    public enum Role implements GrantedAuthority {
        ADMIN,
        USER,
        ;

        @Override
        public String getAuthority() {
            return name();
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    @Size(min = 3, max = 64, message = "Name has to be at least 3 characters long")
    @Column(unique = true)
    private String name;

    @NotBlank
    @Email(message = "Email should be valid")
    @Size(max = 128, message = "Email has to be shorter than 128 characters")
    @Column(unique = true)
    private String email;

    @NotBlank
    @Size(min = 60, max = 60, message = "Password has to be 60 characters long") // bcrypt hash is 60 characters long
    private String password;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_has_games", // Join table
            joinColumns = @JoinColumn(name = "user_id"), // Reference to User
            inverseJoinColumns = @JoinColumn(name = "game_id") // Reference to Game
    )
    private Set<Game> games;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Set.of(role);
    }

    @Override
    public String getUsername() {
        return name;
    }
}
