package com.pomaranczarz.GamesBrowser.response.game;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GameResponseMapper implements ResponseMapper<Game, GameResponse> {
    @Override
    public GameResponse mapFromSingleObject(Game game) {
        return GameResponse.builder()
                .id(game.getId())
                .title(game.getTitle())
                .rating(game.getRating())
                .releaseYear(game.getReleaseYear())
                .description(game.getDescription())
                .build();
    }
}
