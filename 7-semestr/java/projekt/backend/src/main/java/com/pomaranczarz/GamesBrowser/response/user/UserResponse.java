package com.pomaranczarz.GamesBrowser.response.user;

import com.pomaranczarz.GamesBrowser.model.User;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class UserResponse {
    @NotNull
    Long id;

    @NotBlank
    String name;

    @NotBlank
    String email;

    @NotNull
    User.Role role;

    @NotBlank
    private Set<Long> gamesIds;
}
