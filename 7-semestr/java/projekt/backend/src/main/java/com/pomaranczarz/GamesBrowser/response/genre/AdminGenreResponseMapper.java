package com.pomaranczarz.GamesBrowser.response.genre;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AdminGenreResponseMapper implements ResponseMapper<Genre, AdminGenreResponse> {
    @Override
    public AdminGenreResponse mapFromSingleObject(Genre genre) {
        return AdminGenreResponse.builder()
                .id(genre.getId())
                .name(genre.getName())
                .gamesIds(genre.getGames() == null ?
                        Set.of() :
                        genre.getGames().stream().map(Game::getId).collect(Collectors.toSet())
                )
                .build();
    }
}
