package com.pomaranczarz.GamesBrowser.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class GenrePostRequest {
    @NotBlank
    @Size(min = 1, max = 128, message = "Genre name length has to be in range [1; 128]")
    private String name;

    @NotNull
    private Set<Long> gamesIds;
}
