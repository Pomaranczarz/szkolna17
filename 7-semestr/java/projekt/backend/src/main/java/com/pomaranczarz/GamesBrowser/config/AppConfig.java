package com.pomaranczarz.GamesBrowser.config;

import com.pomaranczarz.GamesBrowser.service.CustomUserDetailsService;
import com.pomaranczarz.GamesBrowser.service.PasswordEncoderService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Configuration class for authentication setup.
 */
@Configuration
@AllArgsConstructor
public class AppConfig {
    private final CustomUserDetailsService customUserDetailsService;

    /**
     * Creates instance of {@link PasswordEncoder} used for password encoding in the application.
     *
     * @return instance of {@link PasswordEncoder}
     */
    @Bean
    PasswordEncoder passwordEncoder() {
        return new PasswordEncoderService();
    }

    /**
     * Creates instance of {@link AuthenticationManager} used for authentication.
     *
     * @param config configuration for authentication
     * @return instance of {@link AuthenticationManager}
     */
    @Bean
    AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    /**
     * Creates instance of {@link AuthenticationProvider} used for authentication.
     *
     * @return instance of {@link AuthenticationProvider}
     */
    @Bean
    AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(customUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }
}