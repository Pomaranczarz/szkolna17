package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.dto.GamePatchRequest;
import com.pomaranczarz.GamesBrowser.dto.GamePostRequest;
import com.pomaranczarz.GamesBrowser.model.Game;
import org.springframework.stereotype.Service;

@Service
public class GameDtoService implements DtoService<Game> {
    @Override
    public <U> Game dtoToEntity(U dto) {
        return switch (dto) {
            case GamePostRequest gameDto -> postGameDtoToGame(gameDto);
            case GamePatchRequest gameDto -> patchGameDtoToGame(gameDto);
            default -> null;
        };
    }

    private Game postGameDtoToGame(GamePostRequest gameDto) {
        return Game.builder()
                .title(gameDto.getTitle())
                .description(gameDto.getDescription())
                .releaseYear(gameDto.getReleaseYear())
                .rating(gameDto.getRating())
                .build();
    }

    private Game patchGameDtoToGame(GamePatchRequest gameDto) {
        return Game.builder()
                .title(gameDto.getTitle())
                .description(gameDto.getDescription())
                .releaseYear(gameDto.getReleaseYear())
                .rating(gameDto.getRating())
                .build();
    }
}
