package com.pomaranczarz.GamesBrowser.response;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Interface for mapping objects to responses.
 *
 * @param <T> the type of the object to map
 * @param <U> the type of the response
 */
@Component
public interface ResponseMapper<T, U> {
    /**
     * Maps a single object to a response.
     *
     * @param t the object to map
     * @return the response
     */
    U mapFromSingleObject(T t);

    /**
     * Maps a list of objects to a list of responses.
     *
     * @param ts the list of objects to map
     * @return the list of responses
     */
    default List<U> mapFromList(List<T> ts) {
        return ts.stream()
                .map(this::mapFromSingleObject)
                .toList();
    }
}
