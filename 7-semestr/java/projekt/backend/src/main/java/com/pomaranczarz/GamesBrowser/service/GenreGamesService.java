package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.exception.EntityAlreadyPresentException;
import com.pomaranczarz.GamesBrowser.exception.EntityNotFoundException;
import com.pomaranczarz.GamesBrowser.exception.EntityPersistenceException;
import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GenreGamesService implements OwningRelationService<Genre, Game> {
    private final CrudService<Genre> genreService;
    private final CrudService<Game> gameService;

    /**
     * Returns all games related to the given genre.
     *
     * @param ownerId the id of the genre
     * @return a list of game responses
     */
    @Override
    public List<Game> getOwnedEntities(long ownerId) {
        var genreOpt = genreService.findById(ownerId);
        if (genreOpt.isEmpty())
            throw new EntityNotFoundException("Requested genre [ID: " + ownerId + "] not found");

        var genre = genreOpt.get();

        return genre.getGames().stream().toList();
    }

    /**
     * Adds a game to a genre. Only accessible to users with the ADMIN role.
     *
     * @param ownerId the id of the genre
     * @param ownedId the id of the game to add
     * @return updated genre with added game
     */
    @Override
    public Genre addOwnedEntity(long ownerId, long ownedId) {
        var genreOpt = genreService.findById(ownerId);
        if (genreOpt.isEmpty())
            throw new EntityNotFoundException("Requested genre [ID: " + ownerId + "] not found");

        var gameOpt = gameService.findById(ownedId);
        if (gameOpt.isEmpty())
            throw new EntityNotFoundException("Requested game [ID: " + ownedId + "] not found");

        var genre = genreOpt.get();
        var game = gameOpt.get();

        if (genre.getGames().contains(game))
            throw new EntityAlreadyPresentException(String.format("Game [ID: %d] already present on the genre [ID: %d]", ownedId, ownerId));

        genre.getGames().add(game);
        if (genreService.save(genre).isEmpty())
            throw new EntityPersistenceException("Error saving genre [ID: " + ownerId + "]");

        return genre;
    }

    /**
     * Removes a game from a genre. Only accessible to users with the ADMIN role.
     *
     * @param ownerId the id of the genre
     * @param ownedId the id of the game to remove
     * @return updated genre with removed game
     */
    @Override
    public Genre removeOwnedEntity(long ownerId, long ownedId) {
        var genreOpt = genreService.findById(ownerId);
        if (genreOpt.isEmpty())
            throw new EntityNotFoundException("Requested genre [ID: " + ownerId + "] not found");

        var gameOpt = gameService.findById(ownedId);
        if (gameOpt.isEmpty())
            throw new EntityNotFoundException("Requested game [ID: " + ownedId + "] not found");

        var genre = genreOpt.get();
        var game = gameOpt.get();

        if (!genre.getGames().contains(game))
            throw new EntityNotFoundException(String.format("Game [ID: %d] not present on the genre [ID: %d]", ownedId, ownerId));

        genre.getGames().remove(game);
        if (genreService.save(genre).isEmpty())
            throw new EntityPersistenceException("Error saving genre [ID: " + ownerId + "]");

        return genre;
    }
}