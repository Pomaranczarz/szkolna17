package com.pomaranczarz.GamesBrowser.response.user;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class LoginResponse {
    @NotBlank
    private String token;
    private long expiresIn;
}
