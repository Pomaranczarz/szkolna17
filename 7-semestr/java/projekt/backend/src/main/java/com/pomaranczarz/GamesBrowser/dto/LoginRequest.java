package com.pomaranczarz.GamesBrowser.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class LoginRequest {
    @NotBlank
    @Size(min = 3, max = 64, message = "Name has to be at least 3 characters long")
    private String name;
    @NotBlank
    @Size(min = 6, message = "Password has to be at least 6 characters long")
    private String password;
}
