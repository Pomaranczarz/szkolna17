package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.repository.GameRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GameService implements CrudService<Game> {
    private final GameRepository gameRepository;

    @Override
    public List<Game> findAll() {
        return gameRepository.findAll();
    }

    @Override
    public List<Game> findAllById(Iterable<Long> ids) {
        return gameRepository.findAllById(ids);
    }

    @Override
    public Optional<Game> findById(long id) {
        return gameRepository.findById(id);
    }

    @Override
    public Optional<Game> save(Game game) {
        try {
            return Optional.of(gameRepository.save(game));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public void delete(Game game) {
        gameRepository.delete(game);
    }

    @Override
    public List<Game> saveAll(Iterable<Game> games) {
        return gameRepository.saveAll(games);
    }

    @Override
    public void deleteById(long id) {
        gameRepository.deleteById(id);
    }

    @Override
    public void deleteAllById(Iterable<Long> ids) {
        gameRepository.deleteAllById(ids);
    }

    @Override
    public Optional<Game> upsert(long id, Game game) {
        var gameToUpdateOpt = gameRepository.findById(id);
        if (gameToUpdateOpt.isEmpty())
            return Optional.of(gameRepository.save(game));

        var gameToUpdate = gameToUpdateOpt.get();
        gameToUpdate.setTitle(game.getTitle());
        gameToUpdate.setDescription(game.getDescription());
        gameToUpdate.setRating(game.getRating());
        gameToUpdate.setReleaseYear(game.getReleaseYear());
        gameToUpdate.setUsers(game.getUsers());
        gameToUpdate.setGenres(game.getGenres());

        return Optional.of(gameRepository.save(gameToUpdate));
    }

    @Override
    public void deleteAll(Iterable<? extends Game> games) {
        gameRepository.deleteAll(games);
    }

    @Override
    public Optional<Game> patch(long id, Game game) {
        var gameToUpdateOpt = gameRepository.findById(id);
        if (gameToUpdateOpt.isEmpty())
            return Optional.empty();

        var gameToUpdate = gameToUpdateOpt.get();
        if (game.getTitle() != null) gameToUpdate.setTitle(game.getTitle());
        if (game.getDescription() != null) gameToUpdate.setDescription(game.getDescription());
        if (game.getRating() >= 1 && game.getRating() <= 10) gameToUpdate.setRating(game.getRating());
        if (game.getReleaseYear() >= 1940 && game.getReleaseYear() <= 2100)
            gameToUpdate.setReleaseYear(game.getReleaseYear());
        if (game.getUsers() != null) gameToUpdate.setUsers(game.getUsers());
        if (game.getGenres() != null) gameToUpdate.setGenres(game.getGenres());

        return Optional.of(gameRepository.save(gameToUpdate));
    }
}
