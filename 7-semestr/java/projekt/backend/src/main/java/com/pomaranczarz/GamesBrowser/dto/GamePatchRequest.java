package com.pomaranczarz.GamesBrowser.dto;

import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.validator.constraints.Range;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class GamePatchRequest {
    @Size(min = 1, max = 128, message = "Game title has to have at least 1 and maximum 128 characters")
    private String title;
    @Size(min = 1, max = 512, message = "Game description has to have at least 1 and maximum 512 characters")
    private String description;
    @Range(min = 1940, max = 2100, message = "Release year has to be at least 1940 and at most 2100")
    private int releaseYear;
    @Range(min = 1, max = 10, message = "Rate has to be in range [1; 10]")
    private int rating;
}
