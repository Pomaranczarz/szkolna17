package com.pomaranczarz.GamesBrowser.response.genre;

import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class AdminGenreResponse extends GenreResponse {
    @NotNull
    private Set<Long> gamesIds;
}
