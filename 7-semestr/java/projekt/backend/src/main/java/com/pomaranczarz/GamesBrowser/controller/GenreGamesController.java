package com.pomaranczarz.GamesBrowser.controller;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import com.pomaranczarz.GamesBrowser.response.game.AdminGameResponse;
import com.pomaranczarz.GamesBrowser.response.game.GameResponse;
import com.pomaranczarz.GamesBrowser.response.genre.GenreResponse;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.OwningRelationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/genres/{genreId}/games")
@AllArgsConstructor
public class GenreGamesController {
    private final AuthenticationService authService;
    private final OwningRelationService<Genre, Game> genreGamesService;
    private final ResponseMapper<Game, AdminGameResponse> adminGameResponseMapper;
    private final ResponseMapper<Game, GameResponse> gameResponseMapper;
    private final ResponseMapper<Genre, GenreResponse> genreResponseMapper;

    /**
     * Returns all games related to the given genre.
     *
     * @param genreId the id of the genre
     * @return a list of game responses
     */
    @GetMapping
    public ResponseEntity<List<? extends GameResponse>> getGenreGames(@PathVariable long genreId) {
        return ResponseEntity.ok(
                genreGamesService.getOwnedEntities(genreId).stream().map(
                        this::getGameResponseByUserPrivilege
                ).toList()
        );
    }

    /**
     * Adds a game to the given genre. Only accessible to users with the ADMIN role.
     *
     * @param genreId the id of the genre
     * @param gameId  the id of the game to add
     * @return the genre with the added game
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/{gameId}")
    public ResponseEntity<GenreResponse> addGameToGenre(@PathVariable long genreId, @PathVariable long gameId) {
        return ResponseEntity.ok(
                genreResponseMapper.mapFromSingleObject(
                        genreGamesService.addOwnedEntity(genreId, gameId)
                )
        );
    }

    /**
     * Removes a game from the given genre. Only accessible to users with the ADMIN role.
     *
     * @param genreId the id of the genre
     * @param gameId  the id of the game to remove
     * @return the genre with the removed game
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{gameId}")
    public ResponseEntity<GenreResponse> removeGameFromGenre(@PathVariable long genreId, @PathVariable long gameId) {
        return ResponseEntity.ok(
                genreResponseMapper.mapFromSingleObject(
                        genreGamesService.removeOwnedEntity(genreId, gameId)
                )
        );
    }

    /**
     * Maps a game to a game response, taking into account the current user's role.
     * If the user is an admin, the game is mapped to an admin game response.
     * If the user is a regular user, the game is mapped to a regular game response.
     *
     * @param game the game to map
     * @return the mapped game response
     */
    private GameResponse getGameResponseByUserPrivilege(Game game) {
        return switch (authService.getCurrentUser().getRole()) {
            case ADMIN -> adminGameResponseMapper.mapFromSingleObject(game);
            case USER -> gameResponseMapper.mapFromSingleObject(game);
        };
    }
}
