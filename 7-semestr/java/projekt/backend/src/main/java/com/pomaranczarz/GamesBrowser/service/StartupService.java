package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.model.User;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;

@Component
@AllArgsConstructor
public class StartupService {
    private final UserCrudService userService;
    private final CrudService<Genre> genreService;
    private final CrudService<Game> gameService;

    @PostConstruct
    public void init() {
        addAdminUserIfNotExists();
        addGenres();
        addGames();
    }

    public void addAdminUserIfNotExists() {
        User admin = User.builder()
                .id(1)
                .games(new HashSet<>())
                .role(User.Role.ADMIN)
                .email("admin@admin.net")
                .password("$2a$10$kfAKXbItdVekU7Cv5rELPuerEGQaYYBb8ij/PqJi9ljbvdicAjcPK") // "admin1234"
                .name("admin1234")
                .build();

        if (userService.findById(1).isEmpty())
            userService.save(admin);
    }

    public void addGenres() {
        List<String> genresNames = List.of(
                "Action",
                "Adventure",
                "Puzzle",
                "Simulation",
                "Strategy",
                "Sports",
                "MMO",
                "RPG",
                "Sandbox"
        );

        for (int i = 0; i < genresNames.size(); ++i) {
            if (genreService.findById(i + 1).isEmpty())
                genreService.save(Genre.builder()
                        .id(i + 1)
                        .name(genresNames.get(i))
                        .games(new HashSet<>())
                        .build()
                );
        }
    }

    public void addGames() {
        List<Game> games = List.of(
                Game.builder()
                        .id(1)
                        .title("Counter-Strike: Global Offensive")
                        .description("Counter-Strike: Global Offensive is a multiplayer first person shooter game developed by Valve and published by Valve. It was released in 2012.")
                        .genres(new HashSet<>(List.of(genreService.findById(1).orElse(null))))
                        .rating(9)
                        .releaseYear(2012)
                        .build(),
                Game.builder()
                        .id(2)
                        .title("Grand Theft Auto V")
                        .description("Grand Theft Auto V is a 2013 action-adventure game developed by Rockstar North and published by Rockstar Games.")
                        .genres(new HashSet<>(List.of(
                                genreService.findById(1).orElse(null),
                                genreService.findById(4).orElse(null)
                        )))
                        .rating(9)
                        .releaseYear(2013)
                        .build(),
                Game.builder()
                        .id(3)
                        .title("Grand Theft Auto IV")
                        .description("Grand Theft Auto IV is a 2008 action-adventure game developed by Rockstar North and published by Rockstar Games.")
                        .genres(new HashSet<>(List.of(
                                genreService.findById(1).get(),
                                genreService.findById(4).get()
                        )))
                        .rating(9)
                        .releaseYear(2008)
                        .build(),
                Game.builder()
                        .id(4)
                        .title("Half-Life")
                        .description("Half-Life is a 1998 first-person shooter game developed by Valve Corporation")
                        .genres(new HashSet<>(List.of(
                                genreService.findById(1).get(),
                                genreService.findById(2).get()
                        )))
                        .rating(10)
                        .releaseYear(1998)
                        .build()
        );

        for (var game : games) {
            if (gameService.findById(game.getId()).isEmpty())
                gameService.save(game);
        }
    }
}
