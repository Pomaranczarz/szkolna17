package com.pomaranczarz.GamesBrowser.controller;

import com.pomaranczarz.GamesBrowser.dto.GamePatchRequest;
import com.pomaranczarz.GamesBrowser.dto.GamePostRequest;
import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import com.pomaranczarz.GamesBrowser.response.game.AdminGameResponse;
import com.pomaranczarz.GamesBrowser.response.game.GameResponse;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.CrudService;
import com.pomaranczarz.GamesBrowser.service.DtoService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/games")
public class GameController {
    private final CrudService<Game> gameService;
    private final DtoService<Game> gameDtoService;
    private final AuthenticationService authService;
    private final ResponseMapper<Game, GameResponse> responseMapper;
    private final ResponseMapper<Game, AdminGameResponse> adminResponseMapper;

    /**
     * Retrieves a list of all games.
     *
     * @return a list of game responses
     */
    @GetMapping
    public ResponseEntity<List<? extends GameResponse>> getAllGames() {
        return ResponseEntity.ok(
                gameService.findAll().stream()
                        .map(this::getResponseByUserPrivilege)
                        .toList()
        );
    }

    /**
     * Retrieves a game by its id.
     *
     * @param id the id of the game
     * @return the game with the given id or 404 if not found
     */
    @GetMapping("/{id}")
    public ResponseEntity<? extends GameResponse> getGameById(@PathVariable long id) {
        return gameService.findById(id)
                .map(this::getResponseByUserPrivilege)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Creates a new game.
     *
     * @param game the game to create
     * @return the created game
     */
    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<? extends GameResponse> createGame(@Valid @RequestBody GamePostRequest game) {
        return gameService.save(gameDtoService.dtoToEntity(game))
                .map(this::getResponseByUserPrivilege)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Updates an existing game.
     *
     * @param id      the id of the game
     * @param gameDto the game to update
     * @return the updated game
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<? extends GameResponse> upsertGame(@PathVariable long id, @Validated @RequestBody GamePostRequest gameDto) {
        return gameService.upsert(id, gameDtoService.dtoToEntity(gameDto))
                .map(this::getResponseByUserPrivilege)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Patches an existing game.
     *
     * @param id      the id of the game
     * @param gameDto the game to patch
     * @return the patched game
     */
    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<? extends GameResponse> patchGame(@PathVariable long id, @Validated @RequestBody GamePatchRequest gameDto) {
        return gameService.patch(id, gameDtoService.dtoToEntity(gameDto))
                .map(this::getResponseByUserPrivilege)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Deletes a game.
     *
     * @param id the id of the game
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteGame(@PathVariable long id) {
        gameService.deleteById(id);
    }

    /**
     * Maps a game to a game response, taking into account the current user's role.
     *
     * @param game the game to map
     * @return the mapped game response
     */
    private GameResponse getResponseByUserPrivilege(Game game) {
        return switch (authService.getCurrentUser().getRole()) {
            case ADMIN -> adminResponseMapper.mapFromSingleObject(game);
            case USER -> responseMapper.mapFromSingleObject(game);
        };
    }
}
