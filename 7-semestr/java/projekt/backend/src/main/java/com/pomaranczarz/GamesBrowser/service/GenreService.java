package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.repository.GenreRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GenreService implements CrudService<Genre> {
    private final GenreRepository genreRepository;

    @Override
    public List<Genre> findAll() {
        return genreRepository.findAll();
    }

    @Override
    public Optional<Genre> findById(long id) {
        return genreRepository.findById(id);
    }

    @Override
    public List<Genre> findAllById(Iterable<Long> ids) {
        return genreRepository.findAllById(ids);
    }

    @Override
    public Optional<Genre> save(Genre genre) {
        try {
            return Optional.of(genreRepository.save(genre));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Genre> saveAll(Iterable<Genre> genres) {
        return genreRepository.saveAll(genres);
    }

    @Override
    public void delete(Genre genre) {
        genreRepository.delete(genre);
    }

    @Override
    public void deleteById(long id) {
        genreRepository.deleteById(id);
    }

    @Override
    public void deleteAllById(Iterable<Long> ids) {
        genreRepository.deleteAllById(ids);
    }

    @Override
    public void deleteAll(Iterable<? extends Genre> genres) {
        genreRepository.deleteAll(genres);
    }

    @Override
    public Optional<Genre> upsert(long id, Genre genre) {
        var genreToUpdateOpt = genreRepository.findById(id);
        if (genreToUpdateOpt.isEmpty())
            return Optional.of(genreRepository.save(genre));

        var genreToUpdate = genreToUpdateOpt.get();
        genreToUpdate.setName(genre.getName());
        genreToUpdate.setGames(genre.getGames());

        return Optional.of(genreRepository.save(genreToUpdate));
    }

    @Override
    public Optional<Genre> patch(long id, Genre genre) {
        var genreToPatchOpt = genreRepository.findById(id);
        if (genreToPatchOpt.isEmpty())
            return Optional.empty();

        var genreToPatch = genreToPatchOpt.get();
        if (genre.getName() != null) genreToPatch.setName(genre.getName());
        if (genre.getGames() != null) genreToPatch.setGames(genre.getGames());

        return Optional.of(genreRepository.save(genreToPatch));
    }
}
