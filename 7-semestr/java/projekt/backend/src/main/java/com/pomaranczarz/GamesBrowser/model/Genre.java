package com.pomaranczarz.GamesBrowser.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.Set;

@Entity
@Table(name = "genres")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    @Size(min = 1, max = 128, message = "Genre name length has to be in range [1; 128]")
    @Column(unique = true)
    private String name;

    @ManyToMany(mappedBy = "genres") // Inverse side
    private Set<Game> games;
}
