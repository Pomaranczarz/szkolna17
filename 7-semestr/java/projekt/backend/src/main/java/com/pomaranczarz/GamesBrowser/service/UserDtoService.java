package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.dto.LoginRequest;
import com.pomaranczarz.GamesBrowser.dto.UserPatchRequest;
import com.pomaranczarz.GamesBrowser.dto.UserPostRequest;
import com.pomaranczarz.GamesBrowser.dto.UserRegisterRequest;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.repository.GameRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserDtoService implements DtoService<User> {
    private final GameRepository gameRepository;

    @Override
    public <U> User dtoToEntity(U dto) {
        return switch (dto) {
            case UserPatchRequest userDtoPatch -> patchUserDtoToUser(userDtoPatch);
            case UserRegisterRequest userDtoRegister -> registerUserDtoToUser(userDtoRegister);
            case LoginRequest userDtoLogin -> loginUserDtoToUser(userDtoLogin);
            case UserPostRequest userDtoPostPut -> userDetailsResponseToUser(userDtoPostPut);
            case null, default -> throw new IllegalArgumentException("Invalid user dto type");
        };
    }

    private User patchUserDtoToUser(UserPatchRequest userDto) {
        return User.builder()
                .name(userDto.getName())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .role(userDto.getRole())
                .build();
    }

    private User userDetailsResponseToUser(UserPostRequest userDto) {
        return User.builder()
                .name(userDto.getName())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .role(User.Role.USER)
                .build();
    }

    private User registerUserDtoToUser(UserRegisterRequest userDto) {
        return User.builder()
                .name(userDto.getName())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .role(User.Role.USER)
                .build();
    }

    private User loginUserDtoToUser(LoginRequest userDto) {
        return User.builder()
                .name(userDto.getName())
                .password(userDto.getPassword())
                .build();
    }
}
