package com.pomaranczarz.GamesBrowser.service;

import java.util.List;

public interface OwningRelationService<T, U> {
    List<U> getOwnedEntities(long ownerId);

    T addOwnedEntity(long ownerId, long ownedId);

    T removeOwnedEntity(long ownerId, long ownedId);
}
