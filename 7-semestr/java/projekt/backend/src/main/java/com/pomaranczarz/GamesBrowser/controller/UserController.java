package com.pomaranczarz.GamesBrowser.controller;

import com.pomaranczarz.GamesBrowser.dto.UserPatchRequest;
import com.pomaranczarz.GamesBrowser.dto.UserPostRequest;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import com.pomaranczarz.GamesBrowser.response.user.UserResponse;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.DtoService;
import com.pomaranczarz.GamesBrowser.service.UserCrudService;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@AllArgsConstructor
public class UserController {
    private final UserCrudService userService;
    private final DtoService<User> userDtoService;
    private final AuthenticationService authService;
    private final ResponseMapper<User, UserResponse> responseMapper;

    /**
     * Gets all users in the database.
     *
     * @return A list of {@link UserResponse}s.
     */
    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<UserResponse>> getAllUsers() {
        return ResponseEntity.ok(
                responseMapper.mapFromList(userService.findAll())
        );
    }

    /**
     * Gets the currently authenticated user.
     *
     * @return A {@link UserResponse}.
     */
    @GetMapping("/me")
    public ResponseEntity<UserResponse> authenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        var currentUser = (User) auth.getPrincipal();

        return ResponseEntity.ok(responseMapper.mapFromSingleObject(currentUser));
    }

    /**
     * Gets a user by its id.
     *
     * @param id The user's id.
     * @return A {@link UserResponse}.
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserResponse> getUserById(@PathVariable long id) {
        return userService.findById(id)
                .map(responseMapper::mapFromSingleObject)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Creates a new user.
     *
     * @param userDto The new user's details.
     * @return A {@link UserResponse}.
     */
    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @Transactional
    public ResponseEntity<UserResponse> createUser(@Valid @RequestBody UserPostRequest userDto) {
        userDto.setPassword( // create a password hash first
                authService.getPasswordEncoder().encode(userDto.getPassword())
        );

        return userService.save(userDtoService.dtoToEntity(userDto))
                .map(responseMapper::mapFromSingleObject)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Upserts a user.
     *
     * @param id        The user's id.
     * @param upsertDto The new user details.
     * @return A {@link UserResponse}.
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserResponse> upsertUser(@PathVariable long id, @Valid @RequestBody UserPostRequest upsertDto) {
        // create a password hash first
        upsertDto.setPassword(
                authService.getPasswordEncoder().encode(upsertDto.getPassword())
        );

        return userService.upsert(id, userDtoService.dtoToEntity(upsertDto))
                .map(responseMapper::mapFromSingleObject)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Patches a user.
     *
     * @param id       The user's id.
     * @param patchDto The patch.
     * @return A {@link UserResponse}.
     */
    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserResponse> patchUser(@PathVariable long id, @RequestBody UserPatchRequest patchDto) {
        // create a password hash if present
        if (patchDto.getPassword() != null) {
            patchDto.setPassword(
                    authService.getPasswordEncoder().encode(patchDto.getPassword())
            );
        }

        return userService.patch(id, userDtoService.dtoToEntity(patchDto))
                .map(responseMapper::mapFromSingleObject)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Deletes a user.
     *
     * @param id The user's id.
     * @return A success message.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable long id) {
        authService.authorizeOwnerOrAdmin(id);
        userService.deleteById(id);

        return ResponseEntity.ok("User deleted");
    }
}
