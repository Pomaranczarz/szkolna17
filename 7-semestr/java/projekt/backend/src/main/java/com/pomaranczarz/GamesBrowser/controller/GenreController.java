package com.pomaranczarz.GamesBrowser.controller;

import com.pomaranczarz.GamesBrowser.dto.GenrePatchRequest;
import com.pomaranczarz.GamesBrowser.dto.GenrePostRequest;
import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import com.pomaranczarz.GamesBrowser.response.genre.AdminGenreResponse;
import com.pomaranczarz.GamesBrowser.response.genre.GenreResponse;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.CrudService;
import com.pomaranczarz.GamesBrowser.service.DtoService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/genres")
@AllArgsConstructor
public class GenreController {
    private final AuthenticationService authService;
    private final CrudService<Genre> genreService;
    private final DtoService<Genre> genreDtoService;
    private final ResponseMapper<Genre, GenreResponse> responseMapper;
    private final ResponseMapper<Genre, AdminGenreResponse> adminResponseMapper;

    /**
     * Returns a list of all genres.
     *
     * @return a list of all genres
     */
    @GetMapping
    public ResponseEntity<List<? extends GenreResponse>> getAllGenres() {
        return ResponseEntity.ok(
                genreService.findAll().stream()
                        .map(this::getResponseByUserPrivilege)
                        .toList()
        );
    }

    /**
     * Returns the genre with the given id.
     *
     * @param id the id of the genre
     * @return the genre with the given id
     */
    @GetMapping("/{id}")
    public ResponseEntity<? extends GenreResponse> getGenreById(@PathVariable long id) {
        return genreService.findById(id)
                .map(this::getResponseByUserPrivilege)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Creates a genre.
     *
     * @param genre the genre to create
     * @return the created genre
     */
    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<? extends GenreResponse> createGenre(@Valid @RequestBody GenrePostRequest genre) {
        return genreService.save(genreDtoService.dtoToEntity(genre))
                .map(this::getResponseByUserPrivilege)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Upserts a genre.
     *
     * @param id    the id of the genre
     * @param genre the genre to upsert
     * @return the upserted genre
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<? extends GenreResponse> upsertGenre(@PathVariable long id, @Valid @RequestBody GenrePostRequest genre) {
        return genreService.upsert(id, genreDtoService.dtoToEntity(genre))
                .map(this::getResponseByUserPrivilege)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());

    }

    /**
     * Patches a genre.
     *
     * @param id    the id of the genre
     * @param genre the genre to patch
     * @return the patched genre
     */
    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<? extends GenreResponse> patchGenre(@PathVariable long id, @RequestBody GenrePatchRequest genre) {
        return genreService.patch(id, genreDtoService.dtoToEntity(genre))
                .map(this::getResponseByUserPrivilege)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Deletes a genre.
     *
     * @param id the id of the genre
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteGenre(@PathVariable long id) {
        genreService.deleteById(id);
    }

    /**
     * Returns the response by user privilege.
     *
     * @param genre the genre
     * @return the response by user privilege
     */
    private GenreResponse getResponseByUserPrivilege(Genre genre) {
        return switch (authService.getCurrentUser().getRole()) {
            case ADMIN -> adminResponseMapper.mapFromSingleObject(genre);
            case USER -> responseMapper.mapFromSingleObject(genre);
        };
    }
}
