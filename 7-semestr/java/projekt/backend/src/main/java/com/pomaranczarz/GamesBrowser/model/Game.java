package com.pomaranczarz.GamesBrowser.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.validator.constraints.Range;

import java.util.Set;

@Entity
@Table(name = "games")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    @Size(min = 1, max = 128, message = "Game title has to have at least 1 and maximum 128 characters")
    @Column(unique = true)
    private String title;

    @NotBlank
    @Size(min = 1, max = 512, message = "Game description has to have at least 1 and maximum 512 characters")
    private String description;

    @NotNull
    @Range(min = 1940, max = 2100, message = "Release year has to be at least 1940 and at most 2100")
    private int releaseYear;

    @NotNull
    @Range(min = 1, max = 10, message = "Rate has to be in range [1; 10]")
    private int rating;

    @ManyToMany(mappedBy = "games") // Inverse side
    private Set<User> users;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "games_has_genres", // Join table
            joinColumns = @JoinColumn(name = "game_id"), // Reference to Game
            inverseJoinColumns = @JoinColumn(name = "genre_id") // Reference to Genre
    )
    private Set<Genre> genres;
}
