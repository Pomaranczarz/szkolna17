package com.pomaranczarz.GamesBrowser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamesBrowserApplication {

    public static void main(String[] args) {
        SpringApplication.run(GamesBrowserApplication.class, args);
    }
}
