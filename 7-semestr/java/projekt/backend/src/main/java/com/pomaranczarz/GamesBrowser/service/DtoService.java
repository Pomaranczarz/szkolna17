package com.pomaranczarz.GamesBrowser.service;

public interface DtoService<T> {
    <U> T dtoToEntity(U dto);
}
