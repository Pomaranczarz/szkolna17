package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.dto.LoginRequest;
import com.pomaranczarz.GamesBrowser.dto.UserRegisterRequest;
import com.pomaranczarz.GamesBrowser.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

public interface AuthenticationService {
    Optional<User> authenticate(LoginRequest input);

    Optional<User> register(UserRegisterRequest input);

    User getCurrentUser();

    User authorizeOwnerOrAdmin(long id);

    PasswordEncoder getPasswordEncoder();
}
