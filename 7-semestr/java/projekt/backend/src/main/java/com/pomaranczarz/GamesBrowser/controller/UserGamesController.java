package com.pomaranczarz.GamesBrowser.controller;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import com.pomaranczarz.GamesBrowser.response.game.AdminGameResponse;
import com.pomaranczarz.GamesBrowser.response.game.GameResponse;
import com.pomaranczarz.GamesBrowser.response.user.UserResponse;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.OwningRelationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users/")
@AllArgsConstructor
public class UserGamesController {
    private final OwningRelationService<User, Game> userGamesService;
    private final AuthenticationService authService;
    private final ResponseMapper<User, UserResponse> userResponseMapper;
    private final ResponseMapper<Game, GameResponse> gameResponseMapper;
    private final ResponseMapper<Game, AdminGameResponse> adminGameResponseMapper;

    /**
     * @param id the id of the user
     * @return the games owned by the user
     */
    @GetMapping("{id}/games")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<AdminGameResponse>> getUserGames(@PathVariable long id) {
        return ResponseEntity.ok(
                adminGameResponseMapper.mapFromList(
                        userGamesService.getOwnedEntities(id)
                )
        );
    }

    /**
     * @return the games owned by the current user
     */
    @GetMapping("me/games")
    public ResponseEntity<List<GameResponse>> getMyGames() {
        var currentUser = authService.getCurrentUser();

        return ResponseEntity.ok(
                gameResponseMapper.mapFromList(
                        userGamesService.getOwnedEntities(currentUser.getId())
                )
        );
    }

    /**
     * Add a game to the current user
     *
     * @param gameId the id of the game to add
     * @return the current user with the added game
     */
    @PostMapping("me/games/{gameId}")
    public ResponseEntity<UserResponse> addGameToMyGames(@PathVariable long gameId) {
        var currentUser = authService.getCurrentUser();

        return ResponseEntity.ok(
                userResponseMapper.mapFromSingleObject(
                        userGamesService.addOwnedEntity(currentUser.getId(), gameId)
                )
        );
    }

    /**
     * Remove a game from the current user
     *
     * @param gameId the id of the game to remove
     * @return the current user with the removed game
     */
    @DeleteMapping("me/games/{gameId}")
    public ResponseEntity<UserResponse> removeGameFromMyGames(@PathVariable long gameId) {
        var currentUser = authService.getCurrentUser();

        return ResponseEntity.ok(
                userResponseMapper.mapFromSingleObject(
                        userGamesService.removeOwnedEntity(currentUser.getId(), gameId)
                )
        );
    }

    /**
     * Add a game to a user
     *
     * @param userId the id of the user to add the game to
     * @param gameId the id of the game to add
     * @return the user with the added game
     */
    @PostMapping("{userId}/games/{gameId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserResponse> addGameToUser(@PathVariable long userId, @PathVariable long gameId) {
        return ResponseEntity.ok(
                userResponseMapper.mapFromSingleObject(
                        userGamesService.addOwnedEntity(userId, gameId)
                )
        );
    }

    /**
     * Remove a game from a user
     *
     * @param userId the id of the user to remove the game from
     * @param gameId the id of the game to remove
     * @return the user with the removed game
     */
    @DeleteMapping("{userId}/games/{gameId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserResponse> removeGameFromUser(@PathVariable long userId, @PathVariable long gameId) {
        return ResponseEntity.ok(
                userResponseMapper.mapFromSingleObject(
                        userGamesService.removeOwnedEntity(userId, gameId)
                )
        );
    }
}
