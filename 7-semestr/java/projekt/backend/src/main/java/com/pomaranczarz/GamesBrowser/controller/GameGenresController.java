package com.pomaranczarz.GamesBrowser.controller;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import com.pomaranczarz.GamesBrowser.response.game.AdminGameResponse;
import com.pomaranczarz.GamesBrowser.response.genre.AdminGenreResponse;
import com.pomaranczarz.GamesBrowser.response.genre.GenreResponse;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.OwningRelationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/games/{gameId}/genres")
@AllArgsConstructor
public class GameGenresController {
    private final AuthenticationService authService;
    private final OwningRelationService<Game, Genre> gameGenresService;
    private final ResponseMapper<Game, AdminGameResponse> adminGameResponseMapper;
    private final ResponseMapper<Genre, GenreResponse> genreResponseMapper;
    private final ResponseMapper<Genre, AdminGenreResponse> adminGenreResponseMapper;

    /**
     * Retrieves all genres of a game.
     *
     * @param gameId id of the game to get genres from
     * @return all genres of the game
     */
    @GetMapping
    public ResponseEntity<List<? extends GenreResponse>> getGameGenres(@PathVariable long gameId) {
        return ResponseEntity.ok(
                gameGenresService.getOwnedEntities(gameId).stream().map(
                        this::getGenreResponseByUserPrivilege
                ).toList()
        );
    }

    /**
     * Adds a genre to a game. Only accessible to users with the ADMIN role.
     *
     * @param gameId  id of the game to add genre to
     * @param genreId id of the genre to add
     * @return updated game with added genre
     */
    @PostMapping("/{genreId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AdminGameResponse> addGenreToGame(@PathVariable long gameId, @PathVariable long genreId) {
        return ResponseEntity.ok(
                adminGameResponseMapper.mapFromSingleObject(
                        gameGenresService.addOwnedEntity(gameId, genreId)
                )
        );
    }

    /**
     * Removes a genre from a game. Only accessible to users with the ADMIN role.
     *
     * @param gameId  id of the game to remove genre from
     * @param genreId id of the genre to remove
     * @return updated game with removed genre
     */
    @DeleteMapping("/{genreId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AdminGameResponse> removeGenreFromGame(@PathVariable long gameId, @PathVariable long genreId) {
        return ResponseEntity.ok(
                adminGameResponseMapper.mapFromSingleObject(
                        gameGenresService.removeOwnedEntity(gameId, genreId)
                )
        );
    }

    /**
     * Maps a genre to a genre response, taking into account the current user's role.
     * If the user is an admin, the genre is mapped to an admin genre response.
     * If the user is a regular user, the genre is mapped to a regular genre response.
     *
     * @param genre genre to map to response
     * @return response based on the user's role
     */
    private GenreResponse getGenreResponseByUserPrivilege(Genre genre) {
        return switch (authService.getCurrentUser().getRole()) {
            case ADMIN -> adminGenreResponseMapper.mapFromSingleObject(genre);
            case USER -> genreResponseMapper.mapFromSingleObject(genre);
        };
    }
}
