package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.model.User;

import java.util.Optional;

public interface UserCrudService extends CrudService<User> {
    Optional<User> findByName(String name);
}
