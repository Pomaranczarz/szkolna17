package com.pomaranczarz.GamesBrowser.service;

import java.util.List;
import java.util.Optional;

public interface CrudService<T> {
    List<T> findAll();

    Optional<T> findById(long id);

    List<T> findAllById(Iterable<Long> ids);

    Optional<T> save(T t);

    List<T> saveAll(Iterable<T> t);

    void delete(T t);

    void deleteById(long id);

    void deleteAll(Iterable<? extends T> t);

    void deleteAllById(Iterable<Long> ids);

    Optional<T> upsert(long id, T t);

    Optional<T> patch(long id, T t);
}
