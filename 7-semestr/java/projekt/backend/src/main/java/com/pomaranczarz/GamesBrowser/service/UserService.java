package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService implements UserCrudService {
    private final UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findById(long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAllById(Iterable<Long> ids) {
        return userRepository.findAllById(ids);
    }

    public Optional<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    @Override
    public Optional<User> save(User user) {
        try {
            return Optional.of(userRepository.save(user));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public List<User> saveAll(Iterable<User> users) {
        return userRepository.saveAll(users);
    }

    @Override
    public void deleteById(long id) {
        userRepository.deleteById(id);
    }


    @Override
    public void deleteAllById(Iterable<Long> ids) {
        userRepository.deleteAllByIdInBatch(ids);
    }

    @Override
    public void deleteAll(Iterable<? extends User> users) {
        userRepository.deleteAll(users);
    }

    @Override
    public Optional<User> upsert(long id, User user) {
        var userToUpdateOpt = userRepository.findById(id);
        if (userToUpdateOpt.isEmpty())
            return Optional.of(userRepository.save(user));

        var userToUpdate = userToUpdateOpt.get();
        userToUpdate.setName(user.getName());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setRole(user.getRole());
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setGames(user.getGames());

        return Optional.of(userRepository.save(userToUpdate));
    }

    @Override
    public Optional<User> patch(long id, User user) {
        var userToPatchOpt = userRepository.findById(id);
        if (userToPatchOpt.isEmpty())
            return Optional.empty();

        var userToPatch = userToPatchOpt.get();
        if (user.getRole() != null) userToPatch.setRole(user.getRole());
        if (user.getEmail() != null) userToPatch.setEmail(user.getEmail());
        if (user.getPassword() != null) userToPatch.setPassword(user.getPassword());
        if (user.getName() != null) userToPatch.setName(user.getName());
        if (user.getGames() != null) userToPatch.setGames(user.getGames());

        return Optional.of(userRepository.save(userToPatch));
    }
}
