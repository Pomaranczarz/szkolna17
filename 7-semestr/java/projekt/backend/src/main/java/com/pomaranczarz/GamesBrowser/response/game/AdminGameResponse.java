package com.pomaranczarz.GamesBrowser.response.game;

import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@SuperBuilder
public class AdminGameResponse extends GameResponse {
    @NotNull
    private Set<Long> genresIds;
    @NotNull
    private Set<Long> usersIds;
}
