package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.dto.LoginRequest;
import com.pomaranczarz.GamesBrowser.dto.UserRegisterRequest;
import com.pomaranczarz.GamesBrowser.exception.EntityNotFoundException;
import com.pomaranczarz.GamesBrowser.exception.InvalidRegistrationDataException;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class AuthService implements AuthenticationService {
    private final UserRepository userRepository;
    @Getter
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;

    /**
     * Authenticates a user by checking the login credentials. If the credentials are valid, the method returns the
     * authenticated user. If the credentials are invalid, the method throws a BadCredentialsException.
     *
     * @param input the user's login credentials
     * @return the authenticated user or an empty Optional if the user credentials are invalid
     * @see BadCredentialsException
     */
    @Override
    public Optional<User> authenticate(LoginRequest input) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(input.getName(), input.getPassword())
            );

            return userRepository.findByName(input.getName());
        } catch (Exception e) {
            throw new BadCredentialsException("");
        }
    }

    /**
     * Registers a new user in the system. If the user's registration data is invalid, the method throws an
     * InvalidRegistrationDataException. If the user's registration data is valid, the method returns the newly
     * registered user.
     *
     * @param input the user's registration data
     * @return the newly registered user or an empty Optional if the user's registration data is invalid
     * @see InvalidRegistrationDataException
     */
    @Override
    public Optional<User> register(UserRegisterRequest input) {
        try {
            var user = new User();
            user.setName(input.getName());
            user.setPassword(passwordEncoder.encode(input.getPassword()));
            user.setEmail(input.getEmail());
            user.setRole(User.Role.USER);

            return Optional.of(userRepository.save(user));
        } catch (Exception e) {
            throw new InvalidRegistrationDataException();
        }
    }

    /**
     * Gets the currently authenticated user. If the user is not authenticated, the method throws an
     * AccessDeniedException.
     *
     * @return the currently authenticated user
     * @see AccessDeniedException
     */
    @Override
    public User getCurrentUser() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        var principal = authentication.getPrincipal();
        if (principal instanceof User user) {
            return user;
        }
        throw new AccessDeniedException("User is not authenticated");
    }

    /**
     * Authorizes the user to perform an action on a specific resource. If the user is not the owner of the resource
     * or does not have the admin role, the method throws an AccessDeniedException.
     *
     * @param id the ID of the resource to be accessed
     * @return the user that is authorized to access the resource
     * @see AccessDeniedException
     */
    @Override
    public User authorizeOwnerOrAdmin(long id) {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        var authenticatedUser = (User) authentication.getPrincipal();

        var requestUserOpt = userRepository.findById(id);
        if (requestUserOpt.isEmpty())
            throw new EntityNotFoundException("Requested user [ID: " + id + "] not found");

        var requestUser = requestUserOpt.get();
        if (authenticatedUser.getId() != requestUser.getId() && !authenticatedUser.getRole().equals(User.Role.ADMIN))
            throw new AccessDeniedException("Access denied");

        return requestUser;
    }
}
