package com.pomaranczarz.GamesBrowser.dto;

import com.pomaranczarz.GamesBrowser.model.User;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class UserPatchRequest {
    @Size(min = 3, max = 64, message = "Name has to be at least 3 characters long")
    private String name;
    @Email(message = "Email should be valid")
    private String email;
    @Size(min = 6, message = "Password has to be at least 6 characters long")
    private String password;
    private User.Role role;
}
