package com.pomaranczarz.GamesBrowser.service;

import com.pomaranczarz.GamesBrowser.exception.EntityAlreadyPresentException;
import com.pomaranczarz.GamesBrowser.exception.EntityNotFoundException;
import com.pomaranczarz.GamesBrowser.exception.EntityPersistenceException;
import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserGamesService implements OwningRelationService<User, Game> {
    private final UserCrudService userService;
    private final CrudService<Game> gameService;
    private final AuthenticationService authService;

    /**
     * Gets all games owned by the user with the given id.
     *
     * @param userId the id of the user
     * @return list of games owned by the user
     */
    @Override
    public List<Game> getOwnedEntities(long userId) {
        var user = authService.authorizeOwnerOrAdmin(userId);

        return user.getGames().stream().toList();
    }

    /**
     * Adds a game to the list of games owned by the user with the given id.
     *
     * @param userId the id of the user
     * @param gameId the id of the game to add
     * @return the user with the added game
     */
    @Override
    public User addOwnedEntity(long userId, long gameId) {
        var userOpt = userService.findById(userId);
        if (userOpt.isEmpty())
            throw new EntityNotFoundException("Requested user [ID: " + userId + "] not found");
        var user = userOpt.get();

        var gameOpt = gameService.findById(gameId);
        if (gameOpt.isEmpty())
            throw new EntityNotFoundException("Requested game [ID: " + gameId + "] not found");
        var game = gameOpt.get();

        if (user.getGames().contains(game))
            throw new EntityAlreadyPresentException(String.format("Game [ID: %d] already present on the user [ID: %d]", gameId, userId));

        user.getGames().add(game);
        game.getUsers().add(user);

        if (userService.save(user).isEmpty())
            throw new EntityPersistenceException("Error saving user [ID: " + userId + "]");
        if (gameService.save(game).isEmpty())
            throw new EntityPersistenceException("Error saving game [ID: " + gameId + " ]");

        return user;
    }

    /**
     * Removes a game from the list of games owned by the user with the given id.
     *
     * @param userId the id of the user
     * @param gameId the id of the game to remove
     * @return the user with the removed game
     */
    @Override
    public User removeOwnedEntity(long userId, long gameId) {
        var userOpt = userService.findById(userId);
        if (userOpt.isEmpty())
            throw new EntityNotFoundException("Requested user [ID: " + userId + "] not found");
        var user = userOpt.get();

        var gameOpt = gameService.findById(gameId);
        if (gameOpt.isEmpty())
            throw new EntityNotFoundException("Requested game [ID: " + gameId + "] not found");
        var game = gameOpt.get();

        if (!user.getGames().contains(game))
            throw new EntityNotFoundException(String.format("Game [ID: %d] not present on the user [ID: %d]", gameId, userId));

        user.getGames().remove(game);
        game.getUsers().remove(user);

        if (userService.save(user).isEmpty())
            throw new EntityPersistenceException("Error saving user [ID: " + userId + "]");
        if (gameService.save(game).isEmpty())
            throw new EntityPersistenceException("Error saving game [ID: " + gameId + " ]");

        return user;
    }
}
