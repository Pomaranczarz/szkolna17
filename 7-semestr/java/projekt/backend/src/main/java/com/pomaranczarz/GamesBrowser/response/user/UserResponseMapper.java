package com.pomaranczarz.GamesBrowser.response.user;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class UserResponseMapper implements ResponseMapper<User, UserResponse> {
    @Override
    public UserResponse mapFromSingleObject(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .role(user.getRole())
                .gamesIds(user.getGames() == null ?
                        Set.of() :
                        user.getGames().stream().map(Game::getId).collect(Collectors.toSet())
                )
                .build();
    }
}
