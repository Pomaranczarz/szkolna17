package com.pomaranczarz.GamesBrowser.controller;

import com.pomaranczarz.GamesBrowser.dto.LoginRequest;
import com.pomaranczarz.GamesBrowser.dto.UserRegisterRequest;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.response.ResponseMapper;
import com.pomaranczarz.GamesBrowser.response.user.LoginResponse;
import com.pomaranczarz.GamesBrowser.response.user.UserResponse;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.JwtService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthenticationController {
    private final JwtService jwtService;
    private final AuthenticationService authService;
    private final ResponseMapper<User, UserResponse> responseMapper;

    /**
     * Authenticates a user and returns a JWT token if successful.
     * <p>
     * This method validates the user's login credentials and generates a JWT token upon successful authentication.
     *
     * @param input the user's login credentials
     * @return a response containing the JWT token or an unauthorized status
     */
    @PostMapping("/login")
    public ResponseEntity<LoginResponse> authenticate(@Valid @RequestBody LoginRequest input) {
        return authService.authenticate(input)
                .map(user -> {
                    var token = jwtService.generateToken(user);
                    return ResponseEntity.ok(new LoginResponse(token, jwtService.getExpirationTime()));
                })
                .orElse(ResponseEntity.status(401).build());
    }

    /**
     * Registers a new user in the system.
     * <p>
     * This method handles the user registration process and returns the created user's details upon successful registration.
     *
     * @param input the user's registration data
     * @return a response containing the newly created user or a bad request status
     */
    @PostMapping("/register")
    public ResponseEntity<UserResponse> register(@Valid @RequestBody UserRegisterRequest input) {
        return authService.register(input)
                .map(responseMapper::mapFromSingleObject)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }
}
