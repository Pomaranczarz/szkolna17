package com.pomaranczarz.GamesBrowser.integration;

import com.pomaranczarz.GamesBrowser.exception.EntityNotFoundException;
import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.UserGamesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserGamesControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserGamesService userGamesService;

    @MockBean
    private AuthenticationService authService;

    private final Game game = Game.builder()
            .id(1)
            .title("Game 1")
            .description("Description 1")
            .releaseYear(2010)
            .rating(8)
            .build();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @WithMockUser
    void correctResponseFormatIsReturned() throws Exception {
        when(authService.getCurrentUser()).thenReturn(mock(User.class));

        when(userGamesService.getOwnedEntities(any(Long.class))).thenReturn(
                List.of(game)
        );

        mockMvc.perform(get("/api/users/me/games"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        "[{\"id\":1,\"title\":\"Game 1\",\"description\":\"Description 1\",\"releaseYear\":2010,\"rating\":8}]"
                ));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldGetAllUserGames() throws Exception {
        when(userGamesService.getOwnedEntities(any(Long.class))).thenReturn(
                List.of(game)
        );

        mockMvc.perform(get("/api/users/1/games"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        "[{\"id\":1,\"title\":\"Game 1\",\"description\":\"Description 1\",\"releaseYear\":2010,\"rating\":8}]"
                ));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldGetNotFoundOnInvalidUserId() throws Exception {
        when(userGamesService.getOwnedEntities(any(Long.class))).thenThrow(
                new EntityNotFoundException()
        );

        mockMvc.perform(get("/api/users/1/games"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnGetAllUserGames() throws Exception {
        mockMvc.perform(get("/api/users/1/games"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser
    void userShouldGetAllHisGames() throws Exception {
        when(authService.getCurrentUser()).thenReturn(mock(User.class));

        when(userGamesService.getOwnedEntities(any(Long.class))).thenReturn(
                List.of(game)
        );

        mockMvc.perform(get("/api/users/me/games"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        "[{\"id\":1,\"title\":\"Game 1\",\"description\":\"Description 1\",\"releaseYear\":2010,\"rating\":8}]"
                ));
    }

    @Test
    @WithMockUser
    void userShouldAddGameToHisGames() throws Exception {
        when(authService.getCurrentUser()).thenReturn(mock(User.class));

        when(userGamesService.addOwnedEntity(any(Long.class), any(Long.class))).thenReturn(
                mock(User.class)
        );

        mockMvc.perform(post("/api/users/me/games/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void userShouldGetNotFoundOnTryingToAddHisInvalidGame() throws Exception {
        when(authService.getCurrentUser()).thenReturn(mock(User.class));

        when(userGamesService.addOwnedEntity(any(Long.class), any(Long.class))).thenThrow(
                new EntityNotFoundException()
        );

        mockMvc.perform(post("/api/users/me/games/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    void userShouldRemoveGameFromHisGames() throws Exception {
        when(authService.getCurrentUser()).thenReturn(mock(User.class));

        when(userGamesService.removeOwnedEntity(any(Long.class), any(Long.class))).thenReturn(
                mock(User.class)
        );

        mockMvc.perform(delete("/api/users/me/games/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void userShouldGetNotFoundOnRemovingHisInvalidGame() throws Exception {
        when(authService.getCurrentUser()).thenReturn(mock(User.class));

        when(userGamesService.removeOwnedEntity(any(Long.class), any(Long.class))).thenThrow(
                new EntityNotFoundException()
        );

        mockMvc.perform(delete("/api/users/me/games/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldAddGameToUser() throws Exception {
        when(userGamesService.addOwnedEntity(any(Long.class), any(Long.class))).thenReturn(
                mock(User.class)
        );

        mockMvc.perform(post("/api/users/1/games/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminAddGameToUserShouldGetNotFoundOnInvalidGame() throws Exception {
        when(userGamesService.addOwnedEntity(any(Long.class), any(Long.class))).thenThrow(
                new EntityNotFoundException()
        );

        mockMvc.perform(post("/api/users/1/games/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminAddGameToUserShouldGetNotFoundOnInvalidUser() throws Exception {
        when(userGamesService.addOwnedEntity(any(Long.class), any(Long.class))).thenThrow(
                new EntityNotFoundException()
        );

        mockMvc.perform(post("/api/users/1/games/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldRemoveGameFromUser() throws Exception {
        when(userGamesService.removeOwnedEntity(any(Long.class), any(Long.class))).thenReturn(
                mock(User.class)
        );

        mockMvc.perform(delete("/api/users/1/games/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminRemoveGameFromUserShouldGetNotFoundOnNotPresentGame() throws Exception {
        when(userGamesService.removeOwnedEntity(any(Long.class), any(Long.class))).thenThrow(
                new EntityNotFoundException()
        );

        mockMvc.perform(delete("/api/users/1/games/1"))
                .andExpect(status().isNotFound());
    }
}
