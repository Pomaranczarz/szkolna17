package com.pomaranczarz.GamesBrowser.integration;

import com.pomaranczarz.GamesBrowser.exception.EntityAlreadyPresentException;
import com.pomaranczarz.GamesBrowser.exception.EntityNotFoundException;
import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.service.GameGenresService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class GameGenresControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameGenresService gameGenresService;

    private final Game game = Game.builder()
            .id(1)
            .title("Game 1")
            .description("Description 1")
            .releaseYear(2010)
            .rating(8)
            .build();

    private final Genre genre = Genre.builder()
            .id(1)
            .name("Genre 1")
            .build();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        var genreSet = new HashSet<Genre>();
        genreSet.add(genre);

        var gameSet = new HashSet<Game>();
        gameSet.add(game);
        game.setGenres(genreSet);
        genre.setGames(gameSet);
    }

    @Test
    void shouldGetAllGameGenres() throws Exception {
        User mockUser = User.builder().id(1).name("user").password("password").role(User.Role.USER).build();
        Authentication mockAuth = new UsernamePasswordAuthenticationToken(mockUser, mockUser.getPassword(), mockUser.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(mockAuth);

        when(gameGenresService.getOwnedEntities(any(Long.class))).thenReturn(
                List.of(genre)
        );

        mockMvc.perform(get("/api/games/1/genres"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        "[{\"name\":\"Genre 1\"}]"
                ));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void shouldGetErrorOnDuplicateGenre() throws Exception {
        when(gameGenresService.addOwnedEntity(any(Long.class), any(Long.class))).thenThrow(
                new EntityAlreadyPresentException("Genre [ID: 1] already present on the game [ID: 1]")
        );

        mockMvc.perform(post("/api/games/1/genres/1"))
                .andExpect(status().is(HttpStatus.CONFLICT.value()));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldAddGenreToGame() throws Exception {
        final Game gameWithNoGenres = Game.builder()
                .id(1)
                .title("Game 1")
                .description("Description 1")
                .releaseYear(2010)
                .rating(8)
                .genres(new HashSet<>())
                .build();

        when(gameGenresService.addOwnedEntity(any(Long.class), any(Long.class))).thenReturn(
                gameWithNoGenres
        );

        mockMvc.perform(post("/api/games/1/genres/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "USER")
    void regularUserShouldGetForbiddenOnAddingGenreToGame() throws Exception {
        mockMvc.perform(post("/api/games/1/genres/1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldRemoveGenreFromGame() throws Exception {
        when(gameGenresService.removeOwnedEntity(any(Long.class), any(Long.class))).thenReturn(
                game
        );

        mockMvc.perform(delete("/api/games/1/genres/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void shouldGetNotFoundOnInvalidGenre() throws Exception {
        when(gameGenresService.removeOwnedEntity(any(Long.class), any(Long.class))).thenThrow(
                new EntityNotFoundException("Genre [ID: 1] not present on the game [ID: 1]")
        );

        mockMvc.perform(delete("/api/games/1/genres/1"))
                .andExpect(status().isNotFound());
    }
}
