package com.pomaranczarz.GamesBrowser.unit;

import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.repository.GenreRepository;
import com.pomaranczarz.GamesBrowser.service.GenreService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

public class GenreServiceTests {
    @InjectMocks
    private GenreService genreService;

    @Mock
    private GenreRepository genreRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    Genre genre = Genre.builder()
            .id(1)
            .games(new HashSet<>())
            .name("Genre 1")
            .build();

    @Test
    void findAllShouldReturnAllGenres() {
        when(genreRepository.findAll()).thenReturn(List.of(genre));

        Assertions.assertEquals(List.of(genre), genreService.findAll());
    }

    @Test
    void findByIdShouldReturnGenre() {
        when(genreRepository.findById(1L)).thenReturn(Optional.of(genre));

        Assertions.assertEquals(Optional.of(genre), genreService.findById(1));
    }

    @Test
    void findAllByIdShouldReturnGenres() {
        when(genreRepository.findAllById(List.of(1L))).thenReturn(List.of(genre));

        Assertions.assertEquals(List.of(genre), genreService.findAllById(List.of(1L)));
    }

    @Test
    void saveShouldReturnGenre() {
        when(genreRepository.save(genre)).thenReturn(genre);

        Assertions.assertEquals(Optional.of(genre), genreService.save(genre));
    }

    @Test
    void saveOnErrorShouldReturnEmptyOptional() {
        when(genreRepository.save(genre)).thenThrow(RuntimeException.class);

        Assertions.assertEquals(Optional.empty(), genreService.save(genre));
    }

    @Test
    void saveAllShouldReturnListOfSavedGenres() {
        when(genreRepository.saveAll(List.of(genre))).thenReturn(List.of(genre));

        Assertions.assertEquals(List.of(genre), genreService.saveAll(List.of(genre)));
    }

    @Test
    void upsertShouldCreateGenre() {
        when(genreRepository.findById(1L)).thenReturn(Optional.empty());
        when(genreRepository.save(genre)).thenReturn(genre);

        Assertions.assertEquals(Optional.of(genre), genreService.upsert(1, genre));
    }

    @Test
    void upsertShouldUpdateGenre() {
        when(genreRepository.findById(1L)).thenReturn(Optional.of(genre));
        when(genreRepository.save(genre)).thenReturn(genre);

        Assertions.assertEquals(Optional.of(genre), genreService.upsert(1, genre));
    }

    @Test
    void patchShouldReturnGenre() {
        when(genreRepository.findById(1L)).thenReturn(Optional.of(genre));
        when(genreRepository.save(genre)).thenReturn(genre);

        Assertions.assertEquals(Optional.of(genre), genreService.patch(1, genre));
    }

    @Test
    void patchShouldReturnEmptyOptional() {
        when(genreRepository.findById(1L)).thenReturn(Optional.empty());

        Assertions.assertEquals(Optional.empty(), genreService.patch(1, genre));
    }
}
