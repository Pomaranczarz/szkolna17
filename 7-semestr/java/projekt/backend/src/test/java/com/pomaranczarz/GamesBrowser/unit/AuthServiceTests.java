package com.pomaranczarz.GamesBrowser.unit;

import com.pomaranczarz.GamesBrowser.dto.LoginRequest;
import com.pomaranczarz.GamesBrowser.dto.UserRegisterRequest;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.repository.UserRepository;
import com.pomaranczarz.GamesBrowser.service.AuthService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthServiceTests {
    @InjectMocks
    private AuthService authService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private AuthenticationManager authManager;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldAuthenticateUser() {
        User mockUser = mock(User.class);
        LoginRequest mockLoginRequest = mock(LoginRequest.class);

        when(mockLoginRequest.getName()).thenReturn("");
        when(authManager.authenticate(any(Authentication.class))).thenReturn(mock(Authentication.class));
        when(userRepository.findByName("")).thenReturn(Optional.of(mockUser));

        Assertions.assertEquals(Optional.of(mockUser), authService.authenticate(mockLoginRequest));
    }

    @Test
    void shouldThrowBadCredentialsException() {
        when(authManager.authenticate(any(Authentication.class))).thenThrow(mock(AuthenticationException.class));

        Assertions.assertThrows(AuthenticationException.class, () -> authService.authenticate(mock(LoginRequest.class)));
    }

    @Test
    void shouldRegisterUser() {
        UserRegisterRequest mockUserRegisterRequest = mock(UserRegisterRequest.class);
        User userMock = mock(User.class);

        when(mockUserRegisterRequest.getPassword()).thenReturn("");
        when(passwordEncoder.encode("")).thenReturn("");
        when(userRepository.save(any(User.class))).thenReturn(userMock);

        Assertions.assertEquals(Optional.of(userMock), authService.register(mockUserRegisterRequest));
    }

    @Test
    void shouldThrowInvalidRegistrationDataExceptionOnError() {
        UserRegisterRequest mockUserRegisterRequest = mock(UserRegisterRequest.class);

        when(mockUserRegisterRequest.getPassword()).thenReturn("");
        when(passwordEncoder.encode("")).thenReturn("");
        when(userRepository.save(any(User.class))).thenThrow(RuntimeException.class);

        Assertions.assertThrows(RuntimeException.class, () -> authService.register(mockUserRegisterRequest));
    }

    @Test
    void shouldGetCurrentUser() {
        var mockAuthentication = mock(Authentication.class);
        var userMock = mock(User.class);
        SecurityContextHolder.getContext().setAuthentication(mockAuthentication);

        when(mockAuthentication.getPrincipal()).thenReturn(userMock);

        Assertions.assertEquals(userMock, authService.getCurrentUser());
    }

    @Test
    void shouldThrowAccessDeniedException() {
        var mockAuthentication = mock(Authentication.class);
        SecurityContextHolder.getContext().setAuthentication(mockAuthentication);

        when(mockAuthentication.getPrincipal()).thenReturn(null);

        Assertions.assertThrows(AccessDeniedException.class, () -> authService.getCurrentUser());
    }

    @Test
    void shouldAuthorizeOwner() {
        var mockAuthentication = mock(Authentication.class);
        var mockUser = mock(User.class);
        SecurityContextHolder.getContext().setAuthentication(mockAuthentication);

        when(mockUser.getId()).thenReturn(1L);

        when(mockAuthentication.getPrincipal()).thenReturn(mockUser);
        when(userRepository.findById(1L)).thenReturn(Optional.of(mockUser));

        Assertions.assertEquals(mockUser, authService.authorizeOwnerOrAdmin(1L));
    }

    @Test
    void shouldAuthorizeAdmin() {
        var mockAuthentication = mock(Authentication.class);
        var mockUser = mock(User.class);
        var mockRequestUser = mock(User.class);
        SecurityContextHolder.getContext().setAuthentication(mockAuthentication);

        when(mockUser.getId()).thenReturn(1L);
        when(mockUser.getRole()).thenReturn(User.Role.ADMIN);

        when(mockRequestUser.getId()).thenReturn(2L);

        when(mockAuthentication.getPrincipal()).thenReturn(mockUser);
        when(userRepository.findById(1L)).thenReturn(Optional.of(mockRequestUser));

        Assertions.assertEquals(mockRequestUser, authService.authorizeOwnerOrAdmin(1L));
    }
}
