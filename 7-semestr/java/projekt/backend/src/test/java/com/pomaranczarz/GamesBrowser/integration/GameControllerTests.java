package com.pomaranczarz.GamesBrowser.integration;

import com.pomaranczarz.GamesBrowser.controller.GameController;
import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.CrudService;
import com.pomaranczarz.GamesBrowser.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CrudService<Game> gameService;
    @MockBean
    private UserService userService;
    @MockBean
    private AuthenticationService authService;

    @InjectMocks
    private GameController gameController;

    private final Game game1 = Game.builder().id(1).title("Game 1").description("Description 1").releaseYear(2010).rating(8).build();
    private final Game game2 = Game.builder().id(2).title("Game 2").description("Description 2").releaseYear(2015).rating(4).build();

    private final String game1DetailsJsonString = "{\"id\":1,\"title\":\"Game 1\",\"description\":\"Description 1\",\"releaseYear\":2010,\"rating\":8}";
    private final String game2DetailsJsonString = "{\"id\":2,\"title\":\"Game 2\",\"description\":\"Description 2\",\"releaseYear\":2015,\"rating\":4}";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @WithMockUser(authorities = "USER")
    void userShouldGetAllGames() throws Exception {
        User user = User.builder().role(User.Role.ADMIN).build();
        when(authService.getCurrentUser()).thenReturn(user);

        List<Game> games = List.of(game1, game2);
        when(gameService.findAll()).thenReturn(games);

        mockMvc.perform(get("/api/games"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        "[" + game1DetailsJsonString + "," + game2DetailsJsonString + "]"
                ));

        verify(gameService, times(1)).findAll();
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void testGetGameById() throws Exception {
        User user = User.builder().role(User.Role.ADMIN).build();
        when(authService.getCurrentUser()).thenReturn(user);
        when(gameService.findById(1L)).thenReturn(Optional.of(game1));

        mockMvc.perform(get("/api/games/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(game1DetailsJsonString));

        verify(gameService, times(1)).findById(1L);
    }

    @Test
    void testGetGameById_NotFound() throws Exception {
        Authentication mockAuth = new UsernamePasswordAuthenticationToken(
                User.builder().role(User.Role.ADMIN).build(),
                "password",
                List.of(User.Role.ADMIN)
        );
        SecurityContextHolder.getContext().setAuthentication(mockAuth);
        when(gameService.findById(3L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/api/games/3"))
                .andExpect(status().isNotFound());

        verify(gameService, times(1)).findById(3L);
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldCreateGame() throws Exception {
        User user = User.builder().role(User.Role.ADMIN).build();
        when(authService.getCurrentUser()).thenReturn(user);
        when(gameService.save(any(Game.class))).thenReturn(Optional.of(game1));

        mockMvc.perform(post("/api/games")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(game1DetailsJsonString))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(game1DetailsJsonString));

        verify(gameService, times(1)).save(any(Game.class));
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnCreateGame() throws Exception {
        when(authService.authorizeOwnerOrAdmin(any(Long.class))).thenThrow(AccessDeniedException.class);

        mockMvc.perform(post("/api/games")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(game2DetailsJsonString))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldUpdateGame() throws Exception {
        User user = User.builder().role(User.Role.ADMIN).build();
        when(authService.getCurrentUser()).thenReturn(user);
        when(gameService.upsert(eq(1L), any(Game.class))).thenReturn(Optional.of(game1));

        mockMvc.perform(put("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(game1DetailsJsonString))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(game1DetailsJsonString));

        verify(gameService, times(1)).upsert(eq(1L), any(Game.class));
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnUpdateGame() throws Exception {
        mockMvc.perform(put("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(game1DetailsJsonString))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldDeleteGame() throws Exception {
        doNothing().when(gameService).deleteById(1L);

        mockMvc.perform(delete("/api/games/1"))
                .andExpect(status().isOk());

        verify(gameService, times(1)).deleteById(1L);
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnDeleteGame() throws Exception {
        mockMvc.perform(delete("/api/games/1"))
                .andExpect(status().isForbidden());
    }
}
