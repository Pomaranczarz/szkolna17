package com.pomaranczarz.GamesBrowser.unit;

import com.pomaranczarz.GamesBrowser.dto.GamePatchRequest;
import com.pomaranczarz.GamesBrowser.dto.GamePostRequest;
import com.pomaranczarz.GamesBrowser.service.GameDtoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

public class GameDtoServiceTests {
    private final GameDtoService gameDtoService = new GameDtoService();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldConvertPostDtoToGame() {
        GamePostRequest dto = GamePostRequest.builder()
                .title("title")
                .description("description")
                .releaseYear(2020)
                .rating(10)
                .build();

        var game = gameDtoService.dtoToEntity(dto);

        Assertions.assertAll(
                () -> Assertions.assertEquals("title", game.getTitle()),
                () -> Assertions.assertEquals("description", game.getDescription()),
                () -> Assertions.assertEquals(2020, game.getReleaseYear()),
                () -> Assertions.assertEquals(10, game.getRating())
        );
    }

    @Test
    void shouldConvertPatchDtoToGame() {
        GamePatchRequest dto = GamePatchRequest.builder()
                .title("title")
                .description("description")
                .releaseYear(2020)
                .rating(10)
                .build();

        var game = gameDtoService.dtoToEntity(dto);

        Assertions.assertAll(
                () -> Assertions.assertEquals("title", game.getTitle()),
                () -> Assertions.assertEquals("description", game.getDescription()),
                () -> Assertions.assertEquals(2020, game.getReleaseYear()),
                () -> Assertions.assertEquals(10, game.getRating())
        );
    }
}
