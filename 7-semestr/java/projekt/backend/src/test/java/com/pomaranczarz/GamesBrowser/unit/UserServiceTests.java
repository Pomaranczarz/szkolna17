package com.pomaranczarz.GamesBrowser.unit;

import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.repository.UserRepository;
import com.pomaranczarz.GamesBrowser.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class UserServiceTests {
    @InjectMocks
    private UserService userService;
    @Mock
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    User user = User.builder()
            .id(1)
            .name("user 1")
            .email("user1@mail")
            .role(User.Role.USER)
            .games(new HashSet<>())
            .password("password")
            .build();

    @Test
    void findAllShouldReturnListOfUsers() {
        when(userRepository.findAll()).thenReturn(List.of(user));

        assertEquals(List.of(user), userService.findAll());
    }

    @Test
    void findAllByIdShouldReturnListOfUsers() {
        when(userRepository.findAllById(List.of(1L))).thenReturn(List.of(user));

        assertEquals(List.of(user), userService.findAllById(List.of(1L)));
    }

    @Test
    void findByNameShouldReturnUser() {
        when(userRepository.findByName("user 1")).thenReturn(Optional.of(user));

        assertEquals(Optional.of(user), userService.findByName("user 1"));
    }

    @Test
    void findByInvalidNameShouldReturnNone() {
        when(userRepository.findByName(any(String.class))).thenReturn(Optional.empty());

        assertEquals(Optional.empty(), userService.findByName("user 1"));
    }

    @Test
    void saveShouldReturnSavedUser() {
        when(userRepository.save(user)).thenReturn(user);

        assertEquals(Optional.of(user), userService.save(user));
    }

    @Test
    void saveErrorShouldReturnNone() {
        when(userRepository.save(user)).thenThrow(RuntimeException.class);

        assertEquals(Optional.empty(), userService.save(user));
    }

    @Test
    void saveAllShouldReturnListOfUsers() {
        when(userRepository.saveAll(List.of(user))).thenReturn(List.of(user));

        assertEquals(List.of(user), userService.saveAll(List.of(user)));
    }

    @Test
    void deleteByIdShouldDeleteUser() {
        assertDoesNotThrow(() -> userService.deleteById(1L));
    }

    @Test
    void deleteAllByIdShouldDeleteUsers() {
        assertDoesNotThrow(() -> userService.deleteAllById(List.of(1L)));
    }

    @Test
    void deleteAllShouldDeleteAllUsers() {
        assertDoesNotThrow(() -> userService.deleteAll(List.of(user)));
    }

    @Test
    void upsertShouldReturnUser() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);

        assertEquals(Optional.of(user), userService.upsert(1L, user));
    }

    @Test
    void upsertShouldCreateUser() {
        when(userRepository.findById(1L)).thenReturn(Optional.empty());
        when(userRepository.save(user)).thenReturn(user);

        assertEquals(Optional.of(user), userService.upsert(1L, user));
    }

    @Test
    void patchShouldReturnUser() {
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);

        assertEquals(Optional.of(user), userService.patch(1L, user));
    }
}
