package com.pomaranczarz.GamesBrowser.integration;

import com.pomaranczarz.GamesBrowser.controller.UserController;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.service.AuthenticationService;
import com.pomaranczarz.GamesBrowser.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private AuthenticationService authService;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    private final User user1 = User.builder()
            .name("user1")
            .email("user1@mail.to")
            .role(User.Role.ADMIN)
            .id(1L)
            .password("user1")
            .games(new HashSet<>())
            .build();
    private final String user1DetailsJsonString = "{\"id\":1,\"name\":\"user1\",\"email\":\"user1@mail.to\",\"gamesIds\":[]}";

    private final User user2 = User.builder()
            .name("user2")
            .email("user2@mail.to")
            .role(User.Role.USER)
            .id(2L)
            .password("user2")
            .games(new HashSet<>())
            .build();

    private final String user2DetailsJsonString = "{\"id\":2,\"name\":\"user2\",\"email\":\"user2@mail.to\",\"gamesIds\":[]}";

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldGetAllUsers() throws Exception {
        List<User> users = List.of(user1, user2);
        when(userService.findAll()).thenReturn(users);

        mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        "[{\"id\":1,\"name\":\"user1\",\"email\":\"user1@mail.to\",\"gamesIds\":[]}," +
                                "{\"id\":2,\"name\":\"user2\",\"email\":\"user2@mail.to\",\"gamesIds\":[]}]"
                ));

        verify(userService, times(1)).findAll();
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnGetAllUsers() throws Exception {
        when(userService.findAll()).thenReturn(List.of(user1, user2));

        mockMvc.perform(get("/api/users"))
                .andDo(result -> System.out.println(result.getResponse().getContentAsString()));
    }

    @Test
    void testGetAuthenticatedUser() throws Exception {
        Authentication mockAuth = new UsernamePasswordAuthenticationToken(user2, user2.getPassword(), user2.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(mockAuth);

        mockMvc.perform(get("/api/users/me"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        "{\"name\":\"user2\",\"email\":\"user2@mail.to\",\"gamesIds\":[]}"
                ));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldGetUserById() throws Exception {
        when(userService.findById(1L)).thenReturn(Optional.of(user1));

        mockMvc.perform(get("/api/users/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        verify(userService, times(1)).findById(1L);
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldAddUser() throws Exception {
        var createUserJsonString = "{\"name\":\"John\", \"email\":\"john@example.com\", \"password\":\"password\", \"role\":\"ADMIN\"}";
        when(userService.save(any(User.class))).thenReturn(Optional.of(user1));
        when(authService.getPasswordEncoder()).thenReturn(new BCryptPasswordEncoder());

        mockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createUserJsonString))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        user1DetailsJsonString
                ));

        verify(userService, times(1)).save(any(User.class));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldUpsertUser() throws Exception {
        var upsertUserJsonString = "{" +
                "\"name\":\"Updated Name\"," +
                "\"email\": \"updatedEmail@updated.com\"," +
                "\"role\":\"ADMIN\"," +
                "\"password\":\"password\"" +
                "}";
        when(userService.upsert(eq(1L), any(User.class))).thenReturn(Optional.of(user2));
        when(authService.getPasswordEncoder()).thenReturn(new BCryptPasswordEncoder());

        mockMvc.perform(put("/api/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(upsertUserJsonString))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(
                        user2DetailsJsonString
                ));

        verify(userService, times(1)).upsert(eq(1L), any(User.class));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void adminShouldDeleteUser() throws Exception {
        doNothing().when(userService).deleteById(1L);

        mockMvc.perform(delete("/api/users/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("User deleted"));

        verify(userService, times(1)).deleteById(1L);
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnGetUserById() throws Exception {
        mockMvc.perform(get("/api/users/1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnDeleteUser() throws Exception {
        when(authService.authorizeOwnerOrAdmin(any(Long.class))).thenThrow(AccessDeniedException.class);

        mockMvc.perform(delete("/api/users/1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnUpsertUser() throws Exception {
        var upsertUserJsonString = "{" +
                "\"name\":\"Updated Name\"," +
                "\"email\": \"updatedEmail@updated.com\"," +
                "\"role\":\"ADMIN\"," +
                "\"password\":\"password\"" +
                "}";
        mockMvc.perform(put("/api/users/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(upsertUserJsonString))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnCreateUser() throws Exception {
        var createUserJsonString = "{\"name\":\"John\", \"email\":\"john@example.com\", \"password\":\"password\", \"role\":\"ADMIN\"}";
        mockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createUserJsonString))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = "USER")
    void nonAdminShouldGetForbiddenOnDeleteUserById() throws Exception {
        when(authService.authorizeOwnerOrAdmin(any(Long.class))).thenThrow(AccessDeniedException.class);

        mockMvc.perform(delete("/api/users/1"))
                .andExpect(status().isForbidden());
    }
}
