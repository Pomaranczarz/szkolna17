package com.pomaranczarz.GamesBrowser.unit;

import com.pomaranczarz.GamesBrowser.exception.EntityNotFoundException;
import com.pomaranczarz.GamesBrowser.exception.EntityPersistenceException;
import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.model.Genre;
import com.pomaranczarz.GamesBrowser.service.GameGenresService;
import com.pomaranczarz.GamesBrowser.service.GameService;
import com.pomaranczarz.GamesBrowser.service.GenreService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GameGenreServiceTests {
    @InjectMocks
    private GameGenresService gameGenresService;

    private GameService gameService;

    private GenreService genreService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        gameService = mock(GameService.class);
        genreService = mock(GenreService.class);

        gameGenresService = new GameGenresService(genreService, gameService);
    }

    @Test
    void getOwnedEntitiesShouldReturnListOfGenres() {
        Game mockGame = mock(Game.class);
        Genre mockGenre = mock(Genre.class);

        when(mockGame.getGenres()).thenReturn(Set.of(mockGenre));
        when(gameService.findById(1L)).thenReturn(Optional.of(mockGame));

        Assertions.assertEquals(List.of(mockGenre), gameGenresService.getOwnedEntities(1L));
    }

    @Test
    void getOwnedEntitiesShouldThrowOnInvalidGame() {
        when(gameService.findById(any(Long.class))).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> gameGenresService.getOwnedEntities(1L));
    }

    @Test
    void addOwnedEntityShouldAddGenre() {
        Game game = Game.builder().id(1).genres(new HashSet<>()).users(new HashSet<>()).build();
        Genre genre = Genre.builder().id(1).games(new HashSet<>()).build();

        when(gameService.findById(1L)).thenReturn(Optional.of(game));
        when(genreService.findById(1L)).thenReturn(Optional.of(genre));

        when(gameService.save(any(Game.class))).thenReturn(Optional.of(game));

        Assertions.assertEquals(game, gameGenresService.addOwnedEntity(1L, 1L));
    }

    @Test
    void addOwnedEntityShouldThrowOnInvalidGame() {
        when(gameService.findById(any(Long.class))).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> gameGenresService.addOwnedEntity(1L, 1L));
    }

    @Test
    void addOwnedEntityShouldThrowOnInvalidGenre() {
        when(genreService.findById(any(Long.class))).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> gameGenresService.addOwnedEntity(1L, 1L));
    }

    @Test
    void addOwnedEntityShouldThrowOnPersistenceError() {
        Game game = Game.builder().id(1).genres(new HashSet<>()).users(new HashSet<>()).build();
        Genre genre = Genre.builder().id(1).games(new HashSet<>()).build();

        when(gameService.findById(1L)).thenReturn(Optional.of(game));
        when(genreService.findById(1L)).thenReturn(Optional.of(genre));

        when(gameService.save(any(Game.class))).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityPersistenceException.class, () -> gameGenresService.addOwnedEntity(1L, 1L));
    }

    @Test
    void removeOwnedEntityShouldRemoveGenre() {
        Genre genre = Genre.builder().id(1).games(new HashSet<>()).build();
        Game game = Game.builder().id(1).genres(new HashSet<>(List.of(genre))).users(new HashSet<>()).build();

        when(gameService.findById(1L)).thenReturn(Optional.of(game));
        when(genreService.findById(1L)).thenReturn(Optional.of(genre));

        when(gameService.save(any(Game.class))).thenReturn(Optional.of(game));

        Assertions.assertEquals(game, gameGenresService.removeOwnedEntity(1L, 1L));
    }

    @Test
    void removeOwnedEntityShouldThrowOnInvalidGame() {
        when(gameService.findById(any(Long.class))).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> gameGenresService.removeOwnedEntity(1L, 1L));
    }

    @Test
    void removeOwnedEntityShouldThrowOnInvalidGenre() {
        when(genreService.findById(any(Long.class))).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> gameGenresService.removeOwnedEntity(1L, 1L));
    }

    @Test
    void removeOwnedEntityShouldThrowOnPersistenceError() {
        Genre genre = Genre.builder().id(1).games(new HashSet<>()).build();
        Game game = Game.builder().id(1).genres(new HashSet<>(List.of(genre))).users(new HashSet<>()).build();

        when(gameService.findById(1L)).thenReturn(Optional.of(game));
        when(genreService.findById(1L)).thenReturn(Optional.of(genre));

        when(gameService.save(any(Game.class))).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityPersistenceException.class, () -> gameGenresService.removeOwnedEntity(1L, 1L));
    }
}
