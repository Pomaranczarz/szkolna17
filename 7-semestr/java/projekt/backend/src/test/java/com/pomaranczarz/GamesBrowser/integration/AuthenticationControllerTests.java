package com.pomaranczarz.GamesBrowser.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pomaranczarz.GamesBrowser.dto.LoginRequest;
import com.pomaranczarz.GamesBrowser.dto.UserRegisterRequest;
import com.pomaranczarz.GamesBrowser.model.User;
import com.pomaranczarz.GamesBrowser.response.user.LoginResponse;
import com.pomaranczarz.GamesBrowser.service.AuthService;
import com.pomaranczarz.GamesBrowser.service.JwtService;
import com.pomaranczarz.GamesBrowser.service.UserDtoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private JwtService jwtService;

    @MockBean
    private AuthService authService;

    @MockBean
    private UserDtoService userDtoService;

    private final User regularUser = User.builder()
            .id(1)
            .email("user@user.com")
            .role(User.Role.USER)
            .password("user")
            .name("user")
            .build();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testAuthenticate_Success() throws Exception {
        LoginRequest loginDto = new LoginRequest("user@user.com", "password");
        String token = "mockToken";
        LoginResponse response = new LoginResponse(token, 3600L);

        when(authService.authenticate(any(LoginRequest.class))).thenReturn(Optional.of(regularUser));
        when(jwtService.generateToken(any(User.class))).thenReturn(token);
        when(jwtService.getExpirationTime()).thenReturn(3600L);

        mockMvc.perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginDto)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(response)));
    }

    @Test
    void testAuthenticate_InvalidCredentials() throws Exception {
        LoginRequest loginDto = new LoginRequest("user@user.com", "wrongPassword");

        when(authService.authenticate(any(LoginRequest.class))).thenReturn(Optional.empty());

        mockMvc.perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginDto)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void testRegister_Success() throws Exception {
        when(authService.register(any(UserRegisterRequest.class))).thenReturn(Optional.of(regularUser));

        mockMvc.perform(post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "{\"email\": \"user@user.com\", \"password\": \"user12345\", \"name\": \"user\"}"
                        ))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"name\":\"user\",\"email\":\"user@user.com\",\"role\":\"USER\",\"gamesIds\":[]}"
                ));
    }

    @Test
    void testRegister_UserAlreadyExists() throws Exception {
        when(authService.register(any(UserRegisterRequest.class))).thenReturn(Optional.empty());

        mockMvc.perform(post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "{\"email\": \"newuser@user.com\", \"password\": \"newPassword\", \"name\": \"New User\"}"
                        ))
                .andExpect(status().isBadRequest());
    }
}
