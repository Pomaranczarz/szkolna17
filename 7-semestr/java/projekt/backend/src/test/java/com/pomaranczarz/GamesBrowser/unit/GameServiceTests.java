package com.pomaranczarz.GamesBrowser.unit;

import com.pomaranczarz.GamesBrowser.model.Game;
import com.pomaranczarz.GamesBrowser.repository.GameRepository;
import com.pomaranczarz.GamesBrowser.service.GameService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class GameServiceTests {
    @InjectMocks
    private GameService gameService;

    @Mock
    private GameRepository gameRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    Game game = Game.builder()
            .id(1)
            .users(new HashSet<>())
            .title("Game 1")
            .description("Description 1")
            .rating(5)
            .releaseYear(2010)
            .genres(new HashSet<>())
            .build();

    @Test
    void findAllShouldReturnAllGames() {
        when(gameRepository.findAll()).thenReturn(List.of(game));

        Assertions.assertEquals(List.of(game), gameService.findAll());
    }

    @Test
    void findAllByIdShouldReturnAllGames() {
        when(gameRepository.findAllById(List.of(1L))).thenReturn(List.of(game));

        Assertions.assertEquals(List.of(game), gameService.findAllById(List.of(1L)));
    }

    @Test
    void findByIdShouldReturnGame() {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(game));

        Assertions.assertEquals(Optional.of(game), gameService.findById(1L));
    }

    @Test
    void findByIdShouldReturnNoneOnInvalidId() {
        when(gameRepository.findById(1L)).thenReturn(Optional.empty());

        Assertions.assertEquals(Optional.empty(), gameService.findById(1L));
    }

    @Test
    void saveShouldSaveGame() {
        when(gameRepository.save(game)).thenReturn(game);

        Assertions.assertEquals(Optional.of(game), gameService.save(game));
    }

    @Test
    void saveShouldReturnNoneOnError() {
        when(gameRepository.save(game)).thenThrow(RuntimeException.class);

        Assertions.assertEquals(Optional.empty(), gameService.save(game));
    }

    @Test
    void saveAllShouldReturnSavedGames() {
        when(gameRepository.saveAll(List.of(game))).thenReturn(List.of(game));

        Assertions.assertEquals(List.of(game), gameService.saveAll(List.of(game)));
    }

    @Test
    void upsertShouldUpdateExistingGame() {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(game));
        when(gameRepository.save(any(Game.class))).thenReturn(game);

        Assertions.assertEquals(Optional.of(game), gameService.upsert(1L, game));
    }

    @Test
    void upsertShouldSaveNewGame() {
        when(gameRepository.findById(1L)).thenReturn(Optional.empty());
        when(gameRepository.save(any(Game.class))).thenReturn(game);

        Assertions.assertEquals(Optional.of(game), gameService.upsert(1L, game));
    }

    @Test
    void patchShouldUpdateExistingGame() {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(game));
        when(gameRepository.save(any(Game.class))).thenReturn(game);

        Assertions.assertEquals(Optional.of(game), gameService.patch(1L, game));
    }
}
