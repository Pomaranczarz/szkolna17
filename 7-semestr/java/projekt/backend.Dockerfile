FROM jelastic/maven:3.9.8-openjdk-21.0.2-almalinux-9 AS build-stage

WORKDIR /app

COPY pom.xml .
RUN mvn dependency:go-offline

COPY . .
RUN mvn clean package -DskipTests

FROM openjdk:21

WORKDIR /app

COPY wait-for-it.sh wait-for-it.sh
COPY --from=build-stage /app/target/GamesBrowser-1.0.0.jar GamesBrowser.jar

EXPOSE 5000

ENTRYPOINT [ "./wait-for-it.sh", "db:3306", "--", "java", "-jar", "/app/GamesBrowser.jar" ]
