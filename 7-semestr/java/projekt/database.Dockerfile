FROM mysql:latest

COPY ./database_init.sql /docker-entrypoint-initdb.d/

EXPOSE 3306

CMD ["mysqld"]
