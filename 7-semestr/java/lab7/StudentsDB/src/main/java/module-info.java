module org.example.studentsdb {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires java.sql;

    exports org.example.studentsdb;
    opens org.example.studentsdb.controllers to javafx.fxml;
    opens org.example.studentsdb.models to javafx.base;
}