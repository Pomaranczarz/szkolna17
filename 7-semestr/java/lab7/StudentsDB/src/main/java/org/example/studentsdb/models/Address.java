package org.example.studentsdb.models;

public class Address {
    private final String city;
    private final String street;
    private final Integer houseNumber;
    private final Integer apartmentNumber;

    public Address(String city, String street, Integer houseNumber, Integer apartmentNumber) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.apartmentNumber = apartmentNumber;
    }

    public Address(String city, String street, Integer houseNumber) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.apartmentNumber = null;
    }

    @Override
    public String toString() {
        return city + ", " + street + " " + houseNumber + (apartmentNumber == null || apartmentNumber == 0 ? "" : "/" + apartmentNumber);
    }

    public String intoSqlInsertStatementValues() {
        if (apartmentNumber == null) {
            return String.format("'%s', '%s', %d, NULL", city, street, houseNumber);
        }
        return String.format("'%s', '%s', %d, %d", city, street, houseNumber, apartmentNumber);
    }

    public String intoSqlUpdateStatementValues() {
        if (apartmentNumber == null) {
            return String.format("city = '%s', street = '%s', house_number = %d, apartment_number = NULL", city, street, houseNumber);
        }
        return String.format("city = '%s', street = '%s', house_number = %d, apartment_number = %d", city, street, houseNumber, apartmentNumber);
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public Integer getApartmentNumber() {
        return apartmentNumber;
    }
}
