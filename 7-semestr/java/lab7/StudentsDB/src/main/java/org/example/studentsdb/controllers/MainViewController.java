package org.example.studentsdb.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.example.studentsdb.StudentsDB;
import org.example.studentsdb.models.Student;

public class MainViewController {
    @FXML
    public Button addStudentButton;

    @FXML
    public TableView<Student> studentTable;
    @FXML
    public TableColumn<Student, Integer> idColumn;
    @FXML
    public TableColumn<Student, String> nameColumn;
    @FXML
    public TableColumn<Student, String> lastnameColumn;
    @FXML
    public TableColumn<Student, String> indexColumn;
    @FXML
    public TableColumn<Student, String> addressColumn;
    @FXML
    public TableColumn<Student, String> facultyColumn;
    @FXML
    public TableColumn<Student, Integer> yearOfStudyColumn;
    @FXML
    public TableColumn<Student, Boolean> scholarshipColumn;
    @FXML
    public TableColumn<Student, Double> averageGradeColumn;
    @FXML
    public TableColumn<Student, Void> actionsColumn;

    private final ObservableList<Student> students = FXCollections.observableArrayList();
    private final DatabaseController databaseController = new DatabaseController();

    @FXML
    private void initialize() {
        var studentsFromDB = databaseController.getAll();

        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        lastnameColumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        indexColumn.setCellValueFactory(new PropertyValueFactory<>("indexNumber"));
        addressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        facultyColumn.setCellValueFactory(new PropertyValueFactory<>("faculty"));
        yearOfStudyColumn.setCellValueFactory(new PropertyValueFactory<>("yearOfStudy"));
        scholarshipColumn.setCellValueFactory(new PropertyValueFactory<>("receivedScholarship"));
        averageGradeColumn.setCellValueFactory(new PropertyValueFactory<>("averageGrade"));

        Runnable updateStudents = () -> {
            students.clear();
            students.addAll(databaseController.getAll());
            studentTable.refresh();
        };
        actionsColumn.setCellFactory(new ActionCellFactory(databaseController, students, updateStudents));

        studentTable.setItems(students);

        students.addAll(studentsFromDB);

        addStudentButton.setOnAction(event -> {
            try {
                FXMLLoader loader = new FXMLLoader(StudentsDB.class.getResource("student-form-view.fxml"));
                Parent root = loader.load();
                Stage stage = new Stage();
                stage.setTitle("Add Account");
                stage.setScene(new Scene(root, 500, 780));
                stage.showAndWait();

                updateStudents.run();
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
            }
        });
    }
}