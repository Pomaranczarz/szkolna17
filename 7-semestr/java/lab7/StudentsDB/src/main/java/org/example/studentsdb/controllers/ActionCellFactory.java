package org.example.studentsdb.controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.example.studentsdb.StudentsDB;
import org.example.studentsdb.models.Student;

public class ActionCellFactory implements Callback<TableColumn<Student, Void>, TableCell<Student, Void>> {
    private final DatabaseController databaseController;
    private final ObservableList<Student> students;
    private final Runnable updateStudentsCallback;

    public ActionCellFactory(DatabaseController databaseController, ObservableList<Student> students, Runnable updateStudentsCallback) {
        this.databaseController = databaseController;
        this.students = students;
        this.updateStudentsCallback = updateStudentsCallback;
    }

    @Override
    public TableCell<Student, Void> call(TableColumn<Student, Void> studentVoidTableColumn) {
        return new TableCell<>() {
            private final Button editButton = new Button("Edit");
            private final Button deleteButton = new Button("Delete");

            {
                editButton.setOnAction(event -> { // if button is clicked, create window with edit form and pass edited student id to it
                    try {
                        FXMLLoader loader = new FXMLLoader(StudentsDB.class.getResource("student-form-view.fxml"));
                        Parent root = loader.load();

                        StudentFormController controller = loader.getController();
                        controller.setEditedStudentId(getTableRow().getItem().getId());

                        Stage stage = new Stage();
                        stage.setTitle("Add Account");
                        stage.setScene(new Scene(root, 500, 780));
                        stage.showAndWait();

                        updateStudentsCallback.run();
                    } catch (Exception e) {
                        System.out.println(e.getLocalizedMessage());
                    }
                });

                deleteButton.setOnAction(event -> { // if button is clicked, delete student
                    Student student = getTableRow().getItem();
                    databaseController.delete(student);
                    students.remove(student);
                    updateStudentsCallback.run();
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty)
                    setGraphic(null);
                else {
                    HBox actionButtons = new HBox(5, editButton, deleteButton);
                    setGraphic(actionButtons);
                }
            }
        };
    }
}
