package org.example.studentsdb.models;

public class Student {
    private final Integer id;
    private final String name;
    private final String lastname;
    private final String indexNumber;
    private final Address address;
    private final String faculty;
    private final Integer yearOfStudy;

    private final Boolean receivedScholarship;
    private final Double averageGrade;

    public Student(Integer id,
                   String name,
                   String lastname,
                   String indexNumber,
                   Address address,
                   String faculty,
                   Integer yearOfStudy,
                   Boolean receivedScholarship,
                   Double averageGrade) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.indexNumber = indexNumber;
        this.address = address;
        this.faculty = faculty;
        this.yearOfStudy = yearOfStudy;
        this.receivedScholarship = receivedScholarship;
        this.averageGrade = averageGrade;
    }

    public Student(Integer id,
                   String name,
                   String lastname,
                   String indexNumber,
                   Address address,
                   String faculty,
                   Integer yearOfStudy) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.indexNumber = indexNumber;
        this.address = address;
        this.faculty = faculty;
        this.yearOfStudy = yearOfStudy;
        this.receivedScholarship = false;
        this.averageGrade = null;
    }

    public String intoSqlInsertStatementValues() {
        StringBuilder formatStringBuilder = new StringBuilder();

        formatStringBuilder.append("'%s', '%s', '%s', %s, %d, '%s', ");
        formatStringBuilder.append(receivedScholarship ? "TRUE" : "FALSE");

        formatStringBuilder.append(", ");

        if (averageGrade != null)
            formatStringBuilder.append(averageGrade);
        else
            formatStringBuilder.append("NULL");


        return String.format(
                formatStringBuilder.toString(),
                name,
                lastname,
                indexNumber,
                address.intoSqlInsertStatementValues(),
                yearOfStudy,
                faculty
        );
    }

    public String intoSqlUpdateStatementValues() {
        return String.format(
                "name = '%s', lastname = '%s', index_number = '%s', %s, year_of_study = %d, faculty = '%s', received_scholarship = %s, "
                        + "average_grade = %f",
                name,
                lastname,
                indexNumber,
                address.intoSqlUpdateStatementValues(),
                yearOfStudy,
                faculty,
                receivedScholarship ? "TRUE" : "FALSE",
                averageGrade
        );
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public Address getAddress() {
        return address;
    }

    public String getFaculty() {
        return faculty;
    }

    public Integer getYearOfStudy() {
        return yearOfStudy;
    }

    public Boolean getReceivedScholarship() {
        return receivedScholarship;
    }

    public Double getAverageGrade() {
        return averageGrade;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", indexNumber='" + indexNumber + '\'' +
                ", address=" + address +
                ", faculty='" + faculty + '\'' +
                ", yearOfStudy=" + yearOfStudy +
                ", receivedScholarship=" + receivedScholarship +
                ", averageGrade=" + averageGrade +
                '}';
    }
}
