package org.example.studentsdb.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.example.studentsdb.models.Address;
import org.example.studentsdb.models.Student;

import java.lang.reflect.Field;

public class StudentFormController {
    @FXML
    public TextField nameField;
    @FXML
    public TextField lastnameField;
    @FXML
    public TextField indexField;
    @FXML
    public TextField cityField;
    @FXML
    public TextField streetField;
    @FXML
    public TextField houseNumberField;
    @FXML
    public TextField apartmentNumberField;
    @FXML
    public TextField facultyField;
    @FXML
    public Spinner<Integer> yearOfStudySpinner;
    @FXML
    public CheckBox scholarshipCheckBox;
    @FXML
    public TextField averageGradeField;
    @FXML
    public Button saveButton;
    @FXML
    public Button cancelButton;

    private final DatabaseController databaseController = new DatabaseController();
    private Integer editedStudentId = null;

    @FXML
    private void initialize() {
        setFieldFormatters();

        fillFormWithDefaultValues();

        saveButton.setOnAction(event -> {
            if (validateForm()) {
                var student = new Student(
                        editedStudentId,
                        nameField.getText(),
                        lastnameField.getText(),
                        indexField.getText(),
                        new Address(
                                cityField.getText(),
                                streetField.getText(),
                                Integer.parseInt(houseNumberField.getText()),
                                apartmentNumberField.getText().isEmpty() ? null : Integer.parseInt(apartmentNumberField.getText())
                        ),
                        facultyField.getText(),
                        yearOfStudySpinner.getValue(),
                        scholarshipCheckBox.isSelected(),
                        Double.parseDouble(averageGradeField.getText())
                );

                if (editedStudentId == null)
                    databaseController.insert(student);
                else
                    databaseController.update(student);

                closeForm();
            }
        });

        cancelButton.setOnAction(event -> {
            closeForm();
        });
    }

    private void closeForm() {
        Stage stage = (Stage) saveButton.getScene().getWindow();
        stage.close();
    }

    public void setEditedStudentId(Integer editedStudentId) {
        this.editedStudentId = editedStudentId;
        fillFormWithExistingStudent();
    }


    private boolean validateForm() {
        if (!areRequiredFieldsFilled())
            return false;

        if (indexField.getText().length() != 9) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Niepoprawny numer indeksu");
            alert.showAndWait();
            return false;
        }

        return true;
    }

    private void setFieldFormatters() {
        nameField.setTextFormatter(new TextFieldFormatter(120, "\\p{L}+"));
        lastnameField.setTextFormatter(new TextFieldFormatter(120, "\\p{L}+"));
        indexField.setTextFormatter(new TextFieldFormatter(9, "\\d+"));
        cityField.setTextFormatter(new TextFieldFormatter(120, "\\p{L}+"));
        streetField.setTextFormatter(new TextFieldFormatter(120, "\\p{L}+"));
        houseNumberField.setTextFormatter(new IntegerFieldFormatter(1, 9999));
        apartmentNumberField.setTextFormatter(new IntegerFieldFormatter(1, 9999));
        facultyField.setTextFormatter(new TextFieldFormatter(120, "\\p{L}+"));
        averageGradeField.setTextFormatter(new DoubleFieldFormatter(2.0, 5.0));
    }

    private void fillFormWithDefaultValues() {
        if (editedStudentId != null)
            return;

        nameField.setText("");
        lastnameField.setText("");
        indexField.setText("");
        cityField.setText("");
        streetField.setText("");
        houseNumberField.setText("");
        apartmentNumberField.setText("");
        facultyField.setText("");
        yearOfStudySpinner.getValueFactory().setValue(1);
        scholarshipCheckBox.setSelected(false);
        averageGradeField.setText("");
    }

    private void fillFormWithExistingStudent() {
        var student = databaseController.getStudentById(editedStudentId);

        nameField.setText(student.getName());
        lastnameField.setText(student.getLastname());
        indexField.setText(student.getIndexNumber());
        cityField.setText(student.getAddress().getCity());
        streetField.setText(student.getAddress().getStreet());
        houseNumberField.setText(student.getAddress().getHouseNumber().toString());

        var apartmentNumber = student.getAddress().getApartmentNumber();
        if (apartmentNumber == null || apartmentNumber == 0)
            apartmentNumberField.setText("");
        else
            apartmentNumberField.setText(student.getAddress().getApartmentNumber().toString());

        facultyField.setText(student.getFaculty());
        yearOfStudySpinner.getValueFactory().setValue(student.getYearOfStudy());
        scholarshipCheckBox.setSelected(student.getReceivedScholarship());
        averageGradeField.setText(student.getAverageGrade().toString());
    }

    private static class TextFieldFormatter extends TextFormatter<String> {
        public TextFieldFormatter(int maxLength, String allowedChars) {
            super(change -> {
                if (!change.getText().matches(allowedChars) && !change.getText().isEmpty()) {
                    change.setText("");
                    change.setRange(change.getRangeEnd(), change.getRangeStart());
                }

                if (change.getControlNewText().length() > maxLength) {
                    change.setText(change.getText().substring(0, maxLength));
                    change.setRange(change.getRangeEnd(), change.getRangeStart());
                }
                return change;
            });
        }
    }

    private static class IntegerFieldFormatter extends TextFormatter<String> {
        public IntegerFieldFormatter(int minValue, int maxValue) {
            super(change -> {
                if (!change.getText().matches("\\d+") && !change.getText().isEmpty()) {
                    change.setText("");
                    change.setRange(change.getRangeEnd(), change.getRangeStart());
                }

                if (!change.getControlNewText().isEmpty()) {
                    var value = Integer.parseInt(change.getControlNewText());
                    if (value < minValue || value > maxValue) {
                        change.setText("");
                        change.setRange(change.getRangeEnd(), change.getRangeStart());
                    }
                }

                return change;
            });
        }
    }

    private static class DoubleFieldFormatter extends TextFormatter<String> {
        public DoubleFieldFormatter(double minValue, double maxValue) {
            super(change -> {
                if (!change.getText().matches("\\d+(\\.\\d+)?") && !change.getText().isEmpty()) {
                    change.setText("");
                    change.setRange(change.getRangeEnd(), change.getRangeStart());
                }

                if (!change.getControlNewText().isEmpty()) {
                    var value = Double.parseDouble(change.getControlNewText());
                    if (value < minValue || value > maxValue) {
                        change.setText("");
                        change.setRange(change.getRangeEnd(), change.getRangeStart());
                    }
                }

                return change;
            });
        }
    }

    private Field getField(String name) {
        try {
            return this.getClass().getField(name);

        } catch (Exception e) {
            return null;
        }
    }

    private boolean areRequiredFieldsFilled() {
        Field[] requiredFields = new Field[]{
                getField("nameField"),
                getField("lastnameField"),
                getField("indexField"),
                getField("cityField"),
                getField("streetField"),
                getField("houseNumberField"),
                getField("facultyField"),
                getField("averageGradeField")
        };
        try {
            for (Field field : requiredFields) {
                if (field == null || field.get(this) == null || ((TextField) field.get(this)).getText().isEmpty()) {
                    ((javafx.scene.Node) field.get(this)).requestFocus();
                    return false;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return false;
        }

        return true;
    }
}
