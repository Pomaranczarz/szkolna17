package org.example.studentsdb.controllers;

import org.example.studentsdb.models.Address;
import org.example.studentsdb.models.Student;

import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DatabaseController {
    private static final String DB_URL = "jdbc:sqlite:" + System.getProperty("user.home") + "/students.db";
    private static final String CREATE_TABLE_STRING = "CREATE TABLE IF NOT EXISTS students ("
            + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "name VARCHAR(255) NOT NULL, "
            + "lastname VARCHAR(255) NOT NULL, "
            + "index_number VARCHAR(255) NOT NULL UNIQUE, "
            + "city VARCHAR(255) NOT NULL, "
            + "street VARCHAR(255) NOT NULL, "
            + "house_number INT NOT NULL, "
            + "apartment_number INT, "
            + "year_of_study INT NOT NULL, "
            + "faculty VARCHAR(255) NOT NULL, "
            + "received_scholarship BOOLEAN NOT NULL, "
            + "average_grade DOUBLE );";
    private static final String INSERT_STRING_FORMAT = "INSERT INTO students "
            + "(name, lastname, index_number, city, street, house_number, apartment_number, "
            + "year_of_study, faculty, received_scholarship, average_grade) VALUES (%s);";
    private static final String DELETE_BY_INDEX_STRING_FORMAT = "DELETE FROM students WHERE index_number = '%s';";
    private static final String UPDATE_STUDENT_WITH_ID_STRING_FORMAT = "UPDATE students SET %s WHERE id = %d;";

    public DatabaseController() {
        try (var connection = DriverManager.getConnection(DB_URL)) {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet tables = metaData.getTables(null, null, "students", null);
            if (!tables.next()) { // only create table if it doesn't exist
                Statement statement = connection.createStatement();
                statement.execute(CREATE_TABLE_STRING);
                statement.close();
            }
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public List<Student> getAll() {
        try (var connection = DriverManager.getConnection(DB_URL)) {
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("SELECT * FROM students");
            var students = new ArrayList<Student>();

            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String lastname = resultSet.getString("lastname");
                String indexNumber = resultSet.getString("index_number");
                String city = resultSet.getString("city");
                String street = resultSet.getString("street");
                Integer houseNumber = resultSet.getInt("house_number");
                Integer apartmentNumber = resultSet.getInt("apartment_number");
                Integer yearOfStudy = resultSet.getInt("year_of_study");
                String faculty = resultSet.getString("faculty");
                Boolean receivedScholarship = resultSet.getBoolean("received_scholarship");
                Double averageGrade = resultSet.getDouble("average_grade");

                var address = new Address(city, street, houseNumber, apartmentNumber);
                students.add(new Student(
                        id,
                        name,
                        lastname,
                        indexNumber,
                        address,
                        faculty,
                        yearOfStudy,
                        receivedScholarship,
                        averageGrade
                ));
            }

            return students;
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
        return null;
    }

    public Student getStudentById(Integer id) {
        try (var connection = DriverManager.getConnection(DB_URL)) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(String.format("SELECT * FROM students WHERE id = %d", id));
            if (resultSet.next()) {
                return new Student(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("lastname"),
                        resultSet.getString("index_number"),
                        new Address(
                                resultSet.getString("city"),
                                resultSet.getString("street"),
                                resultSet.getInt("house_number"),
                                resultSet.getInt("apartment_number")
                        ),
                        resultSet.getString("faculty"),
                        resultSet.getInt("year_of_study"),
                        resultSet.getBoolean("received_scholarship"),
                        resultSet.getDouble("average_grade")
                );
            }
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
        return null;
    }

    public void insert(Student student) {
        try (var connection = DriverManager.getConnection(DB_URL)) {
            Statement statement = connection.createStatement();
            System.out.printf((INSERT_STRING_FORMAT) + "%n", student.intoSqlInsertStatementValues());
            statement.execute(
                    String.format(INSERT_STRING_FORMAT, student.intoSqlInsertStatementValues())
            );

            statement.close();
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void delete(Student student) {
        try (var connection = DriverManager.getConnection(DB_URL)) {
            Statement statement = connection.createStatement();

            statement.execute(String.format(DELETE_BY_INDEX_STRING_FORMAT, student.getIndexNumber()));
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void update(Student student) {
        System.out.printf(
                (UPDATE_STUDENT_WITH_ID_STRING_FORMAT) + "%n",
                student.intoSqlUpdateStatementValues(),
                student.getId()
        );
        try (var connection = DriverManager.getConnection(DB_URL)) {
            Statement statement = connection.createStatement();

            statement.execute(
                    String.format(
                            UPDATE_STUDENT_WITH_ID_STRING_FORMAT,
                            student.intoSqlUpdateStatementValues(),
                            student.getId()
                    )
            );
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
    }
}
