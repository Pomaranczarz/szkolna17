package org.example.currencyconverter.models;

public class Currency {
    private final String name;
    private final String code;
    private final double rate;

    public Currency(String name, String code, double rate) {
        this.name = name;
        this.code = code;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return name + " (" + code + ")";
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public double getRate() {
        return rate;
    }
}