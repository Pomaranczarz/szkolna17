package org.example.currencyconverter.controllers;

import org.example.currencyconverter.models.Currency;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class CurrencyDataProvider {
    private static final String BASE_URL = "https://api.nbp.pl/api/exchangerates/tables/";

    public static List<Currency> getCurrencies() {
        try (HttpClient client = HttpClient.newHttpClient()) {
            List<Currency> currencies = new ArrayList<>();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(BASE_URL + "a?format=json"))
                    .GET()
                    .build();

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            JSONArray rates = new JSONArray(response.body())
                    .getJSONObject(0)
                    .getJSONArray("rates");

            for (int i = 0; i < rates.length(); i++) {
                JSONObject rate = rates.getJSONObject(i);
                String currency = rate.getString("currency");
                String code = rate.getString("code");
                double rateValue = rate.getDouble("mid");
                currencies.add(new Currency(currency, code, rateValue));
            }

            return currencies;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
