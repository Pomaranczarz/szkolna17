package org.example.currencyconverter.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import org.example.currencyconverter.models.Currency;

import java.util.Optional;

public class MainViewController {
    @FXML
    public TextField amountField;
    @FXML
    public ComboBox<Currency> fromCurrency;
    @FXML
    public ComboBox<Currency> toCurrency;
    @FXML
    public Label convertedValueLabel;
    @FXML
    public Label convertedValue;

    @FXML
    private Label welcomeText;

    private static final Currency BASE_CURRENCY = new Currency("złoty polski", "PLN", 1.0);

    public void initialize() {
        var currencies = CurrencyDataProvider.getCurrencies();
        currencies.add(BASE_CURRENCY);

        fromCurrency.getItems().addAll(currencies);
        toCurrency.getItems().addAll(currencies);

        amountField.setTextFormatter(new TextFormatter<>(change -> {
            String changeText = change.getControlNewText();

            if (!changeText.matches("\\d+\\.?\\d*") && !changeText.isEmpty()) {
                change.setText("");
                change.setRange(
                        change.getRangeStart(),
                        change.getRangeStart()
                );
            }
            return change;
        }));

        amountField.setOnAction(event -> {
            if (areCurrenciesValid())
                updateConvertedValue();
        });
    }

    @FXML
    public void onCurrencyChanged(ActionEvent actionEvent) {
        if (areCurrenciesValid())
            updateConvertedValue();
    }

    private Optional<Double> getAmount() {
        String amountText = amountField.getText();
        if (amountText.isEmpty()) {
            setErrorsOnAmountField();
            return Optional.empty();
        }

        clearErrorsOnAmountField();
        return Optional.of(Double.parseDouble(amountText));
    }

    private void setErrorsOnAmountField() {
        amountField.requestFocus();
        amountField.setStyle("-fx-prompt-text-fill: red");
    }

    private void clearErrorsOnAmountField() {
        amountField.setStyle(null);
    }

    private boolean areCurrenciesValid() {
        return fromCurrency.getValue() != null && toCurrency.getValue() != null;
    }

    private void updateConvertedValue() {
        Optional<Double> amount = getAmount();
        if (amount.isEmpty()) {
            convertedValue.setText("0.0");
            return;
        }

        Currency fromCurrency = this.fromCurrency.getValue();
        Currency toCurrency = this.toCurrency.getValue();

        convertedValue.setText(String.format("%.2f %s", convertValue(amount.get(), fromCurrency, toCurrency), toCurrency.getCode()));
    }

    private double convertValue(double amount, Currency from, Currency to) {
        return amount * from.getRate() / to.getRate();
    }
}