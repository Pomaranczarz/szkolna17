module org.example.currencyconverter {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.json;
    requires java.net.http;


    exports org.example.currencyconverter;
    opens org.example.currencyconverter.controllers to javafx.fxml;
}