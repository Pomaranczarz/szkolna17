module org.example {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    opens org.example.controllers to javafx.fxml;

    exports org.example;
}