package org.example.controllers;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;

import java.util.Optional;

public class NewAccountViewController {
    FinancialAccountsController financialAccountsController;
    @FXML
    public TextField accountHolderField;
    @FXML
    public TextField accountBalanceField;
    @FXML
    public Button submitBtn;

    private StringProperty accountHolder = new SimpleStringProperty("");
    private DoubleProperty accountBalance = new SimpleDoubleProperty();

    public void initialize() {
        accountHolderField.textProperty().bind(accountHolder);
        accountBalanceField.textProperty().bind(accountBalance.asString());

        accountBalanceField.setTextFormatter(new TextFormatter<>(change -> {
            if (!change.getText().matches("\\d+(\\.\\d+)?") && !change.getText().isEmpty()) {
                change.setText("");
                change.setRange(
                        change.getRangeStart(),
                        change.getRangeStart()
                );
            }
            return change;
        }));
    }

    public void setFinancialAccountsController(FinancialAccountsController financialAccountsController) {
        this.financialAccountsController = financialAccountsController;
    }

    public void handleCreateAccount(ActionEvent actionEvent) {
        var formData = isFormValid();

        formData.ifPresent(data -> {
            financialAccountsController.addAccount(data.holder, data.balance);

            Stage stage = (Stage) submitBtn.getScene().getWindow();
            stage.close();
        });
    }

    private record FormData(String holder, double balance) {
    }

    private Optional<FormData> isFormValid() {
        var holder = getAccountHolder();
        if (holder.isEmpty()) {
            accountHolderField.requestFocus();
            return Optional.empty();
        }

        var balance = getAccountBalance();
        if (balance.isEmpty()) {
            accountBalanceField.requestFocus();
            return Optional.empty();
        }

        return Optional.of(new FormData(holder.get(), balance.get()));
    }

    private Optional<String> getAccountHolder() {
        if (accountHolderField.getText().isEmpty())
            return Optional.empty();

        return Optional.of(accountHolderField.getText());
    }

    private Optional<Double> getAccountBalance() {
        String balanceText = accountBalanceField.getText();
        if (balanceText.isEmpty())
            return Optional.empty();

        try {
            if (balanceText.matches("\\d+(\\.\\d+)?"))
                return Optional.of(Double.parseDouble(balanceText));

            return Optional.empty();
        } catch (NumberFormatException e) {
            accountBalanceField.setText("");
            accountBalanceField.setPromptText("Number");
            return Optional.empty();
        }
    }
}
