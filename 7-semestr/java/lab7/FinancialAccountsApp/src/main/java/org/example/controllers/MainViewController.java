package org.example.controllers;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.example.models.FinancialAccount;

import java.io.IOException;

public class MainViewController {
    //Model
    FinancialAccount currentAccount;
    FinancialAccountsController financialAccountsController;

    //View nodes
    @FXML
    private Label accountHolder;
    @FXML
    private Label accountNumber;
    @FXML
    private Label accountBalance;
    @FXML
    public Label accountInterest;
    @FXML
    private TextField amountTextField;

    private static final double DEBT_LIMIT = -5000;
    private static final double INTEREST_RATE = 0.05;

    public void initialize() {
        try {
            financialAccountsController = new FinancialAccountsController();
            // we're assuming there's always at least 1 account
            currentAccount = financialAccountsController.getCurrentAccount().orElseThrow();

            // link Model with View
            bindAccountToView(currentAccount);

            //link Controller to View - ensure only numeric input (integers) in text field
            amountTextField.setTextFormatter(new TextFormatter<>(change -> {
                String changeText = change.getControlNewText();

                if (!changeText.matches("\\d+\\.?\\d*") && !changeText.isEmpty()) {
                    change.setText("");
                    change.setRange(
                            change.getRangeStart(),
                            change.getRangeStart()
                    );
                }
                return change;
            }));
        } catch (Exception e) {
            makeAlert(e.getLocalizedMessage());
        }
    }

    private void bindAccountToView(FinancialAccount account) {
        accountHolder.textProperty().bind(account.accountHolderProperty());
        accountBalance.textProperty().bind(account.accountBalanceProperty().asString());
        accountNumber.textProperty().bind(account.accountNumberProperty().asString());

        accountInterest.textProperty().bind(
                Bindings.when(account.accountBalanceProperty().lessThanOrEqualTo(0)) // if balance is less than or equal to 0
                        .then("0.00")  // do not compute interest
                        .otherwise(Bindings.createStringBinding(() ->
                                        String.format("%.2f", account.accountBalanceProperty().multiply(INTEREST_RATE).get()),
                                account.accountBalanceProperty()))  // format the interest with two decimal places
        );
    }

    @FXML
    private void handleDeposit(Event event) {
        currentAccount.deposit(getAmount());
        event.consume();
    }

    @FXML
    private void handleWithdrawal(Event event) {
        double amount = getAmount();
        if (currentAccount.getAccountBalance() - amount < DEBT_LIMIT) {
            makeAlert("Debt limit of " + DEBT_LIMIT + " exceeded");
            return;
        }

        currentAccount.withdraw(getAmount());
        event.consume();
    }

    private void makeAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message);

        alert.showAndWait();
    }

    private double getAmount() {
        String amountText = amountTextField.getText();
        if (amountText.isEmpty()) {
            amountTextField.requestFocus();
            return 0;
        }

        return Double.parseDouble(amountText);
    }

    public void handlePreviousAccount(ActionEvent actionEvent) {
        var nextAccount = financialAccountsController.previousAccount();
        if (nextAccount.isEmpty()) {
            makeAlert("No previous account");
            return;
        }
        currentAccount = nextAccount.get();
        bindAccountToView(currentAccount);
    }

    public void handleNextAccount(ActionEvent actionEvent) {
        var nextAccount = financialAccountsController.nextAccount();
        if (nextAccount.isEmpty()) {
            makeAlert("No next account");
            return;
        }
        currentAccount = nextAccount.get();
        bindAccountToView(currentAccount);
    }

    public void handleAddAccount(ActionEvent actionEvent) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/newAccount.fxml"));
            root = loader.load();

            NewAccountViewController controller = loader.getController();
            controller.setFinancialAccountsController(financialAccountsController);

            Stage stage = new Stage();
            stage.setTitle("Add Account");
            stage.setScene(new Scene(root, 300, 300));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initializeWindowCloseHandler(Stage stage) {
        stage.setOnCloseRequest(e -> {
            financialAccountsController.saveAccountsData();
        });
    }
}