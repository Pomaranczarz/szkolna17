package org.example.controllers;

import org.example.models.FinancialAccount;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Optional;

public class FinancialAccountsController {
    private static final Path DATA_PATH = Paths.get(System.getProperty("user.home"), ".bank_account_data", "accounts.txt");
    private final ArrayList<FinancialAccount> accounts;
    private int currentAccountIndex = 0;
    private int maxAccountNumber;

    public FinancialAccountsController() {
        ensureCorrectPath();
        var loadedData = loadAccountsData();
        if (loadedData.isEmpty())
            throw new RuntimeException("Could not load accounts data");

        accounts = loadedData.get();
        maxAccountNumber = accounts.stream().mapToInt(FinancialAccount::getAccountNumber).max().orElse(0);
    }

    public Optional<FinancialAccount> getCurrentAccount() {
        if (accounts == null || accounts.isEmpty())
            return Optional.empty();

        return Optional.of(accounts.get(currentAccountIndex));
    }

    public Optional<FinancialAccount> nextAccount() {
        currentAccountIndex = (currentAccountIndex + 1) % accounts.size();
        return getCurrentAccount();
    }

    public Optional<FinancialAccount> previousAccount() {
        currentAccountIndex = (currentAccountIndex - 1 + accounts.size()) % accounts.size();
        return getCurrentAccount();
    }

    public boolean addAccount(String holder, double balance) {
        if (accounts == null)
            return false;

        accounts.add(new FinancialAccount(holder, ++maxAccountNumber, balance));
        return true;
    }

    public boolean removeCurrentAccount() {
        if (accounts == null || accounts.isEmpty())
            return false;

        accounts.remove(currentAccountIndex);

        if (currentAccountIndex >= accounts.size()) // ensure index stays in bounds
            currentAccountIndex = accounts.size() - 1;

        return true;
    }

    private static Optional<ArrayList<FinancialAccount>> loadAccountsData() {
        try (var reader = new BufferedReader(new FileReader(DATA_PATH.toFile()))) {
            var localAccounts = new ArrayList<FinancialAccount>();
            String line;

            while ((line = reader.readLine()) != null) {
                var accountData = line.split(";");
                localAccounts.add(new FinancialAccount(accountData[0], Integer.parseInt(accountData[1]), Double.parseDouble(accountData[2])));
            }
            return Optional.of(localAccounts);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }

        return Optional.empty();
    }

    private void ensureCorrectPath() {
        try {
            if (Files.notExists(DATA_PATH.getParent())) {
                Files.createDirectories(DATA_PATH.getParent());
            }

            if (Files.notExists(DATA_PATH)) {
                try {
                    DATA_PATH.toFile().createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public void saveAccountsData() {
        try (var writer = new BufferedWriter(new FileWriter(DATA_PATH.toFile()))) {
            for (FinancialAccount account : accounts) {
                writer.write(account.getAccountHolder() + ";" + account.getAccountNumber() + ";" + account.getAccountBalance() + "\n");
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }
}
