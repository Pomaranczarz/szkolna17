package org.example;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class StudentLombok {
    private String name;
    private String lastname;
    private String email;
    private String index;
}
