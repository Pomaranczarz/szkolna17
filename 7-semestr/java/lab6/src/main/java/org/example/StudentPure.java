package org.example;

public class StudentPure {
    private String name;
    private String lastname;
    private String email;
    private String index;

    public StudentPure(String name, String lastname, String email, String index) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.index = index;
    }

    public StudentPure() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "StudentPure{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", index='" + index + '\'' +
                '}';
    }
}
