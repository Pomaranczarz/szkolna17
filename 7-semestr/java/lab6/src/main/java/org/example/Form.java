package org.example;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Scanner;

@RequiredArgsConstructor
@ToString
@Data
public class Form {
    @NonNull
    private String username;

    @NonNull
    private String password;

    public static Form makeForm() {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter username: ");
        String username = sc.nextLine();

        System.out.print("Enter password: ");
        String password = sc.nextLine();

        return new Form(username, password);
    }
}
