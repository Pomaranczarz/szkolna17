package org.example;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<StudentPure> studentsPure = List.of(
                new StudentPure("John", "Doe", "jdoe@me.com", "12345"),
                new StudentPure("Jane", "Doe", "jdoe@me.com", "54321"),
                new StudentPure("Marc", "Stark", "mstark@me.com", "23451"),
                new StudentPure("Peter", "Parker", "pparker@me.com", "34512")
        );

        for (StudentPure student : studentsPure) {
            System.out.println(student);
        }

        List<StudentLombok> studentsLombok = List.of(
                new StudentLombok("John", "Doe", "jdoe@me.com", "12345"),
                new StudentLombok("Jane", "Doe", "jdoe@me.com", "54321"),
                new StudentLombok("Marc", "Stark", "mstark@me.com", "23451"),
                new StudentLombok("Peter", "Parker", "pparker@me.com", "34512")
        );

        for (StudentLombok student : studentsLombok) {
            System.out.println(student);
        }

        Form form = Form.makeForm();
        System.out.println(form);
    }
}