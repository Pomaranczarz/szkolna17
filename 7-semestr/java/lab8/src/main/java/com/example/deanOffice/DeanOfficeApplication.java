package com.example.deanOffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeanOfficeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeanOfficeApplication.class, args);
	}

}
