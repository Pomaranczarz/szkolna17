package com.example.deanOffice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DatabaseController {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Student> getAll() {
        return jdbcTemplate.query(
                "SELECT * FROM student",
                BeanPropertyRowMapper.newInstance(Student.class)
        );
    }

    public Student getById(final int id) {
        return jdbcTemplate.queryForObject(
                "SELECT id, name, surname, averageGrade FROM student WHERE id = ?",
                BeanPropertyRowMapper.newInstance(Student.class),
                id
        );
    }

    public int save(final Iterable<Student> students) {
        students.forEach(student -> {
            this.jdbcTemplate.update(
                    "INSERT INTO student (name, surname, averageGrade) VALUES (?, ?, ?)",
                    student.getName(),
                    student.getSurname(),
                    student.getAverageGrade()
            );
        });
        return 1;
    }

    public int update(final Student student) {
        return jdbcTemplate.update(
                "UPDATE student SET name = ?, surname = ?, averageGrade = ? WHERE id = ?",
                student.getName(),
                student.getSurname(),
                student.getAverageGrade(),
                student.getId()
        );
    }

    public int delete(final int id) {
        return jdbcTemplate.update(
                "DELETE FROM student WHERE id = ?",
                id
        );
    }
}
