package com.example.deanOffice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/student")
@RestController
public class RESTController {
    @Autowired
    DatabaseController databaseController;

    @GetMapping("/")
    public List<Student> getAll() {
        return databaseController.getAll();
    }

    @GetMapping("/{id}")
    public Student getById(@PathVariable("id") final int id) {
        return databaseController.getById(id);
    }

    @PostMapping("")
    public int add(@RequestBody final List<Student> students) {
        return databaseController.save(students);
    }

    @PutMapping("/{id}")
    public int update(@PathVariable("id") final int id, @RequestBody final Student updatedStudent) {
        final var student = this.databaseController.getById(id);
        if (student != null) {
            student.setName(updatedStudent.getName());
            student.setSurname(updatedStudent.getSurname());
            student.setAverageGrade(updatedStudent.getAverageGrade());

            databaseController.update(student);
            return 1;
        } else {
            return -1;
        }
    }

    @PatchMapping("/{id}")
    public int partiallyUpdate(@PathVariable("id") final int id, @RequestBody final Student updatedStudent) {
        final var student = databaseController.getById(id);
        if (student != null) {
            if (updatedStudent.getName() != null) student.setName(updatedStudent.getName());
            if (updatedStudent.getSurname() != null) student.setSurname(updatedStudent.getSurname());
            if (updatedStudent.getAverageGrade() >= 0.0) student.setAverageGrade(updatedStudent.getAverageGrade());

            databaseController.update(student);
            return 1;
        } else {
            return -1;
        }
    }

    @DeleteMapping("/{id}")
    public int delete(@PathVariable("id") final int id) {
        return databaseController.delete(id);
    }
}
