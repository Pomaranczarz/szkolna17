package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookBuilder {

    private String title = null;
    private String author = null;
    private Integer pages = null;
    private Integer year = null;

    private final ArrayList<String> errors = new ArrayList<>();

    public BookBuilder setTitle(String title) {
        if (title == null || title.isEmpty())
            errors.add("Empty title");
        else
            this.title = title;
        return this;
    }

    public BookBuilder setAuthor(String author) {
        if (author == null || author.isEmpty())
            errors.add("Empty author");
        else
            this.author = author;
        return this;
    }

    public BookBuilder setPages(String pages) {
        try {
            this.pages = Integer.parseInt(pages);
            if (this.pages <= 0)
                errors.add("Pages should be greater than 0");
        } catch (Exception e) {
            errors.add("Pages should be a number");
        }

        return this;
    }

    public BookBuilder setYear(String year) {
        try {
            this.year = Integer.parseInt(year);
            if (this.year <= 0)
                errors.add("Year should be greater than 0");
        } catch (Exception e) {
            errors.add("Year should be a number");
        }

        return this;
    }

    public Optional<Book> build() {
        checkIfAllFieldsAreSet();

        if (!errors.isEmpty())
            return Optional.empty();

        return Optional.of(new Book(title, author, pages, year));
    }

    private void checkIfAllFieldsAreSet() {
        if (title == null)
            errors.add("Title is not set");

        if (author == null)
            errors.add("Author is not set");

        if (pages == null)
            errors.add("Pages is not set");

        if (year == null)
            errors.add("Year is not set");
    }

    public List<String> getErrors() {
        return errors;
    }

    public void reset() {
        title = null;
        author = null;
        pages = null;
        year = null;
        errors.clear();
    }
}
