package org.example;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class XMLBookReader {
    public static List<Book> readXML(String filename) {
        List<Book> books = new ArrayList<>();
        BookBuilder bookBuilder = new BookBuilder();

        XMLInputFactory factory = XMLInputFactory.newInstance();

        try (var fileReader = new FileInputStream(filename)) {
            XMLStreamReader xmlReader = factory.createXMLStreamReader(fileReader);

            while (xmlReader.hasNext()) {
                int event = xmlReader.next();

                if (event == XMLStreamReader.START_ELEMENT) {
                    String elementName = xmlReader.getLocalName();

                    switch (elementName) {
                        case "title": // <title>
                            bookBuilder.setTitle(xmlReader.getElementText());
                            break;
                        case "author": // <author>
                            bookBuilder.setAuthor(xmlReader.getElementText());
                            break;
                        case "pages": // <pages>
                            bookBuilder.setPages(xmlReader.getElementText());
                            break;
                        case "year": // <year>
                            bookBuilder.setYear(xmlReader.getElementText());
                            break;
                    }
                } else if (event == XMLStreamReader.END_ELEMENT) { // </book>
                    if ("Book".equals(xmlReader.getLocalName())) {
                        var book = bookBuilder.build();
                        if (book.isPresent())
                            books.add(book.get());
                        else {
                            System.out.println("Errors parsing XML:");
                            bookBuilder.getErrors().forEach(System.out::println);
                        }
                        bookBuilder.reset();
                    }
                }
            }
            xmlReader.close();
        } catch (Exception e) {
            System.out.println("Błąd danych w pliku XML: " + e.getMessage());
            return null;
        }

        return books;
    }
}
