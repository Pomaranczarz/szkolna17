package org.example;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    private static final List<Book> Books = new ArrayList<>(Arrays.asList(
            new Book("The Great Gatsby", "F. Scott Fitzgerald", 180, 1925),
            new Book("To Kill a Mockingbird", "Harper Lee", 281, 1960),
            new Book("Pride and Prejudice", "Jane Austen", 432, 1813),
            new Book("1984", "George Orwell", 328, 1949),
            new Book("The Catcher in the Rye", "J. D. Salinger", 277, 1951),
            new Book("Moby Dick", "Herman Melville", 635, 1851)
    ));

    public static void main(String[] args) {
        writeXML(Books);

        var books = XMLBookReader.readXML("books.xml");
        if (books != null)
            books.forEach(System.out::println);
    }

    private static void writeXML(List<Book> books) {
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        try {
            XMLStreamWriter xmlWriter = factory.createXMLStreamWriter(new FileOutputStream("books.xml"));
            xmlWriter.writeStartDocument("UTF-8", "1.0");
            xmlWriter.writeStartElement("Books");
            for (Book book : books)
                book.serializeInto(xmlWriter);

            xmlWriter.writeEndElement();
            xmlWriter.writeEndDocument();
            xmlWriter.flush();
            xmlWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}