package org.example;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class XMLDeserializer {
    private static final HashMap<Class<?>, Class<?>> Primitives = new HashMap<>();


    public static <T> List<T> deserialize(ByteArrayInputStream xmlData, Class<T> clazz) {
        setupPrimitives();
        List<T> result = new ArrayList<>();

        XMLInputFactory factory = XMLInputFactory.newInstance();
        var fields = clazz.getDeclaredFields();
        T currentObject = null;

        try {
            XMLStreamReader xmlReader = factory.createXMLStreamReader(xmlData);

            while (xmlReader.hasNext()) {
                int event = xmlReader.next();

                if (event == XMLStreamReader.START_ELEMENT) {
                    String elementName = xmlReader.getLocalName();

                    if (elementName.equals(clazz.getSimpleName())) {
                        currentObject = clazz.newInstance();
                    } else {
                        for (Field field : fields) {
                            if (elementName.equals(field.getName())) {
                                field.setAccessible(true);
                                if (field.getType() != String.class) {
                                    Class<?> destType = Primitives.getOrDefault(field.getType(), field.getType());

                                    String stringValue = xmlReader.getElementText();
                                    Object value = convertValue(stringValue, destType);
                                    System.out.println("stringValue: " + stringValue + " type: " + destType + " value: " + value);
                                    if (value != null)
                                        field.set(currentObject, value);

                                    break;
                                }
                            }
                        }
                    }
                } else if (event == XMLStreamReader.END_ELEMENT) {
                    if (currentObject != null) {
                        result.add(currentObject);
                        currentObject = null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private static Object convertValue(String value, Class<?> type) {


        if (type == String.class) {
            return value;
        } else if (type == Integer.class) {
            return Integer.parseInt(value);
        } else if (type == Boolean.class) {
            return Boolean.parseBoolean(value);
        } else if (type == Double.class) {
            return Double.parseDouble(value);
        } else if (type == Long.class) {
            return Long.parseLong(value);
        } else if (type == Float.class) {
            return Float.parseFloat(value);
        } else if (type == Short.class) {
            return Short.parseShort(value);
        } else if (type == Byte.class) {
            return Byte.parseByte(value);
        } else if (type == Character.class) {
            return value.charAt(0);
        }
        return null;
    }

    private static void setupPrimitives() {
        Primitives.put(int.class, Integer.class);
        Primitives.put(boolean.class, Boolean.class);
        Primitives.put(double.class, Double.class);
        Primitives.put(long.class, Long.class);
        Primitives.put(float.class, Float.class);
        Primitives.put(short.class, Short.class);
        Primitives.put(byte.class, Byte.class);
        Primitives.put(char.class, Character.class);
    }
}
