public without sharing class CountryTaxRateController {

    public CountryTaxRateController() {
    }               

    @AuraEnabled(cacheable=true)
    public static List<Country_Tax_Rate__c> getCountries() {

        return [
            SELECT Id, Name, Country__c, Tax__c
            FROM Country_Tax_Rate__c
            WHERE Country__c != null
        ];
    }
}