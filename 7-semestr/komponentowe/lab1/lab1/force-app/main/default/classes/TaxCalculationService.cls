public with sharing class TaxCalculationService {
    private static final Decimal TAX_PERCENTAGE = 23.0;

    public TaxCalculationService() {

    }

    public void calculateTax(Invoice__c inv) {
        inv.Tax__c = TAX_PERCENTAGE;
        inv.TaxAmount__c = inv.Amount__c * TAX_PERCENTAGE / 100;
    }

    public void calculateTaxCountryRate(Invoice__c inv) {
        Country_Tax_Rate__c[] results;
        results = [SELECT Id, Country__c, Tax__c FROM Country_Tax_Rate__c WHERE Country__c = :inv.Country__c];

        if (results.size() > 0) {
            Double percentage = results[0].Tax__c;
            inv.Tax__c = percentage;
            inv.TaxAmount__c = inv.Amount__c * percentage / 100;
        }
        else {
            inv.Tax__c = 0;
            inv.TaxAmount__c = 0;
        }
    }

    public void calculateTaxesCountryRate(Invoice__c[] invoices) {
        Set<String> countries = new Set<String>();
        for (Invoice__c inv : invoices) 
            countries.add(inv.Country__c);

        Country_Tax_Rate__c[] results;
        results = [SELECT Id, Country__c, Tax__c FROM Country_Tax_Rate__c WHERE Country__c IN :countries];

        for (Country_Tax_Rate__c result : results) {
            for (Invoice__c inv : invoices) {
                if (inv.Country__c == result.Country__c) {
                    inv.Tax__c = result.Tax__c;
                    inv.TaxAmount__c = inv.Amount__c * result.Tax__c / 100;
                }
            }
        }
    }
}