import { LightningElement, api, wire } from 'lwc';

import { getRecord, getFieldValue } from "lightning/uiRecordApi";
import INVOICE_OBJECT from "@salesforce/schema/Invoice__c";
import AMOUNT_FIELD from "@salesforce/schema/Invoice__c.Amount__c";
import COUNTRY_CODE_FIELD from "@salesforce/schema/Invoice__c.Country__c";
import getCountries from '@salesforce/apex/CountryTaxRateController.getCountries';

export default class CountryTaxRateComponent extends LightningElement {
    @api recordId;

    invoiceObject = INVOICE_OBJECT;

    @wire(getRecord, { recordId: "$recordId", fields: [AMOUNT_FIELD, COUNTRY_CODE_FIELD] })

    record;
    get countryCode() {
        return this.record.data ? getFieldValue(this.record.data, COUNTRY_CODE_FIELD) : "";
    }

    get amount() {
        return this.record.data ? getFieldValue(this.record.data, AMOUNT_FIELD) : "";
    }

    @wire(getCountries)
    countries;

    get countryOptions() {
        if (this.countries.data) {
            return this.countries.data.map(c => { return { label: c.Name, value: c.Tax__c } });
        } else {
            return [];
        }
    }

    theTax;
    countrySelected(event) {
        this.theTax = event.detail.value;
    }

    get taxAmount() {
        if (this.theTax && this.record.data) {
            return this.amount * this.theTax / 100;
        } else {
            return 0;
        }
    }
}