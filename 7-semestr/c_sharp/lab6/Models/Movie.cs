using System.ComponentModel.DataAnnotations;

namespace lab5.Models;

public class Movie
{
    [Key]
    public int Id { get; set; }

    [Required]
    [MaxLength(50, ErrorMessage = "Tytuł powinien mieć maksymalnie 50 znaków")]
    public string? Title { get; set; }

    [UIHint("LongText")]
    [Required(ErrorMessage = "Opis jest wymagany")]
    public string? Description { get; set; }

    [UIHint("Stars")]
    [Range(1, 5, ErrorMessage = "Ocena filmu musi być liczbą pomiędzy 1 a 5")]
    public int Rating { get; set; }

    [Required(ErrorMessage = "Link do trailera jest wymagany")]
    public string? TrailerLink { get; set; }

    public Genre Genre { get; set; }
}