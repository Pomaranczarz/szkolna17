using lab8.Data;
using lab8.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;



namespace lab8.Controllers;

[Route("api/fox")]
[ApiController]
public class FoxController : ControllerBase
{
    IFoxesRepository _repo;
    public FoxController(IFoxesRepository repo)
    {
        _repo = repo;
    }

    [HttpGet]
    public IActionResult Get()
    {
        return Ok(
            _repo.GetAll()
                .OrderByDescending(x => x.Loves)
                .ThenBy(x => x.Hates)
        );
    }

    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
        var fox = _repo.Get(id);
        if (fox == null)
        {
            return NotFound();
        }
        return Ok(fox);
    }

    [HttpPost]
    [Authorize]
    public IActionResult Post([FromBody] Fox fox)
    {
        _repo.Add(fox);

        return CreatedAtAction(
            nameof(Get),
            new { id = fox.Id },
            fox
        );
    }

    [HttpPut("love/{id}")]
    public IActionResult Love(int id)
    {
        var fox = _repo.Get(id);
        if (fox == null)
            return NotFound();

        fox.Loves++;
        _repo.Update(id, fox);

        return Ok(fox);
    }

    [HttpPut("hate/{id}")]
    public IActionResult Hate(int id)
    {
        var fox = _repo.Get(id);
        if (fox == null)
            return NotFound();

        fox.Hates++;
        _repo.Update(id, fox);

        return Ok(fox);
    }
}
