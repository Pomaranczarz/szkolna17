namespace lab8.Data;

using Models;

public interface IFoxesRepository
{
    void Add(Fox fox);
    Fox Get(int id);
    IEnumerable<Fox> GetAll();
    void Update(int id, Fox f);
}