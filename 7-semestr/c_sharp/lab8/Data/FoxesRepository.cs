using lab8.Models;
using LiteDB;

namespace lab8.Data;

public class FoxesRepository : IFoxesRepository
{
    private readonly LiteDatabase db;

    public FoxesRepository()
    {
        db = new(@"foxes.litedb");
    }

    public void Add(Fox fox)
    {
        var all = db.GetCollection<Fox>();
        fox.Id = all.Count() > 0 ? all.Max(x => x.Id) + 1 : 1;
        db.GetCollection<Fox>().Insert(fox);
    }

    public Fox Get(int id)
    {
        return db.GetCollection<Fox>().FindById(id);
    }

    public IEnumerable<Fox> GetAll()
    {
        return db.GetCollection<Fox>().FindAll();
    }

    public void Update(int id, Fox f)
    {
        var c = db.GetCollection<Fox>().FindById(id);
        c.Loves = f.Loves;
        c.Hates = f.Hates;
        db.GetCollection<Fox>().Update(c);
    }
}