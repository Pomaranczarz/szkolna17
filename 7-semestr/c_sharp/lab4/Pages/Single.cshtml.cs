using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace lab4.Pages
{
    public class SingleModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        [BindProperty(SupportsGet = true)]
        public string? Image { get; set; }
        private readonly string imagesDir;

        public SingleModel(ILogger<IndexModel> logger, IWebHostEnvironment environment)
        {
            _logger = logger;
            imagesDir = Path.Combine(environment.WebRootPath, "images");
        }

        public IActionResult OnGet() => System.IO.File.Exists(Path.Combine(imagesDir, Image)) ? Page() : NotFound();
    }
}
