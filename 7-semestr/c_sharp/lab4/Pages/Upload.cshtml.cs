using ImageMagick;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace lab4.Pages
{
    public class UploadModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        [BindProperty]
        public IFormFile? Upload { get; set; }
        private readonly string imagesDir;

        private readonly MagickImage watermark = new("watermark.png");

        public UploadModel(ILogger<IndexModel> logger, IWebHostEnvironment environment)
        {
            _logger = logger;
            imagesDir = Path.Combine(environment.WebRootPath, "images");
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (Upload != null)
                await SaveUploadedImageWithWatermarkAsync();

            return RedirectToPage("Index");
        }

        private async Task SaveUploadedImageWithWatermarkAsync()
        {
            var extension = GetUploadedFileExtension(Upload!.ContentType);
            var filename = Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + extension;

            using var image = new MagickImage(Upload.OpenReadStream());

            watermark.Evaluate(Channels.Alpha, EvaluateOperator.Divide, 4);
            image.Composite(watermark, Gravity.Southeast, CompositeOperator.Over);

            await image.WriteAsync(Path.Combine(imagesDir, filename));
        }

        private static string GetUploadedFileExtension(string uploadContentType)
        {
            return uploadContentType switch
            {
                "image/png" => ".png",
                "image/gif" => ".gif",
                _ => ".jpg"
            };
        }
    }
}