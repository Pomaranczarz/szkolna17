﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lab7.Models;

public partial class InvoiceLine
{
    public int InvoiceLineId { get; set; }

    public int InvoiceId { get; set; }

    [Display(Name = "Track ID")]
    public int TrackId { get; set; }

    [Display(Name = "Price")]
    public double UnitPrice { get; set; }

    public int Quantity { get; set; }

    public virtual Invoice Invoice { get; set; } = null!;

    public virtual Track Track { get; set; } = null!;
}
