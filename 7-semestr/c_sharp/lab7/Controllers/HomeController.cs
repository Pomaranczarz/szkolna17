using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using lab7.Models;
using lab7.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace lab7.Controllers
{
    public class HomeController(ILogger<HomeController> logger, ChinookDbContext chinook, UserManager<ApplicationUser> userManager) : Controller
    {
        private readonly ILogger<HomeController> _logger = logger;
        private readonly ChinookDbContext _chinook = chinook;
        private readonly UserManager<ApplicationUser> _userManager = userManager;

        public IActionResult Index()
        {
            return View(_chinook.Customers.ToList());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize]
        public async Task<IActionResult> MyOrders()
        {
            var user = await _userManager.GetUserAsync(User);
            var customerId = user.CustomerId;
            return View(await _chinook.Invoices.Where(x => x.CustomerId == customerId).ToListAsync());
        }

        [Authorize]
        public async Task<IActionResult> OrderDetails(int id)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var customerId = user.CustomerId;

                var invoice = _chinook.Invoices
                    .Include(x => x.InvoiceLines)
                    .ThenInclude(x => x.Track)
                    .FirstOrDefault(x => x.InvoiceId == id);

                if (invoice.CustomerId != customerId)
                {
                    return Forbid();
                }

                return View(invoice);
            }
            catch (Exception ex)
            {
                return Forbid();
            }
        }
    }
}
