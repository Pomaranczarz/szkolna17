using Microsoft.AspNetCore.Identity;

namespace lab7;

public class ApplicationUser : IdentityUser
{
    public long CustomerId { get; set; }
}