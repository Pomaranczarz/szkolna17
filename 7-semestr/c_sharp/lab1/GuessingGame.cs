using System.Text.Json;

namespace Lab1
{
    internal class GuessingGame
    {
        private List<HighScore> highScores = [];
        private static readonly string HighScoresFileName = "highscores.json";

        public GuessingGame()
        {
        }

        public void Run()
        {
            LoadHighScores();

            while (true)
            {
                PlayGame();
                ShowHighScores();

                Console.WriteLine("Czy chcesz zagrać ponownie? [t/n]");
                if (Console.ReadLine() != "t")
                    break;
            }

            SaveHighScores();
        }

        private void LoadHighScores()
        {
            if (File.Exists(HighScoresFileName))
            {
                string json = File.ReadAllText(HighScoresFileName);
                try
                {
                    highScores = JsonSerializer.Deserialize<List<HighScore>>(json) ?? [];
                }
                catch (Exception)
                {
                    highScores = [];
                }
            }
        }

        private void SaveHighScores()
        {
            string json = JsonSerializer.Serialize(highScores);
            File.WriteAllText(HighScoresFileName, json);
        }

        private void PlayGame()
        {
            Random random = new();
            var randomNumber = random.Next(1, 101);
            var tryCount = 0;

            while (true)
            {
                Console.WriteLine("Zgadnij liczbę z zakresu 1-100:");

                if (int.TryParse(Console.ReadLine(), out var guess))
                {
                    if (guess == randomNumber)
                        break;
                    else if (guess > randomNumber)
                        Console.WriteLine("Zbyt duża liczba!");
                    else if (guess < randomNumber)
                        Console.WriteLine("Zbyt mała liczba!");
                }
                ++tryCount;
            }

            Console.WriteLine($"Gratulacje! [Liczba prób: {tryCount}]");
            string? nick;
            do
            {
                Console.WriteLine("Podaj swój nick:");
                nick = Console.ReadLine();
            } while (nick == null || nick.Length == 0);

            highScores.Add(new HighScore { Name = nick, Trials = tryCount });
            SaveHighScores();
        }

        private void ShowHighScores()
        {
            foreach (var highScore in highScores.OrderBy(x => x.Trials))
                Console.WriteLine($"{highScore.Name,-10} --- {highScore.Trials,3} prób");
        }
    }
}