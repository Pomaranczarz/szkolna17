using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using lab3.Models;
using System;

namespace lab3.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly PhoneBookService _phoneBook;

    public HomeController(ILogger<HomeController> logger, PhoneBookService phoneBook)
    {
        _logger = logger;
        _phoneBook = phoneBook;
    }

    public IActionResult Index()
    {
        var random = new Random();
        ViewData["random"] = random.NextDouble();

        return View();
    }

    public IActionResult Index2()
    {
        return View(_phoneBook.GetContacts());
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    public IActionResult Create() => View();

    [HttpPost]
    public IActionResult Create(Contact contact)
    {
        if (ModelState.IsValid)
        {
            _phoneBook.Add(contact);
            return RedirectToAction("Index");
        }

        return View();
    }

    public IActionResult Delete(int id)
    {
        if (_phoneBook.Remove(id))
            return RedirectToAction("Index2");

        return NotFound();
    }
}
