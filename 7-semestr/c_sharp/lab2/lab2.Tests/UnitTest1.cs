namespace Lab2.Tests
{
    public class PriceFormatter_Tests
    {
        [Fact]
        public void FormatUsdPrice_ProperFormat_ShouldReturnProperString()
        {
            var data = 0.05m;
            var result = PriceFormatter.FormatUsdPrice(data);
            Assert.Equal("$0.05", result);
        }
    }

    public class Fruit_Tests
    {
        [Fact]
        public void Fruit_ProperFormat_ToString()
        {
            UsdCourse.Current = 1m;
            var fruit = new Fruit { Name = "apple", Price = 1m, IsSweet = true };

            Assert.Equal("Fruit: Name=apple\tIsSweet=True\tPrice=1,00 zł\tUsdPrice=$1.00", fruit.ToString());
        }
    }
}