namespace Lab2
{
    public class Fruit
    {
        public string? Name { get; set; }
        public bool IsSweet { get; set; }
        public decimal Price { get; set; }
        public decimal UsdPrice => Price / UsdCourse.Current;

        public static Fruit Create()
        {
            string[] names = [
                "Apple", "Banana", "Orange", "Pear", "Grape", "Strawberry",
                "Cherry", "Peach", "Watermelon", "Pineapple", "Papaya",
                "Kiwi", "Lemon", "Pomegranate", "Apricot", "Plum",
            ];

            var rand = new Random();

            return new Fruit
            {
                Name = names[rand.Next(names.Length)],
                IsSweet = rand.NextDouble() > 0.5,
                Price = (decimal)(rand.NextDouble() * 10)
            };
        }

        public override string? ToString()
        {
            return $"Fruit: Name={Name}"
                + $"\tIsSweet={IsSweet}"
                + $"\tPrice={PriceFormatter.FormatPlnPrice(Price)}"
                + $"\tUsdPrice={PriceFormatter.FormatUsdPrice(UsdPrice)}";
        }
    }
}