namespace Lab2
{
    using System.Globalization;
    using System.Xml.Linq;

    public class UsdCourse
    {
        public static decimal Current = 0;

        public async static Task<decimal> GetUsdCourseAsync()
        {
            using var wc = new HttpClient();

            // http://api.nbp.pl/#kursyWalut
            var response = await wc.GetAsync("http://api.nbp.pl/api/exchangerates/rates/A/USD?format=xml");

            if (!response.IsSuccessStatusCode)
                throw new InvalidOperationException();

            XDocument xdoc = XDocument.Parse(await response.Content.ReadAsStringAsync());

            var midNode = xdoc.Descendants().Where(node => node.Name == "Mid").First();
            if (midNode != null)
                return Convert.ToDecimal(midNode.Value, new CultureInfo("en-US"));

            throw new InvalidOperationException();
        }

    }
}