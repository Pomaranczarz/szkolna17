﻿using Lab2;


UsdCourse.Current = await UsdCourse.GetUsdCourseAsync();

List<Fruit> fruits = [];
for (int i = 0; i < 15; ++i)
    fruits.Add(Fruit.Create());

fruits.ForEach(Console.WriteLine);

var sweetFruits = fruits.Where(f => f.IsSweet)
                        .OrderByDescending(f => f.Price)
                        .ToList();

Console.WriteLine("Sweet fruits:");
sweetFruits.ForEach(Console.WriteLine);