using System.Globalization;

namespace Lab2
{
    public class PriceFormatter
    {
        public static string FormatUsdPrice(decimal price)
        {
            var usc = new CultureInfo("en-us");
            return price.ToString("C2", usc);
        }

        public static string FormatPlnPrice(decimal price)
        {
            var usc = new CultureInfo("pl-pl");
            return price.ToString("C2", usc);
        }
    }
}