﻿using System.Xml;

namespace Lab1
{
    class XMLReadWithDOMApproach
    {
        public static void Read(string path)
        {
            XmlDocument doc = new();
            doc.Load(path);
            var drugs = doc.GetElementsByTagName("produktLeczniczy");

            int count = 0;
            Dictionary<string, HashSet<string>> uniqueDrugNames = [];
            Dictionary<string, int> cremeProducers = [];
            Dictionary<string, int> tabProducers = [];

            foreach (XmlNode drug in drugs)
            {
                string postac = drug.Attributes.GetNamedItem("postac").Value;
                string substancjaCzynna = drug.Attributes.GetNamedItem("nazwaPowszechnieStosowana").Value;
                string nazwaPowszechna = drug.Attributes.GetNamedItem("nazwaProduktu").Value;
                string nazwaPodmiotu = drug.Attributes.GetNamedItem("podmiotOdpowiedzialny").Value;

                if (postac == "Krem" && substancjaCzynna == "Mometasoni furoas")
                    ++count;

                uniqueDrugNames.TryAdd(substancjaCzynna, []);
                uniqueDrugNames[substancjaCzynna].Add(postac);

                if (postac == "Krem")
                {
                    if (!cremeProducers.TryAdd(nazwaPodmiotu, 1))
                        cremeProducers[nazwaPodmiotu]++;
                }

                if (postac == "Tabletki")
                {
                    if (!tabProducers.TryAdd(nazwaPodmiotu, 1))
                        tabProducers[nazwaPodmiotu]++;
                }
            }

            Console.WriteLine("Liczba produktów leczniczych w postaci kremu, których jedyną substancją czynną jest Mometasoni furoas: {0}", count);
            Console.WriteLine("Liczba preparatów o takiej samej nazwie powszechnej, ale pod różnymi postaciami: {0}", uniqueDrugNames.Where(x => x.Value.Count > 1).Count());
            Console.WriteLine("Nazwa podmiotu produkującego najwięcej kremów: {0}", cremeProducers.MaxBy(x => x.Value).Key);
            Console.WriteLine("Nazwa podmiotu produkującego najwięcej tabletek: {0}", tabProducers.MaxBy(x => x.Value).Key);

            Console.WriteLine("Czterech największych producentów kremów: ");
            var cremeProducersSortedByNrProducts = from producer in cremeProducers orderby producer.Value descending select producer;
            foreach (var producer in cremeProducersSortedByNrProducts.Take(4))
                Console.WriteLine("{0}: {1}", producer.Key, producer.Value);

            Console.WriteLine("Czterech największych producentów tabletek: ");
            var tabsProducersSortedByNrProducts = from producer in tabProducers orderby producer.Value descending select producer;
            foreach (var producer in tabsProducersSortedByNrProducts.Take(4))
                Console.WriteLine("{0}: {1}", producer.Key, producer.Value);
        }
    }
}