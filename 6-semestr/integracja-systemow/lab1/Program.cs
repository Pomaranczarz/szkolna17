﻿using System.Xml;
using Lab1;

namespace lab1
{
    class Lab1
    {

        public static void Main(string[] args)
        {
            string xmlpath = Path.Combine("Assets", "data.xml");

            Console.WriteLine("XML loaded by DOM approach");
            XMLReadWithDOMApproach.Read(xmlpath);

            Console.WriteLine("XML loaded by SAX approach");
            XMLReadWithSAXApproach.Read(xmlpath);

            Console.WriteLine("XML loaded with XPath");
            XMLReadWithXLSTDOM.Read(xmlpath);
        }
    }
}