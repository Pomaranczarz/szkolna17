﻿using System.Xml;
using System.Xml.XPath;

namespace Lab1
{
    class XMLReadWithXLSTDOM
    {
        public static void Read(string xmlpath)
        {
            XPathDocument document = new(xmlpath);
            XPathNavigator navigator = document.CreateNavigator();

            XmlNamespaceManager manager = new(navigator.NameTable);
            manager.AddNamespace("x", "http://rejestrymedyczne.ezdrowie.gov.pl/rpl/eksport-danych-v1.0");

            XPathExpression query = navigator.Compile("/x:produktyLecznicze/x:produktLeczniczy[@postac='Krem' and @nazwaPowszechnieStosowana='Mometasoni furoas']");
            query.SetContext(manager);

            int count = navigator.Select(query).Count;

            Console.WriteLine("Liczba produktów leczniczych w postaci kremu, żech jedyną substancję czynnej jest Mometasoni furoas: {0}", count);

            XPathExpression allDrugsQuery = navigator.Compile("/x:produktyLecznicze/x:produktLeczniczy");
            allDrugsQuery.SetContext(manager);

            Dictionary<string, HashSet<string>> uniqueNames = [];
            var allNames = navigator.Select(allDrugsQuery);

            while (allNames.MoveNext())
            {
                var current = allNames.Current;
                string nazwaPowszechna = current.GetAttribute("nazwaPowszechnieStosowana", "");
                string postac = current.GetAttribute("postac", "");

                uniqueNames.TryAdd(nazwaPowszechna, []);
                uniqueNames[nazwaPowszechna].Add(postac);
            }

            Console.WriteLine("Liczba preparatów o takiej samej nazwie powszechnej, ale podłąnymi postaciami: {0}", uniqueNames.Where(x => x.Value.Count > 1).Count());

            XPathExpression allCremesQuery = navigator.Compile("/x:produktyLecznicze/x:produktLeczniczy[@postac='Krem']/@podmiotOdpowiedzialny");
            allCremesQuery.SetContext(manager);
            XPathExpression allTabsQuery = navigator.Compile("/x:produktyLecznicze/x:produktLeczniczy[@postac='Tabletki']/@podmiotOdpowiedzialny");
            allTabsQuery.SetContext(manager);

            var cremeProducersIt = navigator.Select(allCremesQuery);
            var tabsProducersIt = navigator.Select(allTabsQuery);

            Dictionary<string, int> cremeProducers = [];
            Dictionary<string, int> tabProducers = [];

            while (cremeProducersIt.MoveNext())
            {
                if (!cremeProducers.TryAdd(cremeProducersIt.Current.Value, 1))
                    cremeProducers[cremeProducersIt.Current.Value]++;
            }

            while (tabsProducersIt.MoveNext())
            {
                if (!tabProducers.TryAdd(tabsProducersIt.Current.Value, 1))
                    tabProducers[tabsProducersIt.Current.Value]++;
            }

            Console.WriteLine("Nazwa podmiotu produkującego najwięcej kremów: {0}", cremeProducers.MaxBy(x => x.Value).Key);
            Console.WriteLine("Nazwa podmiotu produkującego najwięcej tabletek: {0}", tabProducers.MaxBy(x => x.Value).Key);

            Console.WriteLine("Czterech największych producentów kremów: ");
            var cremeProducersSortedByNrProducts = from producer in cremeProducers orderby producer.Value descending select producer;
            foreach (var producer in cremeProducersSortedByNrProducts.Take(4))
                Console.WriteLine("{0}: {1}", producer.Key, producer.Value);

            Console.WriteLine("Czterech największych producentów tabletek: ");
            var tabsProducersSortedByNrProducts = from producer in tabProducers orderby producer.Value descending select producer;
            foreach (var producer in tabsProducersSortedByNrProducts.Take(4))
                Console.WriteLine("{0}: {1}", producer.Key, producer.Value);
        }
    }
}