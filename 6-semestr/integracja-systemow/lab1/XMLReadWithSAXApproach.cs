﻿using System.Xml;

namespace Lab1
{
    class XMLReadWithSAXApproach
    {
        public static void Read(string xmlpath)
        {
            XmlReaderSettings settings = new()
            {
                IgnoreComments = true,
                IgnoreProcessingInstructions = true,
                IgnoreWhitespace = true
            };

            XmlReader reader = XmlReader.Create(xmlpath, settings);

            int count = 0;
            Dictionary<string, HashSet<string>> uniqueDrugNames = [];
            Dictionary<string, int> tabProducers = [];
            Dictionary<string, int> cremeProducers = [];

            reader.MoveToContent();

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "produktLeczniczy")
                {
                    string postac = reader.GetAttribute("postac");
                    string substancjaCzynna = reader.GetAttribute("nazwaPowszechnieStosowana");
                    string nazwaPowszechna = reader.GetAttribute("nazwaProduktu");

                    if (postac == "Krem" && substancjaCzynna == "Mometasoni furoas")
                        ++count;

                    uniqueDrugNames.TryAdd(substancjaCzynna, []);
                    uniqueDrugNames[substancjaCzynna].Add(postac);

                    string nazwaPodmiotu = reader.GetAttribute("podmiotOdpowiedzialny");
                    if (postac == "Krem")
                    {
                        if (!cremeProducers.TryAdd(nazwaPodmiotu, 1))
                            cremeProducers[nazwaPodmiotu]++;
                    }

                    if (postac == "Tabletki")
                    {
                        if (!tabProducers.TryAdd(nazwaPodmiotu, 1))
                            tabProducers[nazwaPodmiotu]++;
                    }
                }
            }

            Console.WriteLine("Liczba produktów leczniczych w postaci kremu, żech jedyną substancję czynnej jest Mometasoni furoas: {0}", count);
            Console.WriteLine("Liczba preparatów o takiej samej nazwie powszechnej, ale pod różnymi postaciami: {0}", uniqueDrugNames.Where(x => x.Value.Count > 1).Count());
            Console.WriteLine("Nazwa podmiotu produkującego najwięcej kremów: {0}", cremeProducers.MaxBy(x => x.Value).Key);
            Console.WriteLine("Nazwa podmiotu produkującego najwięcej tabletek: {0}", tabProducers.MaxBy(x => x.Value).Key);

            Console.WriteLine("Czterech największych producentów kremów: ");
            var cremeProducersSortedByNrProducts = from producer in cremeProducers orderby producer.Value descending select producer;
            foreach (var producer in cremeProducersSortedByNrProducts.Take(4))
                Console.WriteLine("{0}: {1}", producer.Key, producer.Value);

            Console.WriteLine("Czterech największych producentów tabletek: ");
            var tabsProducersSortedByNrProducts = from producer in tabProducers orderby producer.Value descending select producer;
            foreach (var producer in tabsProducersSortedByNrProducts.Take(4))
                Console.WriteLine("{0}: {1}", producer.Key, producer.Value);
        }
    }
}