﻿using System;
using System.IO;
using System.Reflection;
using YamlDotNet.Serialization;

namespace csharp
{
    class Configuration
    {
        public bool serialize_from_object { get; set; }
        public Order order { get; set; }
        public Paths paths { get; set; }
    }

    class Order
    {
        public string first { get; set; }
        public string second { get; set; }
        public string third { get; set; }
    }

    class Paths
    {
        public string source_folder { get; set; }
        public string json_source_file { get; set; }
        public string json_destination_file { get; set; }
        public string yaml_destination_file { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var confFile = File.ReadAllText("Assets/basic_config.yaml");
            var deserializer = new DeserializerBuilder().Build();
            var confdata = deserializer.Deserialize<Configuration>(confFile);

            DeserializeJson? deserializator = null;

            foreach (PropertyInfo field in typeof(Order).GetProperties())
            {
                switch (field.GetValue(confdata.order, null))
                {
                    case "serialize_json":
                        Console.WriteLine("serialize_json");
                        {
                            if (confdata.serialize_from_object)
                                SerializeJson.Run(
                                    deserializator,
                                    confdata.paths.source_folder + confdata.paths.json_destination_file
                                );
                            else
                                SerializeJson.Run(
                                    confdata.paths.source_folder + confdata.paths.json_source_file,
                                    confdata.paths.source_folder + confdata.paths.json_destination_file
                                );
                        }
                        break;
                    case "deserialize_json":
                        Console.WriteLine("deserialize_json");
                        {
                            deserializator = new DeserializeJson(confdata.paths.source_folder + confdata.paths.json_source_file);
                            deserializator.SomeStats();
                        }
                        break;
                    case "convert_json_to_yaml":
                        Console.WriteLine("convert_json_to_yaml");
                        {
                            ConvertJsonToYaml.Run(
                                confdata.paths.source_folder + confdata.paths.json_destination_file,
                                confdata.paths.source_folder + confdata.paths.yaml_destination_file
                            );
                            break;
                        }
                    default: continue;
                }
            }
        }
    }
}
