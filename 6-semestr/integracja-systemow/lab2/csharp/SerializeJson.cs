using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace csharp
{
    public class SerializeJson
    {
        private static readonly JsonSerializerOptions options = new()
        {
            WriteIndented = true,
            Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping
        };

        public static void Run(DeserializeJson deserializedData, string fileLocation)
        {
            Console.WriteLine("let's serialize something");

            List<Dictionary<string, object>> lst = new List<Dictionary<string, object>>();
            foreach (var dep in deserializedData.Data)
            {
                Dictionary<string, object> department = new Dictionary<string, object>
                {
                    { "Kod_TERYT", dep["Kod_TERYT"] },
                    { "Województwo", dep["Województwo"] },
                    { "Powiat", dep["Powiat"] },
                    { "typ_JST", dep["typ_JST"] },
                    { "nazwa_urzędu_JST", dep["nazwa_urzędu_JST"] },
                    { "miejscowość", dep["miejscowość"] },
                    { "telefon_z_numerem_kierunkowym", $"{dep["telefon kierunkowy"]}{dep["telefon"]}" }
                };
                lst.Add(department);
            }

            string jsonString = JsonSerializer.Serialize(lst, options);
            File.WriteAllText(fileLocation, jsonString);

            Console.WriteLine("it is done");
        }

        public static void Run(string sourceFileName, string destinationFileName)
        {
            Console.WriteLine("let's serialize something");

            var deserializedData = new DeserializeJson(sourceFileName);
            Run(deserializedData, destinationFileName);
        }
    }
}
