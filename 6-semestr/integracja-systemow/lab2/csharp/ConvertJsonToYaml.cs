using System;
using System.IO;
using System.Text;
using System.Text.Json;
using YamlDotNet.Serialization;

namespace csharp
{
    public static class ConvertJsonToYaml
    {
        public static void Run(List<Dictionary<string, object>> deserializedData, string destinationFileLocation)
        {
            Console.WriteLine("let's convert something");

            List<Dictionary<string, object>> departments = [];

            foreach (var dep in deserializedData)
            {
                Dictionary<string, object> department = [];
                foreach (var kvp in dep)
                {
                    if (kvp.Value != null)
                    {
                        department[kvp.Key] = kvp.Value.ToString();
                    }
                }
                departments.Add(department);
            }

            var serializer = new SerializerBuilder().Build();
            var yamlString = serializer.Serialize(departments);

            File.WriteAllText(destinationFileLocation, yamlString);

            Console.WriteLine("it is done");
        }

        public static void Run(string sourceFile, string destinationFile)
        {
            Console.WriteLine("let's convert something");

            var deserializedData = new DeserializeJson(sourceFile).Data;

            Run(deserializedData, destinationFile);
        }
    }
}
