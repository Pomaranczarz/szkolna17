using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace csharp
{
    public class DeserializeJson
    {
        public List<Dictionary<string, object>> Data { get; }
        private static readonly JsonSerializerOptions options = new()
        {
            AllowTrailingCommas = true,
            ReadCommentHandling = JsonCommentHandling.Skip
        };

        public DeserializeJson(string filename)
        {
            Console.WriteLine("let's deserialize something");
            string jsonString = File.ReadAllText(filename, Encoding.UTF8);

            Data = JsonSerializer.Deserialize<List<Dictionary<string, object>>>(jsonString, options);

            Console.WriteLine("it is done");
        }

        public void SomeStats()
        {
            Dictionary<string, int> depsInVoivodeship = [];
            foreach (var dep in Data)
            {
                string department = dep["Województwo"].ToString().Trim();
                if (depsInVoivodeship.TryGetValue(department, out int value))
                    depsInVoivodeship[department] = value + 1;
                else
                    depsInVoivodeship[department] = 1;
            }

            int idx = 1;
            foreach (var kvp in depsInVoivodeship)
            {
                Console.WriteLine($"{idx}. {kvp.Key}: {kvp.Value}");
                idx++;
            }
        }
    }
}
