# -*- coding: utf-8 -*-
"""
deserialize xml
"""
import xmltodict


class DeserializeXML:
    def __init__(self, filename):
        print("let's deserialize something")
        with open(filename, encoding="utf8") as f:
            self.data = xmltodict.parse(f.read())
        print("it is done")

    def somestats(self):
        urzedow_w_wojewodztwach = {}
        for dep in self.data["departaments"]["department"]:
            wojewodztwo = dep["Województwo"].strip()
            if wojewodztwo in urzedow_w_wojewodztwach:
                urzedow_w_wojewodztwach[wojewodztwo] += 1
            else:
                urzedow_w_wojewodztwach[wojewodztwo] = 1

        for idx, wojewodztwo in enumerate(urzedow_w_wojewodztwach):
            print(f"{idx + 1}. {wojewodztwo}: {urzedow_w_wojewodztwach[wojewodztwo]}")
