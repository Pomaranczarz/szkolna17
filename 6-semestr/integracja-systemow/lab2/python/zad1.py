from deserialize_json import DeserializeJson
from serialize_json import SerializeJson
from convert_json_to_yaml import ConvertJsonToYaml

import yaml


with open("Assets/basic_config.yaml") as conf_file:
    confdata = yaml.load(conf_file, Loader=yaml.FullLoader)

order = ["first", "second", "third"]
operations = [op for op in [confdata["order"][op] for op in order]]

deserializator = None

for operation in operations:
    match operation:
        case "serialize_json":
            SerializeJson.run(
                (
                    deserializator
                    if confdata["serialize_from_object"]
                    else confdata["paths"]["source_folder"]
                    + confdata["paths"]["json_source_file"]
                ),
                confdata["paths"]["source_folder"]
                + confdata["paths"]["json_destination_file"],
            )

        case "deserialize_json":
            deserializator = DeserializeJson(
                confdata["paths"]["source_folder"]
                + confdata["paths"]["json_source_file"]
            )
            deserializator.somestats()
        case "convert_json_to_yaml":
            ConvertJsonToYaml.run(
                confdata["paths"]["source_folder"]
                + confdata["paths"]["json_destination_file"],
                confdata["paths"]["source_folder"]
                + confdata["paths"]["yaml_destination_file"],
            )

        case _:
            pass
