# -*- coding: utf-8 -*-
"""
serialize xml
"""

import dicttoxml
from deserialize_xml import DeserializeXML


class SerializeXML:
    @staticmethod
    def run(source_file_or_data, destination_file):
        print("let's serialize something")

        if isinstance(source_file_or_data, str):
            SerializeXML.run_from_file(source_file_or_data, destination_file)
        else:
            SerializeXML.run_from_data(source_file_or_data, destination_file)

        print("it is done")

    @staticmethod
    def run_from_data(deserialized_data, file_location):
        lst = []
        for dep in deserialized_data:
            lst.append(
                {
                    "Kod_TERYT": dep["Kod_TERYT"],
                    "Województwo": dep["Województwo"],
                    "Powiat": dep["Powiat"],
                    "typ_JST": dep["typ_JST"],
                    "nazwa_urzędu_JST": dep["nazwa_urzędu_JST"],
                    "miejscowość": dep["miejscowość"],
                    "telefon_z_numerem_kierunkowym": (
                        dep["telefon_z_numerem_kierunkowym"]
                        if "telefon_z_numerem_kierunkowym" in dep
                        else f'{dep["telefon kierunkowy" if "telefon kierunkowy" in dep else "telefon_kierunkowy"]}{dep["telefon"]}'
                    ),
                }
            )

        xmlstring = dicttoxml.dicttoxml(
            lst,
            attr_type=False,
            custom_root="departaments",
            item_func=lambda x: "department",
        )

        with open(file_location, "w", encoding="utf-8") as f:
            f.write(xmlstring.decode())

    @staticmethod
    def run_from_file(source_file, destination_file):
        deserialized_data = DesrializeXML(source_file).data

        SerializeXML.run_from_data(deserialized_data, destination_file)
