from serialize_xml import SerializeXML
from deserialize_xml import DeserializeXML
from deserialize_json import DeserializeJson
from serialize_json import SerializeJson

deserializator = DeserializeJson("Assets/data.json")
deserializator.somestats()

SerializeXML.run(deserializator.data, "Assets/data_from_json.xml")

deserializator = DeserializeXML("Assets/data_from_json.xml")
deserializator.somestats()

SerializeJson.run(
    deserializator.data["departaments"]["department"], "Assets/data_from_xml.json"
)
