# -*- coding: utf-8 -*-
"""
json to yaml converter
"""
import yaml
import json


class ConvertJsonToYaml:
    @staticmethod
    def run(deserialized_data, destination_file_location):
        print("let's convert something")
        with open(destination_file_location, "w", encoding="utf-8") as f:
            yaml.dump(deserialized_data, f, allow_unicode=True)
        print("it is done")

    @staticmethod
    def run(source_file, destination_file):
        print("let's convert something")

        with open(source_file, encoding="utf8") as f:
            deserialized_data = json.load(f)
        with open(destination_file, "w", encoding="utf-8") as f:
            yaml.dump(deserialized_data, f, allow_unicode=True)

        print("it is done")
