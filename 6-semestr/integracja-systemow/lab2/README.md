# Runtime Environment
- OS Name:     endeavouros
- OS Version:  
- OS Platform: Linux
- RID:         arch-x64
- Base Path:   /usr/share/dotnet/sdk/8.0.103/

## .NET SDK
- Version:           8.0.103
- Commit:            6a90b4b4bc
- Workload version:  8.0.100-manifests.e99a2be4
- **Zainstalowane biblioteki**
    - YamlDotNet (v. 15.1.2)

## Python
- Python 3.11.8
- **Zainstalowane biblioteki**
    - dicttoxml
    - xmltodict