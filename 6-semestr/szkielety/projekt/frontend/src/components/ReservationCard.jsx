import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { requestConfig } from '../common/RequestConfig';
import { formatDate } from '../common/Utils';
import { useNavigate } from 'react-router-dom';

const ReservationCard = ({ reservation, onDelete }) => {
    const [restaurantName, setRestaurantName] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        getRestaurantName();
    }, []);

    const getRestaurantName = async () => {
        const token = sessionStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }
        const config = requestConfig(token);
        await axios.get(`http://localhost:5000/api/restaurants/${reservation.restaurantId}`, config)
            .then(response => {
                setRestaurantName(response.data.name);
            })
            .catch(error => {
                console.error('Error fetching restaurant:', error.message);
                navigate('/error');
            });
    };

    const handleDelete = async () => {
        const token = sessionStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }
        const config = requestConfig(token);
        await axios.delete(`http://localhost:5000/api/reservations/${reservation._id}`, config)
            .then(() => {
                onDelete();  // Wywołaj onDelete po pomyślnym usunięciu
            })
            .catch(error => {
                console.error('Error deleting reservation:', error.message);
                navigate('/error');
            });
    };

    return (
        <div className='reservation-card' onClick={() => navigate(`/reservation/${reservation._id}`)}>
            <h2 onClick={() => navigate(`/reservation/${reservation._id}`)}>{restaurantName}</h2>
            <p>{formatDate(reservation.reservationDate)}</p>
            <button onClick={handleDelete}>Delete Reservation</button>
        </div>
    );
};

export default ReservationCard;
