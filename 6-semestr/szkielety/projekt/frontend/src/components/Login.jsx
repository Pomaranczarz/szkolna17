import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState({});
    const navigate = useNavigate();

    const handleLogin = async (e) => {
        e.preventDefault();

        await axios.post('http://localhost:5000/api/users/login', { username, password })
            .then(response => {
                sessionStorage.setItem('token', response.data.token);
                navigate('/');
            })
            .catch(error => {
                const errors = {};
                switch (error.response.data.message) {
                    case 'Invalid username':
                        errors.username = 'Invalid username';
                        break;
                    case 'Invalid password':
                        errors.password = 'Invalid password';
                        break;
                    default:
                        navigate('/error');
                        break;
                }
                setErrors(errors);
            })
    };

    return (
        <div>
            <h2>Login</h2>
            <form onSubmit={handleLogin}>
                <div>
                    <label>Username:</label>
                    <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
                    {errors.username && <div className='error'>{errors.username}</div>}
                </div>
                <div>
                    <label>Password:</label>
                    <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                    {errors.password && <div className='error'>{errors.password}</div>}
                </div>
                <button type="submit">Log in</button>
            </form>
            <p>
                Don't have an account? <a href="/register">Sign up</a>
            </p>
        </div>
    );
};

export default Login;
