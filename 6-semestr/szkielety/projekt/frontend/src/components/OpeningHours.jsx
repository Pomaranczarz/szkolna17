import React from "react";

const OpeningHours = ({ openingHours }) => {
    return (
        <div>
            <h3>Opening Hours</h3>
            {
                Object.entries(openingHours).map(([day, hours]) => (
                    <p key={day}>{day}: {hours}</p>
                ))
            }
        </div>
    );
};

export default OpeningHours;