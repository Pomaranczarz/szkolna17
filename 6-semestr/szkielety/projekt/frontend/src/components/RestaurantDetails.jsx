import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from 'react-router-dom';
import OpeningHours from "./OpeningHours";
import { requestConfig } from "../common/RequestConfig";
import axios from "axios";

const RestaurantDetails = () => {
    const [restaurantDetails, setRestaurantDetails] = useState({});
    const [openingHours, setOpeningHours] = useState({});
    const params = useParams();
    const navigate = useNavigate();

    const handleMakeReservation = () => {
        navigate(`/new_reservation/${restaurantDetails._id}`);
    };

    useEffect(() => {
        const getRestaurantData = async () => {
            const restaurantId = params.id;

            const token = sessionStorage.getItem('token');
            if (!token) {
                navigate('/login');
                return;
            }

            const config = requestConfig(token);
            try {
                const response = await axios.get(`http://localhost:5000/api/restaurants/${restaurantId}`, config);
                setRestaurantDetails(response.data);
                setOpeningHours(response.data.openingHours);
            } catch (error) {
                console.log('Error fetching restaurant data:', error);
                navigate('/login');
            }
        };

        getRestaurantData();
    }, [params.id, navigate]);

    return (
        <div>
            <h1>{restaurantDetails.name}</h1>
            <p>{restaurantDetails.description}</p>
            <OpeningHours openingHours={openingHours} />
            <h3>Address</h3>
            <p>{restaurantDetails.address}</p>
            <p>{restaurantDetails.city}</p>
            <h3>Contact</h3>
            <p>{restaurantDetails.contactEmail}</p>
            <p>{restaurantDetails.contactPhone}</p>
            <button onClick={handleMakeReservation}>Make reservation</button>
        </div>
    );
};

export default RestaurantDetails;
