import { useEffect, useState } from "react";
import React from "react";
import RestaurantCard from "./RestaurantCard";
import axios from "axios";
import { requestConfig } from "../common/RequestConfig";
import { useNavigate } from "react-router-dom";

const Restaurants = () => {
    const [restaurants, setRestaurants] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchRestaurants = async () => {
            const token = sessionStorage.getItem('token');
            if (!token) {
                navigate('/login');
                return;
            }

            const config = requestConfig(token);

            try {
                const response = await axios.get('http://localhost:5000/api/restaurants', config);
                setRestaurants(response.data);
            } catch (error) {
                console.error('Error fetching restaurants:', error);
                navigate('/error');
            }
        };

        fetchRestaurants();
    }, [navigate]);

    return (
        <div className="restaurants-list">
            <h1>Restaurants</h1>
            {restaurants.map((restaurant) => (
                <RestaurantCard key={restaurant._id} restaurant={restaurant} id={restaurant._id} />
            ))}
        </div>
    );
};

export default Restaurants;
