import React, { useEffect, useState } from 'react';
import axios from 'axios';
import GuestPage from './GuestPage';
import LoggedHomePage from './LoggedHomePage';

const HomePage = () => {
    const [token, setToken] = useState(null);

    const getToken = () => {
        const token = sessionStorage.getItem('token');
        setToken(token);
    };

    useEffect(() => {
        getToken();
    }, []);

    return token ? (
        <LoggedHomePage />
    ) : (
        <GuestPage />
    );
};

export default HomePage;