import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const Register = () => {
    const [inputFields, setInputFields] = useState({
        username: '',
        email: '',
        password: '',
        confirmPassword: ''
    });
    const [errors, setErrors] = useState({});
    const [submitting, setSubmitting] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        if (Object.keys(errors).length === 0 && submitting) {
            sendFormData();
        }
    }, [errors]);

    const sendFormData = async (e) => {
        const { confirmPassword, ...request } = inputFields;

        await axios.post('http://localhost:5000/api/users/register', {
            ...request
        })
            .then(() => navigate('/login'))
            .catch(error => {
                console.log(error);
                navigate('/error');
            });
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setInputFields(prev => ({ ...prev, [name]: value }));
    };

    const validateInput = () => {
        const errors = {};
        // min 8 characters
        // max 50 characters
        // min 1 lowercase, min 1 uppercase, min 1 number, min 1 symbol
        const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]).{8,50}$/;
        if (inputFields.username.length < 4)
            errors.username = 'Username must be at least 4 characters long';

        if (!passwordRegex.test(inputFields.password))
            errors.password = 'Password must contain at least 1 lowercase, 1 uppercase, 1 number, and 1 symbol';

        if (inputFields.password.length < 8 || inputFields.password.length > 50)
            errors.password = 'Password must be between 8 and 50 characters long';

        if (inputFields.password !== inputFields.confirmPassword)
            errors.confirmPassword = 'Passwords do not match';

        // validate email
        const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        if (!emailRegex.test(inputFields.email))
            errors.email = 'Invalid email address';

        return errors;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setErrors(validateInput());
        setSubmitting(true);
    };

    return (
        <div>
            <h2>Register</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Username:</label>
                    <input type="text" name="username" value={inputFields.username} onChange={handleChange} />
                    {errors.username && <div className='error'>{errors.username}</div>}
                </div>
                <div>
                    <label>Email:</label>
                    <input type="email" name="email" value={inputFields.email} onChange={handleChange} />
                    {errors.email && <div className='error'>{errors.email}</div>}
                </div>
                <div>
                    <label>Password:</label>
                    <input type="password" name="password" value={inputFields.password} onChange={handleChange} />
                    {errors.password && <div className='error'>{errors.password}</div>}
                </div>
                <div>
                    <label>Confirm password:</label>
                    <input type="password" name="confirmPassword" value={inputFields.confirmPassword} onChange={handleChange} />
                    {errors.confirmPassword && <div className='error'>{errors.confirmPassword}</div>}
                </div>
                <button type="submit">Sign up</button>
            </form>
            <p>
                Already have an account? <a href="/login">Sign in</a>
            </p>
        </div>
    );
};

export default Register;
