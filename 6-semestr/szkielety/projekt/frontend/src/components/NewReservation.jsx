import { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import { requestConfig } from '../common/RequestConfig';
import { dayNumberToName, getOpeningHours, RestaurantTime } from '../common/Utils';

const NewReservation = () => {
    const [inputFields, setInputFields] = useState({
        reservationDate: '',
        numberOfGuests: 1,
        specialRequests: '',
        restaurantId: ''
    });
    const [errors, setErrors] = useState({});
    const [submitting, setSubmitting] = useState(false);
    const [restaurants, setRestaurants] = useState([]);

    const navigate = useNavigate();
    const params = useParams();

    useEffect(() => {
        const fetchRestaurants = async () => {
            const token = sessionStorage.getItem('token');
            if (!token) {
                navigate('/login');
                return;
            }

            const config = requestConfig(token);
            try {
                const response = await axios.get('http://localhost:5000/api/restaurants', config);
                setRestaurants(response.data);
            } catch (error) {
                console.log(error);
                navigate('/error');
            }
        };

        fetchRestaurants();
    }, [navigate]);

    useEffect(() => {
        const getRestaurantIdFromParams = () => {
            const restaurantId = params.id;
            if (restaurantId && !inputFields.restaurantId) {
                setInputFields(prev => ({ ...prev, restaurantId }));
            }
        };

        getRestaurantIdFromParams();
    }, [params, inputFields.restaurantId]);

    useEffect(() => {
        if (Object.keys(errors).length === 0 && submitting) {
            sendFormData();
        }
    }, [errors, submitting]);

    const sendFormData = async () => {
        const token = sessionStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }

        const config = requestConfig(token);
        try {
            await axios.post('http://localhost:5000/api/reservations', inputFields, config);
            navigate('/');
        } catch (error) {
            console.log(error);
        }
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setInputFields(prev => ({ ...prev, [name]: value }));
    };

    const validateInput = () => {
        const errors = {};
        if (new Date(inputFields.reservationDate) < Date.now()) {
            errors.reservationDate = 'Reservation date must be in the future';
        }

        const restaurant = restaurants.find(restaurant => restaurant._id === inputFields.restaurantId);
        if (restaurant) {
            const openingHours = restaurant.openingHours;
            const reservationDate = new Date(inputFields.reservationDate);
            const reservationTime = new RestaurantTime(reservationDate.getHours(), reservationDate.getMinutes());

            const weekday = dayNumberToName(reservationDate.getDay());
            const [opening, closing] = getOpeningHours(openingHours, weekday).split('-');
            const { openingTime, closingTime } = (() => {
                let [hours, minutes] = opening.trim().split(':');
                const openingTime = new RestaurantTime(hours, minutes);
                [hours, minutes] = closing.trim().split(':');
                const closingTime = new RestaurantTime(hours, minutes);
                return { openingTime, closingTime };
            })();

            if (reservationTime.valueOf() < openingTime.valueOf() || reservationTime.valueOf() > closingTime.valueOf()) {
                errors.reservationDate = `Reservation time must be between ${opening} and ${closing}`;
            }
        }

        if (Number.parseInt(inputFields.numberOfGuests) < 1) {
            errors.numberOfGuests = 'Number of guests must be at least 1';
        }

        return errors;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setErrors(validateInput());
        setSubmitting(true);
    };

    return (
        <div>
            <h2>New Reservation</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Reservation Date and Time:</label>
                    <input type="datetime-local" locale="pl-pl" name="reservationDate" value={inputFields.reservationDate} onChange={handleChange} required />
                    {errors.reservationDate && <div className="error">{errors.reservationDate}</div>}
                </div>
                <div>
                    <label>Number of Guests:</label>
                    <input type="number" name="numberOfGuests" value={inputFields.numberOfGuests} onChange={handleChange} required />
                    {errors.numberOfGuests && <div className="error">{errors.numberOfGuests}</div>}
                </div>
                <div>
                    <label>Special Requests:</label>
                    <textarea value={inputFields.specialRequests} name="specialRequests" onChange={handleChange} />
                </div>
                <div>
                    <label>Select Restaurant:</label>
                    <select value={inputFields.restaurantId} name="restaurantId" onChange={handleChange} required>
                        <option key="" value="">-- Select Restaurant --</option>
                        {restaurants.map(restaurant => (
                            <option key={restaurant._id} value={restaurant._id}>
                                {restaurant.name}
                            </option>
                        ))}
                    </select>
                </div>
                <button type="submit">Make a Reservation</button>
            </form>
        </div>
    );
};

export default NewReservation;
