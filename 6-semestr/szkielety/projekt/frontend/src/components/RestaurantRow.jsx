import React from 'react';
import { useNavigate } from 'react-router-dom';

const RestaurantRow = ({ restaurant, onDelete }) => {
    const handleDelete = () => onDelete(restaurant._id);
    const navigate = useNavigate();

    return (
        <tr key={restaurant._id} onClick={() => navigate(`/restaurant/${restaurant._id}`)}>
            <td>{restaurant.name}</td>
            <td>{restaurant.description}</td>
            <td>
                <button onClick={handleDelete}>Delete</button>
            </td>
        </tr>
    );
};

export default RestaurantRow;
