import React from 'react';
import ReservationCard from './ReservationCard';

const Reservations = ({ reservations, message, onCardDelete }) => {
    if (!Array.isArray(reservations) || reservations.length === 0) {
        return <p>{message}</p>;
    }

    return (
        <div>
            {reservations.sort((a, b) => new Date(a.reservationDate) - new Date(b.reservationDate)).map(reservation => (
                <div key={reservation._id}>
                    <ReservationCard reservation={reservation} onDelete={onCardDelete} />
                </div>
            ))}
        </div>
    );
};

export default Reservations;
