import React from "react";

const User = ({ user, onDelete }) => {
    const handleDelete = () => onDelete(user._id);
    return (
        <tr key={user._id}>
            <td>{user.username}</td>
            <td>{user.email}</td>
            <td>{user.role}</td>
            <td>
                <button onClick={handleDelete}>Delete</button>
            </td>
        </tr>
    );
};

export default User;
