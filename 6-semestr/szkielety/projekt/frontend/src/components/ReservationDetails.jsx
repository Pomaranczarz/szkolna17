import React, { useEffect, useState } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { requestConfig } from '../common/RequestConfig';
import { formatDate } from '../common/Utils';
import axios from 'axios';

const ReservationDetails = () => {
    const [reservationDetails, setReservationDetails] = useState({});
    const params = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        const getReservationData = async () => {
            const reservationId = params.id;

            const token = sessionStorage.getItem('token');
            if (!token)
                navigate('/login');

            const config = requestConfig(token);
            await axios.get(`http://localhost:5000/api/reservations/details/${reservationId}`, config)
                .then(response => {
                    setReservationDetails(response.data);
                })
                .catch(error => {
                    console.log('Error fetching reservation data:', error)
                    navigate('/login');
                });
        };

        getReservationData();
    }, []);

    return (
        <div>
            <h2>Reservation Details</h2>
            <p>User: {reservationDetails.username}</p>
            <p>Restaurant: <Link to={`/restaurant/${reservationDetails.restaurantId}`}>{reservationDetails.restaurantName}</Link></p>
            <p>Reservation Date: {formatDate(reservationDetails.reservationDate)}</p>
            <p>Number of Guests: {reservationDetails.numberOfGuests}</p>
            <p>Special Requests: {reservationDetails.specialRequests || 'None'}</p>
            <p>Created At: {formatDate(reservationDetails.createdAt)}</p>
        </div>
    );
};

export default ReservationDetails;