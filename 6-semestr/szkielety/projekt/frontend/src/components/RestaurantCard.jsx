import React from "react";
import { useNavigate } from "react-router-dom";

const RestaurantCard = ({ restaurant }) => {
    const navigate = useNavigate();

    return (
        <div id={restaurant._id} className="restaurant-card" onClick={() => navigate(`/restaurant/${restaurant._id}`)}>
            <h2>{restaurant.name}</h2>
            <p>{restaurant.address}</p>
        </div>
    );
};

export default RestaurantCard;