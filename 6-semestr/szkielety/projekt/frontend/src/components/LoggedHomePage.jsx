import { React, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import ReservationCard from './ReservationCard';
import Reservations from './Reservations';
import axios from 'axios';

import { requestConfig } from '../common/RequestConfig';

const LoggedHomePage = () => {
    const navigate = useNavigate();
    const [user, setUser] = useState(null);
    const [upcomingReservations, setUpcomingReservations] = useState([]);
    const [pastReservations, setPastReservations] = useState([]);
    const getReservations = async () => {
        const token = sessionStorage.getItem('token');
        const config = requestConfig(token);
        await axios.get('http://localhost:5000/api/reservations', config)
            .then(response => {
                setUpcomingReservations(response.data.filter(reservation => new Date(reservation.reservationDate) > Date.now()))
                setPastReservations(response.data.filter(reservation => new Date(reservation.reservationDate) < Date.now()))
            })
            .catch(error => {
                console.log(error);
                navigate('/error');
            });
    };
    useEffect(() => {
        getReservations();
    }, []);

    const getUser = async () => {
        const token = sessionStorage.getItem('token');
        if (!token)
            navigate('/login');

        const config = requestConfig(token);
        await axios.post('http://localhost:5000/api/users/me', {}, config)
            .then(response => {
                setUser(response.data);
            })
            .catch(error => {
                console.log("Error getting user info", error.message);
                navigate('/error');
            })
    };
    useEffect(() => {
        getReservations();
        getUser();
    }, []);

    const handleMakeReservation = () => {
        navigate('/new_reservation');
    };

    const handleProfileClick = () => {
        navigate(`/me`);
    };

    return (
        <div>
            <h1>Welcome, {user?.username}!</h1>
            <button onClick={handleMakeReservation}>Make a Reservation</button>
            <button onClick={handleProfileClick}>View Profile</button>
            <h2>Upcoming Reservations</h2>
            <Reservations reservations={upcomingReservations} message="No upcoming reservations" onCardDelete={getReservations} />
            <h2>Past Reservations</h2>
            <Reservations reservations={pastReservations} message="No past reservations" onCardDelete={getReservations} />
        </div>
    );
};

export default LoggedHomePage;