import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { requestConfig } from '../common/RequestConfig';
import { formatDate } from '../common/Utils';
import { useNavigate } from 'react-router-dom';

const UserProfile = () => {
    const [userData, setUserData] = useState({});
    const [isEditing, setIsEditing] = useState(false);
    const [inputFields, setInputFields] = useState({
        username: '',
        email: '',
        password: '',
        confirmPassword: ''
    });
    const [errors, setErrors] = useState({});
    const [submitting, setSubmitting] = useState(false);
    const navigate = useNavigate();

    const handleEdit = () => {
        setIsEditing(true);
    };

    useEffect(() => {
        const getUserData = async () => {
            const token = sessionStorage.getItem('token');
            if (!token)
                navigate('/login');
            const config = requestConfig(token);

            await axios.post(`http://localhost:5000/api/users/me`, {}, config)
                .then(response => {
                    setUserData(response.data);
                    setInputFields({ username: response.data.username, email: response.data.email, password: '', confirmPassword: '' });
                })
                .catch(error => {
                    console.error('Error fetching user data:', error);
                    navigate('/error');
                });
        };

        getUserData();
    }, []);

    const validateChange = () => {
        const errors = {}

        if (inputFields.username.length < 4) {
            errors.username = 'Username must be at least 4 characters long';
        }

        const validateEmail = (email) => {
            return String(email)
                .toLowerCase()
                .match(
                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                );
        };

        if (!validateEmail(inputFields.email)) {
            errors.email = 'Invalid email';
        }

        return errors;
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setInputFields(prev => ({ ...prev, [name]: value }));
    };

    const sendFormData = async () => {
        const token = sessionStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }

        const requestData = {
            username: inputFields.username,
            email: inputFields.email,
            password: inputFields.password
        };

        const config = requestConfig(token);
        await axios.put(`http://localhost:5000/api/users/${userData._id}`, requestData, config)
            .then(() => {
                setIsEditing(false);
                setUserData({ ...userData, ...requestData });
            })
            .catch(error => {
                console.log(error);
                setErrors({ server: error.message });
            });
    };

    useEffect(() => {
        if (Object.keys(errors).length === 0 && submitting) {
            sendFormData();
        }
    }, [errors]);

    const handleSubmit = (e) => {
        e.preventDefault();
        setErrors(validateChange());
        setSubmitting(true);
    };

    return (
        <div className="add-restaurant-container">
            <h2 className="title">My profile</h2>
            {isEditing ? (
                <form className="add-restaurant-form" onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input
                            type="text"
                            name="username"
                            value={inputFields.username}
                            onChange={handleChange}
                            placeholder="Username"
                            className="form-input"
                        />
                        {errors.username && <div className='error'>{errors.username}</div>}
                    </div>
                    <div className="form-group">
                        <input
                            type="email"
                            name="email"
                            value={inputFields.email}
                            onChange={handleChange}
                            placeholder="Email"
                            className="form-input"
                        />
                        {errors.email && <div className='error'>{errors.email}</div>}
                    </div>
                    <div className="form-group">
                        <input
                            type="password"
                            name="password"
                            value={inputFields.password}
                            onChange={handleChange}
                            placeholder="New Password"
                            className="form-input"
                        />
                        {errors.password && <div className='error'>{errors.password}</div>}
                    </div>
                    <div className="form-group">
                        <input
                            type="password"
                            name="confirmPassword"
                            value={inputFields.confirmPassword}
                            onChange={handleChange}
                            placeholder="Confirm New Password"
                            className="form-input"
                        />
                        {errors.confirmPassword && <div className='error'>{errors.confirmPassword}</div>}
                    </div>
                    <button type="submit" className="form-button">Save</button>
                    {errors.server && <div className='error'>{errors.server}</div>}
                </form>
            ) : (
                <div className="user-details">
                    <p>Name: {userData.username}</p>
                    <p>Email: {userData.email}</p>
                    <p>Account created at: {formatDate(userData.createdAt)}</p>
                    <p>Last change at: {formatDate(userData.updatedAt)}</p>
                    {userData.role !== 'admin' && <button onClick={handleEdit} className="form-button">Edit</button>}
                </div>
            )}
        </div>

    );
};

export default UserProfile;