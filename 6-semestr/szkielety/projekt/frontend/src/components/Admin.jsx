import React, { useEffect, useState } from 'react';
import axios from 'axios';
import User from './User';
import RestaurantRow from './RestaurantRow';
import { requestConfig } from '../common/RequestConfig';
import { useNavigate } from 'react-router-dom';

const Admin = () => {
    const [users, setUsers] = useState([]);
    const [restaurants, setRestaurants] = useState([]);
    const [isAdmin, setIsAdmin] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        const checkIfAdmin = async () => {
            const token = sessionStorage.getItem('token');
            if (!token) {
                navigate('/login');
                return;
            }

            const config = requestConfig(token);

            try {
                const response = await axios.post('http://localhost:5000/api/users/me', {}, config);
                if (response.data.role !== 'admin') {
                    navigate('/');
                } else {
                    setIsAdmin(true);
                }
            } catch (error) {
                console.error('Error fetching user data:', error);
                navigate('/error');
            }
        };

        checkIfAdmin();
    }, [navigate]);

    useEffect(() => {
        const fetchUsersAndRestaurants = async () => {
            const token = sessionStorage.getItem('token');
            if (!token) {
                navigate('/login');
                return;
            }

            const config = requestConfig(token);

            if (isAdmin) {
                try {
                    const usersResponse = await axios.get('http://localhost:5000/api/users', config);
                    setUsers(usersResponse.data);
                } catch (error) {
                    console.error('Error fetching users:', error);
                    navigate('/error');
                }

                try {
                    const restaurantsResponse = await axios.get('http://localhost:5000/api/restaurants', config);
                    setRestaurants(restaurantsResponse.data);
                } catch (error) {
                    console.error('Error fetching restaurants:', error);
                    navigate('/error');
                }
            }
        };

        fetchUsersAndRestaurants();
    }, [isAdmin, navigate]);

    const handleUserDelete = async (userId) => {
        const token = sessionStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }

        const config = requestConfig(token);

        try {
            await axios.delete(`http://localhost:5000/api/users/${userId}`, config);
            setUsers(users.filter(user => user._id !== userId));
        } catch (error) {
            console.error('Error deleting user:', error);
            navigate('/error');
        }
    };

    const handleRestaurantDelete = async (restaurantId) => {
        const token = sessionStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }

        const config = requestConfig(token);

        try {
            await axios.delete(`http://localhost:5000/api/restaurants/${restaurantId}`, config);
            setRestaurants(restaurants.filter(restaurant => restaurant._id !== restaurantId));
        } catch (error) {
            console.error('Error deleting restaurant:', error);
            navigate('/error');
        }
    };

    return (
        <div>
            <h1>Users</h1>
            <table>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                    </tr>
                </thead>
                <tbody>
                    {users.filter(user => user.role !== 'admin').map(user => (
                        <User key={user._id} user={user} onDelete={handleUserDelete} />
                    ))}
                </tbody>
            </table>
            <h1>Restaurants</h1>
            <button onClick={() => navigate("/add_restaurant")}>Add Restaurant</button>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {restaurants.map(restaurant => (
                        <RestaurantRow restaurant={restaurant} key={restaurant._id} onDelete={handleRestaurantDelete} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default Admin;
