import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { requestConfig } from '../common/RequestConfig';

const AddRestaurant = () => {
    const [formData, setFormData] = useState({
        name: '',
        address: '',
        city: '',
        description: '',
        contactEmail: '',
        contactPhone: '',
        openingHours: {
            monday: '',
            tuesday: '',
            wednesday: '',
            thursday: '',
            friday: '',
            saturday: '',
            sunday: '',
        },
    });

    const [errors, setErrors] = useState({});
    const navigate = useNavigate();

    const handleChange = (e) => {
        const { name, value } = e.target;
        const [mainKey, subKey] = name.split('.');

        if (subKey) {
            setFormData((prevData) => ({
                ...prevData,
                [mainKey]: {
                    ...prevData[mainKey],
                    [subKey]: value,
                },
            }));
        } else {
            setFormData((prevData) => ({
                ...prevData,
                [name]: value,
            }));
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setErrors({});
        const token = sessionStorage.getItem('token');
        if (!token) {
            navigate('/login');
            return;
        }

        const config = requestConfig(token);

        // Validate form before submitting
        const formErrors = validateForm(formData);
        if (Object.keys(formErrors).length > 0) {
            setErrors(formErrors);
            return;
        }

        try {
            await axios.post('http://localhost:5000/api/restaurants', formData, config);
            navigate('/');
        } catch (error) {
            console.error('Error adding restaurant:', error);
            if (error.response && error.response.data && error.response.data.message) {
                setErrors({ server: error.response.data.message });
            } else {
                setErrors({ server: 'An error occurred while adding the restaurant.' });
            }
        }
    };

    // Add validation rules here
    const validateForm = (data) => {
        const errors = {};

        // Validate each field and set errors if necessary
        if (!data.name.trim()) {
            errors.name = 'Name is required';
        }

        if (!data.address.trim()) {
            errors.address = 'Address is required';
        }

        if (!data.city.trim()) {
            errors.city = 'City is required';
        }

        if (!data.contactEmail.trim()) {
            errors.contactEmail = 'Contact Email is required';
        } else if (!/^\S+@\S+\.\S+$/.test(data.contactEmail)) {
            errors.contactEmail = 'Please enter a valid email address';
        }

        if (!data.contactPhone.trim()) {
            errors.contactPhone = 'Contact Phone is required';
        } else if (!/^\d{3}-\d{3}-\d{3}$/.test(data.contactPhone)) {
            errors.contactPhone = 'Please enter a valid phone number (e.g., 123-456-789)';
        }

        // Validate opening hours format
        for (const day in data.openingHours) {
            console.log(day);
            if (data.openingHours.hasOwnProperty(day)) {
                console.log(data.openingHours[day]);
                if (data.openingHours[day] && !/^([01]?\d|2[0-3]):[0-5]\d-([01]?\d|2[0-3]):[0-5]\d$/.test(data.openingHours[day])) {
                    errors[day] = 'Opening hours must be in the format HH:MM-HH:MM';
                }
            }
        }

        console.log(errors);

        return errors;
    };

    return (
        <div className="add-restaurant-container">
            <h1 className="title">Add New Restaurant</h1>
            <form onSubmit={handleSubmit} className="add-restaurant-form">
                <label className="form-label">
                    Name:
                    <input type="text" name="name" value={formData.name} onChange={handleChange} required className="form-input" />
                    {errors.name && <div className="error">{errors.name}</div>}
                </label>
                <label className="form-label">
                    Address:
                    <input type="text" name="address" value={formData.address} onChange={handleChange} required className="form-input" />
                    {errors.address && <div className="error">{errors.address}</div>}
                </label>
                <label className="form-label">
                    City:
                    <input type="text" name="city" value={formData.city} onChange={handleChange} required className="form-input" />
                    {errors.city && <div className="error">{errors.city}</div>}
                </label>
                <label className="form-label">
                    Description:
                    <textarea name="description" value={formData.description} onChange={handleChange} className="form-textarea"></textarea>
                    {errors.description && <div className="error">{errors.description}</div>}
                </label>
                <label className="form-label">
                    Contact Email:
                    <input type="email" name="contactEmail" value={formData.contactEmail} onChange={handleChange} required className="form-input" />
                    {errors.contactEmail && <div className="error">{errors.contactEmail}</div>}
                </label>
                <label className="form-label">
                    Contact Phone:
                    <input type="text" name="contactPhone" value={formData.contactPhone} onChange={handleChange} required className="form-input" />
                    {errors.contactPhone && <div className="error">{errors.contactPhone}</div>}
                </label>
                <fieldset className="form-fieldset">
                    <legend>Opening Hours</legend>
                    {['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'].map((day) => (
                        <>
                            <label key={day} className="form-label">
                                {day.charAt(0).toUpperCase() + day.slice(1)}:
                                <input
                                    type="text"
                                    name={`openingHours.${day}`}
                                    value={formData.openingHours[day]}
                                    onChange={handleChange}
                                    className="form-input"
                                />
                            </label>
                            <div className="error">
                                {errors[day]}
                            </div>
                        </>
                    ))}
                </fieldset>
                {errors.server && <div className="error">{errors.server}</div>}
                <button type="submit" className="form-button">Add Restaurant</button>
            </form>
        </div>
    );
};

export default AddRestaurant;
