import React from 'react';
import { useNavigate } from 'react-router-dom';

const GuestPage = () => {
    const navigate = useNavigate();

    return (
        <div>
            <h1>Welcome to the Restaurant Reservation App!</h1>
            <p>Please log in or register to continue.</p>
            <button onClick={() => navigate('/login')}>Log In</button>
            <button onClick={() => navigate('/register')}>Register</button>
        </div>
    );
};

export default GuestPage;