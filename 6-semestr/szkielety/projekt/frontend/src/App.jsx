import './App.css'
import HomePage from './components/HomePage';
import Login from './components/Login'
import Register from './components/Register'
import NewReservation from './components/NewReservation';
import UserProfile from './components/UserProfile';
import ReservationDetails from './components/ReservationDetails';
import RestaurantDetails from './components/RestaurantDetails';
import Restaurants from './components/Restaurants';
import ErrorPage from './common/ErrorPage';
import Page404 from './common/Page404';
import NavBar from './common/NavBar';
import Admin from './components/Admin';
import AddRestaurant from './components/AddRestaurant';
import { Route, BrowserRouter, Routes } from 'react-router-dom';

function App() {
  return (
    <>

      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/new_reservation" element={<NewReservation />} />
          <Route path="/new_reservation/:id" element={<NewReservation />} />
          <Route path="/" element={<HomePage />} />
          <Route path="/me" element={<UserProfile />} />
          <Route path="/reservation/:id" element={<ReservationDetails />} />
          <Route path="/restaurants" element={<Restaurants />} />
          <Route path="/restaurant/:id" element={<RestaurantDetails />} />
          <Route path="/add_restaurant" element={<AddRestaurant />} />
          <Route path="/error" element={<ErrorPage />} />
          <Route path="/admin" element={<Admin />} />
          <Route path="*" element={<Page404 />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App
