export const formatDate = (date) => {
    const d = new Date(date);
    return `${d.getHours()}:${d.getMinutes()} ${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
};

export const dayNumberToName = (number) => {
    switch (number) {
        case 0:
            return 'sunday';
        case 1:
            return 'monday';
        case 2:
            return 'tuesday';
        case 3:
            return 'wednesday';
        case 4:
            return 'thursday';
        case 5:
            return 'friday';
        case 6:
            return 'saturday';
    }
};

export const getOpeningHours = (openingHours, weekday) => {
    switch (weekday) {
        case 'sunday':
            return openingHours.sunday;
        case 'monday':
            return openingHours.monday;
        case 'tuesday':
            return openingHours.tuesday;
        case 'wednesday':
            return openingHours.wednesday;
        case 'thursday':
            return openingHours.thursday;
        case 'friday':
            return openingHours.friday;
        case 'saturday':
            return openingHours.saturday;
    }
};

export class RestaurantTime {
    constructor(hours, minutes) {
        this.hours = Number.parseInt(hours);
        this.minutes = Number.parseInt(minutes);
    }

    toString() {
        return `${this.hours}:${this.minutes}`;
    }
}

RestaurantTime.prototype.valueOf = function () {
    return this.hours * 100 + this.minutes;
}