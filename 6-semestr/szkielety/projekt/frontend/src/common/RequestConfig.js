export const requestConfig = (token) => {
    return {
        headers: {
            Authorization: token ? `Bearer ${token}` : undefined
        }
    };
};
