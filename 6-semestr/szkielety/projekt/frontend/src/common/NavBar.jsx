import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { requestConfig } from "../common/RequestConfig";
import axios from "axios";

const NavBar = () => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [isAdmin, setIsAdmin] = useState(false);
    const navigate = useNavigate();

    const handleHomeClick = () => {
        navigate('/');
    };

    const handleRestaurantsClick = () => {
        navigate('/restaurants');
    };

    const handleProfileClick = () => {
        navigate('/me');
    };

    const handleLogoutClick = () => {
        sessionStorage.removeItem('token');
        navigate('/login');

        setIsLoggedIn(false);
    };

    const checkIfAdmin = async () => {
        const token = sessionStorage.getItem('token');
        if (!token)
            navigate('/login');
        const config = requestConfig(token);
        await axios.post('http://localhost:5000/api/users/me', {}, config)
            .then(response => {
                setIsAdmin(response.data.role === 'admin');
            })
            .catch(error => {
                console.error('Error fetching user data:', error);
                navigate('/error');
            });
    };

    useEffect(() => {
        const token = sessionStorage.getItem('token');
        if (token) {
            setIsLoggedIn(true);

            checkIfAdmin();
        }
    });

    return (
        <nav>
            {isLoggedIn ?
                <div>
                    <button onClick={handleHomeClick}>Home</button>
                    <button onClick={handleRestaurantsClick}>Restaurants</button>
                    <button onClick={handleProfileClick}>Profile</button>
                    <button onClick={handleLogoutClick}>Logout</button>
                    {isAdmin ? <button onClick={() => navigate('/admin')}>Admin</button> : null}
                </div>
                : null}
        </nav>
    );
};

export default NavBar;
