const express = require('express');
const router = express.Router();
const userController = require('../controllers/UserController');
const { authenticateUser } = require('../middleware/auth');

router.post('/register', userController.registerUser);
router.post('/login', userController.loginUser);

router.use(authenticateUser);
router.get('/', userController.getAllUsers);
router.get('/:id', userController.getUserById);
router.get('/by-username/:username', userController.getUserByUsername);
router.post('/me', userController.getUserByToken);

router.post('/', userController.createUser);
router.put('/:id', userController.updateUser);
router.delete('/:id', userController.deleteUser);


module.exports = router;
