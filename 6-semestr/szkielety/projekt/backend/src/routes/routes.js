const express = require('express');
const router = express.Router();
const userRoutes = require('./UserRoutes');
const restaurantRoutes = require('./RestaurantRoutes');
const reservationRoutes = require('./ReservationRoutes');
const { logRequests } = require('../middleware/log');

router.use(logRequests);

router.use('/users', userRoutes);
router.use('/restaurants', restaurantRoutes);
router.use('/reservations', reservationRoutes);

module.exports = router;