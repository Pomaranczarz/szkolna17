const express = require('express');
const router = express.Router();
const reservationController = require('../controllers/ReservationController');
const reservationDetailsController = require('../controllers/ReservationDetailsController');
const { authenticateUser } = require('../middleware/auth');

router.use(authenticateUser);

// Create a new reservation
router.post('/', reservationController.createReservation);

// Get all reservations
router.get('/', reservationController.getAllReservations);

// Get reservation by ID
router.get('/:id', reservationController.getReservationById);

// Get reservation details by reservation ID
router.get('/details/:id', reservationDetailsController.getReservationDetails);

// Update reservation by ID
router.put('/:id', reservationController.updateReservation);

// Delete reservation by ID
router.delete('/:id', reservationController.deleteReservation);

module.exports = router;
