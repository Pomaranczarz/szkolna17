const { Restaurant } = require('../models/Restaurant');
const { Reservation } = require('../models/Reservation');
const { User } = require('../models/User');

exports.getReservationDetails = async (req, res) => {
    try {
        const user = req.user;
        const reservation = await Reservation.findById(req.params.id);
        if (!reservation)
            return res.status(404).json({ message: 'Reservation not found' });

        if (user.role !== 'admin' && reservation.userId.toString() !== user._id.toString())
            return res.status(403).json({ message: 'Unauthorized' });

        const reservationOwner = await User.findById(reservation.userId);
        if (!reservationOwner)
            return res.status(404).json({ message: 'Reservation owner not found' });

        const restaurantName = await Restaurant.findById(reservation.restaurantId);
        if (!restaurantName)
            return res.status(404).json({ message: 'Restaurant not found' });

        const responseJson = {
            username: reservationOwner.username,
            restaurantId: reservation.restaurantId,
            restaurantName: restaurantName.name,
            reservationDate: reservation.reservationDate,
            numberOfGuests: reservation.numberOfGuests,
            specialRequests: reservation.specialRequests,
            status: reservation.status,
            createdAt: reservation.createdAt,
        };

        res.status(200).json(responseJson);
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving reservation', error: error.message });
    }
}