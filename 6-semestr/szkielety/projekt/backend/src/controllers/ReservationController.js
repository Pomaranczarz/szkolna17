const { Reservation, validate } = require('../models/Reservation');

/**
 * Creates a new reservation.
 *
 * @param {Object} req - The request object containing the reservation data.
 * @param {Object} res - The response object used to send the result.
 * @return {Promise<void>} - A promise that resolves when the reservation is created successfully.
 */
exports.createReservation = async (req, res) => {
    try {
        if (!req.userId)
            return res.status(404).json({ message: 'User not found' });

        const reservationBody = {
            ...req.body,
            userId: req.userId
        };

        console.log(reservationBody);

        const { error } = validate(reservationBody);
        if (error)
            return res.status(400).json({ message: error.message });

        const newReservation = new Reservation(reservationBody);
        await newReservation.save();
        res.status(201).json(newReservation);
    } catch (error) {
        res.status(500).json({ message: 'Error creating reservation', error });
    }
};


/**
 * Retrieves all reservations based on the user's role. If the user is an admin,
 * retrieves all reservations. Otherwise, retrieves only the reservations belonging
 * to the authenticated user.
 *
 * @param {Object} req - The request object containing the user information.
 * @param {Object} res - The response object used to send the result.
 * @return {Promise<void>} - A promise that resolves when the reservations are retrieved successfully.
 *                           Returns a JSON response with the retrieved reservations.
 *                           If an error occurs, returns a JSON response with an error message.
 */
exports.getAllReservations = async (req, res) => {
    try {
        const user = req.user;
        const reservations = await Reservation.find(user.role === 'admin' ? {} : { userId: user._id });
        res.status(200).json(reservations);
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving reservations', error: error.message });
    }
};


/**
 * Retrieves a reservation by its ID and checks if the requesting user has the necessary permissions.
 *
 * @param {Object} req - The request object containing the user information and the reservation ID.
 * @param {Object} res - The response object used to send the result.
 * @return {Promise<void>} - A promise that resolves when the reservation is retrieved successfully.
 *                           Returns a JSON response with the retrieved reservation if the requesting user has the necessary permissions.
 *                           Returns a JSON response with a 403 status code and an "Unauthorized" message if the requesting user does not have the necessary permissions.
 *                           Returns a JSON response with a 500 status code and an "Error retrieving reservation" message if an error occurs.
 */
exports.getReservationById = async (req, res) => {
    try {
        const reservation = await Reservation.findById(req.params.id);
        const requestingUser = req.user;

        if (requestingUser.role === 'admin' || reservation.userId.toString() === requestingUser._id.toString())
            return res.status(200).json(reservation);
        else
            return res.status(403).json({ message: 'Unauthorized' });
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving reservation', error });
    }
};


/**
 * Updates a reservation with the provided ID.
 *
 * @param {Object} req - The request object containing the reservation data.
 * @param {Object} res - The response object used to send the result.
 * @return {Promise<void>} - A promise that resolves when the reservation is updated successfully.
 */
exports.updateReservation = async (req, res) => {
    try {
        const bodyWithUserIdAppended = {
            ...req.body,
            userId: req.userId
        }
        const { error } = validate(bodyWithUserIdAppended);
        if (error)
            return res.status(400).json({ message: error.details[0].message });

        const reservation = await Reservation.findByIdAndUpdate(req.params.id, bodyWithUserIdAppended, { new: true });
        if (!reservation)
            return res.status(404).json({ message: 'Reservation not found' });
        res.status(200).json(reservation);
    } catch (error) {
        res.status(500).json({ message: 'Error updating reservation', error });
    }
};


/**
 * Deletes a reservation with the specified ID.
 *
 * @param {Object} req - The request object containing the reservation ID.
 * @param {Object} res - The response object used to send the result.
 * @return {Promise<void>} - A promise that resolves when the reservation is deleted successfully.
 *                          - If the requesting user is not an admin and does not have permission to delete the reservation,
 *                            a 403 status code is returned with a 'Unauthorized' message.
 *                          - If the reservation with the specified ID is not found, a 404 status code is returned with a 'Reservation not found' message.
 *                          - If there is an error deleting the reservation, a 500 status code is returned with an 'Error deleting reservation' message and the error object.
 */
exports.deleteReservation = async (req, res) => {
    try {
        const requestingUser = req.user;
        const reservation = await Reservation.findById(req.params.id);

        if (!reservation)
            return res.status(404).json({ message: 'Reservation not found' });

        console.log(requestingUser._id.toString(), reservation.userId.toString())

        if (requestingUser.role !== 'admin' && requestingUser._id.toString() !== reservation.userId.toString())
            return res.status(403).json({ message: 'Unauthorized' });

        await Reservation.findByIdAndDelete(req.params.id);

        res.status(200).json({ message: 'Reservation deleted' });
    } catch (error) {
        res.status(500).json({ message: 'Error deleting reservation', error: error.message });
    }
};

