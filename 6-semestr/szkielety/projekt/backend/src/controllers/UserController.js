const { User, registerValidate, createValidate } = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config();

/**
 * Seeds the admin user into the database if it does not already exist.
 *
 * @return {Promise<void>} - A promise that resolves when the admin user is successfully seeded or if it already exists.
 */
exports.seedAdminUser = async () => {
    try {
        const existingUser = await User.findOne({ username: 'admin', email: 'admin@admin.auth', role: 'admin' });
        if (!existingUser) { // if admin user doesn't exist
            const hashedPassword = await bcrypt.hash('admin', 10);
            const adminUser = new User({
                username: 'admin',
                email: 'admin@admin.auth',
                password: hashedPassword,
                role: 'admin'
            });
            await adminUser.save();
            console.log('User "admin" successfully created');
        }
        else {
            console.log('User "admin" already exists');
        }
    } catch (error) {
        console.error('Error creating user "admin":', error);
    }
};


/**
 * Registers a new user.
 *
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @return {Promise<void>} - A promise that resolves when the user is registered successfully.
 */
exports.registerUser = async (req, res) => {
    try {
        console.log('registerUser called')
        const { error } = registerValidate(req.body);
        if (error) {
            return res.status(400).json({ message: error.details[0].message });
        }

        const { username, email, password } = req.body;
        const existingUser = await User.findOne({ email }); // we check for email, but we could check either of unique User fields (username, email)
        if (existingUser) { // if user already exists
            return res.status(400).json({ message: `User with email "${email}" already exists` });
        }
        // create new user
        const hashedPassword = await bcrypt.hash(password, 10);
        const newUser = new User({
            username,
            email,
            password: hashedPassword,
        });
        await newUser.save();
        res.status(201).json({ message: `Created user ${newUser.username}` });
    } catch (error) {
        res.status(500).json({ message: `Error creating user: ${error}` });
    }
};

/**
 * Logs in a user by checking their username and password.
 *
 * @param {Object} req - The request object containing the username and password.
 * @param {Object} res - The response object used to send the login result.
 * @return {Promise<void>} - A promise that resolves when the login process is complete.
 */
exports.loginUser = async (req, res) => {
    try {
        console.log('loginUser called')
        const { username, password } = req.body;
        // check if user with given email exists
        const user = await User.findOne({ username });

        if (!user) {
            console.log(`Invalid username: ${username}`);
            return res.status(400).json({ message: 'Invalid username' });
        }
        // Porównanie hasła
        const isPasswordValid = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) {
            console.log(`Invalid password: ${password}`);
            return res.status(400).json({ message: 'Invalid password' });
        }
        // Generowanie tokena JWT
        const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, { expiresIn: '1h' });
        res.status(200).json({ token });
    } catch (error) {
        console.log(`Error logging in: ${error}`)
        res.status(500).json({ message: `Error logging in: ${error}` });
    }
};

/**
 * Creates a new user.
 *
 * @param {Object} req - The request object containing the user data.
 * @param {Object} res - The response object used to send the result.
 * @return {Promise<void>} - A promise that resolves when the user is created successfully.
 */
exports.createUser = async (req, res) => {
    try {
        console.log('create user called')
        const { error } = createValidate(req.body);
        if (error) {
            return res.status(400).json({ message: error.details[0].message });
        }

        // Sprawdzenie roli użytkownika
        const user = await User.findById(req.userId);
        if (!user || user.role !== 'admin') { // Możesz dostosować warunek do swoich potrzeb
            return res.status(403).json({ message: 'Unauthorized' });
        }

        const { username, email, password, role } = req.body;
        const hashedPassword = await bcrypt.hash(password, 10);

        const newUser = new User({
            username,
            email,
            password: hashedPassword,
            role
        });

        await newUser.save();
        res.status(201).json({ message: `User ${newUser.username} created` });
    } catch (error) {
        res.status(500).json({ message: `Error creating user: ${error}` });
    }
};

/**
 * Retrieves all users from the database and sends them as a JSON response.
 *
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @return {Promise<void>} A promise that resolves when the response is sent.
 */
exports.getAllUsers = async (req, res) => {
    try {
        console.log('getAllUsers called')
        const user = await User.findById(req.userId);
        if (!user || user.role !== 'admin') {
            return res.status(403).json({ message: 'Unauthorized' });
        }

        const users = await User.find();
        res.status(200).json(users);
    } catch (error) {
        res.status(500).json({ message: `Error fetching users: ${error}` });
    }
};

/**
 * Retrieves a user from the database by their ID and sends them as a JSON response.
 *
 * @param {Object} req - The request object containing the user ID in the params.
 * @param {Object} res - The response object used to send the user data or an error message.
 * @return {Promise<void>} - A promise that resolves when the response is sent.
 */
exports.getUserById = async (req, res) => {
    try {
        console.log('getUserById called')
        const requestingUser = await User.findById(req.userId);
        const requestedUser = await User.findById(req.params.id);
        if (!requestingUser || !requestedUser) {
            return res.status(404).json({ message: `User with ID ${req.params.id} not found` });
        }

        if (requestingUser._id.toString() !== requestedUser._id.toString() || requestingUser.role !== 'admin') {
            return res.status(403).json({ message: 'Unauthorized' });
        }

        res.status(200).json(requestedUser);
    } catch (error) {
        res.status(500).json({ message: `Error fetching user: ${error.message}` });
    }
};

/**
 * Retrieves a user from the database based on the provided authorization token.
 *
 * @param {Object} req - The request object containing the authorization token.
 * @param {Object} res - The response object used to send the user data or an error message.
 * @return {Promise<void>} - A promise that resolves when the response is sent.
 */
exports.getUserByToken = async (req, res) => {
    try {
        console.log('getUserByToken called')
        const user = await User.findById(req.userId);
        return res.status(200).json(user);
    } catch (error) {
        res.status(401).json({ message: 'Unauthorized' });
    }
};

/**
 * Retrieves a user from the database by their username and sends them as a JSON response.
 *
 * @param {Object} req - The request object containing the username in the params.
 * @param {Object} res - The response object used to send the user data or an error message.
 * @return {Promise<void>} - A promise that resolves when the response is sent.
 */
exports.getUserByUsername = async (req, res) => {
    try {
        console.log('getUserByUsername called')
        const requestingUser = await User.findById(req.userId);
        const requestedUser = await User.findOne({ username: req.params.username });
        if (!requestingUser || !requestedUser) {
            return res.status(400).json({ message: `Either you or requested user does not exist` });
        }

        if (requestingUser._id.toString() !== requestedUser._id.toString() || requestingUser.role !== 'admin') {
            return res.status(403).json({ message: 'Unauthorized' });
        }

        res.status(200).json(requestedUser);
    } catch (error) {
        res.status(500).json({ message: `Error fetching user: ${error}` });
    }
};

/**
 * Updates a user in the database with the provided information.
 *
 * @param {Object} req - The request object containing the user ID in the params and the updated user information in the body.
 * @param {Object} res - The response object used to send the updated user data or an error message.
 * @return {Promise<void>} - A promise that resolves when the user is updated successfully.
 */
exports.updateUser = async (req, res) => {
    try {
        const { username, email, password, role } = req.body;

        // Build the update object dynamically
        const updateData = {};
        if (username) updateData.username = username;
        if (email) updateData.email = email;
        if (role) updateData.role = role;
        if (password) updateData.password = password;

        // Validate updateData
        const { error } = createValidate(updateData, !!password, !!role);
        if (error) {
            return res.status(400).json({ message: error.message });
        }

        const requestingUser = await User.findById(req.userId);
        const requestedUser = await User.findById(req.params.id);
        if (password)
            updateData.password = await bcrypt.hash(password, 10);

        if (!requestingUser || !requestedUser) {
            return res.status(400).json({ message: `Either you or requested user does not exist` });
        }

        if (requestingUser._id.toString() !== requestedUser._id.toString() && requestingUser.role !== 'admin') {
            return res.status(403).json({ message: 'Unauthorized' });
        }

        const updatedUser = await User.findByIdAndUpdate(
            req.params.id,
            { $set: updateData },
            { new: true }
        );
        if (!updatedUser) {
            return res.status(404).json({ message: `User with ID ${req.params.id} not found` });
        }

        res.status(200).json({ message: `User updated successfully`, user: updatedUser });
    } catch (error) {
        res.status(500).json({ message: `Error updating user: ${error.message}` });
    }
};

/**
 * Deletes a user from the database based on the provided ID.
 *
 * @param {Object} req - The request object containing the user ID in the params.
 * @param {Object} res - The response object used to send the success/error message.
 * @return {Promise<void>} - A promise that resolves when the user is deleted successfully or sends an error response.
 */
exports.deleteUser = async (req, res) => {
    try {
        const requestingUser = await User.findById(req.userId);
        const requestedUser = await User.findById(req.params.id);
        if (!requestingUser || !requestedUser) {
            return res.status(400).json({ message: `Either you or requested user does not exist` });
        }

        if (requestingUser._id.toString() !== requestedUser._id.toString() && requestingUser.role !== 'admin') {
            return res.status(403).json({ message: 'Unauthorized' });
        }

        const deletedUser = await User.findByIdAndDelete(req.params.id);
        if (!deletedUser) {
            return res.status(404).json({ message: `User with ID ${req.params.id} not found` });
        }
        res.status(200).json({ message: `User deleted successfully` });
    } catch (error) {
        res.status(500).json({ message: `Error deleting user: ${error}` });
    }
};