const { Restaurant, validate } = require('../models/Restaurant');

/**
 * Adds default restaurants to the database.
 *
 * @return {Promise<void>} A promise that resolves when the default restaurants are added.
 */
exports.addDefaultRestaurants = async () => {
    const restaurantsData = [
        {
            name: 'Pizzeria Piękna',
            address: 'ul. Piękna 12',
            city: 'Warszawa',
            description: 'Najlepsze pizzy w mieście!',
            contactEmail: 'pizzeria@example.com',
            contactPhone: '123-456-789',
            openingHours: {
                monday: '10:00 - 22:00',
                tuesday: '10:00 - 22:00',
                wednesday: '10:00 - 22:00',
                thursday: '10:00 - 22:00',
                friday: '10:00 - 23:00',
                saturday: '11:00 - 23:00',
                sunday: '11:00 - 22:00',
            },
        },
        {
            name: 'Kawiarnia Mocha',
            address: 'ul. Mocha 34',
            city: 'Katowice',
            description: 'Najlepsze kawy w mieście!',
            contactEmail: 'kawiarnia@example.com',
            contactPhone: '123-456-789',
            openingHours: {
                monday: '08:00 - 20:00',
                tuesday: '08:00 - 20:00',
                wednesday: '08:00 - 20:00',
                thursday: '08:00 - 20:00',
                friday: '08:00 - 21:00',
                saturday: '09:00 - 21:00',
                sunday: '09:00 - 20:00',
            },
        },
        {
            name: 'Kebab Dżins',
            address: 'ul. Dżins 56',
            city: 'Kraków',
            description: 'Najlepsze kebaby w mieście!',
            contactEmail: 'kebabdziins@example.com',
            contactPhone: '123-456-789',
            openingHours: {
                monday: '11:00 - 22:00',
                tuesday: '11:00 - 22:00',
                wednesday: '11:00 - 22:00',
                thursday: '11:00 - 22:00',
                friday: '11:00 - 23:00',
                saturday: '12:00 - 23:00',
                sunday: '12:00 - 22:00',
            },
        },
        {
            name: 'Sushi Sensation',
            address: 'ul. Sensation 78',
            city: 'Wrocław',
            description: 'Najlepsze sushi w mieście!',
            contactEmail: 'sushisensation@example.com',
            contactPhone: '123-456-789',
            openingHours: {
                monday: '10:00 - 23:00',
                tuesday: '10:00 - 23:00',
                wednesday: '10:00 - 23:00',
                thursday: '10:00 - 23:00',
                friday: '10:00 - 23:00',
                saturday: '10:00 - 23:00',
                sunday: '10:00 - 23:00',
            },
        },
        {
            name: 'Pierogi z węża',
            address: 'ul. Wąrańska 90',
            city: 'Łoźnawa',
            description: 'Jedząc je się odprężam',
            contactEmail: 'pierogizwárańska@example.com',
            contactPhone: '123-456-789',
            openingHours: {
                monday: '10:00 - 23:00',
                tuesday: '10:00 - 23:00',
                wednesday: '10:00 - 23:00',
                thursday: '10:00 - 23:00',
                friday: '10:00 - 23:00',
                saturday: '10:00 - 23:00',
                sunday: '10:00 - 23:00',
            },
        },
    ];
    try {
        console.log('Adding sample restuarnats...');
        restaurantsData.forEach(async restaurant => {
            const existingRestaurant = await Restaurant.findOne({ name: restaurant.name });
            if (existingRestaurant)
                return;
            const newRestaurant = new Restaurant(restaurant);
            await newRestaurant.save();
        })
    }
    catch (error) {
        console.log('Error adding default restaurants:', error);
    }
}

exports.createRestaurant = async (req, res) => {
    try {
        const user = req.user;
        if (user.role !== 'admin')
            return res.status(403).json({ message: 'Unauthorized' });

        const { error } = validate(req.body);
        if (error) return res.status(400).json({ message: error.message });

        const newRestaurant = new Restaurant(req.body);
        await newRestaurant.save();
        res.status(201).json(newRestaurant);
    } catch (error) {
        res.status(500).json({ message: 'Error creating restaurant', error });
    }
};

exports.getAllRestaurants = async (req, res) => {
    try {
        const restaurants = await Restaurant.find();
        res.status(200).json(restaurants);
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving restaurants', error: error.message });
    }
};


exports.getRestaurantById = async (req, res) => {
    try {
        const restaurant = await Restaurant.findById(req.params.id);
        if (!restaurant) return res.status(404).json({ message: 'Restaurant not found' });
        res.status(200).json(restaurant);
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving restaurant', error: error.message });
    }
};


exports.updateRestaurant = async (req, res) => {
    try {
        const user = req.user;
        if (user.role !== 'admin')
            return res.status(403).json({ message: 'Unauthorized' });

        const { error } = validate(req.body);
        if (error) return res.status(400).json({ message: error.message });

        const restaurant = await Restaurant.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!restaurant) return res.status(404).json({ message: 'Restaurant not found' });
        res.status(200).json(restaurant);
    } catch (error) {
        res.status(500).json({ message: 'Error updating restaurant', error });
    }
};


exports.deleteRestaurant = async (req, res) => {
    try {
        const user = req.user;
        if (user.role !== 'admin')
            return res.status(403).json({ message: 'Unauthorized' });

        const restaurant = await Restaurant.findByIdAndDelete(req.params.id);
        if (!restaurant) return res.status(404).json({ message: 'Restaurant not found' });
        res.status(200).json({ message: 'Restaurant deleted', restaurant: restaurant });
    } catch (error) {
        res.status(500).json({ message: 'Error deleting restaurant', error: error.message });
    }
};
