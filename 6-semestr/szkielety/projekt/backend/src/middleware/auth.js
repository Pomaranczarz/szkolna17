const jwt = require('jsonwebtoken');
const { User } = require('../models/User');

/**
 * Authenticates a user by verifying their token.
 *
 * @param {Object} req - The request object.
 * @param {Object} res - The response object.
 * @param {Function} next - The next middleware function in the application's request-response cycle.
 * @return {void}
 */
exports.authenticateUser = async (req, res, next) => {
    try {
        // Pobranie tokena z nagłówka
        if (typeof req.headers.authorization === 'undefined')
            return res.status(400).json({ message: "No auth token provided" });

        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(401).json({ message: 'Unauthorized' });
        }

        const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
        // Przypisanie ID użytkownika do obiektu żądania
        req.userId = decodedToken.userId;
        // jeśli użytkownik o takim id nie istnieje, zwracamy błąd
        const user = await User.findById(req.userId);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }
        req.user = user;
        next(); // Kontynuuj do następnej funkcji pośredniczącej
    } catch (error) {
        return res.status(401).json({ message: 'Unauthorized', error: error.message });
    }
};
