const mongoose = require('mongoose');
const joi = require('joi');

const reservationSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    restaurantId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Restaurant',
        required: true,
    },
    reservationDate: {
        type: Date,
        required: true,
    },
    numberOfGuests: {
        type: Number,
        required: true,
    },
    specialRequests: {
        type: String,
        required: false,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

const Reservation = mongoose.model('Reservations', reservationSchema);

const validate = (data) => {
    const schema = joi.object({
        userId: joi.string().required().label('User ID'),
        restaurantId: joi.string().required().label('Restaurant ID'),
        reservationDate: joi.date().required().label('Reservation Date'),
        numberOfGuests: joi.number().required().label('Number of Guests'),
        specialRequests: joi.string().allow('').optional().label('Special Requests'),
        status: joi.string().valid('pending', 'confirmed', 'cancelled').optional().label('Status'),
    });

    return schema.validate(data);
};

module.exports = { Reservation, validate };
