const mongoose = require('mongoose');
const joi = require('joi');
const passwordComplexity = require('joi-password-complexity');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        default: 'user',
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

const User = mongoose.model('Users', userSchema);

const registerValidate = (data) => {
    const schema = joi.object({
        username: joi.string().required().label('Username'),
        email: joi.string().email().required().label('Email'),
        password: passwordComplexity().required().label('Password'),
    });

    return schema.validate(data);
};

const createValidate = (data, withPassword = false, withRole = false) => {
    const schema = joi.object({
        username: joi.string().regex(/^[a-zA-Z0-9]{4,}$/).required().label('Username'),
        email: joi.string().email().required().label('Email'),
        password: withPassword ? passwordComplexity().required().label('Password') : joi.string().optional().label('Password'),
        role: withRole ? joi.alternatives().try('admin', 'user').required().label('Role') : joi.string().optional().label('Role'),
    });

    return schema.validate(data);
};

module.exports = { User, registerValidate, createValidate };
