const mongoose = require('mongoose');
const joi = require('joi');

const restaurantSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    contactEmail: {
        type: String,
        required: true,
        unique: true,
    },
    contactPhone: {
        type: String,
        required: true,
        unique: true,
    },
    openingHours: {
        monday: { type: String },
        tuesday: { type: String },
        wednesday: { type: String },
        thursday: { type: String },
        friday: { type: String },
        saturday: { type: String },
        sunday: { type: String },
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

const Restaurant = mongoose.model('Restaurants', restaurantSchema);

const validate = (data) => {
    const schema = joi.object({
        name: joi.string().required().label('Name'),
        address: joi.string().required().label('Address'),
        city: joi.string().required().label('City'),
        description: joi.string().optional().label('Description'),
        contactEmail: joi.string().email().required().label('Contact Email'),
        contactPhone: joi.string().required().label('Contact Phone'),
        openingHours: joi.object({
            monday: joi.string().optional().label('Monday'),
            tuesday: joi.string().optional().label('Tuesday'),
            wednesday: joi.string().optional().label('Wednesday'),
            thursday: joi.string().optional().label('Thursday'),
            friday: joi.string().optional().label('Friday'),
            saturday: joi.string().optional().label('Saturday'),
            sunday: joi.string().optional().label('Sunday'),
        }).optional().label('Opening Hours'),
    });

    return schema.validate(data);
};

module.exports = { Restaurant, validate };
