const express = require('express');
const app = express();
const PORT = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let metoda = (req, res, next) => {
    let text = "Metoda: " + req.method + "<br>"
    text += "Ścieżka: " + req.protocol + "://" + req.get('host') + req.originalUrl
    res.send(text)
    next()
}

app.use(metoda);

app.listen(PORT, () => {
    console.log(`Serwer działa na porcie ${PORT}`);
});