const React = require('react')
const HelloMessage = (props) => {
    return <div>
        Użytkownik: <b>{props.username}</b><br></br>
        Email: {props.email}<br></br>
        Wiek: {props.age}
    </div>
}
module.exports = HelloMessage