const express = require('express');
const app = express();
const PORT = 3000;

const degreesToRadians = (degrees) => { return degrees * (Math.PI / 180); };
const radiansToDegrees = (radians) => { return radians * (180 / Math.PI); };

app.get('/', (req, res) => {
    const value = parseFloat(req.query.value);
    if (isNaN(value))
        res.send('Podano nieprawidłową wartość');

    const toRad = req.query.toRad === 'true';

    if (toRad) {
        res.send(`${value} stopni to ${degreesToRadians(value)} radianów`);
    } else {
        res.send(`${value} radianów to ${radiansToDegrees(value)} stopni`);
    }
});

app.listen(PORT, () => console.log(`Serwer działa na porcie ${PORT}`));