const express = require('express');
const app = express();
const PORT = 3000;
const getDate = require('./server-files/getDate');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let metoda = (req, res, next) => {
    console.log(`${getDate()} --- Klient wysłał zapytanie o plik ${req.originalUrl}`);
    res.status(200).send('OK');
}

app.use(metoda);

app.listen(PORT, () => {
    console.log(getDate(), `=== Serwer zostaje uruchomiony na porcie ${PORT}`);
});