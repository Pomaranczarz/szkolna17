const isAuthorized = (req, res, next) => {
    // if user requested /form, allow them
    if (req.originalUrl === "/form") {
        next();
        return;
    }

    const { password } = req.body;
    if (password === "secretpasswd") {
        next();
    } else {
        res.status(401).send("Dostęp zabroniony");
    }
};

module.exports = isAuthorized;
