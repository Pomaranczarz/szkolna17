const express = require('express');
const isAuthorized = require('./autoryzacja');

const router = express.Router();

router.use(isAuthorized);

router.get("/form", (req, res) => {
    res.sendFile("../form10.html");
});

router.post("/result", (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let languages = req.body.language;
    let response = "";

    if (!username || !password) {
        res.send("Uzupełnij dane!");
    } else {
        response += "Użytkownik: " + username + "<br>Hasło: " + password;
    }

    response += "<br>Wybrane języki: <br>";

    if (!languages) {
        response += "- brak";
    }
    else {
        languages.forEach(language => {
            response += "- " + language + "<br>";
        });
    }
    res.send(response);
});

module.exports = router;
