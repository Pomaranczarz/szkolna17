const express = require('express')
const path = require('path')
const app = express()
const PORT = 3000
const users = require('./users.js')

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const data = users.users

app.listen(PORT, () => {
    console.log("PORT: ", PORT)
})

app.get('/api/users', (req, res) => {
    res.json(users);
});

app.get('/api/users/:id', (req, res) => {
    const found = data.some(user => user.id === parseInt(req.params.id))
    if (found) {
        res.json(data.filter(user => user.id === parseInt(req.params.id)))
    } else {
        res.status(400).json({ msg: `Użytkownik o id ${req.params.id} nie został odnaleziony` })
    }
})

app.post('/api/users', (req, res) => {
    const newUser = {
        id: data.length + 1,
        name: req.body.name,
        email: req.body.email,
        status: "aktywny"
    }
    if (!newUser.name || !newUser.email) {
        return res.status(400).json({ msg: 'Wprowadź poprawne imię i nazwisko oraz email' })
    }
    users.push(newUser)
    res.json(data)
})

app.patch('/api/users/:id', (req, res) => {
    const found = data.some(user => user.id === parseInt(req.params.id))
    if (found) {
        const updUser = req.body
        data.forEach(user => {
            if (user.id === parseInt(req.params.id)) {
                user.name = updUser.name ? updUser.name : user.name
                user.email = updUser.email ? updUser.email : user.email
                res.json({ msg: 'Dane użytkownika zaktualizowane', user })
            }
        })
    } else {
        res.status(400).json({ msg: `Użytkownik o id ${req.params.id} nie istnieje` })
    }
})

app.delete('/api/users/:id', (req, res) => {
    const { id } = req.params;
    const index = data.findIndex(user => user.id === parseInt(id));
    if (index !== -1) {
        data.splice(index, 1);
        res.json({ msg: 'Użytkownik został usunięty', data });
    } else {
        res.status(400).json({ msg: `Użytkownik o ID ${id} nie został znaleziony` });
    }
});

