const express = require('express');
const path = require('path');
const app = express();
const PORT = 3000;

const htmlFilesPath = path.join(__dirname, 'html_files');

app.get('/strona[1-5]', (req, res) => {
    res.sendFile(path.join(htmlFilesPath, `strona${req.originalUrl.slice(-1)}.html`));
})

app.listen(PORT, () => {
    console.log(`Serwer działa na porcie ${PORT}`);
});
