const express = require('express');
const path = require('path');
const app = express();
const reactEngine = require('express-react-views');
const PORT = 3000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.set('view engine', 'jsx');
app.engine('jsx', reactEngine.createEngine());

app.get("/form", (req, res) => {
    res.sendFile(path.join(__dirname, "form15.html"));
});

app.post('/about', (req, res) => {
    const { username, email, age } = req.body;
    res.render('about', {
        username: username,
        email: email,
        age: age
    })
})

app.listen(PORT, () => console.log(`Serwer działa na porcie ${PORT}`));