const express = require('express');
const app = express();
const PORT = 3000;

app.get('/', (req, res) => {
    const bgColor = req.query.bg;

    const html = `
        <html>
        <head>
            <title>Kolorowe tło</title>
        </head>
        <body style="background-color: ${bgColor || 'white'};">
        </body>
        </html>
    `;

    res.send(html);
});

app.listen(PORT, () => console.log(`Serwer działa na porcie ${PORT}`));
