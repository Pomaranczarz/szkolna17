const express = require('express');
const app = express();
const PORT = 3000;
const routes = require('./middleware/routes');

app.use(express.urlencoded({ extended: true }));

app.use('/', routes);

app.listen(PORT, () => console.log(`Serwer działa na porcie ${PORT}`));