package pl.pollub.android.app_3;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Helper class binding EditText with validation mechanism.
 */
public class FormTextField {
    private final EditText field;
    private final String key;
    private Toast onErrorToast;
    private FormTextFieldConstraint fieldConstraint;
    private Runnable errorCallback = () -> {
    };
    private Runnable correctCallback = () -> {
    };
    private boolean checkErrorOnFocusLost = false;
    private boolean checkErrorOnContentChange = false;
    private boolean toastOnError = false;
    private CharSequence errorMessage;

    public FormTextField(@NonNull EditText field, String key) {
        this.key = key;
        this.field = field;
    }

    /**
     * Check if field satisfies given constraint
     *
     * @return constraint(fieldContent) if constraint is specified; false otherwise
     */
    public boolean isCorrect() {
        return fieldConstraint != null && fieldConstraint.apply(field.getText());
    }

    /**
     * Get bundle key associated with field
     */
    public String getKey() {
        return key;
    }

    /**
     * Get field content
     */
    public String getContent() {
        return field.getText().toString();
    }

    /**
     * Set field content to a given String
     */
    public void setContent(String content) {
        field.setText(content);
    }

    /**
     * If flag is true, a toast will be shown on error
     *
     * @return this
     */
    public FormTextField withToastOnError(boolean flag) {
        toastOnError = flag;
        if (toastOnError && onErrorToast == null) { // if toast is not already created, create it
            onErrorToast = Toast.makeText(
                    field.getContext(),
                    errorMessage,
                    Toast.LENGTH_SHORT
            );
        }

        updateEventListeners();
        return this;
    }

    /**
     * Sets constraint for field
     *
     * @param constraint constraint for field content
     * @return this
     */
    public FormTextField withConstraint(FormTextFieldConstraint constraint) {
        fieldConstraint = constraint;
        updateEventListeners();
        return this;
    }

    /**
     * Clear field content
     */
    public void clear() {
        field.setText("");
    }

    /**
     * Sets error message
     *
     * @param message error message
     * @return this
     */
    public FormTextField withErrorMessage(CharSequence message) {
        this.errorMessage = message;
        updateEventListeners();
        return this;
    }

    /**
     * If flag is true, error will be checked on focus lost
     *
     * @param flag
     * @return this
     */
    public FormTextField withErrorCheckOnFocusLost(boolean flag) {
        checkErrorOnFocusLost = flag;
        updateEventListeners();
        return this;
    }

    /**
     * If flag is true, error will be checked on content change
     *
     * @param flag
     * @return this
     */
    public FormTextField withErrorCheckOnContentChange(boolean flag) {
        checkErrorOnContentChange = flag;
        updateEventListeners();
        return this;
    }

    /**
     * Sets error callback
     *
     * @param callback callback to be executed when field is incorrect
     * @return this
     */
    public FormTextField onErrorCallback(Runnable callback) {
        this.errorCallback = callback;
        updateEventListeners();
        return this;
    }

    /**
     * Sets correct callback
     *
     * @param callback callback to be executed when field is correct
     * @return this
     */
    public FormTextField onCorrectCallback(Runnable callback) {
        this.correctCallback = callback;
        updateEventListeners();
        return this;
    }

    /**
     * Sets correct and error callback
     *
     * @param callback callback to be executed
     * @return this
     */
    public FormTextField onStateChangedCallback(Runnable callback) {
        this.correctCallback = callback;
        this.errorCallback = callback;
        updateEventListeners();

        return this;
    }

    /**
     * Sets event listeners based on the state of the flags
     */
    private void updateEventListeners() {
        // if no constraint or error message is set, do nothing
        if (fieldConstraint == null || errorMessage == null)
            return;

        if (checkErrorOnFocusLost)
            field.setOnFocusChangeListener(getOnFocusChangeListener());

        if (checkErrorOnContentChange)
            field.addTextChangedListener(getOnContentChangeListener());
    }

    /*
     Returns onFocusChangeListener matching the state of the flags
     if field is incorrect:
         - if toastOnError is true, toast will be shown
         - errorMessage will be set as error for field
         - errorCallback will be executed
     if field is correct:
        - correctCallback will be executed
     */
    private View.OnFocusChangeListener getOnFocusChangeListener() {
        return (view, hasFocus) -> {
            if (hasFocus)
                return;

            if (!fieldConstraint.apply(field.getText())) {
                if (toastOnError)
                    onErrorToast.show();

                field.setError(errorMessage);
                errorCallback.run();
                return;
            }

            correctCallback.run();
        };
    }

    /*
     Returns onContentChangeListener matching the state of the flags
     if field is incorrect:
         - if toastOnError is true, toast will be shown
         - errorMessage will be set as error for field
         - errorCallback will be executed
     if field is correct:
        - correctCallback will be executed
     */
    private FieldTextWatcher getOnContentChangeListener() {
        return new FieldTextWatcher((s) -> {
            if (!fieldConstraint.apply(s)) {
                if (toastOnError)
                    onErrorToast.show();

                field.setError(errorMessage);
                errorCallback.run();
                return;
            }

            correctCallback.run();
        });
    }

    public interface FormTextFieldConstraint extends Function<CharSequence, Boolean> {
    }

    private static class FieldTextWatcher implements TextWatcher {
        private final Consumer<CharSequence> onTextChangedCallback;

        public FieldTextWatcher(Consumer<CharSequence> onTextChangedCallback) {
            this.onTextChangedCallback = onTextChangedCallback;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            onTextChangedCallback.accept(s);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }
}