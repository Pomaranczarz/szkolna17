package pl.pollub.android.app_3;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadService extends IntentService {

    private static final String ACTION_DOWNLOAD = "pl.pollub.android.app_3.action.DOWNLOAD";
    private static final String EXTRA_PARAM1 = "pl.pollub.android.app_3.extra.PARAM1";
    public static final String BROADCAST_INTENT = "pl.pollub.android.app_3.BROADCAST_INTENT";
    public static final String DOWNLOADED_BYTES_INFO = "pl.pollub.android.app_3.DOWNLOADED_BYTES_INFO";
    public static final String NOTIFICATION_ID = "pl.pollub.android.app_3.NOTIFICATION_ID";
    public static final String NOTIFICATION_NAME = "pl.pollub.android.app_3.NOTIFICATION_NAME";

    private final ProgressInfo progressInfo = new ProgressInfo();
    private NotificationManager notificationManager;

    public DownloadService() {
        super("DownloadService");
    }

    public static void startDownloadAction(Context context, String param1) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_DOWNLOAD);
        intent.putExtra(EXTRA_PARAM1, param1);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null)
            return;

        final String action = intent.getAction();
        if (ACTION_DOWNLOAD.equals(action)) {
            final String param1 = intent.getStringExtra(EXTRA_PARAM1);
            handleDownloadAction(param1);
        }
    }

    private void handleDownloadAction(String address) {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationUtils.createNotificationChannel(notificationManager);

        try {
            URL url = new URL(address);
            HttpURLConnection connection = createURLConnection(url);
            progressInfo.setFileSize(connection.getContentLength());

            File file = FileUtils.createFile(getApplicationContext(), url);
            DataInputStream reader = new DataInputStream(connection.getInputStream());
            FileOutputStream fileOutputStream = new FileOutputStream(file.getPath());

            long downloadedBytes = FileUtils.downloadFile(reader, fileOutputStream, progressInfo, this::sendProgressUpdate);
            ProgressInfo.DownloadStatus downloadStatus = progressInfo.getFileSize() > 0 && downloadedBytes == progressInfo.getFileSize()
                    ? ProgressInfo.DownloadStatus.FINISHED
                    : ProgressInfo.DownloadStatus.ERROR;
            progressInfo.setDownloadStatus(downloadStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendProgressUpdate() {
        sendBroadcast();
        notificationManager.notify(1, NotificationUtils.createNotification(getApplicationContext(), progressInfo));
    }

    private void sendBroadcast() {
        Intent broadcastIntent = new Intent(BROADCAST_INTENT);
        Bundle bundle = new Bundle();
        bundle.putParcelable(DOWNLOADED_BYTES_INFO, progressInfo);
        broadcastIntent.putExtras(bundle);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    private HttpURLConnection createURLConnection(URL url) throws Exception {
        return (HttpURLConnection) url.openConnection();
    }
}

