package pl.pollub.android.app_3;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NotificationUtils {

    public static void createNotificationChannel(NotificationManager notificationManager) {
        NotificationChannel notificationChannel = new NotificationChannel(
                DownloadService.NOTIFICATION_ID,
                DownloadService.NOTIFICATION_NAME,
                NotificationManager.IMPORTANCE_LOW
        );
        notificationManager.createNotificationChannel(notificationChannel);
    }

    public static Notification createNotification(Context context, ProgressInfo progressInfo) {
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                notificationIntent,
                PendingIntent.FLAG_IMMUTABLE
        );

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle("App 3")
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_HIGH)
                .setChannelId(DownloadService.NOTIFICATION_ID);

        if (progressInfo.getDownloadStatus() == ProgressInfo.DownloadStatus.IN_PROGRESS)
            builder.setProgress(100, progressInfo.getPercentageProgress(), false);
        else
            builder.setContentText("Download finished");
        return builder.build();
    }
}