package pl.pollub.android.app_3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Patterns;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.net.ssl.HttpsURLConnection;


public class MainActivity extends AppCompatActivity {
    private FormTextField addressEt;
    private EditText fileSizeInfoEt;
    private EditText fileTypeInfoEt;
    private EditText downloadedBytesInfoEt;
    private Button downloadFileInfoBt;
    private Button downloadFileBt;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        downloadFileInfoBt.setOnClickListener(view -> downloadFileInfo());
        downloadFileBt.setOnClickListener(view -> DownloadService.startDownloadAction(this, addressEt.getContent()));
    }

    private void bindViews() {
        addressEt = new FormTextField(findViewById(R.id.address_et), null)
                .withConstraint((address) -> URLUtil.isValidUrl(address.toString()))
                .onStateChangedCallback(this::updateButtonsAvailability)
                .withErrorMessage("Address cannot be empty")
                .withErrorCheckOnFocusLost(true)
                .withErrorCheckOnContentChange(true);

        fileSizeInfoEt = findViewById(R.id.file_size_info_et);
        fileTypeInfoEt = findViewById(R.id.file_type_info_et);
        downloadedBytesInfoEt = findViewById(R.id.downloaded_bytes_info_et);
        downloadFileInfoBt = findViewById(R.id.download_file_info_bt);
        downloadFileBt = findViewById(R.id.download_file_bt);
        progressBar = findViewById(R.id.download_file_progress_prb);

        // disable fields since they should be read-only
        fileSizeInfoEt.setEnabled(false);
        fileTypeInfoEt.setEnabled(false);
        downloadedBytesInfoEt.setEnabled(false);
    }

    private void updateButtonsAvailability() {
        // enable download button only if address is correct
        downloadFileInfoBt.setEnabled(addressEt.isCorrect());
        downloadFileBt.setEnabled(addressEt.isCorrect());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        addressEt.setContent(savedInstanceState.getString("address"));
        fileSizeInfoEt.setText(savedInstanceState.getString("fileSize"));
        fileTypeInfoEt.setText(savedInstanceState.getString("fileType"));
        downloadedBytesInfoEt.setText(savedInstanceState.getString("downloadedBytes"));
        progressBar.setProgress(savedInstanceState.getInt("progress"));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("address", addressEt.getContent());
        outState.putString("fileSize", fileSizeInfoEt.getText().toString());
        outState.putString("fileType", fileTypeInfoEt.getText().toString());
        outState.putString("downloadedBytes", downloadedBytesInfoEt.getText().toString());
        outState.putInt("progress", progressBar.getProgress());
    }

    private void downloadFileInfo() {
        // address is not empty since we can only get here via button which is enabled if address is not empty
        String address = addressEt.getContent();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            FileInfo fileInfo = getFileInfoFromURL(address);
            if (fileInfo == null)
                return;
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> {
                fileTypeInfoEt.setText(fileInfo.getFileType());
                fileSizeInfoEt.setText(String.valueOf(fileInfo.getFileSize() == -1 ? "unknown" : fileInfo.getFileSize()));
            });
        });
    }

    private FileInfo getFileInfoFromURL(String address) {
        HttpsURLConnection connection = null;
        try {
            URL url = new URL(address);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            return new FileInfo(connection.getContentType(), connection.getContentLength());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection.disconnect();
        }
        return null;
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null)
                return;

            Bundle bundle = intent.getExtras();
            if (bundle == null)
                return;

            ProgressInfo progressInfo = bundle.getParcelable(DownloadService.DOWNLOADED_BYTES_INFO);

            switch (progressInfo.getDownloadStatus()) {
                case IN_PROGRESS:
                    downloadedBytesInfoEt.setText(String.valueOf(progressInfo.getDownloadedFileBytes()));
                    progressBar.setProgress(progressInfo.getPercentageProgress());
                    break;
                case ERROR:
                    Toast.makeText(context, "Download error", Toast.LENGTH_SHORT).show();
                    break;
                case FINISHED:
                    Toast.makeText(context, "Download finished", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadcastReceiver, new IntentFilter(DownloadService.BROADCAST_INTENT));
    }

    static class FileInfo {
        private final String fileType;
        private final long fileSize;

        public FileInfo(String fileType, long fileSize) {
            this.fileType = fileType;
            this.fileSize = fileSize;
        }

        public String getFileType() {
            return fileType;
        }

        public long getFileSize() {
            return fileSize;
        }
    }
}