package pl.pollub.android.app_3;

import android.os.Parcel;
import android.os.Parcelable;

public class ProgressInfo implements Parcelable {
    private long fileSize;
    private long downloadedFileBytes;

    public enum DownloadStatus {
        IN_PROGRESS,
        FINISHED,
        ERROR
    }

    private DownloadStatus downloadStatus;

    public ProgressInfo() {
        this.fileSize = 1;
        this.downloadedFileBytes = 0;
        this.downloadStatus = DownloadStatus.IN_PROGRESS;
    }

    protected ProgressInfo(Parcel in) {
        fileSize = in.readLong();
        downloadedFileBytes = in.readLong();
        downloadStatus = DownloadStatus.values()[in.readInt()];
    }

    public static final Creator<ProgressInfo> CREATOR = new Creator<ProgressInfo>() {
        @Override
        public ProgressInfo createFromParcel(Parcel in) {
            return new ProgressInfo(in);
        }

        @Override
        public ProgressInfo[] newArray(int size) {
            return new ProgressInfo[size];
        }
    };

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public long getDownloadedFileBytes() {
        return downloadedFileBytes;
    }

    public void setDownloadedFileBytes(long downloadedFileBytes) {
        this.downloadedFileBytes = downloadedFileBytes;
    }

    public DownloadStatus getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(DownloadStatus downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(fileSize);
        dest.writeLong(downloadedFileBytes);
        dest.writeInt(downloadStatus.ordinal());
    }

    public int getPercentageProgress() {
        return (int) (((double) getDownloadedFileBytes() / (double) getFileSize()) * 100D);
    }
}
