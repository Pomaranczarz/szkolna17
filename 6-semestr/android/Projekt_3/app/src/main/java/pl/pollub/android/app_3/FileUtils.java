package pl.pollub.android.app_3;

import android.content.Context;
import android.util.Log;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;

public class FileUtils {
    private static final int BUFFER_SIZE = 32767;

    public static File createFile(Context context, URL url) throws Exception {
        File tmpFile = new File(url.getFile());
        File file = new File(context.getFilesDir().getPath() + File.separator + "WWW_" + tmpFile.getName());
        if (file.exists()) {
            file.delete();
        }
        return file;
    }

    public static long downloadFile(
            DataInputStream reader,
            FileOutputStream fileOutputStream,
            ProgressInfo progressInfo,
            ProgressUpdateCallback callback
    ) throws Exception {
        byte[] buffer = new byte[BUFFER_SIZE];
        int downloaded = 0;
        long downloadedBytes = downloaded;
        progressInfo.setDownloadedFileBytes(downloadedBytes);

        while (downloaded != -1) {
            fileOutputStream.write(buffer, 0, downloaded);
            downloadedBytes += downloaded;
            downloaded = reader.read(buffer, 0, BUFFER_SIZE);
            progressInfo.setDownloadedFileBytes(downloadedBytes);
            callback.onProgressUpdate();
        }

        reader.close();
        fileOutputStream.close();
        return downloadedBytes;
    }

    public interface ProgressUpdateCallback {
        void onProgressUpdate();
    }
}
