package com.example.projekt_1;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class GradesActivity extends AppCompatActivity {
    static final String GRADES_STATE_KEY = "GRADES_STATE_KEY";
    static final String GRADES_KEY = "GRADES_KEY";
    private Button sendGradesButton;
    private RecyclerView subjectListRv;
    private final ArrayList<Subject> subjects = new ArrayList<>();
    private final Bundle bundleToMain = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grades);
        configureFields();
        getSubjectList();
        setupToolbar();

        if (savedInstanceState != null) {
            extractRestoredGrades(savedInstanceState);
        }

        SubjectListAdapter adapter = makeAdapter(subjects);
        subjectListRv.setAdapter(adapter);
        subjectListRv.setLayoutManager(new LinearLayoutManager(this));
        sendGradesButton.setOnClickListener(view -> {
            Intent intentToMain = getIntent();
            bundleToMain.putString(GradesActivity.GRADES_KEY, stringifyGradesState()); // send back grades
            intentToMain.putExtras(bundleToMain);
            setResult(RESULT_OK, intentToMain);
            this.finish();
        });
    }

    private void configureFields() {
        subjectListRv = findViewById(R.id.subject_list_rv);
        sendGradesButton = findViewById(R.id.send_grades_bt);
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true); // Enable the back button
        }
    }

    private SubjectListAdapter makeAdapter(ArrayList<Subject> subjects) {
        SubjectListAdapter adapter = new SubjectListAdapter(subjects, this);
        adapter.setBundle(bundleToMain);
        adapter.setIntent(getIntent());
        return adapter;
    }

    private void getSubjectList() {
        String[] subjectsNames = getResources().getStringArray(R.array.subjects_names);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                int numberOfSubjects = Integer.parseInt(bundle.getString(MainActivity.NUMBER_OF_GRADES_KEY));
                for (int i = 0; i < numberOfSubjects; i++) {
                    subjects.add(new Subject(subjectsNames[i], 3));
                }
            }
        }
    }

    private void extractRestoredGrades(Bundle savedInstanceState) {
        String gradesStateStr = savedInstanceState.getString(GRADES_STATE_KEY);
        if (gradesStateStr != null) {
            String[] grades = gradesStateStr.split(";");

            for (int i = 0; i < grades.length; ++i) {
                subjects.get(i).setGrade(Integer.parseInt(grades[i]));
            }
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(GRADES_STATE_KEY, stringifyGradesState());
        super.onSaveInstanceState(outState);
    }

    private String stringifyGradesState() {
        StringBuilder gradesState = new StringBuilder();
        subjects.forEach(subject -> {
            gradesState.append(subject.getGrade());
            gradesState.append(";");
        });
        return gradesState.toString();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        extractRestoredGrades(savedInstanceState);
    }
}