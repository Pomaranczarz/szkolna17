package com.example.projekt_1;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;
import java.util.function.Supplier;

public class MainActivity extends AppCompatActivity {
    private FormTextField name;
    private FormTextField lastname;
    private FormTextField markCount;
    private Button submitButton;
    private Button averageSubmitButton;
    private TextView averageText;
    private ActivityResultLauncher<Intent> resultLauncher;
    public static final String NUMBER_OF_GRADES_KEY = "NUMBER_OF_GRADES_KEY";
    private static final String NAME_KEY = "NAME_KEY";
    private static final String LASTNAME_KEY = "LASTNAME_KEY";
    private static final String MARK_COUNT_KEY = "MARK_COUNT_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configureFields();
        resultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), a -> {
        });
        submitButton.setOnClickListener(this::sendBundleToGradesActivity);
    }

    private void configureFields() {
        submitButton = findViewById(R.id.submit_button);
        averageSubmitButton = findViewById(R.id.average_button);
        averageText = findViewById(R.id.average_text);

        name = new FormTextField(findViewById(R.id.name_text_area), NAME_KEY)
                .withConstraint((s) -> !TextUtils.isEmpty(s))
                .withErrorMessage(getString(R.string.invalid_name_message))
                .withToastOnError(true)
                .withErrorCheckOnContentChange(true)
                .withErrorCheckOnFocusLost(true)
                .onCorrectCallback(this::updateButtonVisibility)
                .onErrorCallback(this::updateButtonVisibility);

        lastname = new FormTextField(findViewById(R.id.lastname_text_area), LASTNAME_KEY)
                .withConstraint((s) -> !TextUtils.isEmpty(s))
                .withErrorMessage(getString(R.string.invalid_lastname_message))
                .withToastOnError(true)
                .withErrorCheckOnContentChange(true)
                .withErrorCheckOnFocusLost(true)
                .onCorrectCallback(this::updateButtonVisibility)
                .onErrorCallback(this::updateButtonVisibility);

        markCount = new FormTextField(findViewById(R.id.mark_count_field), MARK_COUNT_KEY)
                .withConstraint(MainActivity::isMarkCountValid)
                .withErrorMessage(getString(R.string.invalid_mark_count_message))
                .withToastOnError(true)
                .withErrorCheckOnContentChange(true)
                .withErrorCheckOnFocusLost(true)
                .onCorrectCallback(this::updateButtonVisibility)
                .onErrorCallback(this::updateButtonVisibility);
    }

    private static boolean isMarkCountValid(CharSequence s) {
        try {
            int value = Integer.parseInt(s.toString());
            return value >= 5 && value <= 15;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null || resultCode != RESULT_OK)
            return;

        Bundle bundle = data.getExtras();
        if (bundle == null)
            return;

        String gradesStr = bundle.getString(GradesActivity.GRADES_KEY);
        if (gradesStr != null) {
            String[] grades = gradesStr.split(";");
            float average = (float) (Arrays.stream(grades).map(Integer::parseInt).reduce(0, Integer::sum)) / grades.length;
            updateAverageInfo(average);
        }
    }

    private void updateAverageInfo(float average) {
        setAverageInfoVisibility(true);
        averageText.setText(String.format(getString(R.string.average_base_text), average));
        String buttonText = getString(average >= 3.f ? R.string.passed : R.string.failed);
        averageSubmitButton.setText(buttonText);
        averageSubmitButton.setOnClickListener((view) -> {
            showEndToastAndQuit(average >= 3.f);
        });
    }

    private void setAverageInfoVisibility(boolean visible) {
        averageSubmitButton.setVisibility(visible ? View.VISIBLE : View.GONE);
        averageText.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void sendBundleToGradesActivity(View view) {
        Intent intent = new Intent(this, GradesActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(NUMBER_OF_GRADES_KEY, markCount.getContent());
        intent.putExtras(bundle);
        resultLauncher.launch(intent);
    }

    private void showEndToastAndQuit(boolean success) {
        if (success)
            Toast.makeText(this, R.string.success, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, R.string.failure, Toast.LENGTH_SHORT).show();

        // wait for 2 seconds and exit
        new Handler().postDelayed(this::finish, 2000);
    }

    private void updateButtonVisibility() {
        Supplier<Boolean> allFieldCorrect = () -> name.isCorrect() && lastname.isCorrect() && markCount.isCorrect();

        submitButton.setVisibility(allFieldCorrect.get() ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(name.getKey(), name.getContent());
        outState.putString(lastname.getKey(), lastname.getContent());
        outState.putString(markCount.getKey(), markCount.getContent());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        name.setContent(savedInstanceState.getString(name.getKey()));
        lastname.setContent(savedInstanceState.getString(lastname.getKey()));
        markCount.setContent(savedInstanceState.getString(markCount.getKey()));
    }
}