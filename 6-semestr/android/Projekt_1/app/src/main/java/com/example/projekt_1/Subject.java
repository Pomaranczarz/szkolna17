package com.example.projekt_1;

public class Subject {
    private String name;
    private int grade;

    public Subject(String name) {
        this.name = name;
        this.grade = 2;
    }

    public Subject(String name, int grade) {
        this.name = name;
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}

