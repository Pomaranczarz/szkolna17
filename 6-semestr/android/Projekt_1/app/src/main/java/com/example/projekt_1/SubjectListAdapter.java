package com.example.projekt_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SubjectListAdapter extends RecyclerView.Adapter<SubjectItemViewHolder> {
    private final List<Subject> subjectList;
    private Bundle bundle;
    private Intent intent;
    private final Activity contextActivity;

    public SubjectListAdapter(List<Subject> subjectList, Activity contextActivity) {
        this.subjectList = subjectList;
        this.contextActivity = contextActivity;
    }

    @NonNull
    @Override
    public SubjectItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View subjectItemView = contextActivity.getLayoutInflater().inflate(R.layout.subject_item, null); // fill layout
        return new SubjectItemViewHolder(subjectItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubjectItemViewHolder holder, int position) {
        Subject currentSubject = subjectList.get(position);
        holder.getSubjectName().setText(currentSubject.getName()); // set subject name
        holder.getSubjectGradeRg().setTag(currentSubject); // set subject reference
        holder.getSubjectGradeRg().check(getGradeId(currentSubject.getGrade())); // set subject grade

        bundle.putString(GradesActivity.GRADES_STATE_KEY, stringifyGradesState());
        intent.putExtras(bundle);
    }

    private String stringifyGradesState() {
        StringBuilder gradesState = new StringBuilder();
        subjectList.forEach(subject -> {
            gradesState.append(subject.getGrade());
            gradesState.append(";");
        });
        return gradesState.toString();
    }

    private int getGradeId(int gradeValue) {
        switch (gradeValue) {
            case 5:
                return R.id.grade_5;
            case 4:
                return R.id.grade_4;
            case 3:
                return R.id.grade_3;
            default:
                return R.id.grade_2;
        }
    }

    @Override
    public int getItemCount() {
        return subjectList.size();
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
