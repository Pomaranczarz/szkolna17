package com.example.projekt_1;

import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SubjectItemViewHolder extends RecyclerView.ViewHolder {
    private final TextView subjectName;
    private final RadioGroup subjectGrade;

    public SubjectItemViewHolder(@NonNull View itemView) {
        super(itemView);
        subjectName = itemView.findViewById(R.id.subject_name_tv);
        subjectGrade = itemView.findViewById(R.id.subject_grade_rg);
        subjectGrade.setOnCheckedChangeListener((radioGroup, i) -> {
            Subject currentSubject = (Subject) radioGroup.getTag(); // get bound subject
            currentSubject.setGrade(getGradeValue(i));
        });
    }

    private int getGradeValue(int gradeId) {
        if (gradeId == R.id.grade_5) {
            return 5;
        } else if (gradeId == R.id.grade_4) {
            return 4;
        } else if (gradeId == R.id.grade_3) {
            return 3;
        } else {
            return 2;
        }
    }

    public TextView getSubjectName() {
        return subjectName;
    }

    public RadioGroup getSubjectGradeRg() {
        return subjectGrade;
    }
}
