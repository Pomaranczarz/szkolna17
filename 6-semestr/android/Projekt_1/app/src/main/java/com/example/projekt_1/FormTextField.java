package com.example.projekt_1;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.function.Consumer;

public class FormTextField {
    private final EditText field;
    private Toast onErrorToast;
    private FormTextFieldConstraint fieldConstraint;
    private Callback errorCallback;
    private Callback correctCallback;
    private boolean checkErrorOnFocusLost = false;
    private boolean checkErrorOnContentChange = false;
    private boolean toastOnError = false;
    private CharSequence errorMessage;
    private final String key;

    public FormTextField(@NonNull EditText field, String key) {
        this.key = key;
        this.field = field;
    }

    /**
     * Check if field satisfies given constraint
     *
     * @return true if field satisfies given constraint; if no constraint is specified, returns false
     */
    public boolean isCorrect() {
        return fieldConstraint != null && fieldConstraint.isFieldCorrect(field.getText());
    }

    /**
     * Get field key
     *
     * @return field key
     */
    public String getKey() {
        return key;
    }

    /**
     * Get field content
     *
     * @return field content
     */
    public String getContent() {
        return field.getText().toString();
    }

    public void setContent(String content) {
        field.setText(content);
    }

    /**
     * If flag is true, a toast will be shown on error
     *
     * @param flag
     * @return this
     */
    public FormTextField withToastOnError(boolean flag) {
        toastOnError = flag;
        if (toastOnError) {
            onErrorToast = Toast.makeText(
                    field.getContext(),
                    errorMessage,
                    Toast.LENGTH_SHORT
            );
        }

        updateEventListeners();
        return this;
    }

    /**
     * Sets constraint for field
     *
     * @param constraint constraint
     * @return this
     */
    public FormTextField withConstraint(FormTextFieldConstraint constraint) {
        fieldConstraint = constraint;
        updateEventListeners();
        return this;
    }

    /**
     * Clear field content
     */
    public void clear() {
        field.setText("");
    }

    public interface FormTextFieldConstraint {
        boolean isFieldCorrect(CharSequence fieldContent);
    }

    /**
     * Sets error message
     *
     * @param message error message
     * @return this
     */
    public FormTextField withErrorMessage(CharSequence message) {
        this.errorMessage = message;
        updateEventListeners();
        return this;
    }

    /**
     * If flag is true, error will be checked on focus lost
     *
     * @param flag
     * @return this
     */
    public FormTextField withErrorCheckOnFocusLost(boolean flag) {
        checkErrorOnFocusLost = flag;
        updateEventListeners();
        return this;
    }

    /**
     * If flag is true, error will be checked on content change
     *
     * @param flag
     * @return this
     */
    public FormTextField withErrorCheckOnContentChange(boolean flag) {
        checkErrorOnContentChange = flag;
        updateEventListeners();
        return this;
    }

    /**
     * Sets error callback
     *
     * @param callback
     * @return this
     */
    public FormTextField onErrorCallback(Callback callback) {
        this.errorCallback = callback;
        updateEventListeners();
        return this;
    }

    /**
     * Sets correct callback
     *
     * @param callback
     * @return this
     */
    public FormTextField onCorrectCallback(Callback callback) {
        this.correctCallback = callback;
        updateEventListeners();
        return this;
    }

    public interface Callback {
        void callback();
    }

    /**
     * Sets event listeners based on the state of the flags
     */
    private void updateEventListeners() {
        if (fieldConstraint == null || errorMessage == null)
            return;

        if (checkErrorOnFocusLost) {
            field.setOnFocusChangeListener((view, hasFocus) -> {
                if (hasFocus)
                    return;

                if (!fieldConstraint.isFieldCorrect(field.getText())) {
                    if (toastOnError)
                        onErrorToast.show();

                    field.setError(errorMessage);
                    errorCallback.callback();
                } else
                    correctCallback.callback();
            });
        }

        if (checkErrorOnContentChange) {
            field.addTextChangedListener(new FieldTextWatcher((s) -> {
                if (!fieldConstraint.isFieldCorrect(s)) {
                    if (toastOnError)
                        onErrorToast.show();

                    field.setError(errorMessage);
                    errorCallback.callback();
                } else
                    correctCallback.callback();
            }));
        }
    }

    private static class FieldTextWatcher implements TextWatcher {
        private final Consumer<CharSequence> onTextChangedCallback;

        public FieldTextWatcher(Consumer<CharSequence> onTextChangedCallback) {
            this.onTextChangedCallback = onTextChangedCallback;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            onTextChangedCallback.accept(s);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }
}
