package pl.pollub.android.app_2;

public interface OnItemClickListener {
    void onItemClick(int position);
}
