package pl.pollub.android.app_2;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class PhoneActivity extends AppCompatActivity {
    private FormTextField manufacturerEt;
    private FormTextField modelEt;
    private FormTextField androidVersionEt;
    private FormTextField webSiteEt;
    private Button webSiteBt;
    private Button cancelBt;
    private Button saveBt;
    private long phoneId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        bindViews();
        updateButtonsEnableState();
        loadDataFromParentBundleToFormFields();

        saveBt.setOnClickListener(this::sendNewPhoneDataToParentActivity);
        cancelBt.setOnClickListener(this::cancelActivity);
        webSiteBt.setOnClickListener(this::openManufacturerWebsite);
    }

    private void bindViews() {
        // For each form field define constraint and error message displayed when constraint is not satisfied

        manufacturerEt = createFormTextField(findViewById(R.id.manufacturer_et), PhonesListActivity.PHONE_MANUFACTURER_KEY,
                manufacturer -> !TextUtils.isEmpty(manufacturer),
                getString(R.string.invalid_manufacturer_msg)
        );
        modelEt = createFormTextField(findViewById(R.id.model_et), PhonesListActivity.PHONE_MODEL_KEY,
                model -> !TextUtils.isEmpty(model),
                getString(R.string.invalid_model_msg)
        );
        androidVersionEt = createFormTextField(findViewById(R.id.android_version_et), PhonesListActivity.PHONE_ANDROID_VERSION_KEY,
                androidVersion -> androidVersion.toString().matches("[1-9][0-9]*"),
                getString(R.string.invalid_android_version_msg)
        );
        webSiteEt = createFormTextField(findViewById(R.id.web_site_et), PhonesListActivity.PHONE_WEB_SITE_KEY,
                website -> URLUtil.isValidUrl(website.toString()),
                getString(R.string.invalid_web_site_msg)
        );

        webSiteBt = findViewById(R.id.web_site_bt);
        cancelBt = findViewById(R.id.cancel_bt);
        saveBt = findViewById(R.id.save_bt);
    }

    // Helper function for creating form fields sharing common properties eg. checking for error on content change
    private FormTextField createFormTextField(
            EditText field,
            String key,
            FormTextField.FormTextFieldConstraint constraint,
            String errorMessage
    ) {
        return new FormTextField(field, key)
                .withConstraint(constraint)
                .withErrorCheckOnContentChange(true)
                .withErrorCheckOnFocusLost(true)
                .withErrorMessage(errorMessage)
                .onStateChangedCallback(this::updateButtonsEnableState);
    }

    private void updateButtonsEnableState() {
        webSiteBt.setEnabled(webSiteEt.isCorrect()); // can go to website if URL is valid
        saveBt.setEnabled(areAllFieldsValid()); // can save phone only if all fields are valid
    }

    private boolean areAllFieldsValid() {
        return manufacturerEt.isCorrect() && modelEt.isCorrect() && androidVersionEt.isCorrect() && webSiteEt.isCorrect();
    }

    private void openManufacturerWebsite(View view) {
        // we don't need to check if url is valid since we can only get here by clicking on webSiteBt
        // which is enabled only if url is valid

        String url = webSiteEt.getContent();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(android.net.Uri.parse(url));
        startActivity(intent);
    }

    private void cancelActivity(View v) {
        Intent intent = getIntent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    private void sendNewPhoneDataToParentActivity(View v) {
        Intent intent = getIntent();
        Bundle bundle = getNewPhoneDataBundle();
        intent.putExtras(bundle);

        setResult(RESULT_OK, intent);
        finish();
    }

    private Bundle getNewPhoneDataBundle() {
        Bundle bundle = new Bundle();
        bundle.putLong(PhonesListActivity.PHONE_ID_KEY, phoneId);
        bundle.putString(manufacturerEt.getKey(), manufacturerEt.getContent());
        bundle.putString(modelEt.getKey(), modelEt.getContent());
        bundle.putString(androidVersionEt.getKey(), androidVersionEt.getContent());
        bundle.putString(webSiteEt.getKey(), webSiteEt.getContent());

        return bundle;
    }

    private void loadDataFromParentBundleToFormFields() {
        Intent intent = getIntent();
        if (intent == null)
            return;

        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return;

        this.phoneId = bundle.getLong(PhonesListActivity.PHONE_ID_KEY);
        if (phoneId <= 0)
            return;

        manufacturerEt.setContent(bundle.getString(manufacturerEt.getKey()));
        modelEt.setContent(bundle.getString(modelEt.getKey()));
        androidVersionEt.setContent(bundle.getString(androidVersionEt.getKey()));
        webSiteEt.setContent(bundle.getString(webSiteEt.getKey()));
    }
}