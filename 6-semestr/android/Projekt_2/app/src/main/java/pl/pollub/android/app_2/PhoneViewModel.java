package pl.pollub.android.app_2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import pl.pollub.android.app_2.model.Phone;
import pl.pollub.android.app_2.model.PhoneRepository;

public class PhoneViewModel extends AndroidViewModel {
    private LiveData<List<Phone>> allPhones;
    private PhoneRepository repository;

    public PhoneViewModel(@NonNull Application application) {
        super(application);
        repository = new PhoneRepository(application);
        allPhones = this.repository.getLiveData();
    }

    public PhoneRepository getRepository() {
        return repository;
    }

    public void insert(Phone phone) {
        repository.insert(phone);
    }

    public void update(Phone phone) {
        repository.update(phone);
    }

    public LiveData<List<Phone>> getAllPhones() {
        return allPhones;
    }

    public void delete(Phone phoneToDelete) {
        repository.delete(phoneToDelete);
    }
}
