package pl.pollub.android.app_2;

import static androidx.recyclerview.widget.ItemTouchHelper.LEFT;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import pl.pollub.android.app_2.model.Phone;

public class PhonesListActivity extends AppCompatActivity {
    public static String PHONE_ID_KEY = "PHONE_ID_KEY";
    public static String PHONE_MANUFACTURER_KEY = "PHONE_MANUFACTURER_KEY";
    public static String PHONE_MODEL_KEY = "PHONE_MODEL_KEY";
    public static String PHONE_ANDROID_VERSION_KEY = "PHONE_ANDROID_VERSION_KEY";
    public static String PHONE_WEB_SITE_KEY = "PHONE_WEB_SITE_KEY";
    private RecyclerView phoneListRv;
    private PhoneInfoListAdapter adapter;
    private PhoneViewModel phoneViewModel;
    private ActivityResultLauncher<Intent> launcher;
    private AlertDialog deleteAllPhonesAlertDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phones_list);
        bindViews();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(getItemTouchHelperCallback());
        itemTouchHelper.attachToRecyclerView(phoneListRv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.delete_all_phones_bt) {
            if (deleteAllPhonesAlertDialog == null)
                createDeleteAllPhonesAlertDialog();

            deleteAllPhonesAlertDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void createDeleteAllPhonesAlertDialog() {
        deleteAllPhonesAlertDialog = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Deleting all phones")
                .setMessage("Are you sure you want to delete all phones?")
                .setPositiveButton("Yes", (dialog, which) -> deleteAllPhones())
                .setNegativeButton("No", null)
                .create();
    }

    private void deleteAllPhones() {
        List<Phone> allPhones = phoneViewModel.getAllPhones().getValue();
        if (allPhones != null)
            allPhones.forEach(phone -> phoneViewModel.delete(phone));
    }

    private ItemTouchHelper.Callback getItemTouchHelperCallback() {
        return new ItemTouchHelper.SimpleCallback(LEFT, LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            // if element is swiped, delete it
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                Phone phoneToDelete = phoneViewModel.getAllPhones().getValue().get(viewHolder.getAdapterPosition());
                phoneViewModel.delete(phoneToDelete);
            }
        };
    }

    private void bindViews() {
        // if we get an activity result, pass it to upsertPhoneFromChildActivity
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::upsertPhoneFromChildActivity);

        // if phone is clicked on, pass it to sendChosenPhoneToEditActivity
        adapter = new PhoneInfoListAdapter(this);
        adapter.setListener(this::sendChosenPhoneToEditActivity);

        // set up RecyclerView
        phoneListRv = findViewById(R.id.phones_list_rv);
        phoneListRv.setAdapter(adapter);
        phoneListRv.setLayoutManager(new LinearLayoutManager(this));

        // instantiate PhoneViewModel
        phoneViewModel = new ViewModelProvider(this).get(PhoneViewModel.class);
        phoneViewModel.getAllPhones().observe(this, phones -> adapter.setPhoneList(phones));

        FloatingActionButton addPhoneFab = findViewById(R.id.add_phone_fab);
        addPhoneFab.setOnClickListener(view -> this.launchAddPhoneActivity());
    }

    private void launchAddPhoneActivity() {
        Bundle bundle = getAddPhoneBundle();
        Intent intent = new Intent(this, PhoneActivity.class);
        intent.putExtras(bundle);
        launcher.launch(intent);
    }

    private Bundle getAddPhoneBundle() {
        Bundle bundle = new Bundle();
        bundle.putLong(PHONE_ID_KEY, 0);
        return bundle;
    }

    private void upsertPhoneFromChildActivity(ActivityResult o) {
        if (o.getResultCode() != RESULT_OK)
            return;

        Intent intent = o.getData();
        if (intent == null)
            return;

        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return;

        // we don't validate the phone data here since it's already validated in PhoneActivity
        Phone phone = bundleToPhone(bundle);
        if (phone == null)
            return;

        if (phone.getId() > 0) // if id > 0 - the incoming phone is edited
            phoneViewModel.update(phone); // so we update it
        else
            phoneViewModel.insert(phone); // otherwise we insert it
    }

    private void sendChosenPhoneToEditActivity(int position) {
        Phone currentPhone = this.adapter.getPhoneList().get(position);

        Bundle bundle = phoneToBundle(currentPhone);
        Intent intent = new Intent(this, PhoneActivity.class);
        intent.putExtras(bundle);

        this.launcher.launch(intent);
    }

    private Phone bundleToPhone(Bundle bundle) {
        long id = bundle.getLong(PHONE_ID_KEY);
        String manufacturer = bundle.getString(PHONE_MANUFACTURER_KEY);
        String model = bundle.getString(PHONE_MODEL_KEY);
        String androidVersion = bundle.getString(PHONE_ANDROID_VERSION_KEY);
        String webSite = bundle.getString(PHONE_WEB_SITE_KEY);

        // if any of the fields is null, return null
        if (manufacturer == null || model == null || androidVersion == null || webSite == null)
            return null;

        if (id > 0)
            return new Phone(id, manufacturer, model, androidVersion, webSite);
        else
            return new Phone(manufacturer, model, androidVersion, webSite);
    }

    private Bundle phoneToBundle(Phone phone) {
        Bundle bundle = new Bundle();
        bundle.putLong(PHONE_ID_KEY, phone.getId());
        bundle.putString(PHONE_MANUFACTURER_KEY, phone.getManufacturer());
        bundle.putString(PHONE_MODEL_KEY, phone.getModel());
        bundle.putString(PHONE_ANDROID_VERSION_KEY, phone.getAndroidVersion());
        bundle.putString(PHONE_WEB_SITE_KEY, phone.getWebSite());

        return bundle;
    }
}