function checkForm() {
    const nameRegex = /^[A-Z][a-z]+( [A-Z][a-z]+)?$/;
    const lastnameRegex = /^[A-Z][a-z]+(-[A-Z][a-z]+)?$/;
    const emailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/

    if (!checkField("name", nameRegex)) {
        document.getElementById("name-error").innerHTML = "Wpisz poprawnie imię!";
        return false;
    } else
        document.getElementById("name-error").innerHTML = "";

    if (!checkField("lastname", lastnameRegex)) {
        document.getElementById("lastname-error").innerHTML = "Wpisz poprawnie nazwisko!";
        return false;
    }

    if (!checkField("email", emailRegex)) {
        document.getElementById("email-error").innerHTML = "Wpisz poprawnie email!";
        return false;
    }

    if (!checkCheckboxes()) {
        document.getElementById("lang-select-error").innerHTML = "Musisz wybrać produkt!"
        return false;
    }

    if (!checkRadio("payment-method")) {
        document.getElementById("payment-method-error").innerHTML = "Musisz wskazać sposób płatności!";
        return false;
    }

    return true;
}

function checkField(fieldId, fieldRegex) {
    let field = document.getElementById(fieldId);

    if (field === null) {
        console.log(fieldId + " is null");
        return false;
    }

    if (field.value === null) {
        console.log(field + ".value is null");
        return false;
    }


    console.log("Field " + fieldId + " isn't null");

    return fieldRegex.test(field.value);
}

function checkRadio(radioName) {
    let fields = document.getElementsByName(radioName);
    for (const element of fields)
        if (element.checked)
            return true;

    return false;
}

function checkBox(boxId) {
    let field = document.getElementById(boxId);

    if (!field)
        return false;

    return field.checked;
}

function checkCheckboxes() {
    return checkBox("php") || checkBox("cpp") || checkBox("java");
}

function showData() {
    let data = "Dane z wypełnionego przez Ciebie formularza:\n";
    data += "Nazwisko: " + document.getElementById("lastname").value;
    data += "\nWiek: " + document.getElementById("age").value;
    data += "\nKraj: " + document.getElementById("country-select").value;
    data += "\nemail: " + document.getElementById("email").value;

    data += "\nWybrane produkty:";
    for (const field of document.getElementsByName("lang-selected"))
        if (field.checked)
            data += " " + field.value;

    data += "\nSposób zapłaty: ";
    for (const field of document.getElementsByName("payment-method"))
        if (field.checked) {
            data += field.value;
            break;
        }

    return window.confirm(data);
}