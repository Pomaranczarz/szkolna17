function getInstallmentAmount() {
    let loanAmount = getValidElementValue("loan-amount", 100, Infinity);
    if (loanAmount === null)
        alert("Niewłaściwa kwota pożyczki");

    let yearlyInterest = getValidElementValue("yearly-interest", 0.01, 100);
    if (yearlyInterest === null)
        alert("Niewłaściwe oprocentowanie");

    let numberOfInstallments = getValidElementValue("number-of-installments", 1, Infinity);
    if (numberOfInstallments === null)
        alert("Niewłaściwa liczba rat");

    let monthlyInterest = yearlyInterest * 0.01 / 12;

    let installmentAmount = (loanAmount * monthlyInterest) / (1 - 1 / Math.pow(1 + monthlyInterest, numberOfInstallments));

    let totalAmountElement = document.getElementById("total-amount");
    let installmentAmountElement = document.getElementById("installment-amount");

    installmentAmountElement.value = installmentAmount.toFixed(2);
    totalAmountElement.value = (installmentAmount * numberOfInstallments).toFixed(2);
}

function getValidElementValue(elementId, validRangeBegin, validRangeEnd) {
    if (validRangeBegin > validRangeEnd)
        return null;

    let value = parseFloat(document.getElementById(elementId).value);

    if (isNaN(value) || value < validRangeBegin || value > validRangeEnd)
        return null;
    else
        return value;
}