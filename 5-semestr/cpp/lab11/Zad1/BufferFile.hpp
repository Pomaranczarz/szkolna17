#ifndef BUFFER_FILE_HPP
#define BUFFER_FILE_HPP

#include "Buffer.hpp"
#include <fstream>
#include <memory>
#include <string>
#include <utility>

class BufferFile : public Buffer {
public:
    BufferFile(const BufferFile&) = delete;
    BufferFile(BufferFile&&) = delete;
    [[nodiscard]] auto operator=(const BufferFile&) -> BufferFile& = delete;
    [[nodiscard]] auto operator=(BufferFile&&) -> BufferFile& = delete;

    explicit BufferFile(std::string filename)
        : filename_{ std::move(filename) }
    {
        file = std::make_unique<std::fstream>(filename_, std::ios::in | std::ios::out | std::ios::app);
        std::cout << "Konstruktor BufferFile\n";
    }

    ~BufferFile() override { std::cout << "Destruktor BufferFile\n"; }

    void add(int a) override { *file << a << std::endl; }

    void write() override
    {
        file->seekg(0);

        std::string line;
        while (std::getline(*file, line)) {
            std::cout << line << std::endl;
        }
    }

private:
    std::string filename_;
    std::unique_ptr<std::fstream> file;
};

#endif        // BUFFER_FILE_HPP