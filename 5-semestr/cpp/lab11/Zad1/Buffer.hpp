#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <iostream>

class Buffer {
public:
    Buffer() { std::cout << "Konstruktor buffer\n"; }

    Buffer(const Buffer&) = default;
    Buffer(Buffer&&) = delete;
    [[nodiscard]] auto operator=(const Buffer&) -> Buffer& = default;
    [[nodiscard]] auto operator=(Buffer&&) -> Buffer& = delete;

    virtual ~Buffer() { std::cout << "Destruktor buffer\n"; }

    virtual void add(int a) = 0;
    virtual void write() = 0;
};

#endif        // BUFFER_HPP