#include "BufferArr.hpp"
#include "BufferFile.hpp"

auto main() -> int
{
    Buffer* buffers[]{ new BufferArr{ 100 },       new BufferFile{ "f1.txt" }, new BufferFile{ "f2.txt" },
                       new BufferFile{ "f3.txt" }, new BufferArr{ 20 },        new BufferArr{ 10 } };

    for (auto& buffer : buffers) {
        for (int i = 0; i < 10; i++) {
            buffer->add(i);
        }

        buffer->write();
    }

    for (auto& buffer : buffers) {
        delete buffer;
    }
}