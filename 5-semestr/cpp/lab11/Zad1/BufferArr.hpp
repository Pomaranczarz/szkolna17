#ifndef BUFFER_ARR_HPP
#define BUFFER_ARR_HPP

#include "Buffer.hpp"
#include <memory>

class BufferArr : public Buffer {
public:
    BufferArr(const BufferArr&) = delete;
    BufferArr(BufferArr&&) = delete;
    [[nodiscard]] auto operator=(const BufferArr&) -> BufferArr& = delete;
    [[nodiscard]] auto operator=(BufferArr&&) -> BufferArr& = delete;

    explicit BufferArr(size_t size)
        : size_{ size }
    {
        std::cout << "Konstruktor klasy BufferArr\n";
        data_ = std::make_unique<int[]>(size);
    }

    ~BufferArr() override { std::cout << "Destruktor klasy BufferArr\n"; }

    [[nodiscard]] auto size() const -> size_t { return size_; }

    [[nodiscard]] auto len() const -> size_t { return len_; }

    void add(int a) override
    {
        if (len_ + 1 == size_) {
            std::cout << "Bufor pelny\n";
            return;
        }

        data_[len_++] = a;
    }

    void write() override
    {
        for (size_t i = 0; i < len_; i++) {
            std::cout << data_[i] << (i + 1 == len_ ? '\n' : ' ');
        }
    }

private:
    std::unique_ptr<int[]> data_;
    size_t size_;
    size_t len_{};
};

#endif        // BUFFER_ARR_HPP