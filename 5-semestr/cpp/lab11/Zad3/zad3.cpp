#include "List.hpp"
#include <algorithm>
#include <iostream>

auto main() -> int
{
    List<int> list;

    for (int i = 0; i < 5; ++i) {
        list.push_back(i);
    }

    for (int i = 10; i < 15; ++i) {
        list.push_front(i);
    }

    std::for_each(list.begin(), list.end(), [](int i) { std::cout << i << " "; });
}