#ifndef LIST_HPP
#define LIST_HPP

#include <iostream>
#include <memory>

template <typename T>
class List {
private:
    struct Node {
        Node(const Node&) = delete;
        Node(Node&&) = delete;
        [[nodiscard]] auto operator=(const Node&) -> Node& = delete;
        [[nodiscard]] auto operator=(Node&&) -> Node& = delete;
        explicit Node(const T& data, std::shared_ptr<Node> next = nullptr, std::shared_ptr<Node> prev = nullptr);

        ~Node();

        T value;

        std::shared_ptr<Node> next;
        std::shared_ptr<Node> prev;
    };

    size_t size_{};

    std::shared_ptr<Node> head{};
    std::shared_ptr<Node> tail{};

public:
    List() = default;

    List(const List&) = delete;
    List(List&&) = delete;
    [[nodiscard]] auto operator=(const List&) -> List& = delete;
    [[nodiscard]] auto operator=(List&&) -> List& = delete;
    ~List();

    void push_back(T value);
    void pop_back();
    void push_front(T value);
    void pop_front();

    [[nodiscard]] auto operator[](size_t index) -> T&;

    [[nodiscard]] auto size() const -> size_t { return size_; }

    void clear();

    class iterator {
    private:
        friend class List;

        std::shared_ptr<Node> node_;

        explicit iterator(std::shared_ptr<Node> node);

    public:
        iterator() = default;
        iterator(const iterator&) = default;
        iterator(iterator&&) noexcept = default;
        auto operator=(const iterator&) -> iterator& = default;
        auto operator=(iterator&&) noexcept -> iterator& = default;
        ~iterator() = default;

        auto operator*() -> T&;
        auto operator->() -> T*;

        auto operator++() -> iterator;
        auto operator++(int) -> iterator;
        auto operator--() -> iterator;
        auto operator--(int) -> iterator;

        auto operator==(const iterator& rhs) const -> bool;
    };

    auto begin() -> iterator { return iterator(head); }

    auto end() -> iterator { return iterator(nullptr); }
};

template <typename T>
List<T>::Node::Node(const T& data, std::shared_ptr<Node> next, std::shared_ptr<Node> prev)
    : value{ data }
    , next{ next }
    , prev{ prev }
{
    std::cout << "Creating node\n";
}

template <typename T>
List<T>::Node::~Node()
{
    std::cout << "Destroying Node " << value << std::endl;
}

template <typename T>
List<T>::~List()
{
    std::cout << "Destroying list\n";
    clear();
}

template <typename T>
void List<T>::push_back(T value)
{
    if (size_ == 0) {
        head = tail = std::make_shared<Node>(value, nullptr, nullptr);
    }
    else {
        tail->next = std::make_shared<Node>(value, nullptr, tail);
        tail = tail->next;
    }

    ++size_;
}

template <typename T>
void List<T>::pop_back()
{
    if (size_ == 0) {
        return;
    }

    if (size_ == 1) {
        head = tail = nullptr;
        --size_;
        return;
    }

    tail = tail->prev;
    tail->next = nullptr;

    --size_;
}

template <typename T>
void List<T>::push_front(T value)
{
    if (size_ == 0) {
        head = tail = std::make_shared<Node>(value, nullptr, nullptr);
    }
    else {
        head->prev = std::make_shared<Node>(value, head, nullptr);
        head = head->prev;
    }

    ++size_;
}

template <typename T>
auto List<T>::operator[](size_t index) -> T&
{
    auto node = head;
    for (size_t i = 0; i < index; ++i) {
        node = node->next;
    }

    return node->value;
}

template <typename T>
void List<T>::pop_front()
{
    if (size_ == 0) {
        return;
    }

    if (size_ == 1) {
        head = tail = nullptr;
        --size_;
        return;
    }

    head = head->next;
    head->prev = nullptr;

    --size_;
}

template <typename T>
void List<T>::clear()
{
    while (size_ > 0) {
        pop_back();
    }
}

template <typename T>
List<T>::iterator::iterator(std::shared_ptr<Node> node)
    : node_{ std::move(node) }
{
}

template <typename T>
auto List<T>::iterator::operator*() -> T&
{
    return node_->value;
}

template <typename T>
auto List<T>::iterator::operator->() -> T*
{
    return node_.get();
}

template <typename T>
auto List<T>::iterator::operator++() -> List<T>::iterator
{
    node_ = node_->next;
    return *this;
}

template <typename T>
auto List<T>::iterator::operator++(int) -> List<T>::iterator
{
    auto tmp = *this;
    node_ = node_->next;
    return tmp;
}

template <typename T>
auto List<T>::iterator::operator--() -> List<T>::iterator
{
    node_ = node_->prev;
    return *this;
}

template <typename T>
auto List<T>::iterator::operator--(int) -> List<T>::iterator
{
    auto tmp = *this;
    node_ = node_->prev;
    return tmp;
}

template <typename T>
auto List<T>::iterator::operator==(const iterator& rhs) const -> bool
{
    return node_ == rhs.node_;
}

#endif        // LIST_HPP