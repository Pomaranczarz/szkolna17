cmake_minimum_required(VERSION 3.5)

project(lab11 VERSION 1.0 LANGUAGES CXX)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_compile_options(
    -Wall
    -Wextra
    -Wshadow
    -Wnon-virtual-dtor
    -pedantic -Wunused
    -Wconversion
    -Wsign-conversion
    -Wdouble-promotion
)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU") # Flags for gcc exclusively
    add_compile_options(
        -Wmisleading-indentation
        -Wduplicated-cond
        -Wduplicated-branches
        -Wlogical-op
        -Wnull-dereference
        -Wuseless-cast
    )
endif()

set(ZAD1_DIR Zad1)
set(ZAD2_DIR Zad2)
set(ZAD3_DIR Zad3)

set(ZAD1_SOURCES
    ${ZAD1_DIR}/zad1.cpp
)

set(ZAD2_SOURCES
    ${ZAD2_DIR}/zad2.cpp
    ${ZAD2_DIR}/Warehouse.hpp
    ${ZAD2_DIR}/Shop.hpp
)

set(ZAD3_SOURCES
    ${ZAD3_DIR}/zad3.cpp
    ${ZAD3_DIR}/List.hpp
)

add_executable(zad1 ${ZAD1_SOURCES})
add_executable(zad2 ${ZAD2_SOURCES})
add_executable(zad3 ${ZAD3_SOURCES})
