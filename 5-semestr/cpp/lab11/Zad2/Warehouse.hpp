#ifndef WAREHOUSE_HPP
#define WAREHOUSE_HPP

#include <iostream>
#include <string>
#include <utility>

class Warehouse {
public:
    Warehouse(const Warehouse&) = default;
    Warehouse(Warehouse&&) = delete;
    [[nodiscard]] auto operator=(const Warehouse&) -> Warehouse& = default;
    [[nodiscard]] auto operator=(Warehouse&&) -> Warehouse& = delete;

    Warehouse(std::string good_name, size_t good_count)
        : good_name_{ std::move(good_name) }
        , good_count_{ good_count }
    {
    }

    [[nodiscard]] auto to_string() const -> std::string { return good_name_ + std::to_string(good_count_); }

    [[nodiscard]] auto get_good_name() const -> std::string { return good_name_; }

    [[nodiscard]] auto get_good_count() const -> size_t { return good_count_; }

    void set_good_count(size_t good_count) { good_count_ = good_count; }

    void set_good_name(const std::string& good_name) { good_name_ = good_name; }

    ~Warehouse() { std::cout << "Destruktor klasy Warehouse.\n"; }

private:
    std::string good_name_;
    size_t good_count_;
};

#endif        // WAREHOUSE_HPP