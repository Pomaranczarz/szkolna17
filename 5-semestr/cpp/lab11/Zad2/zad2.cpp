#include "Shop.hpp"
#include "Warehouse.hpp"
#include <memory>

int main()
{
    // create a few warehouses
    std::shared_ptr<Warehouse> warehouse1 = std::make_shared<Warehouse>("Ksiazki", 10'000);
    std::shared_ptr<Warehouse> warehouse2 = std::make_shared<Warehouse>("Komputery", 20'000);
    std::shared_ptr<Warehouse> warehouse3 = std::make_shared<Warehouse>("Lozka", 15'000);
    std::shared_ptr<Warehouse> warehouse4 = std::make_shared<Warehouse>("Krzesla", 13'500);

    Shop shop1{ "Sklep 1" };
    Shop shop2{ "Sklep 2" };

    shop1.add_warehouse(warehouse1);
    shop1.add_warehouse(warehouse2);
    shop1.add_warehouse(warehouse3);

    shop2.add_warehouse(warehouse3);
    shop2.add_warehouse(warehouse4);

    shop1.sell_goods("Ksiazki", 5'000);
    shop1.sell_goods("Lozka", 8'000);

    shop2.sell_goods("Lozka", 5'000);
    shop2.sell_goods("Krzesla", 10'000);
}