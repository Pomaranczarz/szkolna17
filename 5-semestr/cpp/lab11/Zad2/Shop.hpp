#ifndef SHOP_HPP
#define SHOP_HPP

#include "Warehouse.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>

class Shop {
public:
    Shop(const Shop&) = default;
    Shop(Shop&&) = delete;
    [[nodiscard]] auto operator=(const Shop&) -> Shop& = default;
    [[nodiscard]] auto operator=(Shop&&) -> Shop& = delete;

    explicit Shop(std::string name)
        : name_{ std::move(name) }
    {
    }

    void add_warehouse(std::shared_ptr<Warehouse> warehouse) { warehouses_.push_back(std::move(warehouse)); }

    auto sell_goods(const std::string& name, size_t amount) -> bool
    {
        auto& warehouse = *std::find_if(warehouses_.begin(), warehouses_.end(),
                                        [&name](const auto& wh) { return wh->get_good_name() == name; });

        if (warehouse->get_good_count() < amount) {
            return false;
        }

        warehouse->set_good_count(warehouse->get_good_count() - amount);
        return true;
    }

    [[nodiscard]] auto used_warehouses() const -> std::vector<std::string>
    {
        std::vector<std::string> result;
        for (const auto& wh : warehouses_) {
            result.push_back(wh->get_good_name());
        }
        return result;
    }

    [[nodiscard]] auto get_name() const -> std::string { return name_; }

    ~Shop() { std::cout << "Destruktor klasy Shop.\n"; }

private:
    std::string name_;
    std::vector<std::shared_ptr<Warehouse>> warehouses_;
};

#endif        // SHOP_HPP