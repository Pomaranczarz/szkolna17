#include <QSet>
#include <QString>
#include <iostream>
#include <string>

bool isPangram(const QString& str);

int main(int argc, char *argv[])
{
    std::string line;
    std::cout << "Podaj napis: ";
    std::getline(std::cin, line);

    std::cout << "Czy pangram? - " << std::boolalpha << isPangram(QString::fromStdString(line)) << '\n';
}

bool isPangram(const QString& str)
{
    QSet<QChar> set;
    const QString alphabet{ "abcdefghijklmnopqrstwxyz" };
    for (const auto& c : str)
        set.insert(c);

    for (const auto& c : alphabet)
        if (!set.contains(c))
            return false;

    return true;
}
