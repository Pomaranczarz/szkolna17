/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *addCarButton;
    QPushButton *showCarButton;
    QWidget *formLayoutWidget;
    QFormLayout *addCarFormLayout;
    QLabel *brandFieldLabel;
    QLineEdit *brandField;
    QLabel *modelFieldLabel;
    QLineEdit *modelField;
    QLabel *productionYearLabel;
    QLineEdit *productionYearField;
    QLabel *vinNumberLabel;
    QLineEdit *vinNumberField;
    QPushButton *addCarSubmitButton;
    QPlainTextEdit *showCarsTextField;
    QPushButton *descendingByYear;
    QPushButton *alphabeticalByBrand;
    QPushButton *deleteCarButton;
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout;
    QLabel *deleteFromVinNumberLabel;
    QPushButton *deleteCarFormSubmit;
    QLineEdit *deleteFormVinNumberField;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(803, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        addCarButton = new QPushButton(centralwidget);
        addCarButton->setObjectName("addCarButton");
        addCarButton->setGeometry(QRect(0, 0, 391, 31));
        showCarButton = new QPushButton(centralwidget);
        showCarButton->setObjectName("showCarButton");
        showCarButton->setGeometry(QRect(400, 0, 401, 31));
        formLayoutWidget = new QWidget(centralwidget);
        formLayoutWidget->setObjectName("formLayoutWidget");
        formLayoutWidget->setGeometry(QRect(10, 80, 781, 471));
        addCarFormLayout = new QFormLayout(formLayoutWidget);
        addCarFormLayout->setObjectName("addCarFormLayout");
        addCarFormLayout->setContentsMargins(0, 0, 0, 0);
        brandFieldLabel = new QLabel(formLayoutWidget);
        brandFieldLabel->setObjectName("brandFieldLabel");

        addCarFormLayout->setWidget(0, QFormLayout::LabelRole, brandFieldLabel);

        brandField = new QLineEdit(formLayoutWidget);
        brandField->setObjectName("brandField");

        addCarFormLayout->setWidget(0, QFormLayout::FieldRole, brandField);

        modelFieldLabel = new QLabel(formLayoutWidget);
        modelFieldLabel->setObjectName("modelFieldLabel");

        addCarFormLayout->setWidget(1, QFormLayout::LabelRole, modelFieldLabel);

        modelField = new QLineEdit(formLayoutWidget);
        modelField->setObjectName("modelField");

        addCarFormLayout->setWidget(1, QFormLayout::FieldRole, modelField);

        productionYearLabel = new QLabel(formLayoutWidget);
        productionYearLabel->setObjectName("productionYearLabel");

        addCarFormLayout->setWidget(2, QFormLayout::LabelRole, productionYearLabel);

        productionYearField = new QLineEdit(formLayoutWidget);
        productionYearField->setObjectName("productionYearField");

        addCarFormLayout->setWidget(2, QFormLayout::FieldRole, productionYearField);

        vinNumberLabel = new QLabel(formLayoutWidget);
        vinNumberLabel->setObjectName("vinNumberLabel");

        addCarFormLayout->setWidget(3, QFormLayout::LabelRole, vinNumberLabel);

        vinNumberField = new QLineEdit(formLayoutWidget);
        vinNumberField->setObjectName("vinNumberField");

        addCarFormLayout->setWidget(3, QFormLayout::FieldRole, vinNumberField);

        addCarSubmitButton = new QPushButton(formLayoutWidget);
        addCarSubmitButton->setObjectName("addCarSubmitButton");

        addCarFormLayout->setWidget(4, QFormLayout::FieldRole, addCarSubmitButton);

        showCarsTextField = new QPlainTextEdit(centralwidget);
        showCarsTextField->setObjectName("showCarsTextField");
        showCarsTextField->setEnabled(false);
        showCarsTextField->setGeometry(QRect(0, 70, 801, 481));
        descendingByYear = new QPushButton(centralwidget);
        descendingByYear->setObjectName("descendingByYear");
        descendingByYear->setGeometry(QRect(400, 40, 211, 25));
        alphabeticalByBrand = new QPushButton(centralwidget);
        alphabeticalByBrand->setObjectName("alphabeticalByBrand");
        alphabeticalByBrand->setGeometry(QRect(620, 40, 181, 25));
        deleteCarButton = new QPushButton(centralwidget);
        deleteCarButton->setObjectName("deleteCarButton");
        deleteCarButton->setGeometry(QRect(0, 40, 391, 25));
        formLayoutWidget_2 = new QWidget(centralwidget);
        formLayoutWidget_2->setObjectName("formLayoutWidget_2");
        formLayoutWidget_2->setGeometry(QRect(0, 480, 801, 61));
        formLayout = new QFormLayout(formLayoutWidget_2);
        formLayout->setObjectName("formLayout");
        formLayout->setContentsMargins(0, 0, 0, 0);
        deleteFromVinNumberLabel = new QLabel(formLayoutWidget_2);
        deleteFromVinNumberLabel->setObjectName("deleteFromVinNumberLabel");

        formLayout->setWidget(0, QFormLayout::LabelRole, deleteFromVinNumberLabel);

        deleteCarFormSubmit = new QPushButton(formLayoutWidget_2);
        deleteCarFormSubmit->setObjectName("deleteCarFormSubmit");

        formLayout->setWidget(1, QFormLayout::FieldRole, deleteCarFormSubmit);

        deleteFormVinNumberField = new QLineEdit(formLayoutWidget_2);
        deleteFormVinNumberField->setObjectName("deleteFormVinNumberField");

        formLayout->setWidget(0, QFormLayout::FieldRole, deleteFormVinNumberField);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 803, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        addCarButton->setText(QCoreApplication::translate("MainWindow", "Dodaj samoch\303\263d", nullptr));
        showCarButton->setText(QCoreApplication::translate("MainWindow", "Poka\305\274 samochody", nullptr));
        brandFieldLabel->setText(QCoreApplication::translate("MainWindow", "Marka", nullptr));
        modelFieldLabel->setText(QCoreApplication::translate("MainWindow", "Model", nullptr));
        productionYearLabel->setText(QCoreApplication::translate("MainWindow", "Rok produkcji", nullptr));
        vinNumberLabel->setText(QCoreApplication::translate("MainWindow", "Numer VIN", nullptr));
        addCarSubmitButton->setText(QCoreApplication::translate("MainWindow", "Dodaj samoch\303\263d", nullptr));
        descendingByYear->setText(QCoreApplication::translate("MainWindow", "Malej\304\205co wzgl\304\231dem roku", nullptr));
        alphabeticalByBrand->setText(QCoreApplication::translate("MainWindow", "Alfabetycznie wzgl\304\231dem marki", nullptr));
        deleteCarButton->setText(QCoreApplication::translate("MainWindow", "Usu\305\204 samoch\303\263d", nullptr));
        deleteFromVinNumberLabel->setText(QCoreApplication::translate("MainWindow", "Numer VIN", nullptr));
        deleteCarFormSubmit->setText(QCoreApplication::translate("MainWindow", "Usu\305\204", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
