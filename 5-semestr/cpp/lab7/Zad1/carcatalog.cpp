#include "carcatalog.hpp"

CarCatalog::CarCatalog()
{
}

void CarCatalog::addCar(const Car& car)
{
    auto it
        = std::find_if(cars.begin(), cars.end(), [vin = car.vinNumber](const Car& c) { return c.vinNumber == vin; });

    if (it != cars.end())
        return;

    cars.push_back(car);
}

void CarCatalog::removeCar(int vinNumber)
{
    for (auto it = cars.begin(); it != cars.end(); ++it)
        if (it->vinNumber == vinNumber) {
            cars.erase(it);
            return;
        }
}

QString CarCatalog::toString() const
{
    QString all;
    for (int i = 0; i < cars.size(); ++i)
        all += QString::number(i + 1) + ". " + cars[i].toString() + '\n';

    return all;
}

CarCatalog CarCatalog::sorted(std::function<bool(const Car&, const Car&)> predicate)
{
    auto cpy = *this;

    std::sort(cpy.cars.begin(), cpy.cars.end(), predicate);

    return cpy;
}

size_t CarCatalog::count(std::function<bool(const Car&)> predicate)
{
    return std::count_if(cars.begin(), cars.end(), predicate);
}
