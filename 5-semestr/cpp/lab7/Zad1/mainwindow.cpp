#include "mainwindow.hpp"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initFields();
    initLabels();

    setAddCarHidden(true);
    setShowCarsHidden(true);
    setDeleteCarHidden(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addCarButton_clicked()
{
    setAddCarHidden(false);
    setDeleteCarHidden(true);
    setShowCarsHidden(true);
}

void MainWindow::on_showCarButton_clicked()
{
    setAddCarHidden(true);
    setShowCarsHidden(false);
    setDeleteCarHidden(true);

    ui->showCarsTextField->setPlainText(cars.toString());
}

void MainWindow::on_addCarSubmitButton_clicked()
{
    auto newCar = readForm();
    cars.addCar(newCar);
}

Car MainWindow::readForm()
{
    QString brandName = ui->brandField->text();
    ui->brandField->setText("");
    QString modelName = ui->modelField->text();
    ui->modelField->setText("");
    QString productionYear = ui->productionYearField->text();
    ui->productionYearField->setText("");
    QString vinNumber = ui->vinNumberField->text();
    ui->vinNumberField->setText("");

    return Car{ brandName, modelName, productionYear.toInt(), vinNumber.toInt() };
}

void MainWindow::initLabels()
{
    labels.append(ui->brandFieldLabel);
    labels.append(ui->modelFieldLabel);
    labels.append(ui->productionYearLabel);
    labels.append(ui->vinNumberLabel);
}

void MainWindow::initFields()
{
    fields.append(ui->brandField);
    fields.append(ui->modelField);
    fields.append(ui->productionYearField);
    fields.append(ui->vinNumberField);
}

void MainWindow::setAddCarHidden(bool hidden)
{
    for (const auto& label : labels)
        label->setHidden(hidden);

    for (const auto& field : fields)
        field->setHidden(hidden);

    ui->addCarSubmitButton->setHidden(hidden);
}

void MainWindow::setShowCarsHidden(bool hidden)
{
    ui->showCarsTextField->setHidden(hidden);
}

void MainWindow::setDeleteCarHidden(bool hidden)
{
    ui->deleteFromVinNumberLabel->setHidden(hidden);
    ui->deleteFormVinNumberField->setHidden(hidden);
    ui->deleteCarFormSubmit->setHidden(hidden);
}

void MainWindow::on_deleteCarFormSubmit_clicked()
{
    QString vinNumber = ui->deleteFormVinNumberField->text();

    bool status;
    int vinValue = vinNumber.toInt(&status);

    if (!status) {
        ui->deleteFormVinNumberField->setText("Błąd");
    }

    cars.removeCar(vinValue);
}

void MainWindow::on_descendingByYear_clicked()
{
    auto sorted = cars.sorted([](const Car& l, const Car& r) { return l.productionYear > r.productionYear; });

    ui->showCarsTextField->setPlainText(sorted.toString());

    setAddCarHidden(true);
    setShowCarsHidden(false);
    setDeleteCarHidden(true);
}

void MainWindow::on_alphabeticalByBrand_clicked()
{
    setAddCarHidden(true);
    setShowCarsHidden(false);
    setDeleteCarHidden(true);

    auto sorted = cars.sorted([](const Car& l, const Car& r) { return l.brand.compare(r.brand) < 0; });

    ui->showCarsTextField->setPlainText(sorted.toString());
}

void MainWindow::on_deleteCarButton_clicked()
{
    setDeleteCarHidden(false);
    setShowCarsHidden(true);
    setAddCarHidden(true);
}
