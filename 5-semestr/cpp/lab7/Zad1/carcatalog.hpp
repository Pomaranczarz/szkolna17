#ifndef CARCATALOG_HPP
    #define CARCATALOG_HPP

#include <QString>
#include <QVector>

struct Car {
    QString brand;
    QString model;
    int productionYear;
    int vinNumber;

    Car(const QString& brand, const QString& model, int productionYear, int vinNumber)
        : brand{ brand }
        , model{ model }
        , productionYear{ productionYear }
        , vinNumber{ vinNumber }
    {
    }

    QString toString() const
    {
        return brand + " " + model + ", " + QString::number(productionYear) + " {" + QString::number(vinNumber) + "}";
    }
};

class CarCatalog
{
public:
    CarCatalog();
    void addCar(const Car& car);
    void removeCar(int vinNumber);

    CarCatalog sorted(std::function<bool(const Car&, const Car&)>);

    size_t count(std::function<bool(const Car&)>);

    QString toString() const;

private:
    QVector<Car> cars;
};

#endif // CARCATALOG_HPP
