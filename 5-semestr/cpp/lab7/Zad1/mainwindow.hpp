#ifndef MAINWINDOW_HPP
    #define MAINWINDOW_HPP

    #include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#include "carcatalog.hpp"
#include <QLabel>
#include <QLineEdit>
#include <QMap>
#include <QString>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_addCarButton_clicked();

    void on_showCarButton_clicked();

    void on_addCarSubmitButton_clicked();

    void on_deleteCarFormSubmit_clicked();

    void on_descendingByYear_clicked();

    void on_alphabeticalByBrand_clicked();

    void on_deleteCarButton_clicked();

private:
    Car readForm();
    void initLabels();
    void initFields();

    void setAddCarHidden(bool hidden);
    void setShowCarsHidden(bool hidden);
    void setDeleteCarHidden(bool hidden);

private:
    Ui::MainWindow *ui;
    QVector<QLabel*> labels;
    QVector<QLineEdit*> fields;
    CarCatalog cars;
};
#endif // MAINWINDOW_HPP
