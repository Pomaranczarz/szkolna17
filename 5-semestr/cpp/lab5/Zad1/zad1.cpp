#include <iostream>
#include <set>
#include <vector>

[[nodiscard]] auto uniq_sum(const std::vector<int>& v) -> std::pair<int, int>;

auto main() -> int
{
    std::vector<int> v{ 1, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    auto [dup, sum] = uniq_sum(v);

    std::cout << dup << "\t" << sum << '\n';
}

auto uniq_sum(const std::vector<int>& v) -> std::pair<int, int>
{
    std::set<int> set;
    std::pair<int, int> result{};

    for (const auto& elem : v) {
        if (auto insert = set.insert(elem); !insert.second) {
            result.first = elem;
            continue;
        }

        result.second += elem;
    }

    return result;
}