#include "Dictionary.hpp"

auto main() -> int
{
    Dictionary dict;
    (void) dict.add_word("cat", "kot");
    (void) dict.add_word("dog", "pies");
    (void) dict.add_word("bird", "ptak");
    (void) dict.add_word("cow", "krowa");
    (void) dict.add_word("zebra", "zebra");
    (void) dict.add_word("owl", "sowa");
    (void) dict.add_word("butterfly", "motyl");

    dict.print();

    std::cout << std::string(40, '=') << '\n';

    dict.print_sorted();

    (void) dict.delete_word("owl");

    std::cout << std::string(40, '=') << '\n';

    dict.print_sorted();
}