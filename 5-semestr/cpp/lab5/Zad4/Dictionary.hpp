#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <algorithm>
#include <iostream>
#include <map>
#include <optional>
#include <string>
#include <vector>

class Dictionary {
public:
    Dictionary() = default;

    [[nodiscard]] auto add_word(const std::string& word, const std::string& translation) -> bool
    {
        return words.insert({ word, translation }).second;
    }

    [[nodiscard]] auto delete_word(const std::string& word) -> bool { return words.erase(word) > 0; }

    void print() const
    {
        for (const auto& [word, translation] : words) {
            std::cout << word << ": " << translation << '\n';
        }
    }

    [[nodiscard]] auto get_translation(const std::string& word) -> std::optional<std::string>
    {
        if (auto it = words.find(word); it != words.end()) {
            return { it->second };
        }

        return {};
    }

    void print_sorted() const
    {
        std::vector<std::pair<std::string, std::string>> vec;

        std::for_each(words.begin(), words.end(), [&vec](const auto& elem) { vec.push_back(elem); });

        std::sort(vec.begin(), vec.end(), [](const auto& l, const auto& r) { return l.second > r.second; });

        for (const auto& [word, translation] : vec) {
            std::cout << word << ": " << translation << '\n';
        }
    }

private:
    std::map<std::string, std::string> words;
};

#endif