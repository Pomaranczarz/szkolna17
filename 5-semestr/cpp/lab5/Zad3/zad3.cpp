#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <string>

[[nodiscard]] auto get_histogram(const std::string& s) -> std::map<char, size_t>
{
    std::map<char, size_t> map;

    for (const auto& c : s) {
        ++map[c];
    }

    return map;
}

[[nodiscard]] auto get_added_char(const std::string& a, const std::string& b) -> char
{
    std::map<char, size_t> a_hist = get_histogram(a);
    std::map<char, size_t> b_hist = get_histogram(b);

    std::map<char, size_t> key_diff;

    std::set_symmetric_difference(a_hist.begin(), a_hist.end(), b_hist.begin(), b_hist.end(),
                                  std::inserter(key_diff, key_diff.begin()));

    if (!key_diff.empty()) {
        return key_diff.begin()->first;
    }

    // klucze są takie same
    for (const auto& [key, value] : a_hist) {
        if (b_hist[key] != value) {
            return key;
        }
    }

    return '\0';
}

auto main() -> int
{
    std::string a = "abcdefgh";
    std::string b = "defhgccba";

    std::cout << get_added_char(a, b);
}