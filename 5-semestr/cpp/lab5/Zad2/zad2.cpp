#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>

template <typename T>
[[nodiscard]] auto contains(const std::set<T>& set, T value) -> bool
{
    return std::find(set.cbegin(), set.cend(), value) != set.cend();
}

template <typename T>
[[nodiscard]] auto set_xor(const std::set<T>& l, const std::set<T>& r) -> std::set<T>
{
    std::set<T> result;

    std::set_symmetric_difference(l.cbegin(), l.cend(), r.cbegin(), r.cend(), std::inserter(result, result.begin()));

    // std::copy_if(l.cbegin(), l.cend(), std::inserter(result, result.begin()),
    //              [&r](const auto& elem) { return !contains(r, elem); });

    // std::copy_if(r.cbegin(), r.cend(), std::inserter(result, result.begin()),
    //              [&l](const auto& elem) { return !contains(l, elem); });

    return result;
}

auto main() -> int
{
    std::set<int> l{ 1, 2, 3, 4, 5, 6, 7, 8 };
    std::set<int> r{ 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

    std::set<int> result = set_xor(l, r);

    for (const auto& elem : result) {
        std::cout << elem << " ";
    }
}
