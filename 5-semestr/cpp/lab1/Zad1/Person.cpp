#include "Person.h"
#include <iostream>

using namespace std;

Person::Person(string name, string surname, int age)
    : mName{ std::move(name) }
    , mSurname{ std::move(surname) }
    , mAge{ age }
{
    cout << "Konstruktor klasy bazowej - Person" << endl;
}

Person::Person()
    : mAge{}
{
    cout << "Konstruktor bez. klasy bazowej – Person" << endl;
}

void Person::set_age(int age1)

{
    mAge = age1;
}

auto Person::get_surname() const -> string
{
    return mSurname;
}

auto Person::is_18() const -> bool
{
    return mAge >= 18;
}

void Person::show_info_person() const
{
    cout << "Imie: " << mName << " nazwisko: " << mSurname << "wiek: " << mAge << endl;
}

void Person::init(const string& name, const string& surname, int age)
{
    this->mName = name;
    this->mSurname = surname;
    this->mAge = age;
}