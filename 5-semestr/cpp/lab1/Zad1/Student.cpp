#include "Student.h"
#include <iostream>
#include <utility>

Student::Student(std::string name, std::string surname, int age, std::string index)
    : Person{ std::move(name), std::move(surname), age }
    , mIndex{ std::move(index) }
{
    std::cout << "Konstruktor klasy pochodnej Student" << std::endl;
}

void Student::set_index(const std::string& newIndex)
{
    mIndex = newIndex;
}

[[nodiscard]] auto Student::get_index() const -> std::string
{
    return mIndex;
}

void Student::show_info_student() const
{
    Person::show_info_person();
    std::cout << "Index: " << mIndex;
}