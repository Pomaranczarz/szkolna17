#include "Person.h"
#include "Student.h"
#include "Teacher.h"
#include <iostream>

using namespace std;

auto main() -> int
{
    cout << endl << "Obiekty klasy Person" << endl;

    Person o1("Grazyna", "Nowak", 45);
    o1.show_info_person();
    cout << "Czy pelnoletnia?: " << o1.is_18() << endl;
    o1.set_age(16);
    cout << endl << "Obiekty klasy Teacher" << endl;

    Teacher n1("Barbara", "Kowalska", 56, 30, "Phd");
    n1.show_info_teacher();
    cout << "Czy ma Phd?: " << n1.is_phd() << endl;
    n1.set_age(34);

    Student staticArrayStaticObjects[2]{
        {"Grazyna",  "Nowak",    45, "123456"},
        { "Barbara", "Kowalska", 56, "654321"}
    };

    auto* dynamicArrayStaticObjects = new Student[2];
    dynamicArrayStaticObjects[0] = { "Grazyna", "Nowak", 45, "123456" };
    dynamicArrayStaticObjects[1] = { "Barbara", "Kowalska", 56, "654321" };
    delete[] dynamicArrayStaticObjects;

    Student* staticArrayDynamicObjects[2];
    staticArrayDynamicObjects[0] = new Student{ "Grazyna", "Nowak", 45, "123456" };
    staticArrayDynamicObjects[1] = new Student{ "Barbara", "Kowalska", 56, "654321" };
    delete staticArrayDynamicObjects[0];
    delete staticArrayDynamicObjects[1];

    auto** dynamicArrayDynamicObjects = new Student*[2];
    dynamicArrayDynamicObjects[0] = new Student{ "Grazyna", "Nowak", 45, "123456" };
    dynamicArrayDynamicObjects[1] = new Student{ "Barbara", "Kowalska", 56, "654321" };
    delete dynamicArrayDynamicObjects[0];
    delete dynamicArrayDynamicObjects[1];
    delete[] dynamicArrayDynamicObjects;
}