#ifndef TEACHER_H_INCLUDED
#define TEACHER_H_INCLUDED

#include "Person.h"

class Teacher : public Person {
private:
    int mExperience;
    std::string mTitle;

public:
    Teacher(const std::string& name, const std::string& surname, int age, int experience, const std::string& title);
    Teacher();
    void set_title(const std::string& title);

    [[nodiscard]] auto get_experience() const -> int;
    [[nodiscard]] auto is_phd() const -> bool;
    void show_info_teacher() const;
};
#endif        // TEACHER_H_INCLUDED