#include "Teacher.h"
#include <iostream>

using namespace std;

Teacher::Teacher(const string& name, const string& surname, int age, int experience, const string& title)
    : Person{ name, surname, age }
    , mExperience{ experience }
    , mTitle{ title }
{
    cout << "Konstruktor klasy pochodnej Teacher" << endl;
}

Teacher::Teacher()
    : mExperience{}
{
    cout << "Konstruktor bez. klasy pochodnej Teacher" << endl;
}

void Teacher::set_title(const string& title1)
{
    mTitle = title1;
}

auto Teacher::get_experience() const -> int
{
    return mExperience;
}

auto Teacher::is_phd() const -> bool
{
    return mTitle == "Phd";
}

void Teacher::show_info_teacher() const
{
    show_info_person();
    cout << "Staz pracy: " << mExperience << " tytul naukowy: " << mTitle << endl;
}