#ifndef STUDENT_H_INCLUDED
#define STUDENT_H_INCLUDED

#include "Person.h"

class Student : public Person {
public:
    Student(std::string name, std::string surname, int age, std::string index);
    Student() = default;

    void set_index(const std::string& newIndex);
    [[nodiscard]] auto get_index() const -> std::string;

    void show_info_student() const;

private:
    std::string mIndex;
};

#endif        // STUDENT_H_INCLUDED