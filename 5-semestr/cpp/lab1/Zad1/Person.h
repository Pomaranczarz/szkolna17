#ifndef PERSON_H_INCLUDED
#define PERSON_H_INCLUDED

#include <string>

class Person {
private:
    std::string mName;
    std::string mSurname;
    int mAge;

public:
    Person(std::string name, std::string surname, int age);
    Person();

    void set_age(int age);
    [[nodiscard]] auto get_surname() const -> std::string;
    [[nodiscard]] auto is_18() const -> bool;

    void show_info_person() const;
    void init(const std::string& name, const std::string& surname, int age);
};
#endif        // PERSON_H_INCLUDED