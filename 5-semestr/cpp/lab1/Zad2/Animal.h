#ifndef ANIMAL_H
#define ANIMAL_H

#include <string>

class Animal {
public:
    Animal() = default;
    Animal(int limbNr, std::string name, bool protectedAnimal = true);

    void set_limb_nr(int newLimbNr);
    void set_name(const std::string& newName);
    void set_protected(bool newStatus);

    [[nodiscard]] auto get_limb_nr() const -> int;
    [[nodiscard]] auto get_name() const -> std::string;
    [[nodiscard]] auto is_protected() const -> bool;

    void give_voice() const;
    void info() const;

private:
    int mLimbNr{};
    std::string mName;
    bool mProtectedAnimal{};
};

#endif        //