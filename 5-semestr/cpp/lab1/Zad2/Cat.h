#ifndef CAT_H
#define CAT_H

#include "Animal.h"

class Cat : public Animal {
public:
    Cat() = default;
    Cat(int limbNr, std::string name, bool protectedAnimal = true);

    void init_cat(int limbNr, const std::string& name, bool protectedAnimal, int levelOfMouseHunting, int mice[5]);

    void set_level_of_mouse_hunting(int value);
    [[nodiscard]] auto get_level_of_mouse_hunting() const -> int;

    void init_mice(int values[5]);
    [[nodiscard]] auto get_mice(int index) const -> int;

    void give_voice() const;
    void info() const;

private:
    int mLevelOfMouseHunting = 0;
    int mMice[5]{};
};

#endif        // CAT_H