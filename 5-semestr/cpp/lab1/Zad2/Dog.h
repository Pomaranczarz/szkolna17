#ifndef DOG_H
#define DOG_H

#include "Animal.h"

class Dog : public Animal {
public:
    enum class SkillType
    {
        GUIDE,
        TRACKER,
    };

    Dog() = default;

    Dog(int limbNr, std::string name, bool protectedAnimal, std::string breed, int levelOfGuideSkills,
        int levelOfTrackerSkills);

    Dog(int limbNr, const std::string& name, bool protectedAnimal = true);

    void set_skill_level(SkillType type, int value);
    [[nodiscard]] auto get_skill_level(SkillType type) const -> int;

    void give_voice() const;
    void info() const;

private:
    static auto is_value_in_range(int value, int rangeBegin, int rangeEnd) -> bool;

private:
    std::string mBreed;
    int mLevelOfGuideSkills{};
    int mLevelOfTrackerSkills{};
};

#endif        // DOG_H