#include "Cat.h"
#include "Dog.h"
#include <algorithm>
#include <iostream>
#include <iterator>

auto how_many_protected_animals(Animal* animals, size_t size) -> size_t;
void print_tracker_dogs(Dog* dogs, size_t size);
void print_mice_for_each_cat(Cat* cats, size_t size);

auto main() -> int
{
    Animal animals[4]{
        {2,  "Kaczka",     false},
        { 4, "Pies",       false},
        { 4, "Niedzwiedz", true },
        { 2, "Orzel",      true }
    };

    Dog dogs[3]{
        {4,  "Reksio", false, "Reksio", 5, 3},
        { 4, "Burek",  false, "Burek",  2, 9},
        { 4, "Azor",   false, "Azor",   6, 1}
    };

    Cat cats[3]{
        {4,  "Puszek",  false},
        { 4, "Kicius",  false},
        { 4, "Mruczek", false}
    };

    int mice1[]{ 1, 2, 3, 4, 5 };
    int mice2[]{ 2, 2, 2, 2, 2 };
    int mice3[]{ 6, 2, 7, 1, 3 };

    cats[0].init_mice(mice1);
    cats[1].init_mice(mice2);
    cats[2].init_mice(mice3);

    std::cout << how_many_protected_animals(animals, std::size(animals)) << '\n';

    std::cout << '\n' << std::string(40, '=') << '\n';

    print_tracker_dogs(dogs, std::size(dogs));

    std::cout << '\n' << std::string(40, '=') << '\n';

    print_mice_for_each_cat(cats, std::size(cats));
}

auto how_many_protected_animals(Animal* animals, size_t size) -> size_t
{
    size_t count = 0;
    for (size_t i = 0; i < size; ++i) {
        if (animals[i].is_protected()) {
            ++count;
        }
    }

    return count;
}

void print_tracker_dogs(Dog* dogs, size_t size)
{
    using SkillType = Dog::SkillType;

    for (size_t i = 0; i < size; ++i) {
        auto& dog = dogs[i];
        if (dog.get_skill_level(SkillType::TRACKER) > dog.get_skill_level(SkillType::GUIDE)) {
            dog.info();
        }
    }
}

void print_mice_for_each_cat(Cat* cats, size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        int mice = 0;
        for (int j = 0; j < 5; ++j) {
            mice += cats[i].get_mice(j);
        }

        std::cout << cats[i].get_name() << ": " << mice << '\n';
    }
}