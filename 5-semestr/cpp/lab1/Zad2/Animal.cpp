#include "Animal.h"
#include <ios>
#include <iostream>
#include <utility>

Animal::Animal(int limbNr, std::string name, bool protectedAnimal)
    : mLimbNr{ limbNr }
    , mName{ std::move(name) }
    , mProtectedAnimal{ protectedAnimal }
{
}

void Animal::set_limb_nr(int newLimbNr)
{
    mLimbNr = newLimbNr;
}

void Animal::set_name(const std::string& newName)
{
    mName = newName;
}

void Animal::set_protected(bool newStatus)
{
    mProtectedAnimal = newStatus;
}

auto Animal::get_limb_nr() const -> int
{
    return mLimbNr;
}

auto Animal::get_name() const -> std::string
{
    return mName;
}

auto Animal::is_protected() const -> bool
{
    return mProtectedAnimal;
}

void Animal::give_voice() const
{
    std::cout << "Chrum, miau, hau, piiiii\n";
}

void Animal::info() const
{
    std::cout << "LimbNr: " << mLimbNr << "\tName: " << mName << "\tProtected: " << std::boolalpha << mProtectedAnimal
              << '\n';
}