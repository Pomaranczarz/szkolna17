#include "Cat.h"
#include "Animal.h"
#include <iostream>
#include <stdexcept>
#include <utility>

Cat::Cat(int limbNr, std::string name, bool protectedAnimal)
    : Animal{ limbNr, std::move(name), protectedAnimal }
{
}

void Cat::init_cat(int limbNr, const std::string& name, bool protectedAnimal, int levelOfMouseHunting, int mice[5])
{
    set_limb_nr(limbNr);
    set_name(name);
    set_protected(protectedAnimal);
    set_level_of_mouse_hunting(levelOfMouseHunting);
    init_mice(mice);
}

void Cat::set_level_of_mouse_hunting(int value)
{
    if (value < 1 || value > 10) {
        throw std::invalid_argument("MouseHunting value has to be in range <1; 10>");
    }

    mLevelOfMouseHunting = value;
}

auto Cat::get_level_of_mouse_hunting() const -> int
{
    return mLevelOfMouseHunting;
}

void Cat::init_mice(int values[5])
{
    for (int i = 0; i < 5; ++i) {
        mMice[i] = std::max(0, values[i]);
    }
}

auto Cat::get_mice(int index) const -> int
{
    if (index < 0 || index > 4) {
        throw std::invalid_argument("Mice index has to be in range <0; 4>");
    }

    return mMice[index];
}

void Cat::give_voice() const
{
    std::cout << "Miau miau\n";
}

void Cat::info() const
{
    Animal::info();
    std::cout << "LevelOfMouseHunting: " << mLevelOfMouseHunting << "\tMice: ";
    for (int i : mMice) {
        std::cout << i << " ";
    }
    std::cout << '\n';
}