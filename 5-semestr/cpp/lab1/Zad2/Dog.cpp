#include "Dog.h"
#include <iostream>
#include <stdexcept>
#include <utility>

Dog::Dog(int limbNr, std::string name, bool protectedAnimal, std::string breed, int levelOfGuideSkills,
         int levelOfTrackerSkills)
    : Animal{ limbNr, std::move(name), protectedAnimal }
    , mBreed{ std::move(breed) }
{
    set_skill_level(SkillType::GUIDE, levelOfGuideSkills);
    set_skill_level(SkillType::TRACKER, levelOfTrackerSkills);
}

Dog::Dog(int limbNr, const std::string& name, bool protectedAnimal)
    : Animal{ limbNr, name, protectedAnimal }
{
}

void Dog::set_skill_level(SkillType type, int value)
{
    if (!is_value_in_range(value, 1, 10)) {
        throw std::invalid_argument("Skill level has to be in range <1; 10>");
    }

    if (type == SkillType::GUIDE) {
        mLevelOfGuideSkills = value;
    }
    else if (type == SkillType::TRACKER) {
        mLevelOfTrackerSkills = value;
    }
    else {
        throw std::invalid_argument("Unknown SkillType");
    }
}

auto Dog::get_skill_level(SkillType type) const -> int
{
    if (type == SkillType::GUIDE) {
        return mLevelOfGuideSkills;
    }

    if (type == SkillType::TRACKER) {
        return mLevelOfTrackerSkills;
    }

    throw std::invalid_argument("Unknown SkillType");
}

void Dog::give_voice() const
{
    std::cout << "Hau, hau\n";
}

void Dog::info() const
{
    Animal::info();
    std::cout << "Breed: " << mBreed << "\tGuideSkills: " << mLevelOfGuideSkills
              << "\tTrackerSkills: " << mLevelOfTrackerSkills << '\n';
}

auto Dog::is_value_in_range(int value, int rangeBegin, int rangeEnd) -> bool
{
    return value >= rangeBegin && value <= rangeEnd;
}