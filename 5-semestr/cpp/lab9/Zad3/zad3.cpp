#include <algorithm>
#include <cctype>
#include <charconv>
#include <format>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

static constexpr auto FILENAME{ "dane.csv" };
static constexpr auto MEN_FILENAME{ "m.csv" };
static constexpr auto WOMEN_FILENAME{ "w.csv" };

template <typename T>
[[nodiscard]] auto from_string(const std::string& s) -> T
{
    T value{};
    auto result = std::from_chars(s.data(), s.data() + s.size(), value);
    if (result.ec == std::errc()) {
        return value;
    }

    throw std::invalid_argument{ "Invalid argument" };
}

class Student {
public:
    Student() = default;

    Student(std::string name, std::string lastname, char sex, unsigned short grade,
            const std::optional<std::string>& email = std::nullopt)
        : _name{ std::move(name) }
        , _lastname{ std::move(lastname) }
        , _sex{ sex }
        , _grade{ grade }
        , _email{ email }
    {
    }

    [[nodiscard]] auto get_name() const -> const std::string& { return _name; }

    [[nodiscard]] auto get_lastname() const -> const std::string& { return _lastname; }

    [[nodiscard]] auto get_sex() const -> char { return _sex; }

    [[nodiscard]] auto get_grade() const -> unsigned short { return _grade; }

    [[nodiscard]] auto get_email() const -> std::optional<std::string> { return _email; }

    [[nodiscard]] auto to_csv_line() const -> std::string
    {
        std::stringstream ss;
        ss << _name << ";" << _lastname << ";" << _sex << ";" << _grade << ";";

        if (_email) {
            ss << *_email;
        }

        ss << std::endl;
        return ss.str();
    }

    [[nodiscard]] static auto from_csv_line(const std::string& line) -> Student
    {
        std::stringstream ss{ line };
        std::string name;
        std::string lastname;
        std::string sex;
        std::string grade;
        std::string email;

        std::getline(ss, name, ';');
        std::getline(ss, lastname, ';');
        std::getline(ss, sex, ';');
        std::getline(ss, grade, ';');
        std::getline(ss, email);

        return Student{ name, lastname, sex[0], from_string<unsigned short>(grade),
                        (email.empty() ? std::nullopt : std::optional<std::string>(email)) };
    }

private:
    std::string _name;
    std::string _lastname;
    char _sex{};
    unsigned short _grade{};
    std::optional<std::string> _email;
};

class StudentsCatalog {
public:
    void add_student(const Student& student) { _students.push_back(student); }

    [[nodiscard]] auto to_csv(std::function<bool(const Student&)> predicate = default_predicate) const -> std::string
    {
        std::stringstream ss;

        std::for_each(_students.begin(), _students.end(), [&](const Student& student) {
            if (predicate(student)) {
                ss << student.to_csv_line();
            }
        });

        return ss.str();
    }

    [[nodiscard]] auto to_string(const std::function<bool(const Student&)>& predicate = default_predicate) const
        -> std::string
    {
        std::string result{};
        for (const auto& student : _students) {
            if (predicate(student)) {
                result
                    += std::format("{:_^20}{:_^20}{:_^20}{:_^20}{:_^20}\n", student.get_name(), student.get_lastname(),
                                   student.get_sex(), student.get_grade(), student.get_email().value_or(""));
            }
        }

        return result;
    }

    [[nodiscard]] auto first_n_to_string(size_t N) const -> std::string
    {
        std::string result{};
        for (size_t i = 0, n = std::min(N, _students.size()); i < n; ++i) {
            result += std::format("{:_^20}{:_^20}{:_^20}{:_^20}{:_^20}\n", _students[i].get_name(),
                                  _students[i].get_lastname(), _students[i].get_sex(), _students[i].get_grade(),
                                  _students[i].get_email().value_or(""));
        }

        return result;
    }

    [[nodiscard]] static auto from_csv(const std::string& filename) -> StudentsCatalog
    {
        std::ifstream data{ filename };
        StudentsCatalog catalog;
        std::string line;
        while (std::getline(data, line)) {
            catalog.add_student(Student::from_csv_line(line));
        }
        return catalog;
    }

    [[nodiscard]] auto sorted(std::function<bool(const Student&, const Student&)> predicate) const -> StudentsCatalog
    {
        StudentsCatalog result{ *this };
        std::sort(result._students.begin(), result._students.end(), std::move(predicate));

        return result;
    }

private:
    static auto default_predicate([[maybe_unused]] const Student&) -> bool { return true; }

    std::vector<Student> _students;
};

[[nodiscard]] auto is_name_valid(const std::string& name) -> bool
{
    bool isUpper = std::isupper(name[0]) != 0;
    bool areAllLatin = (size_t) std::count_if(name.begin(), name.end(), [](const char& c) { return std::isalpha(c); })
                    == name.size();

    return isUpper && areAllLatin;
}

[[nodiscard]] auto is_email_valid(const std::string& email) -> bool
{
    return email.find('@') != std::string::npos;
}

[[nodiscard]] auto read_student() -> Student
{
    // empty standard input
    std::cin.ignore(1);

    std::string name;
    std::string lastname;
    std::string sex;
    std::string grade;
    std::string email;

    std::cout << "Enter student's name: ";
    std::getline(std::cin, name);
    std::cout << "Enter student's lastname: ";
    std::getline(std::cin, lastname);
    std::cout << "Enter student's sex: ";
    std::getline(std::cin, sex);
    std::cout << "Enter student's grade: ";
    std::getline(std::cin, grade);
    std::cout << "Enter student's email: ";
    std::getline(std::cin, email);

    if (!is_name_valid(name)) {
        throw std::runtime_error("Invalid name");
    }
    if (!is_name_valid(lastname)) {
        throw std::runtime_error("Invalid lastname");
    }
    if (sex[0] != 'K' && sex[0] != 'M') {
        throw std::runtime_error("Invalid sex");
    }
    auto gradeValue = from_string<unsigned short>(grade);
    if (gradeValue < 2 || gradeValue > 5) {
        throw std::runtime_error("Invalid grade");
    }

    return Student{ name, lastname, sex[0], gradeValue,
                    (is_email_valid(email) ? std::optional<std::string>(email) : std::nullopt) };
}

void print_menu()
{
    std::cout << "1. Add student" << std::endl;
    std::cout << "2. List students" << std::endl;
    std::cout << "3. Save students" << std::endl;
    std::cout << "4. Split by sex" << std::endl;
    std::cout << "5. Sort by grade" << std::endl;
    std::cout << "6. Exit" << std::endl;
}

void print_list_menu()
{
    std::cout << "1. Print all students" << std::endl;
    std::cout << "2. Print students with given lastname" << std::endl;
    std::cout << "3. Print first N students" << std::endl;
}

auto main() -> int
{
    StudentsCatalog catalog = StudentsCatalog::from_csv(FILENAME);

    while (true) {
        print_menu();
        int choice = 0;
        size_t n = 0;
        Student student;
        std::string lastname;
        std::fstream file;
        std::cin >> choice;
        switch (choice) {
        case 1:
            try {
                student = read_student();
                catalog.add_student(student);
            }
            catch (std::runtime_error& e) {
                std::cerr << e.what() << std::endl;
            }
            break;
        case 2:
            print_list_menu();
            std::cin >> choice;
            switch (choice) {
            case 1: std::cout << catalog.to_string(); break;
            case 2:
                std::cout << "Enter lastname: ";
                std::getline(std::cin, lastname);
                std::cout << catalog.to_string([&lastname](const Student& s) { return s.get_lastname() == lastname; });
                break;
            case 3:
                std::cout << "Enter N: ";
                std::cin >> n;
                std::cout << catalog.first_n_to_string(n);
                break;
            default: continue;
            }
            break;
        case 3:
            file.open(FILENAME, std::ios::out);
            file << catalog.to_csv();
            file.close();
            break;
        case 4:
            file.open(MEN_FILENAME, std::ios::out);
            file << catalog.to_csv([](const Student& s) { return s.get_sex() == 'M'; });
            file.close();
            file.open(WOMEN_FILENAME, std::ios::out);
            file << catalog.to_csv([](const Student& s) { return s.get_sex() == 'K'; });
            file.close();
            break;
        case 5:
            std::cout << catalog
                             .sorted([](const Student& l, const Student& r) { return l.get_grade() < r.get_grade(); })
                             .to_string();
            break;
        default: return 0;
        }
    }
}