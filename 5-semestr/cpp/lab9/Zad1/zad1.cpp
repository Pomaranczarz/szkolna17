#include <fstream>
#include <iostream>

auto main(int /*argc*/, char** argv) -> int
{
    auto filename = std::string{ argv[1] };

    std::ifstream file{ filename };

    if (!file.good()) {
        std::cerr << "Blad pliku\n";
        return -1;
    }

    std::ofstream outfile{ filename + ".trimmed" };
    std::string line;

    while (!file.eof()) {
        std::getline(file, line);
        if (auto pos = line.find("//"); pos != std::string::npos) {
            line = line.substr(0, pos);
        }

        outfile << line << '\n';
    }
}