#include <format>
#include <ios>
#include <iostream>
#include <limits>
#include <stdexcept>

void ignore_line()
{
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

auto main() -> int
{
    int sum = 0;
    int number{};

    while (true) {
        try {
            std::cout << std::format("Podaj liczbe (aktualna suma: {})\n", sum);
            std::cin >> number;

            if (!std::cin) {
                std::cerr << "Niepoprawne dane, sprobuj ponownie\n";

                std::cin.clear();
                ignore_line();
                continue;
            }

            sum += number;

            if (sum > 21) {
                throw std::runtime_error{ "Suma > 21" };
            }

            if (sum == 21) {
                break;
            }
        }
        catch (std::runtime_error& e) {
            std::cerr << e.what() << '\n';
            sum -= number;
            std::cin.clear();
            ignore_line();
        }
    }
}