#include <iostream>
#include <regex>
#include <string>

auto main() -> int
{
    std::regex reg("([0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2})|([0-9]{1,2}:[0-9]{1,2})");

    std::string tests[]{ "12:34", "12:34:56", "1:34", "1:34:45", "122:34", "12:345:24", "12:34:248" };

    for (const auto& test : tests) {
        if (std::regex_match(test, reg)) {
            std::cout << test << " is valid\n";
        }
        else {
            std::cout << test << " is invalid\n";
        }
    }
}