#include <fstream>
#include <iostream>
#include <regex>
#include <string>

auto main() -> int
{
    std::regex nameRegex("[A-Z][a-z]+");
    std::regex lastnameRegex("[A-Z][a-z]+(-[A-Z][a-z]+)?");
    std::regex ageRegex("[0-9]{1,2}");
    std::regex phoneNumberRegex("[1-9][0-9]{8}");
    std::regex emailRegex("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+");

    std::string name;
    std::string lastname;
    std::string age;
    std::string phoneNumber;
    std::string email;

    while (true) {
        std::cout << "Enter your name: ";
        std::getline(std::cin, name);
        std::cout << "Enter your lastname: ";
        std::getline(std::cin, lastname);
        std::cout << "Enter your age: ";
        std::getline(std::cin, age);
        std::cout << "Enter your phone number: ";
        std::getline(std::cin, phoneNumber);
        std::cout << "Enter your email: ";
        std::getline(std::cin, email);

        std::ofstream file{ "data.csv", std::ios::app };

        // if everything matches according regex
        if (std::regex_match(name, nameRegex) && std::regex_match(lastname, lastnameRegex)
            && std::regex_match(age, ageRegex) && std::regex_match(phoneNumber, phoneNumberRegex)
            && std::regex_match(email, emailRegex)) {        // save it to a file
            file << name << ";" << lastname << ";" << age << ";" << phoneNumber << ";" << email << "\n";
        }
        else {
            std::cout << "Niepoprawne dane!\n";
        }
    }
}