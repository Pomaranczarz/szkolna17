#include <iostream>
#include <regex>
#include <string>

auto main() -> int
{
    std::regex streetNameRegex("[A-Z][a-z]+(\\s[A-Z][a-z]+)?");
    std::regex postalCodeRegex("[0-9]{2}-[0-9]{3}");
    std::regex houseNumberRegex("[1-9]+[A-Z]?");
    std::regex flatNumberRegex("[1-9]+");

    std::string streetNames[]{ "Baker Street", "Ulica Sezamkowa", "Generala Andersa", "12 XD", "Gleboka" };
    std::string postalCodes[]{ "22-400", "18-307", "02-222", "1-456", "1-400" };
    std::string houseNumbers[]{ "12", "12A", "12B", "12C", "12D", "1", "Adam" };

    for (const auto& street : streetNames) {
        if (std::regex_match(street, streetNameRegex)) {
            std::cout << street << " is a valid street name.\n";
        }
        else {
            std::cout << street << " is not a valid street name.\n";
        }
    }

    for (const auto& postalCode : postalCodes) {
        if (std::regex_match(postalCode, postalCodeRegex)) {
            std::cout << postalCode << " is a valid postal code.\n";
        }
        else {
            std::cout << postalCode << " is not a valid postal code.\n";
        }
    }

    for (const auto& houseNumber : houseNumbers) {
        if (std::regex_match(houseNumber, houseNumberRegex)) {
            std::cout << houseNumber << " is a valid house number.\n";
        }
        else {
            std::cout << houseNumber << " is not a valid house number.\n";
        }
    }
}