#include <fstream>
#include <iostream>
#include <regex>
#include <set>
#include <string>

auto main() -> int
{
    std::ifstream file{ "data.csv" };
    std::regex domainFromEmailRegex("@.+");
    std::regex phoneNumberEndingWithEvenDigit("[0-9]{8}[02468]");
    std::regex mergedLastnames("[A-Z][a-z]+-[A-Z][a-z]+");

    std::set<std::string> uniqueDomains;
    std::set<std::string> desiredPhoneNumbers;
    std::set<std::string> mergedLastnamesSet;
    std::map<std::string, size_t> nameHistogram;

    std::string line;
    while (std::getline(file, line)) {
        auto name = line.substr(0, line.find(';'));
        nameHistogram[name]++;

        std::string l = line;
        for (std::smatch sm; std::regex_search(line, sm, domainFromEmailRegex); line = sm.suffix()) {
            uniqueDomains.insert(sm.str());
        }

        l = line;
        for (std::smatch sm; std::regex_search(line, sm, phoneNumberEndingWithEvenDigit); line = sm.suffix()) {
            desiredPhoneNumbers.insert(sm.str());
        }

        l = line;
        for (std::smatch sm; std::regex_search(line, sm, mergedLastnames); line = sm.suffix()) {
            mergedLastnamesSet.insert(sm.str());
        }
    }

    std::cout << "Unique domains: \n";
    for (const auto& domain : uniqueDomains) {
        std::cout << domain << '\n';
    }

    std::cout << "\nDesired phone numbers: \n";
    for (const auto& phoneNumber : desiredPhoneNumbers) {
        std::cout << phoneNumber << '\n';
    }

    std::cout << "\nMerged lastnames: \n";
    for (const auto& lastname : mergedLastnamesSet) {
        std::cout << lastname << '\n';
    }

    std::cout << "\nName histogram: \n";
    for (const auto& [name, n] : nameHistogram) {
        std::cout << name << ": " << n << '\n';
    }
}