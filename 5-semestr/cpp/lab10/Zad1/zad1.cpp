#include <iostream>
#include <regex>
#include <string>

auto main() -> int
{
    std::regex regx("(0|[1-9][0-9]*)\\.[0-9]+");
    std::string str{ "Ala000.547ma kota1.2" };

    for (std::smatch sm; std::regex_search(str, sm, regx); str = sm.suffix()) {
        std::cout << "sm.size(): " << sm.size() << '\n';
        std::cout << "sm.length(): " << sm.length() << '\n';
        std::cout << "sm.position(): " << sm.position() << '\n';
        std::cout << "sm.str(): " << sm.str() << '\n';
        std::cout << "sm.prefix(): " << sm.prefix() << '\n';
        std::cout << "sm.suffix(): " << sm.suffix() << '\n';

        std::cout << std::string(40, '=') << '\n';
    }

    std::cout << "\nWersja z while: \n\n";

    str = "Ala000.547ma kota1.2";
    std::smatch sm;
    while (std::regex_search(str, sm, regx)) {
        std::cout << "sm.size(): " << sm.size() << '\n';
        std::cout << "sm.length(): " << sm.length() << '\n';
        std::cout << "sm.position(): " << sm.position() << '\n';
        std::cout << "sm.str(): " << sm.str() << '\n';
        std::cout << "sm.prefix(): " << sm.prefix() << '\n';
        std::cout << "sm.suffix(): " << sm.suffix() << '\n';

        std::cout << std::string(40, '=') << '\n';

        str = sm.suffix();
    }
}