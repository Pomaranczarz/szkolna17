#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/indexed_by.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/ordered_index_fwd.hpp>
#include <boost/multi_index_container.hpp>
#include <iostream>
#include <string>

using namespace std;

class student {
public:
    string imie;
    string nazwisko;
    int indeks;
    string plec;

    void show() const;
};

void student::show() const
{
    cout << "imie: " << imie << "\tnazwisko: " << nazwisko << "\tindeks: " << indeks << "\tplec: " << plec << endl;
}

using namespace boost::multi_index;

using student_multi
    = multi_index_container<student, indexed_by<ordered_non_unique<member<student, std::string, &student::nazwisko>>,
                                                ordered_unique<member<student, int, &student::indeks>>,
                                                ordered_non_unique<member<student, std::string, &student::plec>>,
                                                ordered_non_unique<member<student, std::string, &student::imie>>>>;

using nazwisko_type = student_multi::nth_index<0>::type;
using indeks_type = student_multi::nth_index<1>::type;
using plec_type = student_multi::nth_index<2>::type;
using imie_type = student_multi::nth_index<3>::type;

int main()
{
    student_multi students;

    students.insert({ "Ewa", "Kowalska", 1, "Kobieta" });
    students.insert({ "Jan", "Wojcik", 3, "Mezczyzna" });
    students.insert({ "Zofia", "Wojcik", 4, "Kobieta" });
    students.insert({ "Adam", "Nowak", 2, "Mezczyzna" });

    cout << endl << "wg. :nazwiska " << endl;
    for (nazwisko_type::iterator it = students.get<0>().begin(); it != students.get<0>().end(); ++it)
        it->show();

    cout << endl << "wg. indeksu: " << endl;
    for (indeks_type::iterator it = students.get<1>().begin(); it != students.get<1>().end(); ++it)
        it->show();

    cout << "wg. plci" << endl;
    for (plec_type::iterator it = students.get<2>().begin(); it != students.get<2>().end(); ++it)
        it->show();

    cout << "wg. imienia: " << endl;
    for (imie_type::iterator it = students.get<3>().begin(); it != students.get<3>().end(); ++it)
        it->show();

    return 0;
}