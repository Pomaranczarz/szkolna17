#include <boost/fusion/container/vector/vector.hpp>
#include <iostream>
#include <map>
#include <string>
#include <type_traits>

using boost::fusion::vector;

template <typename T>
void add_type(std::map<std::string, size_t>& map)
{
    ++map[typeid(T).name()];
}

template <typename... Args>
[[nodiscard]] auto type_histogram([[maybe_unused]] const vector<Args...>& vec) -> std::map<std::string, size_t>
{
    std::map<std::string, size_t> result;

    if (sizeof...(Args) > 0) {
        (add_type<Args>(result), ...);
    }

    return result;
}

auto main() -> int
{
    vector<int, int, char, char, bool, int, bool, bool, double, float, float, float, float, char, int> v;
    auto map = type_histogram(v);

    for (const auto& [type, count] : map) {
        std::cout << type << ": " << count << '\n';
    }
}
