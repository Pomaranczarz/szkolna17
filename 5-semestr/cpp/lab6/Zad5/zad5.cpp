#include <algorithm>
#include <any>
#include <boost/fusion/algorithm/transformation/push_back.hpp>
#include <boost/fusion/container/vector/vector.hpp>
#include <boost/fusion/sequence.hpp>
#include <iostream>
#include <string>
#include <type_traits>
#include <variant>
#include <vector>

template <typename... Args>
struct VariantCastProxy {
    std::variant<Args...> v;

    template <typename... ToArgs>
    operator std::variant<ToArgs...>() const        // NOLINT
    {
        return std::visit([](auto&& arg) -> std::variant<ToArgs...> { return arg; }, v);
    }
};

template <class... Args>
auto variant_cast(const std::variant<Args...>& v) -> VariantCastProxy<Args...>
{
    return { v };
}

template <typename T, typename... Args>
concept TypePackContains = (std::is_same_v<T, Args> || ...);

template <typename T, typename... Args>
concept TypePackNotContains = !TypePackContains<T, Args...>;

template <typename... Args, typename T>
    requires TypePackNotContains<T, Args...>
auto push_back(const std::vector<std::variant<Args...>>& vec, T value) -> std::vector<std::variant<Args..., T>>
{
    std::vector<std::variant<Args..., T>> result{ vec.size() + 1 };

    for (const auto& elem : vec) {
        result.push_back(variant_cast(elem));
    }

    result.push_back(value);

    return result;
}

template <typename... Args, typename T>
    requires TypePackContains<T, Args...>
auto push_back(const std::vector<std::variant<Args...>>& vec, T value) -> std::vector<std::variant<Args...>>
{
    std::vector<std::variant<Args...>> result{ vec.begin(), vec.end() };

    result.push_back(value);

    return result;
}

auto main() -> int
{
    std::vector<std::variant<int, std::string, bool, double>> vec{ 10, "C++", true, 3.14 };

    auto printVecElem = [](auto&& elem) {
        std::visit([](auto&& value) { std::cout << value << ' '; }, elem);
    };

    std::cout << "Trzeci element w vec: ";
    printVecElem(vec[2]);
    std::cout << '\n';

    auto vec2 = push_back(vec, 'M');
    auto vec3 = push_back(vec2, 'X');
}