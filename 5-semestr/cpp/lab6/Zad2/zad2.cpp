#include <algorithm>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/indexed_by.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/range/adaptor/uniqued.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/size.hpp>
#include <iostream>
#include <iterator>
#include <optional>
#include <string>

struct Contact {
    std::string m_name;
    std::string m_lastname;
    std::string m_phone_number;
    std::string m_street;
    int m_age;

    friend auto operator<<(std::ostream& out, const Contact& c) -> std::ostream&
    {
        return out << "Contact{ m_name: \"" << c.m_name << "\", m_lastname: \"" << c.m_lastname
                   << "\", m_phone_number: \"" << c.m_phone_number << "\", m_street: \"" << c.m_street
                   << "\", m_age: " << c.m_age << " }";
    }
};

using namespace boost::multi_index;

using MultiContact
    = multi_index_container<Contact, indexed_by<hashed_non_unique<member<Contact, std::string, &Contact::m_name>>,
                                                hashed_non_unique<member<Contact, std::string, &Contact::m_lastname>>,
                                                hashed_unique<member<Contact, std::string, &Contact::m_phone_number>>,
                                                hashed_non_unique<member<Contact, std::string, &Contact::m_street>>,
                                                hashed_non_unique<member<Contact, int, &Contact::m_age>>>>;

class Contacts : protected MultiContact {
public:
    using MultiContact::begin;
    using MultiContact::cbegin;
    using MultiContact::cend;
    using MultiContact::end;
    using MultiContact::insert;

    [[nodiscard]] auto living_on_the_street(const std::string& street) -> std::vector<Contact>
    {
        std::vector<Contact> result;

        auto& streetIndex = get<3>();

        std::copy_if(streetIndex.begin(), streetIndex.end(), std::back_inserter(result),
                     [&street](const Contact& c) { return c.m_street == street; });

        return result;
    }

    [[nodiscard]] auto find_contacts_by_age(int ageBegin, int ageEnd) -> std::vector<Contact>
    {
        auto& ageIndex = get<4>();
        std::vector<Contact> result;

        std::copy_if(ageIndex.begin(), ageIndex.end(), std::back_inserter(result),
                     [ageBegin = ageBegin, ageEnd = ageEnd](const Contact& c) {
            return c.m_age >= ageBegin && c.m_age <= ageEnd;
        });

        return result;
    }

    [[nodiscard]] auto find_by_phone_number(const std::string& phone_number) -> std::optional<Contact>
    {
        auto& numberIndex = get<2>();

        if (auto it = numberIndex.find(phone_number); it != numberIndex.end()) {
            return *it;
        }

        return {};
    }

    void change_street_name(const std::string& from, const std::string& to)
    {
        auto& streetIndex = get<3>();

        for (auto it = streetIndex.begin(); it != streetIndex.end(); ++it) {
            streetIndex.modify(it, [&from, &to](Contact& c) {
                if (c.m_street == from)
                    c.m_street = to;
            });
        }
    }

    [[nodiscard]] auto count_adults() const
    {
        const auto& ageIndex = get<4>();

        return std::count_if(ageIndex.begin(), ageIndex.end(), [](const Contact& c) { return c.m_age >= 18; });
    }

    [[nodiscard]] auto count_unique_lastnames() const
    {
        const auto& lastnameIndex = get<1>();
        std::set<std::string> uniqNames;

        std::for_each(lastnameIndex.begin(), lastnameIndex.end(),
                      [&uniqNames](auto& c) { uniqNames.insert(c.m_lastname); });

        return uniqNames.size();
    }

    void print() const
    {
        for (const auto& contact : *this) {
            std::cout << contact << '\n';
        }
    }
};

auto main() -> int
{
    Contacts contacts;

    contacts.insert({ "Jan", "Kowalski", "1234", "Zielona", 23 });
    contacts.insert({ "Adam", "Janowski", "4321", "Biala", 34 });
    contacts.insert({ "Adam", "Kowalski", "7890", "Czerwona", 45 });
    contacts.insert({ "Jan", "Janowski", "8765", "Niebieska", 35 });
    contacts.insert({ "Karol", "Kowalski", "5678", "Rozowa", 26 });

    contacts.print();

    contacts.insert({ "Jan", "Kowalski", "1357", "Zielona", 37 });

    std::cout << std::string(40, '=') << '\n';

    contacts.print();
}