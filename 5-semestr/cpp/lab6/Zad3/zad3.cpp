#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

template <typename T>
[[nodiscard]] auto less_than_avg(const T& avg, const T& elem) -> bool
{
    return elem < avg;
}

template <typename T>
[[nodiscard]] auto between_avg_and_median(const T& avg, const T& median, const T& elem) -> bool
{
    return elem >= std::min(avg, median) && elem <= std::max(avg, median);
}

template <typename T>
[[nodiscard]] auto positive(const T& elem) -> bool
{
    return elem > 0;
}

using namespace std::placeholders;

template <typename T>
void f(const std::vector<T>& vec)
{
    auto sum = std::accumulate(vec.begin(), vec.end(), T{ 0 });

    auto avg = sum / static_cast<T>(vec.size() - 1);
    auto median = vec.size() % 2 == 0 ? (vec[vec.size() / 2] + vec[vec.size() / 2 + 1]) / 2 : vec[vec.size() / 2];

    std::ostream_iterator<T> out{ std::cout, " " };
    std::cout << "Elementy mniejsze niz srednia: ";
    std::copy_if(vec.begin(), vec.end(), out, std::bind(less_than_avg<T>, avg, _1));
    std::cout << "\nElementy pomiedzy srednia i mediana: ";
    std::copy_if(vec.begin(), vec.end(), out, std::bind(between_avg_and_median<T>, avg, median, _1));
    std::cout << "\nElementy dodatnie: ";
    std::copy_if(vec.begin(), vec.end(), out, positive<T>);
}

auto main() -> int
{
    std::vector<int> v{ -3, -2, -1, 0, 1, 2, 3 };

    f(v);
}