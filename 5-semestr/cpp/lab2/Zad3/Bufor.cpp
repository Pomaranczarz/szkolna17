#include "Bufor.h"
#include <iostream>

Bufor::Bufor()
    : mData{ new int[10] }
    , mSize{ 10 }
    , mLastIndex{ 0 }
{
}

Bufor::Bufor(int size)
    : mData{ new int[static_cast<unsigned long>(size)] }
    , mSize{ size }
    , mLastIndex{ 0 }
{
}

Bufor::~Bufor()
{
    delete[] mData;
}

void Bufor::add(int value)
{
    mData[mLastIndex++] = value;
}

auto Bufor::get_index() const -> int
{
    return mLastIndex;
}

auto Bufor::get_size() const -> int
{
    return mSize;
}

auto Bufor::get_tab(int i) const -> int
{
    return mData[i];
}

auto Bufor::get_first() const -> int
{
    return mLastIndex;
}

void Bufor::set_first(int value)
{
    mData[mLastIndex] = value;
}

void Bufor::set_tab(int pos, int value)
{
    mData[pos] = value;
}

void Bufor::print() const
{
    std::cout << "[";
    for (int i = 0; i < mLastIndex; ++i) {
        std::cout << mData[i] << (i + 1 == mLastIndex ? "" : ", ");
    }
    std::cout << "]";
}