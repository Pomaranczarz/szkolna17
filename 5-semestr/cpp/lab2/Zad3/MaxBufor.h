#ifndef MAX_BUFOR_H
#define MAX_BUFOR_H

#include "Bufor.h"
#include <iostream>

class MaxBufor : public Bufor {
public:
    MaxBufor() = default;

    MaxBufor(int size)
        : Bufor{ size }
    {
    }

    virtual void add(int value) override
    {
        if (get_index() >= get_size()) {
            std::cout << "Bufor is out of space\n";
            return;
        }

        if (get_index() == 0 || value > mMax) {        // we're adding first elem
            mMax = value;
            Bufor::add(value);
        }
        else {
            Bufor::add(value);
        }
    }

    [[nodiscard]] auto calculate() const -> double override { return mMax; }

private:
    int mMax;
};

#endif        // MAX_BUFOR_H