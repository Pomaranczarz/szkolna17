#include "MaxBufor.h"
#include "MeanBufor.h"
#include <experimental/random>
#include <iostream>

using std::experimental::randint;

int main()
{
    Bufor* buffers[4];
    buffers[0] = new MaxBufor{};
    buffers[1] = new MaxBufor{ 5 };
    buffers[2] = new MeanBufor{};
    buffers[3] = new MeanBufor{ 5 };

    for (auto& buffer : buffers) {
        for (int j = 0; j < buffer->get_size(); ++j)
            buffer->add(randint(-10, 10));
    }

    for (auto& buffer : buffers) {
        buffer->print();
        std::cout << "\tCalculate: " << buffer->calculate() << '\n';
    }
}