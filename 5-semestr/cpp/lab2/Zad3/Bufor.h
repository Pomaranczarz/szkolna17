#ifndef BUFOR_H
#define BUFOR_H

#include <cstddef>

class Bufor {
public:
    Bufor();
    Bufor(const Bufor&) = default;
    Bufor(Bufor&&) = delete;
    auto operator=(const Bufor&) -> Bufor& = default;
    auto operator=(Bufor&&) -> Bufor& = delete;
    explicit Bufor(int size);

    virtual ~Bufor();

    virtual void add(int value);

    [[nodiscard]] virtual auto calculate() const -> double = 0;
    [[nodiscard]] auto get_index() const -> int;
    [[nodiscard]] auto get_size() const -> int;
    [[nodiscard]] auto get_tab(int i) const -> int;
    [[nodiscard]] auto get_first() const -> int;
    void set_first(int value);
    void set_tab(int pos, int value);

    void print() const;

private:
    int* mData;
    int mSize;
    int mLastIndex;
};

#endif        // BUFOR_H