#ifndef MEAN_BUFOR_H
#define MEAN_BUFOR_H

#include "Bufor.h"

class MeanBufor : public Bufor {
public:
    MeanBufor() = default;

    MeanBufor(const MeanBufor&) = default;
    MeanBufor(MeanBufor&&) = delete;
    auto operator=(const MeanBufor&) -> MeanBufor& = default;
    auto operator=(MeanBufor&&) -> MeanBufor& = delete;

    explicit MeanBufor(int size)
        : Bufor{ size }
    {
    }

    ~MeanBufor() override = default;

    [[nodiscard]] auto calculate() const -> double override
    {
        double sum = 0.0;
        for (int i = 0; i < get_index(); ++i) {
            sum += get_tab(i);
        }

        return sum / get_index();
    }
};

#endif        // MEAN_BUFOR_H