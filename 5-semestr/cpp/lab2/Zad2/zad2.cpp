#include "Developer.h"
#include "Employee.h"
#include "TeamLeader.h"
#include <iostream>

void print_who_work_more_than5_years(Employee** employees, size_t size);
auto how_many_earn_less_than_mean_bonus(Employee** employees, size_t size) -> size_t;

auto main() -> int
{
    Employee* employees[5];

    employees[0] = new Developer{ "Nowak", 23, 1, 4000 };
    employees[1] = new TeamLeader{ "Kleks", 45, 8, 8700 };
    employees[2] = new Developer{ "Bocian", 34, 12, 10200 };
    employees[3] = new TeamLeader{ "Zielony", 26, 1, 4500 };
    employees[4] = new Developer{ "Bialy", 56, 25, 15675 };

    print_who_work_more_than5_years(employees, 5);
    std::cout << '\n' << how_many_earn_less_than_mean_bonus(employees, 5);
}

void print_who_work_more_than5_years(Employee** employees, size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        if (employees[i]->get_experience() > 5) {
            employees[i]->show();
        }
    }
}

auto how_many_earn_less_than_mean_bonus(Employee** employees, size_t size) -> size_t
{
    int sum = 0;
    for (size_t i = 0; i < size; ++i) {
        sum += employees[i]->calculate_bonus(employees[i]->get_salary());
    }

    int mean = sum / static_cast<int>(size);

    size_t count = 0;
    for (size_t i = 0; i < size; ++i) {
        if (employees[i]->calculate_bonus(employees[i]->get_salary()) < mean) {
            ++count;
        }
    }

    return count;
}