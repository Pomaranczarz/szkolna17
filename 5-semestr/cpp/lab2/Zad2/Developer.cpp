#include "Developer.h"

Developer::Developer(const std::string& surname, int age, int experience, int salary)
    : Employee{ surname, age, experience, salary }
{
}

auto Developer::calculate_bonus(int value) const -> int
{
    return value + value * (get_salary() + get_experience()) / 5;
}