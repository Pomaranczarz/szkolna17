#ifndef DEVELOPER_H
#define DEVELOPER_H

#include "Employee.h"

class Developer : public Employee {
public:
    Developer() = default;
    Developer(const std::string& surname, int age, int experience, int salary);

    [[nodiscard]] auto calculate_bonus(int value) const -> int override;
};

#endif        // DEVELOPER_H