#include "TeamLeader.h"
#include <iostream>

TeamLeader::TeamLeader(const std::string& surname, int age, int experience, int salary)
    : Employee{ surname, age, experience, salary }
{
}

auto TeamLeader::calculate_bonus(int value) const -> int
{
    return value * (1 + get_salary() + get_experience());
}

void TeamLeader::show() const
{
    Employee::show();
    std::cout << "Jestem Team Leader z " << get_experience() << " letnim doswiadczeniem\n";
}