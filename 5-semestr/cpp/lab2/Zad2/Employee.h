#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>

class Employee {
public:
    Employee() = default;
    Employee(const Employee&) = default;
    Employee(Employee&&) = delete;
    auto operator=(const Employee&) -> Employee& = default;
    auto operator=(Employee&&) -> Employee& = delete;
    Employee(std::string surname, int age, int experience, int salary);

    virtual ~Employee() = default;

    [[nodiscard]] auto age_employment() const -> int;

    virtual void show() const;
    [[nodiscard]] virtual auto calculate_bonus(int value) const -> int = 0;

    [[nodiscard]] auto get_surname() const -> std::string;
    [[nodiscard]] auto get_age() const -> int;
    [[nodiscard]] auto get_experience() const -> int;
    [[nodiscard]] auto get_salary() const -> int;

private:
    std::string mSurname;
    int mAge{};
    int mExeprience{};
    int mSalary{};
};

#endif        // EMPLOYEE_H