#include "Employee.h"
#include <iostream>
#include <utility>

Employee::Employee(std::string surname, int age, int experience, int salary)
    : mSurname{ std::move(surname) }
    , mAge{ age }
    , mExeprience{ experience }
    , mSalary{ salary }
{
}

void Employee::show() const
{
    std::cout << "Surname: " << mSurname << "\tAge: " << mAge << "\tExperience: " << mExeprience << "\tSalary"
              << mSalary << '\n';
}

auto Employee::age_employment() const -> int
{
    return mAge - mExeprience;
}

auto Employee::get_surname() const -> std::string
{
    return mSurname;
}

auto Employee::get_age() const -> int
{
    return mAge;
}

auto Employee::get_experience() const -> int
{
    return mExeprience;
}

auto Employee::get_salary() const -> int
{
    return mSalary;
}