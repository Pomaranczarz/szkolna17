#ifndef TEAMLEADER_H
#define TEAMLEADER_H

#include "Employee.h"

class TeamLeader : public Employee {
public:
    TeamLeader() = default;
    TeamLeader(const std::string& surname, int age, int experience, int salary);

    [[nodiscard]] auto calculate_bonus(int value) const -> int override;

    void show() const override;
};

#endif        // TEAMLEADER_H