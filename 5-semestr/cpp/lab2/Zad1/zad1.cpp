#include "Circle.h"
#include "Figure.h"
#include "Square.h"
#include <iostream>

using namespace std;

auto main() -> int
{
    Figure* f1 = new Square{ 4 };
    Figure* f2 = new Circle{ 2 };
    f1->calculate_area();
    f1->show();
    f2->calculate_area();
    f2->show();

    delete f1;
    delete f2;

    Figure* tab[3];

    tab[0] = new Square{ 4 };
    tab[1] = new Square{ 2 };
    tab[2] = new Circle{ 5 };

    for (auto& figure : tab) {
        figure->calculate_area();
        std::cout << figure->get_circumference() << '\n';
        delete figure;
    }
}