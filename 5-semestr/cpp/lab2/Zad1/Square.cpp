#include "Square.h"
#include <iostream>

using namespace std;

Square::Square(float a)
    : mSide{ a }
{
    cout << "Konstruktor klasy Square" << endl;
}

Square::~Square()
{
    cout << "Destruktor klasy Square" << endl;
}

void Square::calculate_area()
{
    set_area(mSide * mSide);
}

void Square::show()
{
    cout << "Show w klasie Sqaure, pole: " << get_area() << endl;
}

auto Square::get_circumference() const -> float
{
    return 4 * mSide;
}