#ifndef CIRCLE_H_INCLUDED
#define CIRCLE_H_INCLUDED

#include "Figure.h"

class Circle : public Figure {
public:
    Circle(const Circle&) = default;
    Circle(Circle&&) = delete;
    auto operator=(const Circle&) -> Circle& = default;
    auto operator=(Circle&&) -> Circle& = delete;
    explicit Circle(float r1);
    ~Circle() override;

    void calculate_area() override;
    [[nodiscard]] auto get_circumference() const -> float override;

private:
    float r;
};
#endif        // CIRCLE_H_INCLUDED