#ifndef SQUARE_H_INCLUDED
#define SQUARE_H_INCLUDED
#include "Figure.h"

class Square : public Figure {
public:
    Square(const Square&) = default;
    Square(Square&&) = delete;
    auto operator=(const Square&) -> Square& = default;
    auto operator=(Square&&) -> Square& = delete;
    explicit Square(float a1);
    ~Square() override;

    void calculate_area() override;
    [[nodiscard]] auto get_circumference() const -> float override;

    void show() override;

private:
    float mSide{};
};
#endif        // SQUARE_H_INCLUDED