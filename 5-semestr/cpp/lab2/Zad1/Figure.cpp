#include "Figure.h"
#include <iostream>

using namespace std;

Figure::Figure()
{
    cout << "Konstruktor klasy bazowej Figure" << endl;
}

Figure::~Figure()
{
    cout << "Wirtualny destruktor klasy bazowej Figure" << endl;
}

auto Figure::get_area() const -> float
{
    return mArea;
}

void Figure::set_area(float area)
{
    this->mArea = area;
}

void Figure::show()
{
    cout << "Pole: " << mArea << endl;
}