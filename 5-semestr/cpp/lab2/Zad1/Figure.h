#ifndef FIGURE_H_INCLUDED
#define FIGURE_H_INCLUDED

class Figure {
public:
    Figure();

    Figure(const Figure&) = default;
    Figure(Figure&&) = delete;
    auto operator=(const Figure&) -> Figure& = default;
    auto operator=(Figure&&) -> Figure& = delete;

    explicit Figure(float mArea)
        : mArea(mArea)
    {
    }

    virtual ~Figure();

    [[nodiscard]] auto get_area() const -> float;
    void set_area(float area);
    virtual void calculate_area() = 0;
    [[nodiscard]] virtual auto get_circumference() const -> float = 0;

    virtual void show();

private:
    float mArea{};
};
#endif        // FIGURE_H_INCLUDED