#include "Circle.h"
#include <cmath>
#include <iostream>

using namespace std;

template <typename T>
static constexpr T PI = static_cast<T>(M_PI);

Circle::Circle(float r1)
    : r{ r1 }
{
    cout << "Konstruktor w klasie Circle" << endl;
}

Circle::~Circle()
{
    cout << "Destruktor w klasie Circle" << endl;
}

void Circle::calculate_area()
{
    set_area(PI<float> * r * r);
}

auto Circle::get_circumference() const -> float
{
    return 2 * PI<float> * r;
}