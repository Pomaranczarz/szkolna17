#include <cmath>
#include <gtest/gtest.h>

auto is_prime(int number) -> bool
{
    if (number <= 1) {
        return false;
    }

    const int ROOT = static_cast<int>(std::sqrt(number));

    for (int i = 2; i <= ROOT; ++i) {
        if (number % i == 0) {
            return false;
        }
    }

    return true;
}

auto main(int argc, char** argv) -> int
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(isPrime, LessThanOne)
{
    EXPECT_FALSE(is_prime(-45));
    EXPECT_FALSE(is_prime(0));
    EXPECT_FALSE(is_prime(1));
}

TEST(isPrime, Prime)
{
    EXPECT_TRUE(is_prime(2));
    EXPECT_TRUE(is_prime(7));
    EXPECT_TRUE(is_prime(2137));
}

TEST(isPrime, NotPrime)
{
    EXPECT_FALSE(is_prime(8));
    EXPECT_FALSE(is_prime(420));
    EXPECT_FALSE(is_prime(1024));
}