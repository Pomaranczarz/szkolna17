#include <gtest/gtest.h>
#include <string>
#include <utility>

class BankAccount {
private:
    std::string last_name_;
    std::string account_number_;
    float balance_;

public:
    BankAccount(std::string last_name, std::string account_number, float balance)
        : last_name_{ std::move(last_name) }
        , account_number_{ std::move(account_number) }
        , balance_{ balance }
    {
    }

    void add(float amount)
    {
        if (amount < 0) {
            return;
        }

        balance_ += amount;
    }

    void withdraw(float amount)
    {
        if (amount < 0 || balance_ - amount < 0) {
            return;
        }

        balance_ -= amount;
    }

    [[nodiscard]] auto is_millionaire() const -> bool { return balance_ >= 1'000'000; }

    [[nodiscard]] auto get_lastname() const -> std::string { return last_name_; }

    [[nodiscard]] auto get_account_number() const -> std::string { return account_number_; }

    [[nodiscard]] auto get_balance() const -> float { return balance_; }

    void set_lastname(const std::string& last_name) { last_name_ = last_name; }

    void set_balance(float amount)
    {
        if (amount < 0) {
            return;
        }

        balance_ = amount;
    }

    void set_account_number(const std::string& account_number) { account_number_ = account_number; }
};

auto main(int argc, char** argv) -> int
{
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}

class BankAccountTest : public testing::Test {
public:
    BankAccount account{ "Kowalski", "1234567890", 1200.37f };

    void SetUp() override {}
};

TEST_F(BankAccountTest, CanGetLastname)
{
    ASSERT_STREQ(account.get_lastname().c_str(), "Kowalski");
}

TEST_F(BankAccountTest, CanGetAccountNumber)
{
    ASSERT_STREQ(account.get_account_number().c_str(), "1234567890");
}

TEST_F(BankAccountTest, CanGetBalance)
{
    ASSERT_EQ(account.get_balance(), 1200.37F);
}

TEST_F(BankAccountTest, CanSetLastname)
{
    account.set_lastname("Nowak");
    ASSERT_STREQ(account.get_lastname().c_str(), "Nowak");
}

TEST_F(BankAccountTest, CanSetAccountNumber)
{
    account.set_account_number("0987654321");
    ASSERT_STREQ(account.get_account_number().c_str(), "0987654321");
}

TEST_F(BankAccountTest, CanSetBalance)
{
    account.set_balance(123.45F);
    ASSERT_EQ(account.get_balance(), 123.45F);
}

TEST_F(BankAccountTest, BalanceDoesntChangeOnInvalidValue)
{
    account.set_balance(-34.27f);
    ASSERT_NE(account.get_balance(), -34.27f);
}

TEST_F(BankAccountTest, CanAdd)
{
    const float PREV_BALANCE = account.get_balance();
    account.add(12.F);

    ASSERT_EQ(account.get_balance(), PREV_BALANCE + 12.F);
}

TEST_F(BankAccountTest, CanWithdraw)
{
    const float PREV_BALANCE = account.get_balance();
    account.withdraw(34.27F);

    ASSERT_EQ(account.get_balance(), PREV_BALANCE - 34.27F);
}

TEST_F(BankAccountTest, AddDoesntChangeBalanceOnNegativeAmount)
{
    account.add(-12.34f);
    ASSERT_EQ(account.get_balance(), 1200.37f);
}

TEST_F(BankAccountTest, WithdrawDoesntChangebalanceOnNegativeAmount)
{
    account.withdraw(-3.45F);
    ASSERT_EQ(account.get_balance(), 1200.37F);
}

TEST_F(BankAccountTest, SetBalanceDoesntChangeBalanceOnNegativeAmount)
{
    account.set_balance(-3.f);
    ASSERT_EQ(account.get_balance(), 1200.37f);
}