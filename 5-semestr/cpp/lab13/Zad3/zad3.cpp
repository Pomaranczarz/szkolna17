#include <gtest/gtest.h>
#include <limits>
#include <stdexcept>

auto getReciprocal(float number) -> float
{
    if (number == 0) {
        throw std::invalid_argument("Can't get reciprocal of 0");
    }

    return 1 / number;
}

auto main(int argc, char** argv) -> int
{
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}

TEST(getReciprocal, ThrowsOnZero)
{
    EXPECT_THROW(getReciprocal(0), std::invalid_argument);
}

TEST(getReciprocal, ReturnsReciprocal)
{
    static constexpr float EPSILON = 0.01F;

    EXPECT_NEAR(getReciprocal(3), 0.33333, EPSILON);
    EXPECT_NEAR(getReciprocal(12), 0.08333, EPSILON);
    EXPECT_NEAR(getReciprocal(100), 0.001, EPSILON);
}