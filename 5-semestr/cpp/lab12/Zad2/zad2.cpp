#include <algorithm>
#include <format>
#include <iostream>
#include <utility>
#include <vector>

class Car {
public:
    Car(std::string brand, int productionYear, int engineCapacity)
        : _brand{ std::move(brand) }
        , _productionYear{ productionYear }
        , _engineCapacity{ engineCapacity }
    {
    }

    [[nodiscard]] auto get_brand() const -> std::string { return _brand; }

    [[nodiscard]] auto get_production_year() const -> int { return _productionYear; }

    [[nodiscard]] auto get_engine_capacity() const -> int { return _engineCapacity; }

    void set_brand(const std::string& brand) { _brand = brand; }

    void set_production_year(int productionYear) { _productionYear = productionYear; }

    void set_engine_capacity(int engineCapacity) { _engineCapacity = engineCapacity; }

    friend auto operator<<(std::ostream& out, const Car& c) -> std::ostream&
    {
        return out << std::format("Marka: {:10}\tRok produkcji: {:5}\tPojemnosc:{:4}", c._brand, c._productionYear,
                                  c._engineCapacity);
    }

private:
    std::string _brand;
    int _productionYear;
    int _engineCapacity;
};

auto main() -> int
{
    std::vector<Car> cars{
        {"Ford",     2002, 3 },
        { "Polonez", 1984, 1 },
        { "Lada",    1993, 2 },
        { "Ursus",   2023, 8 },
        { "Man",     2015, 12},
        { "Autosan", 1999, 3 }
    };

    auto printVec = [&cars] {
        for (size_t i = 0; i < cars.size(); ++i) {
            std::cout << i + 1 << ". " << cars[i] << '\n';
        }
    };

    std::cout << "Samochody: \n";
    printVec();

    std::cout << "Posortowane rosnaco po roku: \n";
    std::sort(cars.begin(), cars.end(),
              [](const Car& l, const Car& r) { return l.get_production_year() < r.get_production_year(); });

    printVec();

    std::cout << "Posortowane rosnaco po pojemnosci: \n";
    bool ascending = true;
    std::sort(cars.begin(), cars.end(), [&ascending](const Car& l, const Car& r) {
        if (ascending) {
            return l.get_engine_capacity() < r.get_engine_capacity();
        }
        else {
            return l.get_engine_capacity() > r.get_engine_capacity();
        }
    });
    printVec();
}