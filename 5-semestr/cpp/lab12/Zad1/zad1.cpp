#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

auto main() -> int
{
    std::vector<int> v{ -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };

    auto printVec = [](const auto& vec) {
        std::for_each(vec.begin(), vec.end(), [](const auto& elem) { std::cout << elem << ", "; });
    };

    std::cout << "Wejsciowe dane: ";
    printVec(v);

    double avg = std::accumulate(v.begin(), v.end(), 0.0) / (double) v.size();
    std::cout << "\nSrednia: " << avg << '\n';
    auto even = std::count_if(v.begin(), v.end(), [](const int& elem) { return elem % 2 == 0; });
    std::cout << "Ile parzystych: " << even << '\n';
    std::erase_if(v, [](const int& elem) { return elem < 0; });
    std::cout << "Po usunieciu ujemnych: ";
    printVec(v);

    std::partition(v.begin(), v.end(), [](const int& elem) { return elem % 2 == 0; });
    std::cout << "\nPo sortowaniu: ";
    printVec(v);

    std::for_each(v.begin(), v.end(), [](auto& elem) { elem *= -1; });
    std::cout << "\nPo zmianie kazdej wartosci na przeciwna: ";
    printVec(v);

    auto count = std::count_if(v.begin(), v.end(), [](const auto& elem) { return elem < 3; });

    std::cout << "\nElementow mniejszych od 3: " << count << '\n';
}