#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <tuple>
#include <vector>

using ret_type = std::tuple<size_t, size_t, std::string>;

template <typename Container>
auto f(const Container& cont) -> ret_type
{
    ret_type result;

    std::get<0>(result) = std::min_element(cont.begin(), cont.end(), [](const std::string& l, const std::string& r) {
                              return l.size() < r.size();
                          })->size();

    std::get<1>(result) = std::accumulate(cont.begin(), cont.end(), 0UL,
                                          [](size_t sum, const std::string& s) { return sum + s.size(); })
                        / cont.size();

    std::get<2>(result) = *std::max_element(
        cont.begin(), cont.end(), [](const std::string& l, const std::string& r) { return l.size() < r.size(); });

    return result;
}

auto main() -> int
{
    std::vector<std::string> v{
        "siema",
        "co",
        "tam",
        "Ala ma kota",
    };

    auto [shortest, avg, longest] = f(v);
    std::cout << shortest << " " << avg << " " << longest << '\n';
}