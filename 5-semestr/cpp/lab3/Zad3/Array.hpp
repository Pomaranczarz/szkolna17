#ifndef ARRAY_H
#define ARRAY_H

#include <algorithm>
#include <cstddef>
#include <initializer_list>
#include <iostream>
#include <stdexcept>
#include <utility>

template <typename T, size_t N>
class BaseArray {
public:
    BaseArray()
    {
        std::for_each(begin(), end(), [](auto& elem) { elem = T{}; });
    }

    BaseArray(std::initializer_list<T>&& ilist)
        : BaseArray{}
    {
        size_t index = 0;
        for (auto it = ilist.begin(); it != ilist.end() && index < N; ++it) {
            mData[index++] = *it;
        }
    }

    BaseArray(const BaseArray& other)
    {
        for (size_t i = 0; i < N; ++i) {
            mData[i] = other.mData[i];
        }
    }

    BaseArray(BaseArray&& other) noexcept
    {
        for (size_t i = 0; i < N; ++i) {
            mData[i] = std::move(other.mData[i]);
        }
    }

    [[nodiscard]] auto operator=(const BaseArray& other) -> BaseArray&
    {
        if (&other == this) {
            return *this;
        }

        std::copy(std::begin(other.mData), std::end(other.mData), std::begin(mData));

        return *this;
    }

    [[nodiscard]] auto operator=(BaseArray&& other) noexcept -> BaseArray&
    {
        if (&other == this) {
            return *this;
        }

        std::copy(std::begin(other.mData), std::end(other.mData), std::begin(mData));

        return *this;
    }

    ~BaseArray() = default;

    [[nodiscard]] auto operator[](size_t index) -> T& { return mData[index]; }

    [[nodiscard]] auto at(size_t index) const -> T&
    {
        if (index < 0 || index > N) {
            throw std::out_of_range("Index is out of range");
        }

        return mData[index];
    }

    [[nodiscard]] auto size() const -> size_t { return N; }

    [[nodiscard]] auto begin() -> T* { return mData; }

    [[nodiscard]] auto end() -> T* { return mData + N; }

    [[nodiscard]] auto cbegin() -> const T* { return mData; }

    [[nodiscard]] auto cend() -> const T* { return mData + N; }

    [[nodiscard]] auto rbegin() -> T* { return mData + N - 1; }

    [[nodiscard]] auto rend() -> T* { return mData - 1; }

    [[nodiscard]] auto crbegin() -> const T* { return mData + N - 1; }

    [[nodiscard]] auto crend() -> const T* { return mData - 1; }

    [[nodiscard]] auto max() const -> T
    {
        T max = mData[0];

        for (size_t i = 1; i < N; ++i) {
            if (mData[i] > max) {
                max = mData[i];
            }
        }

        return max;
    }

    [[nodiscard]] auto sorted() const -> BaseArray<T, N>
    {
        auto result = *this;
        std::sort(result.begin(), result.end(), [](const T& l, const T& r) { return l < r; });

        return result;
    }

    friend auto operator<<(std::ostream& out, const BaseArray& array) -> std::ostream&
    {
        out << "[";
        for (size_t i = 0; i < N; ++i) {
            out << array.mData[i] << (i + 1 == N ? "]" : ",");
        }

        return out;
    }

protected:
    T mData[N];
};

template <typename T, size_t N>
class Array : public BaseArray<T, N> {};

template <size_t N>
class Array<std::string, N> {
private:
    using BaseArray<std::string, N>::mData;

public:
    [[nodiscard]] auto max() const -> std::string
    {
        std::string max = mData[0];
        for (size_t i = 0; i < N; ++i) {
            if (mData[i].size() > max.size()) {
                max = mData[i];
            }
        }

        return mData;
    }

    [[nodiscard]] auto sorted() const -> Array
    {
        auto result = *this;
        std::sort(result.begin(), result.end(),
                  [](const std::string& l, const std::string& r) { return l.size() < r.size(); });

        return result;
    }
};

#endif        // ARRAY_H
