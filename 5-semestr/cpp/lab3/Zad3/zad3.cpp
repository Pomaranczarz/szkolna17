#include "Array.hpp"

int main()
{
    BaseArray<int, 10> arr{ 6, 7, 1, 3, 4, 2, 9, 0, 12 };

    std::cout << arr << '\n';

    std::cout << arr.sorted() << '\n';
    std::cout << arr.max() << '\n';
}