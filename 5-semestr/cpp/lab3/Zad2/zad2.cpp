#include <array>
#include <concepts>
#include <iostream>
#include <type_traits>
#include <vector>

#if __cplusplus >= 202002L
template <typename T>
concept Iterable = requires (T container) {
    {
        container.begin()
    } -> std::same_as<typename T::iterator>;
    {
        container.end()
    } -> std::same_as<typename T::iterator>;
};

#else

template <typename T>
struct Iterable {
    template <typename U>
    static auto test(U* p) -> decltype(p->begin(), p->end(), std::true_type{});

    template <typename U>
    static auto test(...) -> std::false_type;

    static constexpr bool VALUE = decltype(test<T>(nullptr))::value;
};

#endif

#if __cplusplus >= 202002L
template <Iterable T>
auto min(const T& container)
#else
template <typename T>
using EnabledContainedType = typename std::enable_if<Iterable<T>::value, typename T::value_type>::type;

template <typename T>
auto min(const T& container) -> EnabledContainedType<T>
#endif
{
    auto min = *container.begin();
    for (const auto& elem : container) {
        if (elem < min) {
            min = elem;
        }
    }

    return min;
}

template <typename T, size_t N>
auto min(T (&array)[N]) -> T
{
    T min = array[0];
    for (size_t i = 0; i < N; ++i) {
        if (array[i] < min) {
            min = array[i];
        }
    }

    return min;
}

template <typename T>
auto min(T* array, size_t size) -> T
{
    T min = array[0];
    for (size_t i = 0; i < size; ++i) {
        if (array[i] < min) {
            min = array[i];
        }
    }

    return min;
}

auto main() -> int
{
    std::vector<int> v{ 2, 4, 6, 1, 7, 2, 12, 0 };
    int arr[]{ 1, 2, 21, 4, 5, 6, 7, 16 };
    std::array<double, 6> a{ 12.3, 34.5, 43.2, 19.0, 56.7, 5.78 };

    std::cout << min(v) << '\n' << min(arr) << '\n' << min(a) << '\n';
}