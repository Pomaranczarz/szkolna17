#include <algorithm>
#include <experimental/random>
#include <iostream>
#include <sstream>
#include <vector>

template <typename T>
class ByDigitsSum {
public:
    [[nodiscard]] auto operator()(const T& l, const T& r) -> bool { return get_digits_sum(l) < get_digits_sum(r); }

private:
    [[nodiscard]] auto get_digits_sum(T number) -> size_t
    {
        size_t sum = 0;
        while (number) {
            sum += number % 10;
            number /= 10;
        }

        return sum;
    }
};

template <typename T>
[[nodiscard]] auto get_digits_count(T number) -> size_t
{
    size_t digits = 0;
    while (number) {
        ++digits;
        number /= 10;
    }

    return digits;
}

template <typename T>
[[nodiscard]] auto by_digits_count(const T& l, const T& r) -> bool
{
    return get_digits_count(l) < get_digits_count(r);
}

auto main() -> int
{
    using std::experimental::randint;

    std::vector<int> v;
    auto print = [](const auto& elem) {
        std::cout << elem << ' ';
    };

    for (int i = 0; i < 20; ++i) {
        v.push_back(randint(-300, 300));
    }

    std::for_each(v.begin(), v.end(), print);
    std::cout << '\n';

    std::sort(v.begin(), v.end(), ByDigitsSum<int>{});

    std::for_each(v.begin(), v.end(), print);
    std::cout << '\n';

    std::sort(v.begin(), v.end(), by_digits_count<int>);

    std::for_each(v.begin(), v.end(), print);
    std::cout << '\n';
}