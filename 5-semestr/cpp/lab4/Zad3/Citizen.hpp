#ifndef CITIZEN_HPP
#define CITIZEN_HPP

#include <string>

class Citizen {
public:
    Citizen() = default;
    Citizen(std::string name, std::string surname, int age, char sex, std::string postalCode);

    void show() const;

    [[nodiscard]] auto get_name() const -> std::string;
    [[nodiscard]] auto get_surname() const -> std::string;
    [[nodiscard]] auto get_age() const -> int;
    [[nodiscard]] auto get_sex() const -> char;
    [[nodiscard]] auto get_postal_code() const -> std::string;

private:
    std::string _name;
    std::string _surname;
    int _age{};
    char _sex{};
    std::string _postal_code;
};

#endif        // CITIZEN_HPP