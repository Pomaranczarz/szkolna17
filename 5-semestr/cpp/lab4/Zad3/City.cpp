#include "City.hpp"
#include "Citizen.hpp"
#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <utility>

City::City(std::string name)
    : _city_name{ std::move(name) }
{
}

void City::add_citizen(const Citizen& citizen)
{
    _citizens.push_back(citizen);
}

void City::delete_citizen(const std::string& surname, int age)
{
    std::erase_if(_citizens,
                  [&](const Citizen& citizen) { return citizen.get_surname() == surname && citizen.get_age() == age; });
}

void City::show_city() const
{
    std::cout << _city_name << '\n';
}

void City::show_citizens() const
{
    std::for_each(_citizens.begin(), _citizens.end(), [](const Citizen& citizen) { citizen.show(); });
}

auto City::city_citizens() const -> size_t
{
    return _citizens.size();
}

auto City::count_women() const -> size_t
{
    return std::count_if(_citizens.begin(), _citizens.end(),
                         [](const Citizen& citizen) { return citizen.get_sex() == 'k'; });
}

auto City::adults() const -> size_t
{
    return std::count_if(_citizens.begin(), _citizens.end(),
                         [](const Citizen& citizen) { return citizen.get_age() >= 18; });
}

auto City::postal_codes() const -> size_t
{
    std::map<std::string, int> stats;

    for (const auto& citizen : _citizens) {
        stats[citizen.get_postal_code()]++;
    }

    for (const auto& [code, citizens] : stats) {
        std::cout << code << ": " << citizens << '\n';
    }

    return stats.size();
}
