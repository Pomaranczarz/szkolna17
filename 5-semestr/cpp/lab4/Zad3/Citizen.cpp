#include "Citizen.hpp"
#include <iostream>
#include <utility>

Citizen::Citizen(std::string name, std::string surname, int age, char sex, std::string postalCode)
    : _name{ std::move(name) }
    , _surname{ std::move(surname) }
    , _age{ age }
    , _sex{ sex }
    , _postal_code{ std::move(postalCode) }
{
}

void Citizen::show() const
{
    std::cout << _name << " " << _surname << ": " << _sex << " " << _age << "(" << _postal_code << ")";
}

auto Citizen::get_name() const -> std::string
{
    return _name;
}

auto Citizen::get_surname() const -> std::string
{
    return _surname;
}

auto Citizen::get_age() const -> int
{
    return _age;
}

auto Citizen::get_sex() const -> char
{
    return _sex;
}

auto Citizen::get_postal_code() const -> std::string
{
    return _postal_code;
}