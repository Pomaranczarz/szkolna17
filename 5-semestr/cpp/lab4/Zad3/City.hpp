#ifndef CITY_HPP
#define CITY_HPP

#include "Citizen.hpp"
#include <vector>

class City {
public:
    explicit City(std::string name);

    void add_citizen(const Citizen& citizen);
    void delete_citizen(const std::string& surname, int age);
    void show_citizens() const;
    void show_city() const;
    [[nodiscard]] auto count_women() const -> size_t;
    [[nodiscard]] auto city_citizens() const -> size_t;
    [[nodiscard]] auto adults() const -> size_t;
    [[nodiscard]] auto postal_codes() const -> size_t;

private:
    std::vector<Citizen> _citizens;
    std::string _city_name;
};

#endif        // CITY_HPP