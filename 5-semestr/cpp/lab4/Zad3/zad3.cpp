#include "Citizen.hpp"
#include "City.hpp"
#include <algorithm>
#include <experimental/random>
#include <iostream>
#include <random>
#include <vector>

enum class Mode
{
    POSTAL_CODES,
    NUMBER_OF_CITIZENS,
};

void show_cities(const std::vector<City>& cities);
void the_most(const std::vector<City>& cities, Mode mode);
void statistics(const std::vector<City>& cities);
void sort_cities(std::vector<City>& cities);

auto main() -> int
{
    std::vector<std::string> maleNames{ "Jan", "Andrzej", "Zbyszek", "Hieronim" };
    std::vector<std::string> femaleNames{ "Maria", "Anna", "Jolanta", "Janina", "Aniela" };
    std::vector<std::string> surnames{ "Nowak", "Kowalski", "Zdun", "Kowalczyk", "Wilk" };

    std::vector<std::string> postalCodes{ "22-100", "22-101", "22-102", "22-103", "22-104", "22-105" };

    std::vector<Citizen> citizens;

    using std::experimental::randint;

    for (int i = 0; i < 20; ++i) {
        char sex = randint(0, 100) < 50 ? 'm' : 'k';
        std::string name;

        if (sex == 'm') {
            name = maleNames[randint(0, (int) maleNames.size() - 1)];
        }
        else {
            name = femaleNames[randint(0, (int) maleNames.size() - 1)];
        }

        citizens.emplace_back(name, surnames[randint(0, (int) surnames.size() - 1)], randint(13, 80), sex,
                              postalCodes[randint(0, (int) postalCodes.size() - 1)]);
    }

    std::vector<City> cities{ City{ "Radom" }, City{ "Warszawa" }, City{ "Lublin" }, City{ "Walbrzych" },
                              City{ "Wachock" } };

    for (auto& city : cities) {
        int size = randint(10, 50);
        for (int i = 0; i < size; ++i) {
            city.add_citizen(citizens[randint(0, (int) citizens.size() - 1)]);
        }
    }

    show_cities(cities);

    the_most(cities, Mode::NUMBER_OF_CITIZENS);
    the_most(cities, Mode::POSTAL_CODES);

    statistics(cities);

    sort_cities(cities);

    show_cities(cities);

    std::vector<int> v{ 3, 2, 4, 5, 1, 0 };
}

void show_cities(const std::vector<City>& cities)
{
    for (const auto& city : cities) {
        city.show_city();
        city.show_citizens();
        std::cout << '\n' << std::string(40, '=') << '\n';
    }
}

void the_most(const std::vector<City>& cities, Mode mode)
{
    if (mode == Mode::NUMBER_OF_CITIZENS) {
        auto min = cities.begin();
        for (auto it = cities.cbegin(); it != cities.cend(); ++it) {
            if (it->city_citizens() > min->city_citizens()) {
                min = it;
            }
        }

        std::cout << "Najmniej mieszkancow: ";
        min->show_city();
    }
    else if (mode == Mode::POSTAL_CODES) {
        size_t most = 0;
        auto city = cities.cbegin();
        for (auto it = cities.cbegin(); it != cities.cend(); ++it) {
            size_t codes = it->postal_codes();
            if (codes > most) {
                most = codes;
                city = it;
            }
        }

        std::cout << "Najwiecej roznych kodow pocztowych: ";
        city->show_city();
    }
    else {
        return;
    }
}

void statistics(const std::vector<City>& cities)
{
    for (const auto& city : cities) {
        city.show_city();
        std::cout << city.city_citizens() << '\t';
        std::cout << city.count_women() << '\t' << city.city_citizens() - city.count_women() << '\n';
        std::cout << city.adults() << '\t' << city.city_citizens() - city.adults() << '\n';
    }
}

void sort_cities(std::vector<City>& cities)
{
    std::sort(cities.begin(), cities.end(),
              [](const City& c1, const City& c2) { return c1.city_citizens() < c2.city_citizens(); });
}