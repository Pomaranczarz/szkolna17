#include <algorithm>
#include <experimental/random>
#include <iostream>
#include <iterator>
#include <list>

int main()
{
    using std::experimental::randint;

    const size_t n = randint(1, 20);

    std::list<int> list;
    for (size_t i = 0; i < n; ++i) {
        int random = randint(-100, 100);

        if (random < 0)
            list.push_back(random);
        else
            list.push_front(random);
    }

    std::for_each(list.begin(), list.end(), [](const auto& elem) { std::cout << elem << ' '; });
}