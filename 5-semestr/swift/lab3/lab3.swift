import Foundation

func zad1() {
    print("Podaj ciąg: ")
    let str = readLine()!
    print("Podaj znak: ")
    let c = (readLine()!)[str.startIndex]
    print("Podaj liczbę: ")
    let n = getchar() - 48

    if str.hasPrefix(String(c)) {
        print("Napis zaczyna się od \(c)")
    }
    if str.hasSuffix(String(c)) {
        print("Napis kończy się na \(c)")
    }
    if let i = str.firstIndex(of: c) {
        let index: Int = str.distance(from: str.startIndex, to: i)
        if index == n {
            print("\(c) znajduje się na \(n). pozycji od początku")
        }
    }
    if let i = str.lastIndex(of: c) {
        let index: Int = str.distance(from: i, to: str.index(before: str.endIndex))
        print(index)
        if index == n {
            print("\(c) znajduje się na \(n). pozycji od końca")
        }
    }
}

func zad2() {
    print("Podaj dwa ciągi: ")
    let s1 = readLine()!
    let s2 = readLine()!

    if s1 == s2 {
        print("Podane ciągi są identyczne")
    }

    print("Podaj prefiks: ")
    let pref = readLine()!

    print("Podaj sufiks: ")
    let suf = readLine()!

    if s1.hasPrefix(pref) {
        print("\(pref) występuje w \(s1)")
    }
    if s2.hasPrefix(pref) {
        print("\(pref) występuje w \(s2)")
    }

    if s1.hasSuffix(suf) {
        print("\(suf) występuje w \(s1)")
    }
    if s2.hasSuffix(suf) {
        print("\(suf) występuje w \(s2)")
    }
}

func zad3() {
    print("Podaj rok: ")
    if let yearStr = readLine() {
        if let year = Int(yearStr) {
            if year < 0 {
                print("Rok nie może być mniejszy od 0")
                return
            }

            print(year)

            if (year % 400 == 0) || (year % 100 != 0) &&  (year % 4 == 0) {
                print ("Podany rok jest przestępny")
            }
            else {
                print("Podany rok nie jest przestępny")
            }
        }
        else {
            print("Błąd konwersji")
        }
    }
    else {
        print("Błąd wczytywania")
    }
}

func calculateCentury(forYear year: Int) -> Int {
    if year <= 0 {
        return 0
    }
    
    var century = year / 100
    
    if year % 100 != 0 {
        century += 1
    }
    
    return century
}

func zad4() {
    print("Podaj rok z przedziału <2, 3010>:")
    let input = readLine()!

    if let value = Int(input) {
        if value < 2 || value > 3010 {
            print("Wartość spoza zakresu")
            return
        }

        print("Rok \(value) to \(calculateCentury(forYear: value)). wiek")
    }
    else {
        print("Błąd konwersji")
    }
}

func calculateGrantValue(forAverage average: Float) -> Int {
    if average > 4.5 {
        return 200
    } else if average >= 4.0 {
        return 150
    } else if average >= 3.0 {
        return 100
    } else {
        return 0
    }
}


func zad5() {
    print("Podaj średnią: ")
    let input = readLine()!

    if let avg = Float(input) {
        if (avg < 2.0 || avg > 5.0) {
            print("Niepoprawna wartość")
            return
        }

        print("Wysokość stypdendium: \(calculateGrantValue(forAverage: avg))")
    }
    else {
        print("Błąd konwersji")
    }
}

func dodawanie(_ a: Int, _ b: Int) -> Int {
    return a + b
}

func odejmowanie(_ a: Int, _ b: Int) -> Int {
    return a - b
}

func mnozenie(_ a: Int, _ b: Int) -> Int {
    return a * b
}

func dzielenie(_ a: Int, _ b: Int) -> String {
    if b == 0 {
        return "Nie można dzielić przez 0."
    }
    return "Wynik dzielenia: \(a / b)"
}

func zad6() {
    print("Podaj dwie liczby całkowite:")
    if let liczba1 = Int(readLine() ?? ""), let liczba2 = Int(readLine() ?? "") {
        print("Wybierz operację:")
        print("1 - Dodawanie")
        print("2 - Odejmowanie")
        print("3 - Mnożenie")
        print("4 - Dzielenie")
        
        if let wybor = Int(readLine() ?? "") {
            switch wybor {
            case 1:
                let wynik = dodawanie(liczba1, liczba2)
                print("Wynik dodawania: \(wynik)")
            case 2:
                let wynik = odejmowanie(liczba1, liczba2)
                print("Wynik odejmowania: \(wynik)")
            case 3:
                let wynik = mnozenie(liczba1, liczba2)
                print("Wynik mnożenia: \(wynik)")
            case 4:
                let wynik = dzielenie(liczba1, liczba2)
                print(wynik)
            default:
                print("Nieprawidłowy wybór.")
            }
        } else {
            print("Nieprawidłowy wybór operacji.")
        }
    } else {
        print("Podane dane nie są liczbami całkowitymi.")
    }
}

func zad7() {
    print("Podaj znak:")
    if let input = readLine(), let character = input.first?.lowercased() {
        switch character {
        case "a", "e", "i", "o", "u":
            print("To jest samogłoska.")
        case "b"..."z":
            print("To jest spółgłoska.")
        case "0"..."9":
            print("To jest cyfra.")
        default:
            print("To jest inny znak.")
        }
    } else {
        print("Nie podano znaku.")
    }
}

zad7()