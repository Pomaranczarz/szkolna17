import Foundation

func zad1() {
    var tab: [Int] = Array(repeating: 1, count: 12)
    for i in tab {
        print(i)
    }
}

func zad2() {
    var tab: [Int] = Array(repeating: 0, count: 10)
    for i in 0...9 {
        tab[i] = Int.random(in: 1...100)
    }

    print("Tablica na początku: \(tab)")

    while true {
        print("Enter number: ")
        guard let n = Int(readLine()!) else {
            continue
        }

        tab[0] = n;
        print("Tablica po dodaniu liczby na początek: \(tab)")
        break;
    }

    let randomIndex = Int.random(in: 0...9)
    tab[randomIndex] = tab[0]

    print("Tablica po dodaniu liczby na losowe miejsce: \(tab)")
}

func zad3() {
    print("Podaj rozmiar tablicy")
    guard let size = Int(readLine()!) else {
        print("Niepoprawny rozmiar")
        return
    }

    if size < 1 {
        print("Niepoprawny rozmiar")
        return
    }

    var tab : [Int] = Array(repeating: 0, count: size)

    var i = 0
    while i < size {
        print("Podaj \(i + 1). element: ")
        guard let element = Int(readLine()!) else {
            print("Należy podac liczbę całkowitą!")
            i -= 1
            continue
        }

        tab[i] = element
        i += 1
    }

    print("Podaj liczbę: ")
    while true {
        guard let number = Int(readLine()!) else {
            print("Należy podać liczbę całkowitą")
            continue
        }

        if tab[0] == number {
            print("Podana liczba jest pierwszym elementem tablicy")
        }

        if tab[size - 1] == number {
            print("Podana liczba jest ostatnim elementem tablicy")
        }

        break;
    }
}

func zad4() {
    // celowo zmniejszone z powodu przepełnienia przy mnożeniu
    let size = Int.random(in: 3...20)
    var tab = Array(repeating: 0, count: size)

    for i in 0..<size {
        tab[i] = Int.random(in: 3...100)
    }

    print(tab)

    let sum = Double(tab.reduce(0, +))
    let product = Double(tab.filter{ $0 % 2 == 0 }.reduce(1, *))
    let inverse_sum = tab.map{ 1.0 / Double($0) }.reduce(1, +)

    let arithmetic_mean = sum / Double(size)
    let geometric_mean = pow(product, 1.0 / Double(size))
    let harmonic_mean = Double(size) / inverse_sum

    print("Średnia arytmetyczna: \(arithmetic_mean)")
    print("Średnia geometryczna: \(geometric_mean)")
    print("Średnia harmoniczna: \(harmonic_mean)")
}

func zad5() {
    var size = 0
    while true {
        guard let n = Int(readLine()!) else {
            print("Należy podać dodatnią liczbę całkowitą")
            continue
        }

        if n < 1 {
            print("Należy podać dodatnią liczbę całkowitą")
            continue
        }

        size = n
        break
    }

    var tab1 = Array(repeating: 0, count: size)
    var tab2 = Array(repeating: 0, count: size)

    var i = 0
    while i < size {
        tab1[i] = Int.random(in: 1...20)
        tab2[i] = Int.random(in: 1...20)

        i += 1
    }

    print("tab1 = \(tab1)")
    print("tab2 = \(tab2)")

    tab1 = tab1.sorted()
    tab2 = tab2.sorted()

    print("Czy są takie same? - \(tab1 == tab2)")
}

func zad6() {
    var size = 0
    while true {
        guard let n = Int(readLine()!) else {
            print("Należy podać dodatnią liczbę całkowitą")
            continue
        }

        if n < 1 {
            print("Należy podać dodatnią liczbę całkowitą")
            continue
        }

        size = n
        break
    }

    var i = 0
    var tab = Array(repeating: 0, count: size)

    while i < size {
        print("Podaj \(i + 1). element: ")
        guard let number = Int(readLine()!) else {
            print("Należy podać liczbę całkowitą")
            continue
        }

        tab[i] = number
        i += 1
    }

    // find longest ascending series
    var longest = 1
    var current = 1
    var start = 0
    
    for i in 1..<size {
        if tab[i] > tab[i - 1] {
            current += 1
        } else {
            if current > longest {
                longest = current
                start = i - current
            }

            current = 1
        }
    }

    if current > longest {
        longest = current
        start = size - current
    }

    print("Najdłuższy ciąg rosnący: \(longest)")
    print("Od \(start + 1) do \(start + longest)")
}

zad6()