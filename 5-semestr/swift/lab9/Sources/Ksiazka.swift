class Ksiazka {
    private var autor: Osoba
    private var tytul: String
    private var rok_wydania: Int
    private var liczba_stron: Int

    init(autor: Osoba, tytul: String, rok_wydania: Int, liczba_stron: Int) {
        assert(liczba_stron > 0, "liczba stron musi być > 0")

        self.autor = autor
        self.tytul = tytul
        self.rok_wydania = rok_wydania
        self.liczba_stron = liczba_stron
    }

    func info() {
        print("autor: ", terminator: "")
        autor.info()
        print("ksiazka: tytul: \(tytul), rok wydania: \(rok_wydania), liczba stron: \(liczba_stron)")
    }

    static func <(ksiazka1: Ksiazka, ksiazka2: Ksiazka) -> Bool {
        return ksiazka1.rok_wydania < ksiazka2.rok_wydania
    }

    static func >(ksiazka1: Ksiazka, ksiazka2: Ksiazka) -> Bool {
        return ksiazka1.rok_wydania > ksiazka2.rok_wydania
    }
}