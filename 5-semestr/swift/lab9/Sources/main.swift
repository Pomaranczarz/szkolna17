import Foundation

enum Quadrant: Int, CaseIterable {
    case I = 1, II = 2, III = 3, IV = 4
}

class Point {
    private var x: Double
    private var y: Double

    func get_x() -> Double {
        return self.x
    }

    func get_y() -> Double {
        return self.y
    }

    init() {
        self.x = 0
        self.y = 0
    }

    init(withBothCoords: Double) {
        self.x = withBothCoords
        self.y = withBothCoords
    }

    init(x: Double, y: Double) {
        self.x = x
        self.y = y
    }

    func distance(fromPoint other: Point) -> Double {
        return sqrt(pow(self.x - other.x, 2) + pow(self.y - other.y, 2))
    }

    func quadrant() -> Quadrant {
        if self.x > 0 && self.y > 0 {
            return Quadrant.I
        }
        else if self.x < 0 && self.y > 0 {
            return Quadrant.II
        }
        else if self.x < 0 && self.y < 0 {
            return Quadrant.III
        }
        else {
            return Quadrant.IV
        }
    }

    func toString() -> String {
        return "x: \(self.x), y: \(self.y) {\(quadrant())}"
    }

    static func >(p1: Point, p2: Point) -> Int {
        let p1_quadrant = p1.quadrant().rawValue
        let p2_quadrant = p2.quadrant().rawValue

        if p1_quadrant > p2_quadrant {
            return 2
        }
        else if p1_quadrant == p2_quadrant {
            return 1
        }
        else {
            return 0
        }
    }

    static func <(p1: Point, p2: Point) -> Int {
        switch p1 > p2 {
            case 2: return 0
            case 1: return 1
            case 0: return 2
            default: exit(1)
        }
    }
}

func punktWzgledemKWadratu(p: Point, k: Double) -> Int {
    let x = p.get_x()
    let y = p.get_y()

    let x_abs = abs(x)
    let y_abs = abs(y)

    if x_abs < k && y_abs < k {
        return 1
    }
    else if (x_abs == k && y_abs == k) || (x_abs == k && y_abs < k) || (y_abs == k && x_abs < k) {
        return 0
    }
    else {
        return -1
    }
}

func zad1() {
    let p1 = Point(withBothCoords: 3)
    let p2 = Point(x: 4, y: 2)

    print("p1: " + p1.toString())
    print("p2: " + p2.toString())

    print("p1 > p2: \(p1 > p2)")
    print("p1 < p2: \(p1 < p2)")

    print("Odległość pomiędzy p1 i p2: \(p1.distance(fromPoint: p2))")

    print("p1 leży w \(p1.quadrant()). ćwiartce")
    print("p2 leży w \(p2.quadrant()). ćwiartce")

    let k = 3.0
    let p1_wzgledem_kwadratu = punktWzgledemKWadratu(p: p1, k: k)
    let p2_wzgledem_kwadratu = punktWzgledemKWadratu(p: p2, k: k)

    switch p1_wzgledem_kwadratu {
        case 1: print("p1 leży wewnątrz kwadratu o boku \(k).")
        case 0: print("p1 leży na obwodzie kwadratu o boku \(k).")
        case -1: print("p1 leży poza kwadratem o boku \(k).")
        default: exit(1)
    }

    switch p2_wzgledem_kwadratu {
        case 1: print("p2 leży wewnątrz kwadratu o boku \(k).")
        case 0: print("p2 leży na obwodzie kwadratu o boku \(k).")
        case -1: print("p2 leży poza kwadratem o boku \(k).")
        default: exit(1)
    }
}

func zad2() {
    let os1 = Osoba(imie: "adam", nazwisko: "kowalski", rok_urodzenia: 2000)
    let os2 = Osoba(imie: "jan", nazwisko: "nowak", rok_urodzenia: 2005)

    os1.info()
    os2.info()

    let k1 = Ksiazka(autor: os1, tytul: "kot", rok_wydania: 2000, liczba_stron: 300)
    let k2 = Ksiazka(autor: os2, tytul: "pies", rok_wydania: 2005, liczba_stron: 200)

    k1.info()
    k2.info()

    let books = [
        k1,
        k2,
        Ksiazka(autor: os1, tytul: "ksiazka3", rok_wydania: 2015, liczba_stron: 128),
        Ksiazka(autor: Osoba(imie: "andrzej", nazwisko: "janowski", rok_urodzenia: 1990), tytul: "ksiazka1", rok_wydania: 2003, liczba_stron: 287),
        Ksiazka(autor: os2, tytul: "ksiazka2", rok_wydania: 2005, liczba_stron: 492)
    ]

    print("Książki: ")
    for k in books {
        k.info()
    }

    let sorted_books = books.sorted{ $0 < $1 }

    print("Posortowane książki: ")
    for k in sorted_books {
        k.info()
    }
}

zad2()