class Osoba {
    var imie: String
    var nazwisko: String
    var rok_urodzenia: Int

    init(imie: String, nazwisko: String, rok_urodzenia: Int) {
        assert(rok_urodzenia > 1900, "rok urodzenia musi być > 1900")

        self.imie = imie
        self.nazwisko = nazwisko
        self.rok_urodzenia = rok_urodzenia
    }

    func info() {
        print("imie: \(imie), nazwisko: \(nazwisko), rok urodzenia: \(rok_urodzenia)")
    }
}