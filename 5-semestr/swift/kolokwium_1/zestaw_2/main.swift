import Foundation

protocol FromStringConstruction {
    init?(_ keyValue: String)
}

extension Int: FromStringConstruction{}
extension Double: FromStringConstruction{}

func readValue<T: FromStringConstruction>(_ msg: String = "", constraint: ((T) -> Bool)? = nil) -> T {
    print(msg)

    while true {
        if let value = T(readLine()!) {
            if let constraint = constraint {
                if constraint(value) {
                    return value
                }
                else {
                    print("Podana wartość nie spełnia ograniczeń")
                }
            }
            else {
                return value 
            }
        }

        print("Należy podać wartość typu \(T.self)")
    }
}

func zad1() {
    let p: Double = readValue("Podaj p:")
    let n: Int = readValue("Podaj n:", constraint: { $0 > 3 })
    
    var tab: [Double] = [];
    for i in 0..<n {
        tab.append(readValue("Podaj \(i + 1). element:"))
    }

    var closest = (index: 0, diff: abs(tab[0] - p))
    for i in 1..<tab.count {
        if abs(tab[i] - p) < closest.diff {
            closest = (i, abs(tab[i] - p))
        }
    }

    print("Najblizszy element: \(tab[closest.index]) o indeksie: \(closest.index)")
}

func isPrime(_ num: Int) -> Bool {
    if num < 2 {
        return false
    }

    var i = 2;
    let end = sqrt(Double(num));

    while Double(i) < end {
        if num % i == 0 {
            return false
        }

        i += 1
    }

    return true
}

func printPrimesLessThan(_ number: Int) {
    for i in 2..<number {
        if isPrime(i) {
            print(i)
        }
    }
}

func zad2() {
    let number = Int.random(in: 20...200)

    printPrimesLessThan(number)
}

func zad3() {} // ta sama sytuacja co w zestaw_2