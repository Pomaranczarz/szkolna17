import Foundation   

protocol FromStringConstruction {
    init?(_ keyValue: String)
}

extension Int: FromStringConstruction{}
extension Double: FromStringConstruction{}

func readValue<T: FromStringConstruction>(_ msg: String = "", constraint: ((T) -> Bool)? = nil) -> T {
    print(msg)

    while true {
        if let value = T(readLine()!) {
            if let constraint = constraint {
                if constraint(value) {
                    return value
                }
            }
            else {
                return value 
            }
        }

        print("Należy podać wartość typu \(T.self)")
    }
}

func readPESEL() -> [Int] {
    while true {
        let pesel = readLine()!

        if pesel.count != 11 {
            print("Pesel powinien składać się wyłącznie z 11 cyfr")
            continue
        }

        if !pesel.allSatisfy( { $0.isNumber } ) {
            print("Pesel powinien składać się wyłącznie z cyfr")
            continue
        }

        return pesel.map { Int(String($0))! }
    }
}

func zad1() {
    let pesel = readPESEL()

    let temp = pesel.prefix(10)
    let wages = [ 1, 3, 7, 9 ]
    var i = 0

    let sum = temp.map{ digit in
        var result = digit

        result *= wages[i]
        i = (i + 1) % wages.count

        return result
    }.reduce(0, +)

    let controlDigit = 10 - sum % 10

    if controlDigit == pesel[10] {
        print("Pesel jest poprawny")
    }
    else {
        print("Pesel jest niepoprawny")
    }
}

func getValueAtArray(array: [Int], index: Int) -> Int {
    if index < 0 || index >= array.count {
        return 0
    }
    else {
        return array[index]
    }
}

func movingAverage(array: [Int], window: [Int]) -> [Double] {
    var result: [Double] = []

    for i in 0..<array.count {
        var sum = 0
        var window_index = 0
        for index in (i - 2)...i {
            sum += getValueAtArray(array: array, index: index) * window[window_index]
            window_index += 1
        }
        result.append(Double(sum) / Double(window.count))
    }

    return result
}

func zad2() {
    let n: Int = readValue("Podaj n: ")
    var tab: [Int] = []

    for i in 0..<n {
        tab.append(readValue("Podaj \(i + 1). element: "))
    }

    let okno_size: Int = readValue("Podaj rozmiar okna [3; 7]: ", constraint: { $0 >= 3 && $0 <= 7 })
    var okno: [Int] = []

    for i in 0..<okno_size {
        okno.append(readValue("Podaj \(i + 1). element okna: "))
    }

    let result = movingAverage(array: tab, window: okno)

    print(result)
}

func zad3() {
    let n = Int.random(in: 10...20)

    var tab: [[Int]] = []

    for i in 0..<n {
        tab.append([])
        for j in 0..<n {
            if i == j {
                tab[i].append(Int.random(in: 0...100) < 50 ? 1 : -1)
            }
            else {
                tab[i].append(Int.random(in: -100...100))
            }
        }
    }

    let max = tab.map{ $0.max()! }.max()!
    let second_max = tab.map{ $0.max()! }.filter{ $0 != max }.max()!

    let oddIndexValuesSum = tab.enumerated().filter{ $0.offset % 2 == 1 }.map{ $1 }.reduce([], +).reduce(0, +)
    let evenIndexValuesSum = tab.enumerated().filter{ $0.offset % 2 == 0 }.map{ $1 }.reduce([], +).reduce(0, +)

    print(Double(evenIndexValuesSum) / Double(oddIndexValuesSum))

    let lessThanColRowProduct = tab.enumerated().map{ (row_index, row) in
        let i = row_index
        return row.enumerated().filter{ (index, value) in
            value < i * index
        }
    }.count

    print("Liczba komórek, których wartość jest mniejsza od iloczynu indeksu wiersza i kolumny tej komórki: \(lessThanColRowProduct)")
}

zad3()