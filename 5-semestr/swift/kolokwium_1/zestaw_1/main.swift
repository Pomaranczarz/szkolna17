import Foundation

func readCredtiCardNumber() -> String {
    while true {
        var cardNumber = readLine()!

        if cardNumber.allSatisfy({ $0.isNumber }) {
            while cardNumber.count != 16 {
                cardNumber = "0" + cardNumber
            }

            return cardNumber
        }

        print("Numer karty powinien składać się wyłącznie z cyfr")
    }
}

func cardNumberToArrayOfDigits(_ number: String) -> [Int] {
    return number.map { Int(String($0))! }
}

func zad1() {
    var cardNumber = cardNumberToArrayOfDigits(readCredtiCardNumber())

    var mul = 2;
    cardNumber = cardNumber.map{ x in 
        let res = x * mul
        mul = (mul == 2 ? 1 : 2)
        return res
    }

    let sum = cardNumber.reduce(0, +)

    if sum % 10 == 0 {
        print("Numer karty jest poprawny")
    }
    else {
        print("Numer karty jest niepoprawny")
    }
}

typealias RGB = (Int, Int, Int)
typealias HSV = (Double, Double, Double)

func rgbToHsv(_ color: RGB) -> HSV {
    let R_ = Double(color.0) / 255
    let G_ = Double(color.1) / 255
    let B_ = Double(color.2) / 255

    let Cmax = max(R_, G_, B_)
    let Cmin = min(R_, G_, B_)

    let delta = Cmax - Cmin

    print("Cmax: \(Cmax), Cmin: \(Cmin)")

    let V = Cmax // tak jest zgodnie z faktyczną formułą
    // let V = delta // tak jest zgodnie z formułą z zadania
    let S = Cmax == 0 ? 0 : delta / Cmax
    let H = switch Cmax {
        case R_: 60 * (((G_ - B_) / delta).truncatingRemainder(dividingBy: 6))
        case G_: 60 * ((B_ - R_) / delta + 2)
        case B_: 60 * ((R_ - G_) / delta + 4)
        default: exit(1)
    }

    return HSV(H, S, V)
}

func readRGBValue() -> Int {
    while true {
        if let value = Int(readLine()!) {
            if value >= 0 && value <= 255 {
                return value
            }
        }

        print("Wartość powinna być z zakresu [0; 255]")
    }
}

func zad2() {
    let rgb = (readRGBValue(), readRGBValue(), readRGBValue())

    print("\(rgb) -> HSV: \(rgbToHsv(rgb))")
}

func readPositiveInt(msg: String) -> Int {
    print(msg)

    while true {
        if let value = Int(readLine()!) {
            if value > 0 {
                return value
            }
        }

        print("Wartość powinna być dodatnia")
    }
}

func zad3() { // to zadanie jest piramidalnie △ głupie i zawiera za mało informacji w swojej treści do jego rozwiązania
    let M = readPositiveInt(msg: "Podaj M")
    let P = readPositiveInt(msg: "Podaj P")

    var departaments = Array(repeating: Array(repeating: 0, count: P), count: M)

    for dep_nr in 0..<M {
        for worker_nr in 0..<P {
            departaments[dep_nr][worker_nr] = Int.random(in: 800...2600)
        }
    }

    let min = departaments.map{ $0.min()! }.min()!

    print("Najmniejsza kwota pensji: \(min)")
}

zad3()