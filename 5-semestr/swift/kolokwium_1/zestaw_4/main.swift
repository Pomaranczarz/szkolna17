import Foundation

protocol FromStringConstruction {
    init?(_ keyValue: String)
}

extension Int: FromStringConstruction{}
extension Double: FromStringConstruction{}

func readValue<T: FromStringConstruction>(_ msg: String = "", constraint: ((T) -> Bool)? = nil) -> T {
    print(msg)

    while true {
        if let value = T(readLine()!) {
            if let constraint = constraint {
                if constraint(value) {
                    return value
                }
            }
            else {
                return value 
            }
        }

        print("Należy podać wartość typu \(T.self)")
    }
}

func zad1() {
    let str = readLine()!

    var str_length = 0
    for _ in str {
        str_length += 1
    }

    let charsInRange = str.filter{ $0.isNumber && $0 >= "2" && $0 <= "9" }.count

    var add = true
    let modified = str.map{ c in
        if c.isNumber && c >= "2" && c <= "9" {
            return String(Int(String(c))! - 2)
        }
        else {
            let result = (add ? String(c) : "")
            add = !add

            return result
        }
    }.joined()

    print("Wczytany ciąg: \(str), liczba elementów: \(str_length), ilość cyfr z zakesu [2; 9]: \(charsInRange)")
    print("Końcowy ciąg: \(modified)")
}

func adjustPolynomials(_ W: [Int], _ P: [Int]) -> ([Int], [Int]) {
    var W = W
    var P = P

    let max_len = max(W.count, P.count)

    if W.count < max_len {
        W = W + Array(repeating: 0, count: max_len - W.count)
    }

    if P.count < max_len {
        P = P + Array(repeating: 0, count: max_len - P.count)
    }

    return (W, P)
}

func sumPolynomials(_ W: [Int], _ P: [Int]) -> [Int] {
    if W.count != P.count {
        print("Wielomiany muszą składac się z tej samej ilość wspołczynników: skorzystaj najpierw z funkcji adjustPolynomials")
        exit(1)
    }

    var result = Array(repeating: 0, count: W.count)
    
    for i in 0..<W.count {
        result[i] = W[i] + P[i]
    }

    return result
}   

func printPolynomial(_ W: [Int]) {
    W.enumerated().forEach{
        if $1 != 0 {
            print("\($1)x^\($0)", terminator: "")
            if $0 != W.count - 1 {
                print(" + ", terminator: "")
            }
        }
    }

    print();
}

func zad2() {
    let n: Int = readValue("Podaj n: ")
    let k: Int = readValue("podaj k: ")

    var W = Array(repeating: 0, count: n)
    var P = Array(repeating: 0, count: k)

    for i in 0..<n {
        W[n - 1 - i] = readValue("Podaj wartość współczynnika przy potędze \(n - 1 - i): ")
    }

    for i in 0..<k {
        P[k - 1 - i] = readValue("Podaj wartość współczynnika przy potędze \(k - 1 - i): ")
    }

    printPolynomial(W)
    printPolynomial(P)

    (W, P) = adjustPolynomials(W, P)

    print("Suma: ")
    printPolynomial(sumPolynomials(W, P))
}


func zad3() {
    let n: Int = readValue("Podaj n: ")
    var d: [Int] = []

    for i in 0...n {
        d.append(readValue("Podaj \(i + 1). element: "))
    }

    var j = 0
    

    while j < n {
        var p_min = j
        var i = j + 1

        while i <= n {
            if d[i] < d[p_min] {
                p_min = i
            }

            i += 1
        }

        d.swapAt(p_min, j)
        j += 1
    }

    print(d)
}

zad3()