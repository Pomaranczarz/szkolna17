import Foundation

protocol FromStringConstruction {
    init?(_ keyValue: String)
}

extension Int: FromStringConstruction{}
extension Double: FromStringConstruction{}

func readValue<T: FromStringConstruction>(_ msg: String = "", constraint: ((T) -> Bool)? = nil) -> T {
    print(msg)

    while true {
        if let value = T(readLine()!) {
            if let constraint = constraint {
                if constraint(value) {
                    return value
                }
            }
            else {
                return value 
            }
        }

        print("Należy podać wartość typu \(T.self)")
    }
}

func getNthSeriesElement(_ n: Int) -> Int {
    if n < 1 {
        print("Liczba musi być >= 1")
        exit(1)
    }

    return switch n {
        case 1: 1
        case 2: -2
        default: getNthSeriesElement(n - 1) + (n % 2 == 0 ? getNthSeriesElement(n - 2) : 2)
    }
}

func zad1() {
    let n: Int = readValue("Podaj n")

    print("\(n). wyraz ciągu: \(getNthSeriesElement(n))")

    for i in 1...20 {
        print(getNthSeriesElement(i))
    }
}

func readDoubleDigitYear() -> Int {
    while true {
        let year = readLine()! 

        if year.count != 4 {
            print("Rok powinien składać się wyłącznie z 4 cyfr")
            continue
        }

        if year.prefix(2) == year.suffix(2) {
            return Int(year)!
        }

        print("Pierwsze cyfry muszą się powtarzać")
    }
}

func isDoubleDigitYear(_ year: Int) -> Bool {
    return (year / 100) == (year % 100)
}

func zad2() {
    let year1 = readDoubleDigitYear()
    let year2 = readDoubleDigitYear()

    for year in (min(year1, year2) + 1)..<max(year1, year2) {
        if isDoubleDigitYear(year) {
            print(year)
        }
    }
}

func NWD(_ a: Int, _ b: Int) -> Int {
    return b == 0 ? a : NWD(b, a % b)
}

func zad3() {
    let a: Int = readValue("Podaj a:", constraint: { $0 > 0 })
    let b: Int = readValue("Podaj b:", constraint: { $0 > 0 })

    print("NWD(\(a), \(b)) = \(NWD(a, b))")
}

zad3()