import Foundation

func zad1() {
    if let number = Int(readLine()!) {
        var a = 1, b = 1
        if (number > 1) {
            print("\(a)\n\(b) ")
        }
        var c = a + b
        
        while (c <= number) {
            print("\(c) ")
            a = b
            b = c
            
            c = a + b
        }
        
    }
    else  {
        print("Złe dane!")
    }
}

func zad2() {
    if var number = Int(readLine()!) {
        number = abs(number)
        
        if (number < 2) {
            print("Podana liczba nie jest pierwsza")
            return;
        }
        
        let num_sqrt = sqrt(Double(number))
        var i = 2;
        while Double(i) < num_sqrt {
            if (number % i == 0) {
                print("Podana liczba nie jest pierwsza")
                return;
            }
            
            i += 2
        }
        
        print("Podana liczba jest pierwsza")
        
    }
    else  {
        print("Złe dane!")
    }
}

func to_digits(a: Int) -> [Int] {
    var result: [Int] = []
    
    if a == 0 {
        return [0]
    }
    
    var temp = a
    while temp > 0 {
        result.append(temp % 10)
        temp /= 10
    }
    
    return result.reversed();
}

func zad3() {
    if let number = Int(readLine()!) {
        let num = abs(number)
        
        if (num < 100) {
            print("Liczba musi być co najmniej 3 - cyfrowa")
            return
        }
        
        let digits = to_digits(a: num)
        let product = Double(digits.reduce(1, *))
        
        print("Średnia geometryczna cyfr: \(pow(product, 1.0 / Double(digits.count)))")
    }
    else {
        print("Złe dane")
    }
}

func is_palindrome(str: String) -> Bool {
    let unicased = str.lowercased()
    let trimmed = unicased.filter{ !$0.isWhitespace }
    let reversed = trimmed.reversed()
    
    for (c1, c2) in zip(trimmed, reversed) {
        if c1 != c2 {
            return false
        }
    }
    
    return true;
}

func zad4() {
    let string = readLine()!
    
    print("Czy palindrom? - \(is_palindrome(str: string))")
}

func zad5() {
    var numbers: [Int] = []
    
    while let number = Int(readLine()!) {
        if number != numbers.last {
            numbers.append(number)
        }
        else {
             print("Dwie kolejne liczby nie mogą być takie same")
        }
    }
    
    var result: (max: Int, min: Int) = (0, 0)
    var i = 1
    while i + 1 < numbers.count {
        let window: (prev: Int, mid: Int, nxt: Int) = (numbers[i - 1], numbers[i], numbers[i + 1]);
        
        if (window.mid > window.prev && window.mid > window.nxt) {
            result.max += 1
        }
        if (window.mid < window.prev && window.mid < window.nxt) {
            result.min += 1
        }
        
        print("\(window): \(result)")

        i += 1
    }
}

zad5()
