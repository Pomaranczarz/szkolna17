import Foundation

typealias osoba = (imie: String, nazwisko: String, rok_urodzenia: Int)

func compare(_ os1: osoba, _ os2: osoba) {
    if (os1.rok_urodzenia == os2.rok_urodzenia) {
        print(os1, os2)
    }
    else if (os1.rok_urodzenia > os2.rok_urodzenia) {
        print("starsza osoba: \(os1)")
        print("mołodsza osoba: \(os2)")
    }
    else {
        print("starsza osoba: \(os2)")
        print("młodsza osoba: \(os1)")
    }
}

func zad1() {
    let os1 = osoba(imie: "adam", nazwisko: "kowalski", rok_urodzenia: 2000)
    let os2 = osoba(imie: "jan", nazwisko: "nowak", rok_urodzenia: 2005)

    compare(os1, os2)

}

enum Ocena: Double, CaseIterable {
    case Dwa = 2.0, DwaIPol = 2.5, Trzy = 3.0, TrzyIPol = 3.5, Cztery = 4.0, CzteryIPol = 4.5, Piec = 5.0
}

typealias student = (nazwisko: String, ocena1: Ocena, ocena2: Ocena, ocena3: Ocena)

func srednia(_ student: student) -> (nazwisko: String, srednia: Double) {
    let sum = student.ocena1.rawValue + student.ocena2.rawValue + student.ocena3.rawValue
    
    return (student.nazwisko, sum / 3)
}

protocol FromStringConstucion {
    init?(_ keyValue: String)
}

extension Double: FromStringConstucion {
}

extension String: FromStringConstucion {
}

extension Int: FromStringConstucion {
}

func ocena_from_value(_ value: Double) -> Ocena {
    let rawValues = Ocena.allCases.map{ $0.rawValue }
    
    if rawValues.contains(value) {
        return Ocena.init(rawValue: value)!
    }
    else {
        print("Nie ma oceny o wartości \(value)")
        exit(1)
    }
}

func readValue<T: FromStringConstucion>(_ msg: String) -> T {
    print(msg, terminator: " ")

    while true {
        guard let input = T(readLine()!) else {
            print("Niepoprawna wartość")
            continue
        }

        return input
    }
}

func zad2() {
    var studenci: [student] = []

    for _ in 0...2 {
        var student: student
        
        student.nazwisko = readValue("Podaj imię: ")
        student.ocena1 = ocena_from_value(readValue("Podaj 1. ocenę:"))
        student.ocena2 = ocena_from_value(readValue("Podaj 2. ocenę:"))
        student.ocena3 = ocena_from_value(readValue("Podaj 3. ocenę:"))
        
        studenci.append(student)
    }
    
    let studenci_srednia = studenci.map{ srednia($0) }.sorted{ $0.srednia > $1.srednia }
    
    print(studenci_srednia)
}

enum Miesiac: Int, CaseIterable {
    case Styczen = 1, Luty, Marzec, Kwiecien, Maj, Czerwiec, Lipiec, Sierpien, Wrzesien, Pazdziernik, Listopad, Grudzien
}

func pora_roku(_ miesiac: Miesiac) -> String {
    switch miesiac.rawValue {
        case 3...5: return "Wiosna"
        case 6...8: return "Lato"
        case 9...11: return "Jesień"
        default: return "Zima"
    }
}

func miesiac_from_value(_ value: Int) -> Miesiac {
    let rawValues = Miesiac.allCases.map{ $0.rawValue }
    
    if rawValues.contains(value) {
        return Miesiac.init(rawValue: value)!
    }
    else {
        print("Nie ma miesiąca o numerze \(value)")
        exit(1)
    }
}

func zad3() {
    let msc = miesiac_from_value(readValue("Podaj numer miesiąca"))
    
    print("\(msc) to \(pora_roku(msc))")
}

enum Standard: Int, CaseIterable {
    case Wysoki = 3, Sredni = 2, Niski = 1
}

func standard_from_value(_ value: String) -> Standard {
    switch value {
        case "Wysoki": return Standard.Wysoki
        case "Sredni": return Standard.Sredni
        case "Niski": return Standard.Niski
    default:
        print("Niepoprawny standard")
        exit(1)
    }
}

typealias mieszkanie = (lokalizacja: String, powierzchnia: Double, cena_za_metr: Double, standard: Standard)

func get_price(_ mieszkanie: mieszkanie) -> Double {
    return mieszkanie.powierzchnia * mieszkanie.cena_za_metr
}

func zad4() {
    var liczba_mieszkan: Int
    
    repeat {
        liczba_mieszkan = readValue("Podaj liczbę mieszkań")
    } while liczba_mieszkan < 1
    
    var mieszkania: [mieszkanie] = []
    for _ in 0..<liczba_mieszkan {
        var mieszk: mieszkanie
        
        mieszk.lokalizacja = readValue("Podaj lokalizację: ")
        mieszk.powierzchnia = readValue("Podaj powierzchnię: ")
        mieszk.cena_za_metr = readValue("Podaj cenę za metr: ")
        mieszk.standard = standard_from_value(readValue("Podaj standard [Wysoki/Sredni/Niski]: "))
        
        mieszkania.append(mieszk)
    }
    
    let sorted_by_price = mieszkania.sorted{ get_price($0) < get_price($1) }
    
    let max = sorted_by_price.first!
    let min = sorted_by_price.last!
    
    print("Najdrosze: \(max) [\(get_price(max))]")
    print("Najtańsze: \(min) [\(get_price(min))]")
    
    let sorted_by_standard = mieszkania.sorted{ $0.standard.rawValue > $1.standard.rawValue }
    
    sorted_by_standard.forEach{
        print($0)
    }
}

zad4()
