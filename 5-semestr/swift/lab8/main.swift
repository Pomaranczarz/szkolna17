import Foundation

protocol FromStringConstruction {
    init?(_ keyValue: String)
}

extension Int: FromStringConstruction {}
extension Double: FromStringConstruction {}
extension String: FromStringConstruction {}

func readValue<T: FromStringConstruction>(_ msg: String, constraint: (T) -> Bool = { _ in true }) -> T {
    print(msg)

    while true {
        if let value = T(readLine()!) {
            if constraint(value) {
                return value
            }
            else {
                print("Nie spełniono ograniczenia")
            }
        }
        else {
            print("Należy podać wartość typu \(T.self)")
        }
    }
}

struct LiczbaZespolona {
    var re: Double
    var im: Double

    init(_ re: Double, _ im: Double) {
        self.re = re
        self.im = im
    }

    init?(_ keyValue: String) {
        var components = keyValue.components(separatedBy: "+")
        components = components.map{ $0.trimmingCharacters(in: CharacterSet.whitespaces) }

        if components.count == 1 {
            if components[0].hasSuffix("i") {
                let im = Double(components[0].replacingOccurrences(of: "i", with: ""))

                if let im = im {
                    self.im = im
                    self.re = 0
                }
                else {
                    return nil
                }
            }
            else {
                let re = Double(components[0])
                
                if let re = re {
                    self.re = re
                    self.im = 0
                } 
                else {
                    return nil
                }
            }
        }
        else if components.count == 2 {
            if let imaginaris = components.enumerated().first(where: { $0.element.hasSuffix("i") }) {
                let im = Double(imaginaris.element.replacingOccurrences(of: "i", with: ""))
                let re = Double(components[0])

                if let re = re, let im = im {
                    self.re = re
                    self.im = im
                }
                else {
                    return nil
                }
            }
            else {
                let r1 = Double(components[0])
                let r2 = Double(components[1])

                if let r1 = r1, let r2 = r2 {
                    self.re = r1 + r2
                    self.im = 0
                }
                else {
                    return nil
                }
            }
        }
        else {
            return nil
        }
    }

    static func +(lhs: LiczbaZespolona, rhs: LiczbaZespolona) -> LiczbaZespolona {
        return LiczbaZespolona(lhs.re + rhs.re, lhs.im + rhs.im)
    }

    static func -(lhs: LiczbaZespolona, rhs: LiczbaZespolona) -> LiczbaZespolona {
        return LiczbaZespolona(lhs.re - rhs.re, lhs.im - rhs.im)
    }

    static func *(lhs: LiczbaZespolona, rhs: LiczbaZespolona) -> LiczbaZespolona {
        return LiczbaZespolona(lhs.re * rhs.re - lhs.im * rhs.im, lhs.re * rhs.im + lhs.im * rhs.re)
    }

    static func /(lhs: LiczbaZespolona, rhs: LiczbaZespolona) -> LiczbaZespolona {
        return LiczbaZespolona(
            (lhs.re * rhs.re + lhs.im * rhs.im) / (pow(rhs.re, 2) + pow(rhs.im, 2)), 
            (lhs.im * rhs.re - lhs.re * rhs.im) / (pow(rhs.re, 2) + pow(rhs.im, 2))
        )
    }
}

extension LiczbaZespolona: FromStringConstruction {}

extension String {
    init(_ zespolona: LiczbaZespolona) {
        self.init("\(zespolona.re) + \(zespolona.im)i")
    }
}

func zad1() {
    let operations: [String: (LiczbaZespolona, LiczbaZespolona) -> LiczbaZespolona] = [
        "+": { $0 + $1 },
        "-": { $0 - $1 },
        "*": { $0 * $1 },
        "/": { $0 / $1 }
    ]

    while true {
        let z1: LiczbaZespolona = readValue("Podaj 1. liczbę zespoloną: ")
        let z2: LiczbaZespolona = readValue("Podaj 2. liczbę zespoloną: ")

        let operation = readValue("Podaj operację: ", constraint: { operations.keys.contains($0) })

        let result = operations[operation]!(z1, z2)
        print("Wynik: \(String(result))")
    }
}

struct RzutOszczepem {
    var id: Int
    var rzuty: [Double]

    init(id: Int, rzuty: [Double]) {
        self.id = id
        self.rzuty = rzuty
    }

    init?(_ keyValue: String) {
        let components = keyValue.split(separator: " ")

        if components.count != 4 {
            return nil
        }

        if let id = Int(components[0]) {
            self.id = id

            var rzuty: [Double] = []
            for i in 0..<3 {
                if let rzut = Double(components[i + 1]) {
                    rzuty.append(rzut)
                }
                else {
                    return nil
                }
            }

            self.rzuty = rzuty
        }
        else {
            return nil
        }
    }

    func sredniaRzutow() -> Double {
        return rzuty.reduce(0, +) / Double(rzuty.count)
    }
}

extension String {
    init(_ keyValue: RzutOszczepem) {
        self.init("\(keyValue.id): \(keyValue.rzuty)")
    }
}

extension RzutOszczepem: FromStringConstruction {}

func zad2() {
    var rzuty: [RzutOszczepem] = []

    for i in 0..<4 {
        let rzut: RzutOszczepem = readValue("Podaj dane rzutu nr. \(i + 1) [id, rzut1, rzut2, rzut3]: ")
        rzuty.append(rzut)
    }

    let avgs = rzuty.map{ $0.sredniaRzutow() }
    let max = avgs.max()!
    let min = avgs.min()!

    print("Rzut o najwyzszej sredniej: \(max)")

    rzuty.removeAll(where: { $0.sredniaRzutow() == min })

    let rzuty_str = rzuty.map{ String($0) }.joined(separator: ", ")

    print("Rzuty po usunięciu tych o najmniejszej średniej: \(rzuty_str)")
}

func zad3() {
    var samochody: Set<String> = Set([
        "Suzuki Swift",
        "Polonez FSO",
        "Toyota Camry",
        "Łada Samara",
        "Volkswagen Passat",
        "Audi A6"
    ])

    for samochod in samochody {
        print(samochod)
    }

    let model: String = readValue("Podaj model samochodu: ")

    if let deleted = samochody.remove(model) {
        print("Usunieto: \(deleted)")
    }
    else {
        print("Nie znaleziono samochodu")
    }
}

func zad4() {
    var studenci: [Int: String] = [:]

    for _ in 0..<5 {
        let id: Int = readValue("Podaj ID: ", constraint: { $0 > 0 })
        let imie: String = readValue("Podaj nazwisko: ")

        studenci[id] = imie
    }

    for (id, nazwisko) in studenci {
        print("\(id): \(nazwisko)")
    }

    let id: Int = readValue("Podaj ID: ", constraint: { $0 > 0 })
    if let nazwisko = studenci[id] {
        print("\(id): \(nazwisko)")
    } 
    else {
        print("Nie znaleziono studenta o podanym ID")
    }

    let id2: Int = readValue("Podaj ID: ", constraint: { $0 > 0 })
    if let nazwisko = studenci.removeValue(forKey: Int(id2)) {
        print("Usunieto: \(nazwisko)")
    }
    else {
        print("Nie znaleziono studenta o podanym ID")
    }
}

struct Lot {
    var miejsceWylotu: [Int: String]
    var miejsceDocelowe: [Int: String]
    var czasPodrozy: Int

    init(miejsceWylotu: [Int: String], miejsceDocelowe: [Int: String], czasPodrozy: Int) {
        self.miejsceWylotu = miejsceWylotu
        self.miejsceDocelowe = miejsceDocelowe
        self.czasPodrozy = czasPodrozy
    }
}

extension String {
    init(_ keyValue: Lot) {
        self.init("Miejsce wylotu: \(keyValue.miejsceWylotu)\nMiejsce docelowe: \(keyValue.miejsceDocelowe)\nCzas podrozy: \(keyValue.czasPodrozy)")
    }
}

func zad5() {
    let n: Int = readValue("Podaj liczbę lotów: ", constraint: { $0 > 0 })
    var loty: [Lot] = []

    for _ in 0..<n {
        var miejsceWylotu: [Int: String] = [:]
        var miejsceDocelowe: [Int: String] = [:]
        var czasPodrozy: Int = 0

        print("Podaj miejsca wylotu [nr_lotniska1 nazwa1 nr_lotniska2 nazwa2 ...]: ");
        let miejscaWylotu = readLine()!.split(separator: " ")
        if miejscaWylotu.count < 2 || miejscaWylotu.count % 2 != 0 {
            print("Niepoprawne dane")
            exit(1)
        }

        for i in stride(from: 0, to: miejscaWylotu.count, by: 2) {
            miejsceWylotu[Int(miejscaWylotu[i])!] = String(miejscaWylotu[i + 1])
        }

        print("Podaj miejsca docelowe [nr_lotniska1 nazwa1 nr_lotniska2 nazwa2 ...]: ");
        let miejscaDocelowe = readLine()!.split(separator: " ")
        if miejscaDocelowe.count < 2 || miejscaDocelowe.count % 2 != 0 {
            print("Niepoprawne dane")
            exit(1)
        }

        for i in stride(from: 0, to: miejscaDocelowe.count, by: 2) {
            miejsceDocelowe[Int(miejscaDocelowe[i])!] = String(miejscaDocelowe[i + 1])
        }

        czasPodrozy = readValue("Podaj czas podrozy: ", constraint: { $0 > 0 })

        loty.append(Lot(miejsceWylotu: miejsceWylotu, miejsceDocelowe: miejsceDocelowe, czasPodrozy: czasPodrozy))
    }

    for lot in loty {
        print(String(lot))
    }

    let avg = Double(loty.map{ $0.czasPodrozy }.reduce(0, +)) / Double(loty.count)

    loty = loty.filter{ Double($0.czasPodrozy) > avg }  

    print("Loty, które trwają dłużej niż średnia: ")
    for lot in loty {
        print(String(lot))
    }
}

zad5()