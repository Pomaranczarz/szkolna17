import Foundation

func readSize(_ msg: String) -> Int {
    print(msg, terminator: " ")
    while true {
        guard let size = Int(readLine()!) else {
            print("Należy podać dodatnią liczbę całkowitą")
            continue
        }

        if size < 1 {
            print("Należy podać dodatnią liczbę całkowitą")
            continue
        }

        return size
    }
}

func zad1() {
    let width = readSize("Podaj szerokość: ")
    let height = readSize("Podaj wysokość: ")

    var board = Array(repeating: Array(repeating: 0, count: width), count: height)

    for i in 0..<height {
        for j in 0..<width {
            board[i][j] = Int.random(in: 0...100)
            print("\(board[i][j])".padding(toLength: 4, withPad: " ", startingAt: 0), terminator: " ")
        }

        print()
    }
}

func zad2() {
    let width = readSize("Podaj szerokość: ")
    let height = readSize("Podaj wysokość: ")
    
    var board = Array(repeating: Array(repeating: 0.0, count: width), count: height)

    typealias result = (value: Double, i: Int, j: Int)

    var max: result = (-101.0, 0, 0)
    var min: result = (101.0, 0, 0)

    for i in 0..<height {
        for j in 0..<width {
            board[i][j] = Double.random(in: -100...100)

            if board[i][j] > max.value {
                max = (board[i][j], i, j)
            }

            if board[i][j] < min.value {
                min = (board[i][j], i, j)
            }

            print("\(board[i][j])".padding(toLength: 6, withPad: " ", startingAt: 0), terminator: " ")
        }

        print()
    }

    var userInput = 0.0
    while true {
        guard let input = Double(readLine()!) else {
            print("Niepoprawna wartość")
            continue
        }

        userInput = input
        break
    }

    let occurences = board.reduce(0, { 
        $0 + $1.filter{ 
            abs(userInput - $0) < 0.001 
        }.count 
    })

    print("Max: \(max)")
    print("Min: \(min)")
    print("Ile razy \(userInput) występuje: \(occurences)")
}

protocol FromStringConstucion {
    init?(_ keyValue: String)
}

extension Double: FromStringConstucion {
}

func readValue<T: FromStringConstucion>(_ msg: String) -> T {
    print(msg, terminator: " ")

    while true {
        guard let input = T(readLine()!) else {
            print("Niepoprawna wartość")
            continue
        }

        return input
    }
}

func transpose(_ matrix: [[Double]]) -> [[Double]] {
    let width = matrix.count
    let height = matrix[0].count

    var result = Array(repeating: Array(repeating: 0.0, count: width), count: height)

    for i in 0..<width {
        for j in 0..<height {
            result[j][i] = matrix[i][j]
        }
    }

    return result
}

func is_symmetric(matrix: [[Double]]) -> Bool {
    let transposed = transpose(matrix)

    for i in 0..<matrix.count {
        for j in 0..<matrix[0].count {
            if matrix[i][j] != transposed[i][j] {
                return false
            }
        }
    }

    return true
}

func norm(matrix: [[Double]]) -> Double {
    return sqrt(matrix.reduce(0, { $0 + $1.reduce(0, +) }))
}

func row_sum_vec(matrix: [[Double]]) -> [Double] {
    return matrix.map{ $0.reduce(0, +) }
}

func transpose(matrix: [[Double]]) -> [[Double]] {
    var result = [[Double]]()

    for index in 0..<matrix.first!.count {
        result.append(matrix.map{ $0[index] });
    }

    return result
}

func column_max_vec(matrix: [[Double]]) -> [Double] {
    return transpose(matrix).map{ $0.max()! }
}

func is_diagonal(matrix: [[Double]]) -> Bool {
    if matrix.count != matrix[0].count {
        return false
    }
    
    for i in 0..<matrix.count {
        for j in 0..<matrix[0].count {
            if i != j && matrix[i][j] != 0 {
                return false
            }
        }
    }

    return true
}

func has_dominating_diagonal(matrix: [[Double]]) -> Bool {
    if matrix.count != matrix[0].count {
        return false
    }

    let row_sum = row_sum_vec(matrix: matrix)

    for i in 0..<matrix.count {
        if matrix[i][i] <= row_sum[i] - matrix[i][i] {
            return false
        }
    }

    return true
}

func print_2d(matrix: [[Double]]) {
    for i in 0..<matrix.count {
        for j in 0..<matrix[0].count {
            print("\(matrix[i][j]) ", terminator: "")
        }
        print();
    }
}

func zad3() {
    let width = readSize("Podaj szerokość: ")
    let height = readSize("Podaj wysokość: ")
    
    var board = Array(repeating: Array(repeating: 0.0, count: width), count: height)

    typealias result = (value: Double, i: Int, j: Int)

    for i in 0..<height {
        for j in 0..<width {
            board[i][j] = readValue("Podaj wartość [\(i), \(j)]:")
        }
    }

    for i in 0..<height {
        for j in 0..<width {
            print("\(board[i][j])".padding(toLength: 6, withPad: " ", startingAt: 0), terminator: " ")
        }
        print()
    }

    print("Czy symetryczna? - \(is_symmetric(matrix: board))");
    print("pkt. 2: \(row_sum_vec(matrix: board))");
    print("Norma: \(norm(matrix: board))");
    print("pkt. 4: \(column_max_vec(matrix: board))");
    print("Czy diagonalna? - \(is_diagonal(matrix: board))");
    print("Czy dominująca główna przekątna? - \(has_dominating_diagonal(matrix: board))");
    
}

zad3()