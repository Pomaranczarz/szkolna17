import Foundation

func zad1() {
    print("Podaj krawędź sześcianu: ")
    if let a = Double(readLine()!) {
        let pc = 6 * a * a
        let v = a * a * a
        
        print(String(format: "PC: %.2f\tV: %.2f", pc, v))
    }
    else {
        print("Błąd konwersji")
    }
}

func zad2(a: Double, b: Double, z: Double, s: Double, cena: Double) -> Double {
    let pokoj = 2 * a + 2 * b - s
    let korytarz = 2 * z + 2 * b - 2 * s
    
    return (pokoj + korytarz) * cena
}

func zad3() {
    let numbers = [Int.random(in: 1...9), Int.random(in: 1...9), Int.random(in: 1...9)]
    var sum = 0
    for elem in numbers {
        sum += elem
    }
    
    print("Wylosowane liczby: \(numbers)\tŚrednia: \(Float(sum) / Float(numbers.count))")
}

func zad4() {
    print("Podaj 3 razy ocenę i wagę oddzielone spacją")
    
    var oceny: [(ocena: Int, waga: Int)] = [];
    
    for _ in 0...2 {
        let input = readLine()!
        let tokens = input.split(separator: " ")
        oceny.append((Int(tokens[0])!, Int(tokens[1])!))
    }
    
    print("Oceny: \(oceny)")
    
    var suma_ocen = 0
    var suma_wag = 0
    for (ocena, waga) in oceny {
        suma_wag += waga
        suma_ocen += ocena * waga
    }
    
    print("Średnia: \(suma_ocen / suma_wag)")
}

func zad5() {
    let jednoliniowy = "To jest napis jedoliniowy"
    let wieloliniowy = """
                        To jest napis
                        wieloliniowy
                        """

    print("Jednoliniowy: \(jednoliniowy)\nWieloliniowy: \(wieloliniowy)")
}

func zad6() {
    print("""

    "Nauka kodowania to nie tylko nauka języka technologii.

    To odkrywanie nowych sposobów myślenia

    i urzeczywistnianie rozmaitych koncepcji."

    """)
}

func zad7() {
    print("Podaj imię: ")
    let imie = readLine()!
    print("Podaj drugie imię: ")
    let drugie_imie = readLine()!
    print("Podaj nazwisko: ")
    let nazwisko = readLine()!
    print("Podaj rok urodzenia: ")
    let rok_urodzenia = readLine()!

    let polaczone = imie + " " + drugie_imie + " " + nazwisko + " " + rok_urodzenia

    print("Połączone: " + polaczone)

    var bez_drugiego = polaczone
    bez_drugiego.replace(drugie_imie, with: "")
    print("Bez drugiego imienia: " + bez_drugiego);

    var bez_roku = polaczone
    bez_roku.replace(rok_urodzenia, with: "")
    bez_roku += String(2023 - Int(rok_urodzenia)!)
    print("Bez roku z wiekiem: " + bez_roku)

    print("Czy imię zaczyna się literą 'D'? \(imie.starts(with: "D"))")
}

zad4()
