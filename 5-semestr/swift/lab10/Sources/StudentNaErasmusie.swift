import Foundation

class StudentNaErasmusie : Student {
    var nazwa_uczelni: String
    var data_rozpoczecia: Date
    var data_zakonczenia: Date
    var kursy: [String]

    init(nr_indeksu: String, kierunek_studiow: KierunekStudiow, rok_studiow: Int, oceny: [Ocena], nazwa_uczelni: String, data_rozpoczecia: Date, data_zakonczenia: Date, kursy: [String]) {
        precondition(kursy.count == 5, "Niepoprawna ilość kursów")
        precondition(data_rozpoczecia < data_zakonczenia, "Niepoprawny przedział dat")

        self.nazwa_uczelni = nazwa_uczelni
        self.data_rozpoczecia = data_rozpoczecia
        self.data_zakonczenia = data_zakonczenia
        self.kursy = kursy

        super.init(nr_indeksu: nr_indeksu, kierunek_studiow: kierunek_studiow, rok_studiow: rok_studiow, oceny: oceny)
    }

    override func info() {
        super.info()

        print("Nazwa uczelni: \(self.nazwa_uczelni)\nData rozpoczącia: \(self.data_rozpoczecia)\nData zakonczenia: \(self.data_zakonczenia)\nKursy: ", terminator: "")
        for kurs in self.kursy {
            print(kurs, terminator: " ")
        }
        print()
    }

    func czas_erasumusa() -> TimeInterval {
        return self.data_rozpoczecia.distance(to: self.data_zakonczenia)
    }

    func zaliczyl() -> Bool {
        return self.oceny.allSatisfy{ $0.value >= 3.0 }
    }
}