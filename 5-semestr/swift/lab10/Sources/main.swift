import Foundation

func print_pracownik(_ p: Pracownik) {
    p.info()

    print("Pracuje \(p.ile_lat_pracuje()) lat")
    print("Zarobek: \(p.zarobek())")
}

func zad1() {
    let p1 = Pracownik(imie: "Adam", nazwisko: "Kowalski", rok_urodzenia: 1990, rok_zatrudnienia: 2010, stanowisko: .Pracownik, stawka_za_godzine: 20, liczba_godzin: 160, nazwa_firmy: "XYZ")
    let p2 = Pracownik(imie: "Jan", nazwisko: "Nowak", rok_urodzenia: 2000, rok_zatrudnienia: 2007, stanowisko: .Pracownik, stawka_za_godzine: 21.37, liczba_godzin: 420, nazwa_firmy: "ABC")

    print_pracownik(p1)
    print_pracownik(p2)
}

import Foundation

protocol FromStringConstruction {
    init?(_ keyValue: String)
}

extension Int: FromStringConstruction {}
extension Double: FromStringConstruction {}
extension String: FromStringConstruction {}
extension Ocena: FromStringConstruction {}

func readValue<T: FromStringConstruction>(_ msg: String, constraint: (T) -> Bool = { _ in true }) -> T {
    print(msg)

    while true {
        if let value = T(readLine()!) {
            if constraint(value) {
                return value
            }
            else {
                print("Nie spełniono ograniczenia")
            }
        }
        else {
            print("Należy podać wartość typu \(T.self)")
        }
    }
}

func read_student() -> Student {
    let nr_indeksu: String = readValue("Podaj nr. indeksu: ")
    let kierunek_studiow: KierunekStudiow = KierunekStudiow(rawValue: readValue("Podaj kierunek studiów [Informatyka/Ekonomia/Matematyka/Fizyka/Chemia]:"))!
    let rok_studiow: Int = readValue("Podaj rok studiów: ")
    var oceny: [Ocena] = []

    for _ in 0..<5 {
        oceny.append(readValue("Podaj ocenę: "))
    }

    return Student(nr_indeksu: nr_indeksu, kierunek_studiow: kierunek_studiow, rok_studiow: rok_studiow, oceny: oceny)
}

func print_from_kierunek(studenci: [Student], kierunek: KierunekStudiow) {
    studenci.filter{ $0.kierunek_studiow == kierunek }.forEach{ $0.info() }
}

func zad2() {
    let n: Int = readValue("Podaj liczbę studentów: ")
    var studenci: [Student] = []

    for _ in 0..<n {
        studenci.append(read_student())
    }

    let kierunek = KierunekStudiow(rawValue: readValue("Podaj kierunek studiów [Informatyka/Ekonomia/Matematyka/Fizyka/Chemia]:"))!

    print_from_kierunek(studenci: studenci, kierunek: kierunek)
}

func zad3() {
    let s = StudentNaErasmusie(
        nr_indeksu: "1234", 
        kierunek_studiow: KierunekStudiow.Informatyka, 
        rok_studiow: 3, oceny: [Ocena(5.0), Ocena(3.5), Ocena(3), Ocena(3.5), Ocena(2)], 
        nazwa_uczelni: "UCZELNIA INFORMATYCZNA", 
        data_rozpoczecia: Date(timeIntervalSince1970: TimeInterval(0)), 
        data_zakonczenia: Date(), 
        kursy: ["Kurs1", "Kurs2", "Kurs3", "Kurs4", "Kurs5"])

    s.info()

    print("Czas erasumsa: \(s.czas_erasumusa())")
    print("Czy zaliczył?: \(s.zaliczyl())")
}

zad3()