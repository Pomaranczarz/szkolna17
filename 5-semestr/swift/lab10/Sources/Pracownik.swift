enum Stanowisko: CaseIterable {
    case Sprzedawca
    case Kierownik
    case Pracownik
}

class Pracownik : Osoba {
    var rok_zatrudnienia: Int
    var stanowisko: Stanowisko
    var stawka_za_godzine: Double
    var liczba_godzin: Int
    var nazwa_firmy: String

    init(imie: String, nazwisko: String, rok_urodzenia: Int, rok_zatrudnienia: Int, stanowisko: Stanowisko, stawka_za_godzine: Double, liczba_godzin: Int, nazwa_firmy: String) {
        assert(rok_zatrudnienia > 1950, "rok zatrudnienia musi być > 1950")
        assert(stawka_za_godzine > 0, "stawka za godzine musi być > 0")
        assert(liczba_godzin >= 0, "liczba godzin musi być >= 0")

        self.liczba_godzin = liczba_godzin
        self.stawka_za_godzine = stawka_za_godzine
        self.rok_zatrudnienia = rok_zatrudnienia
        self.stanowisko = stanowisko
        self.nazwa_firmy = nazwa_firmy
        
        super.init(imie: imie, nazwisko: nazwisko, rok_urodzenia: rok_urodzenia)
    }

    override func info() {
        super.info()

        print("rok zatrudnienia: \(rok_zatrudnienia), stanowisko: \(stanowisko), stawka za godzine: \(stawka_za_godzine), liczba godzin: \(liczba_godzin), nazwa firmy: \(nazwa_firmy)")
    }

    func ile_lat_pracuje() -> Int {
        return 2023 - rok_zatrudnienia
    }

    func zarobek() -> Double {
        return Double(liczba_godzin) * stawka_za_godzine
    }
}