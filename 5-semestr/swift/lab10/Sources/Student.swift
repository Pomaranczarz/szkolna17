import Foundation

enum KierunekStudiow: String {
    case Informatyka = "Informatyka", Ekonomia = "Ekonomia", Matematyka = "Matematyka", Fizyka = "Fizyka", Chemia = "Chemia"
}

class Ocena {
    private var ocena: Double

    private let valid_oceny = [2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]

    init(_ value: Double) {
        precondition(valid_oceny.contains(value))

        self.ocena = value
    }
    
    required convenience init(_ value: String) {
        self.init(Double(value)!)
    }

    var value: Double {
        get {
            return self.ocena
        }

        set (newValue) {
            precondition(valid_oceny.contains(newValue))

            self.ocena = newValue
        }
    }
}

extension String {
    init(_ value: Ocena) {
        self.init(String(value.value))
    }
}

class Student {
    var nr_indeksu: String
    var kierunek_studiow: KierunekStudiow
    var rok_studiow: Int
    var oceny: [Ocena]

    init(nr_indeksu: String, kierunek_studiow: KierunekStudiow, rok_studiow: Int, oceny: [Ocena]) {
        precondition(oceny.count == 5, "Niepoprawna ilość ocen")
        precondition(rok_studiow >= 1 && rok_studiow <= 5, "Niepoprawny rok studiów")

        self.nr_indeksu = nr_indeksu
        self.kierunek_studiow = kierunek_studiow
        self.rok_studiow = rok_studiow
        self.oceny = oceny
    }

    var srednia: Double {
        get {
            return self.oceny.map{ $0.value }.reduce(0, +) / Double(self.oceny.count)
        }
    }

    func info() {
        print("Indeks: \(self.nr_indeksu)\nKierunek: \(self.kierunek_studiow)\nRok: \(self.rok_studiow)\nOceny: ",  terminator: "")
        for ocena in self.oceny {
            print(String(ocena), terminator: " ")
        }
        print()
    }
}