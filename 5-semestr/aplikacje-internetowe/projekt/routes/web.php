<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// PUBLIC HOMEPAGE ROUTE
Route::view('/', 'welcome');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index');

Route::resource('/products', ProductsController::class);
Route::get('/products-all', [ProductsController::class, 'index_all']);
Route::get('/products-complete', [ProductsController::class, 'index_complete']);
Route::get('/products-incomplete', [ProductsController::class, 'index_incomplete']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
