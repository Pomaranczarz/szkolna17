<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    protected $rules = [
        'name' => 'required|max:60',
        'description' => 'max:155',
        'completed' => 'numeric',
    ];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();

        return view('products.index', [
            'products' => Product::orderBy('created_at', 'asc')->where('user_id', $user->id)->get(),
            'productsInComplete' => Product::orderBy('created_at', 'asc')->where('user_id', $user->id)->where('completed', '0')->get(),
            'productsComplete' => Product::orderBy('created_at', 'asc')->where('user_id', $user->id)->where('completed', '1')->get(),
        ]);
    }

    public function index_all()
    {
        $user = Auth::user();

        return view('products.filtered', [
            'products' => Product::orderBy('created_at', 'asc')->where('user_id', $user->id)->get(),
        ]);
    }

    public function index_complete()
    {
        $user = Auth::user();

        return view('products.filtered', [
            'products' => Product::orderBy('created_at', 'asc')->where('user_id', $user->id)->where('completed', '1')->get(),
        ]);
    }

    public function index_incomplete()
    {
        $user = Auth::user();

        return view('products.filtered', [
            'products' => Product::orderBy('created_at', 'asc')->where('user_id', $user->id)->where('completed', '0')->get(),
        ]);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $user = Auth::user();
        $product = $request->all();
        $product['user_id'] = $user->id;

        Product::create($product);

        return redirect('/products')->with('success', 'Product added');
    }

    public function edit($id)
    {
        $product = Product::query()->findOrFail($id);

        return view('products.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        $this->validate($request, $this->rules);

        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->completed = $request->input('completed') == "1" ? "1" : "0";
        $product->save();

        return redirect('products')->with('success', 'Product updated');
    }


    public function destroy($id)
    {
        Product::findOrFail($id)->delete();

        return redirect('/products')->with('success', 'Product deleted');
    }
}