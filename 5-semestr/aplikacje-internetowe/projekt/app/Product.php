<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'completed', 'user_id'];

    public function user() 
    {
        return $this->belongsTo(User::class);
    }
}