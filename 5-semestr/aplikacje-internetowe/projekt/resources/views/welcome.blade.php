@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    This is WarehouseManager landing page.
                    @guest
                    Please <a href="{{ route('login') }}">login</a> or <a href="{{ route('register') }}"> register </a> to use WarehouseManager.
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
