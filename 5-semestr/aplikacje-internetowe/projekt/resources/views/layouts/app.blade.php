<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'WarehouseManager') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'WarehouseManager') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="navbar-nav">
        <li class="nav-item {{ Request::is('home') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('/home') }}">Home</a>
        </li>
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ms-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/login') }}">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/register') }}">Register</a>
            </li>
        @else
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                    Products <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li class="nav-item {{ Request::is('products') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/products') }}">
                            <i class="fa fa-btn fa-tasks" aria-hidden="true"></i>Products Overview
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('products-all') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/products-all') }}">
                            <i class="fa fa-btn fa-filter" aria-hidden="true"></i>All Products
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('products-incomplete') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/products-incomplete') }}">
                            <i class="fa fa-btn fa-square-o" aria-hidden="true"></i>Incomplete Products
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('products-complete') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/products-complete') }}">
                            <i class="fa fa-btn fa-check-square-o" aria-hidden="true"></i>Complete Products
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('products/create') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/products/create') }}">
                            <i class="fa fa-btn fa-plus" aria-hidden="true"></i>Create Products
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-end" role="menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/logout') }}">
                            <i class="fa fa-btn fa-sign-out" aria-hidden="true"></i>Logout
                        </a>
                    </li>
                </ul>
            </li>
        @endguest
    </ul>
</div>

            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
