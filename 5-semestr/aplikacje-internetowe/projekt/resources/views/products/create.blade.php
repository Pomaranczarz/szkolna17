<x-layouts.app>

    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">

            <!-- Display Validation Errors -->
            @include('common.status')

            <div class="panel panel-default">
                <div class="panel-heading">
                    Create New Product
                </div>
                <div class="panel-body">

                    @include('products.partials.create-product')

                </div>
                <div class="panel-footer">
                    <a href="{{ route('products.index') }}" class="btn btn-sm btn-info" type="button">
                        <span class="fa fa-reply" aria-hidden="true"></span> Back to Products
                    </a>
                </div>
            </div>
        </div>
    </div>

</x-layouts.app>
