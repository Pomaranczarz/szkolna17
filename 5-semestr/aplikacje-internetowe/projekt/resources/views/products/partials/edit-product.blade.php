<form method="POST" action="{{ route('projects.update', $project->slug) }}" class="form-horizontal" role="form">
    @method('PATCH')
    @csrf

    @include('projects.partials._form', ['submit_text' => 'Edit Project', 'submit_icon' => 'pencil'])
</form>
