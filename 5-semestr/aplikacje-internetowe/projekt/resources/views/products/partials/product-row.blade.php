<tr>

    <!-- Product Id -->
    <td class="table-text">
        {{ $product->id }}
    </td>

    <!-- Product Name -->
    <td class="table-text">
        {{ $product->name }}
    </td>

    <!-- Product Description -->
    <td>
        {{ $product->description }}
    </td>

    <!-- Product Status -->
    <td>

        @if ($product->completed === 1)

            <span class="label label-success">
                Complete
            </span>

        @else

            <span class="label label-default">
                Incomplete
            </span>

        @endif

    </td>

    <!-- Product Status Checkbox -->
    <td>
        {{-- Uncomment the code below if you want to use the form for updating the product status --}}
        {{--
        <form method="POST" action="{{ route('products.update', $product->id) }}" class="form-inline" role="form">
            @method('PUT')
            @csrf
            <div class="checkbox">
                <label for="completed">
                    <input type="checkbox" name="completed" id="completed" value="1" {{ $product->completed ? 'checked' : '' }} onChange="this.form.submit()">
                </label>
            </div>
        </form>
        --}}
    </td>

    <!-- Product Edit Icon -->
    <td>
        <a href="{{ route('products.edit', $product->id) }}" class="pull-right">
            <span class="fa fa-pencil fa-fw" aria-hidden="true"></span>
            <span class="sr-only">Edit Product</span>
        </a>
    </td>
</tr>
