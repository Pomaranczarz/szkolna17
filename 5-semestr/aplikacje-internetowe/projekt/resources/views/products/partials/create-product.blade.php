<!-- New Product Form -->
<form method="POST" action="{{ route('products.store') }}" class="form-horizontal" role="form">
    @csrf

    <!-- User ID -->
    <input type="hidden" name="user_id" value="{{ Auth::id() }}">

    <!-- Product Name -->
    <div class="form-group">
        <label for="task-name" class="col-sm-3 control-label">Product name</label>
        <div class="col-sm-6">
            <input type="text" name="name" id="task-name" class="form-control" value="{{ old('name') }}">
        </div>
    </div>

    <!-- Product Description -->
    <div class="form-group">
        <label for="task-description" class="col-sm-3 control-label">Description</label>
        <div class="col-sm-6">
            <textarea name="description" id="task-description" class="form-control" maxlength="155">{{ old('description') }}</textarea>
        </div>
    </div>

    <!-- Add Product Button -->
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <button type="submit" class="btn btn-default">
                <span class="fa fa-plus fa-fw" aria-hidden="true"></span> Create Product
            </button>
        </div>
    </div>
</form>
