@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-1 col-sm-10">

            <!-- Display Validation Errors -->
            @include('common.status')

            <!-- Current Products -->
            @if (count($products) > 0)

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">

                                @if (Request::is('products-all'))
                                    All Products
                                @elseif (Request::is('products-incomplete'))
                                    Incomplete Products
                                @elseif (Request::is('products-complete'))
                                   Complete Products
                                @else
                                    No Products
                                @endif

                                <div class="pull-right">

                                    <a href="{{ url('/products-all') }}" class="btn btn-sm btn-default {{ Request::is('products-all') ? 'active' : '' }}" type="button">
                                        <span class="fa fa-tasks" aria-hidden="true"></span> <span class="hidden-xs">All</span>
                                    </a>
                                    <a href="{{ url('/products-incomplete') }}" class="btn btn-sm btn-default {{ Request::is('products-incomplete') ? 'active' : '' }}" type="button">
                                        <span class="fa fa-square-o" aria-hidden="true"></span> <span class="hidden-xs">Incomplete</span>
                                    </a>
                                    <a href="{{ url('/products-complete') }}" class="btn btn-sm btn-default {{ Request::is('products-complete') ? 'active' : '' }}" type="button">
                                        <span class="fa fa-check-square-o" aria-hidden="true"></span> <span class="hidden-xs">Complete</span>
                                    </a>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped task-table table-condensed">
                                <thead>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th colspan="3">Status</th>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                        @include('products.partials.product-row')
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <a href="{{ url('/products/create') }}" class="btn btn-sm btn-primary pull-right" type="button">
                                    <span class="fa fa-plus" aria-hidden="true"></span> New Product
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

            @else

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Create New Product
                    </div>
                    <div class="panel-body">

                        @include('products.partials.create-product')

                    </div>
                </div>

            @endif

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('table').DataTable();
        } );
    </script>


<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script>

@endsection