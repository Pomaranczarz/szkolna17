@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <!-- Display Validation Errors -->
                @include('common.status')

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Editing Product <strong>{{ $product->name }}</strong>
                    </div>
                    <div class="panel-body">

                        <form method="POST" action="{{ route('products.update', $product) }}">
                            @csrf
                            @method('PUT')


                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-sm-offset-1 control-label text-right">Product Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" id="name" class="form-control" value="{{ $product->name }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-sm-3 col-sm-offset-1 control-label text-right">Product Description</label>
                                <div class="col-sm-6">
                                    <textarea name="description" id="description" class="form-control">{{ $product->description }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="status" class="col-sm-3 col-sm-offset-1 control-label text-right">Status</label>
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label for="status">
                                            <input type="checkbox" name="completed" id="status" {{ $product->completed ? 'checked' : '' }} value="1"> Complete
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-offset-4 col-sm-6">
                                    <button type="submit" class="btn btn-success btn-block">
                                        <span class="fa fa-save fa-fw" aria-hidden="true"></span> <span class="hidden-xxs">Save</span> <span class="hidden-xs">Changes</span>
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('products.index') }}" class="btn btn-sm btn-info" type="button">
                            <span class="fa fa-reply" aria-hidden="true"></span> Back to Products
                        </a>

                        <form action="{{ route('products.destroy', $product->id) }}" method="POST" class="form-inline pull-right">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <span class="fa fa-trash fa-fw" aria-hidden="true"></span> <span class="hidden-xxs">Delete</span> <span class="hidden-sm hidden-xs">Product</span>
                            </button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
