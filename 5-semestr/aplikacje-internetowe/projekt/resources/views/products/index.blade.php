@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-1 col-sm-10">

            <!-- Display Validation Errors -->
            @include('common.status')

            @if (count($products) > 0)

                <div id="content">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                        <li class="active"><a href="#all" data-toggle="tab"><span class="fa fa-tasks" aria-hidden="true"></span> <span class="hidden-xs">All</span></a></li>
                        <li><a href="#incomplete" data-toggle="tab"><span class="fa fa-square-o" aria-hidden="true"></span> <span class="hidden-xs">Incomplete</span></a></li>
                        <li><a href="#complete" data-toggle="tab"><span class="fa fa-check-square-o" aria-hidden="true"></span> <span class="hidden-xs">Complete</span></a></li>
                    </ul>
                    <div id="my-tab-content" class="tab-content">

                    @include('products/partials/product-tab', ['tab' => 'all', 'products' => $products, 'title' => 'All Products', 'status' => 'active'])
                    @include('products/partials/product-tab', ['tab' => 'incomplete', 'products' => $productsInComplete, 'title' => 'Incomplete Products'])
                    @include('products/partials/product-tab', ['tab' => 'complete', 'products' => $productsComplete, 'title' => 'Complete Products'])


                    </div>
                </div>
            @else

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Create New Product
                    </div>
                    <div class="panel-body">

                        @include('products.partials.create-product')

                    </div>
                </div>

            @endif

        </div>
    </div>
@endsection