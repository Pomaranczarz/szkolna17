#include <cmath>
#include <iomanip>
#include <iostream>

double f1(int n) {
	if (n == 0)
		return 0.182;
	else
		return (1.0 / n) - (5 * f1(n - 1));
}

double f2(int n) {
	if (n == 9)
		return 0.017;
	else
		return 0.2 * (1.0 / (n + 1) - f2(n + 1));
}

void zad3() {
	std::cout << "Wzor rekurencyjny 1: \n";
	for (int i = 0; i <= 8; ++i)
		std::cout << i << ". " << std::setprecision(12) << f1(i) << '\n';
	/*
	    0. 0.182
	    1. 0.09
	    2. 0.05
	    3. 0.0833333333333
	    4. -0.166666666667
	    5. 1.03333333333
	    6. -5
	    7. 25.1428571429
	    8. -125.589285714
	*/

	std::cout << "Wzor rekurencyjny 2: \n";
	for (int i = 9; i >= 1; --i)
		std::cout << i << ". " << f2(i) << '\n';
	/*
	    1. 0.0883922162184
	    2. 0.0580389189079
	    3. 0.0431387387937
	    5. 0.0284684698413
	    6. 0.0243243174603
	    7. 0.0212355555556
	    4. 0.0343063060317
	    8. 0.0188222222222
	    9. 0.017
	*/

	// Prawidłowy jest sposób drugi
}

double sum1() {
	double sum = 0;
	for (int i = 100; i <= 10000; ++i)
		if (i % 2 == 0)
			sum += 1.0 / i;
		else
			sum -= 1.0 / i;

	return sum;
}

double sum2() {
	double positive_sum = 0;
	double negative_sum = 0;

	for (int i = 100; i < 10'000; i += 2)
		positive_sum += 1.0 / i;

	for (int i = 100; i < 10'000; i += 2)
		negative_sum += 1.0 / i;

	return positive_sum - negative_sum;
}

void zad4() {
	std::cout << std::setprecision(16) << "S_1 = " << sum1() << '\n';
	std::cout << "S_2 = " << sum2() << '\n';

	/*
	    S_1 = 0.005074996250249899
	    S_2 = 0
	*/
}

std::pair<double, double> szkolna(float p, float q) {
	return {
		p + std::sqrt(p * p - q),
		p - std::sqrt(p * p - q)
	};
}

std::pair<double, double> zmodyfikowana(float p, float q) {
	double x1, x2;
	if (p >= 0) {
		x1 = p + std::sqrt(p * p - q);
		x2 = q / x1;
	}
	else {
		x2 = p - std::sqrt(p * p - q);
		x1 = q / x2;
	}

	return { x1, x2 };
}

void zad5() {
	float p1 = -0.435001, q1 = 0.174 * std::pow(10, -5);
	float p2 = std::pow(10, 5), q2 = 2;

	auto szkolna_wynik_1 = szkolna(p1, q1);
	auto zmodyfikowana_wynik_1 = zmodyfikowana(p1, q1);

	std::cout << std::setprecision(8) << "Metoda szkolna: x1 = " << szkolna_wynik_1.first << " x2 = " << szkolna_wynik_1.second << '\n';
	std::cout << "Metoda zmodyfikowana: x1 = " << zmodyfikowana_wynik_1.first << " x2 = " << zmodyfikowana_wynik_1.second << '\n';

	auto szkolna_wynik_2 = szkolna(p2, q2);
	auto zmodyfikowana_wynik_2 = zmodyfikowana(p2, q2);

	std::cout << "Metoda szkolna: x1 = " << szkolna_wynik_2.first << " x2 = " << szkolna_wynik_2.second << '\n';
	std::cout << "Metoda zmodyfikowana: x1 = " << zmodyfikowana_wynik_2.first << " x2 = " << zmodyfikowana_wynik_2.second << '\n';

	/*
	    p = -0.435001, q = 0.174 * 10^(-5):
	    Metoda szkolna: x1 = -1.9967556e-06 x2 = -0.87
	    Metoda zmodyfikowana: x1 = -2e-06 x2 = -0.87

	    p = 10^5, q = 2:
	    Metoda szkolna: x1 = 200000 x2 = 0
	    Metoda zmodyfikowana: x1 = 200000 x2 = 1e-05
	*/
}

int main() {
	zad3();
	zad4();
	zad5();
}
