def proste(A, b):
    n = len(A)

    assert len(A) == len(b), "Różna liczba rzędów w macierzy A i wektorze b"
    assert len(A) != 0, "Macierz A jest pusta"

    for row in A:
        assert len(row) == len(A), "Macierz A nie jest kwadratowa"

    for i in range(n):
        max_element = abs(A[i][i])
        max_row = i
        for k in range(i + 1, n):
            if abs(A[k][i]) > max_element:
                max_element = abs(A[k][i])
                max_row = k

        if max_row != i:
            A[i], A[max_row] = A[max_row], A[i]
            b[i], b[max_row] = b[max_row], b[i]

        for k in range(i + 1, n):
            c = -A[k][i] / A[i][i]
            for j in range(i, n):
                A[k][j] = 0 if i == j else c * A[i][j]
            b[k] += c * b[i]


    x = [0 for i in range(len(A))]
    for i in range(len(A) - 1, -1, -1):
        x[i] = b[i]
        for k in range(i + 1, len(A)):
            x[i] -= A[i][k] * x[k]
        if (A[i][i] != 0):
            x[i] /= A[i][i]
    return x


def odwrotne(A, b):
    n = len(A)
    for i in range(n - 1, -1, -1):
        for k in range(i - 1, -1, -1):
            if(A[i][i] != 0):
                c = -A[k][i] / A[i][i]
            for j in range(i, n):
                A[k][j] = 0 if i == j else c * A[i][j]

            b[k] += c * b[i]


def get_input():
    n = int(input("Podaj ilosc rownan w ukladzie rownan: "))
    A = [[] for i in range(n)]
    b = []
    for i in range(n):
        wiersz = input(f"Podaj { n + 1 } wyrazow wolnych { i + 1 } rownania (po spacji): ")

        for j in range(len(wiersz.split(" ")) - 1):
            A[i].append(int(wiersz.split(" ")[j]))

        b.append(int(wiersz.split(" ")[-1]))

    return A, b


if __name__ == "__main__":
    A, b = get_input()

    for v in proste(A, b):
        print(v)
