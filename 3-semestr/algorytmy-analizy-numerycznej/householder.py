from math import sqrt


def householder(a):
    length = len(a)
    sum = 0

    for i in a:
        sum += pow(abs(i), 2)
        a2 = sqrt(sum)
        assert (a2 * (a2 + abs(a[0]))) != 0, "Dzielenie przez 0"
        b = 1 / (a2 * (a2 + abs(a[0])))
    print(f'Beta = { b }')

    e = a[0] / abs(a[0])
    a[0] = e * (a2 + abs(a[0]))
    U = [[0 for i in range(length)] for j in range(length)]

    for i in range(length):
        for j in range(length):
            U[i][j] = a[i] * a[j]

    P = [[0 for i in range(length)] for j in range(length)]
    for i in range(length):
        for j in range(length):
            if i == j:
                P[i][j] = 1 

    for i in range(length):
        for j in range(length):
            P[i][j] = P[i][j] - (b * U[i][j])

    for i in range(length):
        print(P[i])

    t = -a2 * e
    Pa = [0 for i in range(length)]
    Pa[0] = t
    print(f'Pa = { Pa }')


def get_input():
    n = int(input("Podaj rozmiar wektora: "))
    a = [0.0 for i in range(n)]
    print(f'Podaj kolejno { n } liczb: ')
    for i in range(n):
        a[i] = float(input())

    return a


if __name__ == "__main__":
    a = get_input()

    householder(a)
