def f(x):
    return x * x - 5


def fprim(x):
    return 2 * x


def bisect(a, b, eps, delta):
    fa = f(a)
    fb = f(b)

    if abs(fa) < eps:
        return a
    if abs(fb) < eps:
        return b

    if fa * fb < 0:
        while True:
            s = (a - b) * 0.5
            fs = f(s)

            if fa * fs < 0:
                b = s
                fb = fs
            else:
                a = s
                fa = fs
            
            if not (abs(fs) > eps and abs(b - a) > delta):
                break

        return 0.5 * (a + b)
    return None


def Newton(x, eps, delta, MAX):
    fx = f(x)
    xk = float()

    if abs(fx) < eps:
        return x
    else:
        iter = 0
        while iter < MAX:
            d = f(x) / fprim(x)
            xk = x - d
            fx = f(xk)
            iter += 1
            x = xk

            if not (abs(fx) > eps and abs(d) > delta):
                break

        return xk


def falsi(s, u, eps, delta):
    fs = f(s)
    fu = f(u)

    if abs(fs) < eps:
        x = s
        return True
    if abs(fu) < eps:
        x = u
        return True
    if fs * fu < 0:
        while True:
            x = s - fs * (u - s) / fu - fs
            fx = f(x)

            if abs(fx) < eps:
                return True
            if fs * fx < 0:
                u = x
                fu = fx
            else:
                s = x
                fs = fx
            
            if not (abs(s - u) > delta):
                break
        return True
    else:
        return False

