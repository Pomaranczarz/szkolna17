def heun(function, x0, y0, h, n):
    data = [0 for i in range(n)]
    data[0] = y0
    x = x0
    
    for i in range(1, n):
        data[i] = data[i - 1] * 0.5 * h * (function(x, data[i - 1]) + function(x + h, data[i - 1] + h * function(x, data[i - 1])))
        x += h

    return data