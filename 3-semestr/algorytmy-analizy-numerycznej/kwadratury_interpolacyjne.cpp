#include <cmath>
#include <iomanip>
#include <iostream>

double f(double x) {
	return std::sqrt(1 - x * x);
}

double trapezy(double a, double b, int m) {
	const double h = (b - a) / m;
	double result = 0.5 * f(a);

	int i = 1;
	for (double x = a + h; i < m; x += h, ++i)
		result += f(x);

	result += f(b);
	result *= h;

	return result;
}

double simpson(double a, double b, int m) {
	if (m % 2 != 0)
		return 0;

	const double h = (b - a) / m;
	const double h_div_3 = h / 3;
	double result = f(a);

	for (int i = 1; i < m; i += 2)
		result += 4 * f(a + i * h);

	for (int i = 2; i < m; i += 2)
		result += 2 * f(a + i * h);

	result += f(b);
	result *= h_div_3;

	return result;
}

double trzyOsme(double a, double b, int m) {
	if (m % 2 != 0)
		return 0;

	const double h = (b - a) / m;
	const double h3_div_8 = (3 * h) / 8;
	double result = f(a);

	for (int i = 3; i < m; i += 3)
		result += 2 * f(a + i * h);

	for (int i = 1; i < m; i += ((i + 1) % 3 == 0 ? 2 : 1))
		result += 3 * f(a + i * h);

	result += f(b);
	result *= h3_div_8;

	return result;
}

int main() {
	double results[4][3]{
		{6, 60, 600}
	};

	for (int i = 0; i < 3; ++i) {
		results[1][i] = trapezy(-1, 1, results[0][i]) * 2;
		results[2][i] = simpson(-1, 1, results[0][i]) * 2;
		results[3][i] = trzyOsme(-1, 1, results[0][i]) * 2;
	}

	std::cout << "m\t\ttrapezy\t\tsimpson\t\ttrzy osme\n"
		    << std::setprecision(6);

	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 4; ++j)
			std::cout << results[j][i] << "\t\t";

		std::cout << '\n';
	}
}
