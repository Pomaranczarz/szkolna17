#include <iostream>
#include <vector>

float lagrange(
    int n, 
    const std::vector<float>& wezly, 
    const std::vector<float>& wartosci, 
    float c
    ) 
{
    float result = 0;
    for (int i = 0; i < n; ++i) {
        float l = 1;
        for (int j = 0; j < n; ++j) {
            if (j == i)
                continue;
                
            l *= (c - wezly[j]) / (wezly[i] - wezly[j]);
        }

        result += wartosci[i] * l;
    }

    return result;
}

bool czy_wezly_poprawne(const std::vector<float>& wezly) {
    for (int i = 0; i < wezly.size(); ++i)
        for (int j = i + 1; j < wezly.size(); ++j)
            if (wezly[i] == wezly[j]) 
                return false;

    return true;
}

void wczytaj_dane(
    int& n, 
    std::vector<float>& wezly, 
    std::vector<float>& wartosci, 
    float& c) 
{
    std::cout << "Podaj n: ";
    std::cin >> n;
    if (n <= 0) {
        std::cout << "Bledne n\n";
        return;
    }

    n += 1; // poniewaz przyjeto (n + 1) za wartosc n

    std::cout << "Podaj kolejne " << n << " wezly: \n";
    for (int i = 0; i < n; ++i) {
        float wezel;
        std::cin >> wezel;
        wezly.push_back(wezel);
    }

    std::cout << "Podaj kolejne " << n << " wartosci: \n";
        for (int i = 0; i < n; ++i) {
        float wartosc;
        std::cin >> wartosc;
        wartosci.push_back(wartosc);
    }

    std::cout << "Podaj wartosc c: \n";
    std::cin >> c;
}

void wyswietl_wynik(int n, const std::vector<float>& wezly, const std::vector<float>& wartosci, float c) {
    std::cout << "p(" << c << ") = " << lagrange(n, wezly, wartosci, c) << '\n';
}

int main() {
    std::vector<float> wezly;
    std::vector<float> wartosci;
    int n;
    float c; 

    wczytaj_dane(n, wezly, wartosci, c);

    if (!czy_wezly_poprawne(wezly)) {
        std::cout << "Podane wezly nie sa poprawne\n";
        return EXIT_FAILURE;
    }

    wyswietl_wynik(n, wezly, wartosci, c);
}