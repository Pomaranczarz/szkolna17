def choleski(a):
    n = len(a)
    sum_ = 0.0
    ml = [ [0] * len(i) for i in a ]

    for s in range(n):
        for i in range(s, n):
            sum_ = 0.0

            for j in range(s):
                sum_ += ml[i][j] * ml[s][j].conjugate()

            ml[i][s] = a[i][s] - sum_

            if ml[s][s] == 0:
                raise ZeroDivisionError

            ml[i][s] = ml[i][s] ** 0.5 if s == i else ml[i][s] / ml[s][s]

    return ml


if __name__ == "__main__":
    a = [
        [4, 2j, 2],
        [-2j, 5, -5j],
        [2, 5j, 6]
    ]

    try:
        ml = choleski(a)
    except ZeroDivisionError:
        print("Dzielenie przez 0!")

    for row in ml:
        for elem in row:
            print(elem, end=' ')

        print()