from math import sin, cos
import matplotlib.pyplot as plt


def silnia(n):
    if n < 0:
        return None

    wynik = 1
    for i in range(1, n + 1):
        wynik *= i

    return wynik


def akx(x):
    n = len(x)
    kx = [ 0.0 ] * n

    for i in range(1, n):
        if x[i] == x[i - 1]:
            kx[i] = kx[i - 1] + 1
        else:
            kx[i] = 0

    return kx


def hermit(x, kx, y):
    n = len(x)
    r = [ 0.0 ] * n
    a = [ 0.0 ] * n

    pom = float()
    indeks = int()

    for i in range(n):
        if kx[i] == 0:
            r[i] = y[i]
            pom = y[i]
            indeks = i
        else:
            r[i] = pom
        
        for k in range(i - 1, -1, -1):
            if x[i] != x[k]:
                r[k] = (r[k + 1] - r[k]) / (x[i] - x[k])
            else:
                r[k] = y[indeks + i - k] / silnia(int(kx[indeks + i - k]))

        a[i] = r[0]

    return a


def horner(x, a, c):
    n = len(a) - 1
    b = a[n]

    for k in range(n - 1, -1, -1):
        b = b * (c - x[k]) + a[k]

    return b


def function(x):
    return sin(x) + cos(x) * cos(x)


x = [ 0.0, 0.5, 1.5, 2.5, 4.0, 5.0 ]
y = [ 1.0, 1.25, 1.001, 1.24, -0.3296, -0.87846 ]

y_0 = []
x_new = [ i / 10 for i in range(0, 51) ]

for c in x_new:
    y_0.append(sin(c) + cos(c)**2)

kx = akx(x)
a = hermit(x, kx, y)

y_1 = []
for c in x_new:
    y_1.append(horner(x, a, c))

xw = [ 0.0, 1.5, 1.5, 1.5, 2.5, 2.5, 2.5, 4.0, 5.0 ]
yw = [ 1.0, 1.001, -0.07, 0.983, 1.24, 0.158, -1.1658, -0.3296, -0.8785 ]

kx = akx(xw)
a = hermit(xw, kx, yw)

y_2 = []
for c in x_new:
    y_2.append(horner(xw, a, c))

plt.figure(figsize = (12, 8))
plt.plot(x, y, 'ko', label="węzły interpolacji")
plt.plot(x_new, y_0, 'b', label="interpolowana funkcja f(x) = sin(x) + cos(x) * cos(x)")
plt.plot(x_new, y_1, 'r', label='wielomian interpolacyjny - węzły jednokrotne')
plt.plot(x_new, y_2, label='wielomian interpolacyjny - węzły wielokrotne')
plt.legend(loc='lower left')

plt.show()