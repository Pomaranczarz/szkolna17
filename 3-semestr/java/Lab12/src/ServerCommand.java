import java.util.ArrayList;
import java.util.Arrays;

enum CommandType {
    CHANGE_NICKNAME
}

public class ServerCommand {
    ServerClient caller;
    CommandType commandType;
    ArrayList<String> commandArguments;

    public ServerCommand(ServerClient caller, CommandType commandType, ArrayList<String> commandArguments) {
        this.caller = caller;
        this.commandType = commandType;
        this.commandArguments = commandArguments;
    }

    public ServerCommand(ServerClient caller, String rawCommand) {
        this.caller = caller;
        this.commandType = getCommandType(rawCommand);
        this.commandArguments = getCommandArguments(rawCommand);
    }

    static CommandType parseCommandType(String command) {
        switch (command) {
            case "!nickname": return CommandType.CHANGE_NICKNAME;
            default: throw new RuntimeException("Given string is not a command");
        }
    }

    static CommandType getCommandType(String rawCommand) {
        return parseCommandType(rawCommand.substring(0, rawCommand.indexOf(' ')));
    }

    static ArrayList<String> getCommandArguments(String rawCommand) {
        return new ArrayList<String>(
                Arrays.asList(rawCommand.substring(rawCommand.indexOf(' ') + 1).split(" "))
        );
    }
}

