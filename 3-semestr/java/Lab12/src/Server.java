import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Server {
    ArrayList<ServerClient> serverClients;
    final int MAX_NICKNAME_LEN = 30;
    final int MAX_MESSAGE_LEN = 200;
    boolean DISPLAY_UNSTRIPPED_MESSAGES = true;

    String stripMessage(String message) {
        return message.substring(0, Math.min(MAX_MESSAGE_LEN, message.length()));
    }

    private boolean canBeCommand(String message) {
        return message.startsWith("!");
    }

    private boolean isValidNickname(String nickname) {
        return  nickname.length() <= MAX_NICKNAME_LEN;
    }

    private void logToServer(String message) {
        if (DISPLAY_UNSTRIPPED_MESSAGES) {
            System.out.println(message);
            message = stripMessage(message);
        }
        else {
            message = stripMessage(message);
            System.out.println(message);
        }
    }

    public void handleCommand(ServerCommand clientServerCommand) {
        var commandType = clientServerCommand.commandType;
        var caller = clientServerCommand.caller;
        var arguments = clientServerCommand.commandArguments;

        switch (commandType) {
            case CHANGE_NICKNAME: caller.setNickname(arguments.get(0)); break;
            default: throw new RuntimeException("Invalid command type");
        }
    }

    private void  

    public void sendToAll(String message) {
        try {
            serverClients.forEach(serverClient -> {
                serverClient.writeToStream(message);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Server().run();
    }

    public void run() {
        serverClients = new ArrayList<>();
        try (ServerSocket serverSocket = new ServerSocket(2020)) {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                ServerClient newServerClient = new ServerClient("Anonim", clientSocket.getInetAddress(), new PrintWriter(clientSocket.getOutputStream()));
                serverClients.add(newServerClient);

                Thread clientThread = new Thread(new ClientHandler(clientSocket, newServerClient));
                clientThread.start();
                System.out.println("Połączono z " + newServerClient);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public class ClientHandler implements Runnable {
        BufferedReader reader;
        Socket socket;
        ServerClient serverClient;

        public ClientHandler(Socket socket, ServerClient serverClient) {
            try {
                this.serverClient = serverClient;
                this.socket = socket;
                InputStreamReader reader = new InputStreamReader(socket.getInputStream());
                this.reader = new BufferedReader(reader);
            } catch (Exception ex) {

                ex.printStackTrace();
            }
        }

        @Override
        public void run() {
            String message;
            try {
                while ((message = reader.readLine()) != null) {
                    logToServer(serverClient + ": " + message);

                    if (canBeCommand(message)) {
                        try {
                            ServerCommand clientServerCommand = new ServerCommand(serverClient, message);
                            handleCommand(clientServerCommand);
                        }
                        catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                    else {
                        sendToAll(serverClient.getNickname() + ": " + message);
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}