import java.io.PrintWriter;
import java.net.InetAddress;

class ServerClient {
    String nickname;
    InetAddress inetAddress;
    PrintWriter outputStream;

    public ServerClient(String nickname, InetAddress inetAddress, PrintWriter outputStream) {
        this.nickname = nickname;
        this.inetAddress = inetAddress;
        this.outputStream = outputStream;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public void writeToStream(String message) {
        outputStream.println(message);
        outputStream.flush();
    }

    @Override
    public String toString() {
        return "{nickname='" + nickname + '\'' +
                ", inetAddress=" + inetAddress + '}';
    }
}
