import java.io.*;

public class Main {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("tekst.txt"))) {
            String line;
            String encryptedLine;
           try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("zaszyfrowany.txt"))) {
               while ((line = bufferedReader.readLine()) != null) {
                   encryptedLine = CaesarEncryptor.encrypt(line, 1);
                   bufferedWriter.write(encryptedLine + "\n");
               }
           }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("zaszyfrowany.txt"))) {
            String encryptedLine;
            String decryptedLine;

            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("odszyfrowany.txt"))) {
                while((encryptedLine = bufferedReader.readLine()) != null) {
                    decryptedLine = CaesarEncryptor.decrypt(encryptedLine, 1);
                    bufferedWriter.write(decryptedLine + "\n");
                }
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}