class CaesarEncryptor {
    private static final int ALPHABET_SIZE = 'z' - 'a' + 1;

    public static String encrypt(String message, int offset) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < message.length(); ++i)
            if (Character.isAlphabetic(message.codePointAt(i)) && message.codePointAt(i) < 128)
                result.append(getEncryptedCharacter(message.charAt(i), offset));
            else
                result.append(message.charAt(i));

        return result.toString();
    }

    public static String decrypt(String message, int offset) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < message.length(); ++i)
            if (Character.isAlphabetic(message.codePointAt(i)) && message.codePointAt(i) < 128)
                result.append(getDecryptedCharacter(message.charAt(i), offset));
            else
                result.append(message.charAt(i));

        return result.toString();
    }

    private static char getEncryptedCharacter(char c, int offset) {
        if (Character.isUpperCase(c))
            return (char)((c - 'A' + offset) % ALPHABET_SIZE + 'A');
        else
            return (char)((c - 'a' + offset) % ALPHABET_SIZE + 'a');

    }

    private static char getDecryptedCharacter(char c, int offset) {
        if (Character.isUpperCase(c))
            return (char)((ALPHABET_SIZE + c - 'A' - offset) % ALPHABET_SIZE + 'A');
        else
            return (char)((ALPHABET_SIZE + c - 'a' - offset) % ALPHABET_SIZE + 'a');
    }
}
