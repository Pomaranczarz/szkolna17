import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static Calculator calculator = new Calculator();
    public static void main(String[] args) {
        // calculatorTest();
        // expAndMantissaDiv();
        // printSumOfSquaredDigitsOfNumberInRange();
    }

    public static int getSumOfSquaredDigits(int number) {
        int sum = 0;
        while (number > 0) {
            sum += Math.pow((number % 10), 2);
            number /= 10;
        }

        return sum;
    }

    public static void printSumOfSquaredDigitsOfNumberInRange() {
        String input = scanner.nextLine();
        try {
            int number = Integer.parseInt(input);
            if (number < 99 || number > 999)
                throw new IllegalArgumentException("Number out of range (<99; 999>)");

            System.out.println(getSumOfSquaredDigits(number));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void expAndMantissaDiv() {
        while (true) {
            System.out.println("Enter number:");
            float number = scanner.nextFloat();

            double exponent = Math.getExponent(number);
            double mantissa = number / Math.pow(2, exponent);

            try {
                System.out.println(calculator.compute('/', exponent, mantissa));
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }

            break;
        }
    }

    public static void calculatorTest() {
        char operation;
        double arg1, arg2;

        while (true) {
            System.out.println("Enter operation (+,-,*,/,^,r for root, e for exit): ");
            String input = scanner.nextLine();
            operation = input.charAt(0);
            if (operation == 'e')
                break;

            try {
                System.out.println("Enter two arguments on separate lines:");
                input = scanner.nextLine();
                arg1 = Double.parseDouble(input);
                input = scanner.nextLine();
                arg2 = Double.parseDouble(input);
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
                continue;
            }

            try {
                System.out.println("Result: " + calculator.compute(operation, arg1, arg2));
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
