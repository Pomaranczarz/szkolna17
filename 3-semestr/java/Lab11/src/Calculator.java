import java.util.HashMap;
import java.util.function.*;

public class Calculator {
    public enum Operation {
        Add, Sub, Div, Mul, Pow, Rad
    }
    private final HashMap<Operation, ToDoubleBiFunction<Double, Double>> operations;

    public Calculator() {
        operations = new HashMap<>();
        operations.put(Operation.Add, (Double x, Double y) -> x + y);
        operations.put(Operation.Sub, (Double x, Double y) -> x - y);
        operations.put(Operation.Mul, (Double x, Double y) -> x * y);
        operations.put(Operation.Div, Calculator::safeDivision);
        operations.put(Operation.Pow, (Double x, Double y) -> Math.pow(x, y));
        operations.put(Operation.Rad, Calculator::safeRoot);
    }

    public Double compute(Character operation, Double arg1, Double arg2) throws IllegalArgumentException {
        return operations.get(
                parseOperation(operation)
        ).applyAsDouble(arg1, arg2);
    }

    private static Double safeDivision(Double dividend, Double divisor) throws IllegalArgumentException {
        if (divisor == 0)
            throw new IllegalArgumentException("Cannot divide by zero");
        else
            return dividend / divisor;
    }

    private static Double safeRoot(Double number, Double rootDegree) throws IllegalArgumentException {
        if (rootDegree % 2 == 0 && number < 0)
            throw new IllegalArgumentException("Cannot take even degree root of negative number");
        else
            return Math.pow(number, 1 / rootDegree);
    }

    private static Operation parseOperation(Character op) throws UnsupportedOperationException {
        return switch (op) {
            case '+' -> Operation.Add;
            case '-' -> Operation.Sub;
            case '*' -> Operation.Mul;
            case '/' -> Operation.Div;
            case '^' -> Operation.Pow;
            case 'r' -> Operation.Rad;
            default -> throw new UnsupportedOperationException("Unsupported operation");
        };
    }
}
