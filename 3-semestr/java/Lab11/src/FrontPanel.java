import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

public class FrontPanel {
    private JButton Button1;
    private JButton Button3;
    private JButton Button2;
    private JButton Button4;
    private JButton Button5;
    private JButton Button6;
    private JButton Button7;
    private JButton Button8;
    private JButton Button9;
    private JButton Button0;
    private JButton AddButton;
    private JButton SubtractButton;
    private JButton MultiplyButton;
    private JButton DivideButton;
    private JButton RootButton;
    private JButton PowerButton;
    private JButton EqualsButton;
    private JTextPane InputArea;
    public JPanel MainPanel;
    private JButton DecimalDot;
    private JButton ClearButton;

    private boolean isNextOperationNew = false;

    Stack<Double> operands;
    char operator;

    private class CalculatorNumberButtonListener implements ActionListener {
        private final char number;
        final FrontPanel frontPanel = FrontPanel.this;
        public CalculatorNumberButtonListener(char number, StringBuilder builder) {
            this.number = number;
            frontPanel.currentInputAreaText = builder;
        }
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (frontPanel.isNextOperationNew) {
                clearInputArea();
                frontPanel.isNextOperationNew = false;
            }

            frontPanel.currentInputAreaText.append(number);
            updateInputArea();
        }
    }

    private class CalculatorOperationButtonListener implements ActionListener {
        final FrontPanel frontPanel = FrontPanel.this;
        private final char op;

        public CalculatorOperationButtonListener(char op, char globalOperator) {
            this.op = op;
            frontPanel.operator = globalOperator;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            frontPanel.operator = op;
            frontPanel.operands.push(getNumberFromInputArea());
            clearInputArea();
        }
    }

    StringBuilder currentInputAreaText = new StringBuilder();

    Calculator calculator = new Calculator();

    private double getNumberFromInputArea() {
        var content = InputArea.getText();
        try {
            return Double.parseDouble(content);
        }
        catch (NumberFormatException e) {
            return 0;
        }
    }

    private void updateInputArea() {
        int MAX_OUTPUT_LEN = 9;
        InputArea.setText(currentInputAreaText.substring(0, Math.min(MAX_OUTPUT_LEN, currentInputAreaText.length())));
    }

    private void clearInputArea() {
        currentInputAreaText.delete(0, currentInputAreaText.length());
        updateInputArea();
    }

    public FrontPanel() {
        SimpleAttributeSet attribs = new SimpleAttributeSet();
        StyleConstants.setAlignment(attribs, StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(attribs, 36);
        InputArea.setParagraphAttributes(attribs, true);
        operands = new Stack<>();

        AddButton.addActionListener(new CalculatorOperationButtonListener('+', operator));
        SubtractButton.addActionListener(new CalculatorOperationButtonListener('-', operator));
        MultiplyButton.addActionListener(new CalculatorOperationButtonListener('*', operator));
        DivideButton.addActionListener(new CalculatorOperationButtonListener('/', operator));
        RootButton.addActionListener(new CalculatorOperationButtonListener('r', operator));
        PowerButton.addActionListener(new CalculatorOperationButtonListener('^', operator));
        EqualsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                operands.push(getNumberFromInputArea());
                clearInputArea();
                double rightOperand = operands.pop();
                double leftOperand = operands.pop();

                currentInputAreaText.append(calculator.compute(operator, leftOperand, rightOperand).toString());
                updateInputArea();
                isNextOperationNew = true;
            }
        });

        Button1.addActionListener(new CalculatorNumberButtonListener('1', currentInputAreaText));
        Button2.addActionListener(new CalculatorNumberButtonListener('2', currentInputAreaText));
        Button3.addActionListener(new CalculatorNumberButtonListener('3', currentInputAreaText));
        Button4.addActionListener(new CalculatorNumberButtonListener('4', currentInputAreaText));
        Button5.addActionListener(new CalculatorNumberButtonListener('5', currentInputAreaText));
        Button6.addActionListener(new CalculatorNumberButtonListener('6', currentInputAreaText));
        Button7.addActionListener(new CalculatorNumberButtonListener('7', currentInputAreaText));
        Button8.addActionListener(new CalculatorNumberButtonListener('8', currentInputAreaText));
        Button9.addActionListener(new CalculatorNumberButtonListener('9', currentInputAreaText));
        Button0.addActionListener(new CalculatorNumberButtonListener('0', currentInputAreaText));
        DecimalDot.addActionListener(new CalculatorNumberButtonListener('.', currentInputAreaText){
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (currentInputAreaText.indexOf(".") == -1)
                    super.actionPerformed(actionEvent);
            }
        });
        ClearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                operands.clear();
                operator = '\0';
                clearInputArea();
            }
        });
    }
}
